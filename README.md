# cli-db

Tool que permite:
 Conectare a cualquier DB desde un pod y realizar consultas sql 
 Exportar datos "como inserts" formateados para postgres y luego Importar en entorno local
 Consultar datos maestros a distintos esquemas dado un e-mail.   

## Build

./gradlew build 

## Modo de uso para consultas: (ejemplo)

Levantar telepresence

Obtener las secrets

export COLUMNS

java -jar ./cli-db/cli-db/build/libs/cli-db-all.jar JDBC_URL JDBC_USER JDBC_PASSWORD
 

## Exportar datos de un ambiente: (ejemplo)
 
Crear un archico "exportTables.txt" con las conecciones a las DB's y los selects a las tablas 

Levantar telepresence

java -jar ./cli-db/cli-db/build/libs/cli-db-all.jar export exportTables.txt

Va a generar un archivo "importDataFile.txt" con los inserts formateados para poder correrlos en postgres.

## Importar datos en el ambiente local: (ejemplo)

Se puede hacer manualmente en cada esquema con dbeaber o sinó también se puede hacer con cli-db import. 

en entorno local

java -jar ./cli-db/cli-db/build/libs/cli-db-all.jar import importDataFile.txt

## Posibles inconvenientes.

- import: No se puede conectar a una DB local, revisar la cadena JDBC_URL=... y ;JDBC_USER=... ya que se genera automanticamente y puede diferir 
que lo que tenemos configurado en el entorno local. 

- import: por cada tabla se hace un delete de todos los datos antes de correr los inserts, si nos
da error de integridad por alguna constraint, conviene borrar todo el esquema y regenerarlo (../gradlew flywayMigrate) 

- import: Los campos booleanos en postgres deben estar definidos como int4, si alguno quedó definido como boolean, cambiarlo antes de hacer el import.

## Funcion userInfo:

- Estando siempre en la raiz del workspace: 
- Conectarse al ambiente deseado dev, pre, pro
- Levantar telepresense
- Copiar el archivo connetion.properties al raiz del workspace (con los secrets configurados)

java -jar ./cli-db/cli-db/build/libs/cli-db-all.jar userInfo <e-mail>

El html generado quedará guardado en la carperta ./userInfo/"env"  (dev, pre o pro)