plugins {
    id 'java'
    id 'idea'
    id 'application'
    id 'com.google.protobuf' version '0.9.4'
    id 'com.github.johnrengelman.shadow' version '7.1.2'
}

repositories {
    mavenCentral()
    gradlePluginPortal()
}

git {
    implementation 'https://github.com/xentelar/finite-state-machine.git', {
        tag '0.1.5'
    }
    implementation 'https://git-codecommit.us-east-1.amazonaws.com/v1/repos/vertx-rest.git', {
        tag '1.4.3'
        authGroup 'smart'
    }
    implementation 'https://git-codecommit.us-east-1.amazonaws.com/v1/repos/vertx-grpc.git', {
        tag '1.2.0'
        authGroup 'smart'
    }
    implementation 'https://git-codecommit.us-east-1.amazonaws.com/v1/repos/persistence.git', {
        tag '1.1.0'
        authGroup 'smart'
    }
    implementation 'https://git-codecommit.us-east-1.amazonaws.com/v1/repos/configuration.git', {
        tag '1.5.3'
        authGroup 'smart'
    }
    implementation 'https://git-codecommit.us-east-1.amazonaws.com/v1/repos/exception.git', {
        tag '1.0.1'
        authGroup 'smart'
    }
    implementation 'https://git-codecommit.us-east-1.amazonaws.com/v1/repos/vertx-fundamentals.git', {
        tag '1.2.0'
        authGroup 'smart'
    }
}

dependencies {

    implementation 'io.vertx:vertx-core:4.3.0'
    implementation 'io.vertx:vertx-web:4.3.0'
    implementation 'io.vertx:vertx-web-client:4.3.0'
    implementation 'io.vertx:vertx-kafka-client:4.3.0'
    implementation 'io.vertx:vertx-opentracing:4.3.0'

    implementation 'io.vertx:vertx-health-check:4.3.0'
    implementation 'io.vertx:vertx-micrometer-metrics:4.3.0'
    implementation 'io.micrometer:micrometer-registry-prometheus:1.8.1'

    implementation group: 'org.springframework', name: 'spring-context', version: '5.3.20'

    implementation 'org.eclipse.persistence:org.eclipse.persistence.jpa:2.7.13'
    implementation group: 'org.postgresql', name: 'postgresql', version: '42.6.0'
    implementation group: 'com.oracle.database.jdbc', name: 'ojdbc10', version: '19.19.0.0'

    implementation group: 'com.fasterxml.jackson.core', name: 'jackson-databind', version: '2.10.1'
    implementation group: 'com.fasterxml.jackson.datatype', name: 'jackson-datatype-jsr310', version: '2.10.1'
    implementation group: 'com.mchange', name: 'c3p0', version: '0.9.5.5'

    implementation 'org.slf4j:jcl-over-slf4j:1.7.20'
    implementation 'ch.qos.logback:logback-classic:1.1.7'
    implementation group: 'net.logstash.logback', name: 'logstash-logback-encoder', version: '6.3'

    implementation group: 'commons-io', name: 'commons-io', version: '2.6'
    implementation group: 'org.apache.commons', name: 'commons-lang3', version: '3.9'

    implementation 'io.vertx:vertx-grpc:4.3.0'

    implementation 'com.google.protobuf:protobuf-gradle-plugin:0.9.4'
    implementation group: 'com.google.protobuf', name: 'protobuf-java', version: '3.23.0'

    implementation group: 'io.opentracing.contrib', name: 'opentracing-vertx-web', version: '1.0.0'
    implementation group: 'io.opentracing.contrib', name: 'opentracing-grpc', version: '0.2.3'
    implementation group: 'io.jaegertracing', name: 'jaeger-core', version: '1.5.0'
    implementation group: 'io.jaegertracing', name: 'jaeger-thrift', version: '1.5.0'

    // Use JUnit test framework
    testImplementation 'junit:junit:4.12'
}

sourceSets {
    main {
        java {
            srcDir 'build/generated/source/proto/main/grpc'
            srcDir 'build/generated/source/proto/main/java'
        }
    }
}

application {
    mainClassName = 'sync.BootstrapVerticle'
}

shadowJar {
    archiveBaseName = project.archivesBaseName
}

jar {
    manifest {
        attributes 'Main-Class': 'sync.BootstrapVerticle'
    }
}

protobuf {
    protoc {
        artifact = 'com.google.protobuf:protoc:3.23.0'
    }
    plugins {
        grpc {
            artifact = 'io.grpc:protoc-gen-grpc-java:1.55.1'
        }
        vertx {
            artifact = "io.vertx:vertx-grpc-protoc-plugin:4.3.0"
        }
    }
    generateProtoTasks {
        all()*.plugins {
            grpc
            vertx
        }
        ofSourceSet('main')
    }
}

compileJava {
    targetCompatibility = 17
    sourceCompatibility = 17
}

java {
    disableAutoTargetJvm()
}
