
-- ## Los checkeos de las querys de 1) a 5) de cada esquema debe dar 0

-- ## Si un checkeo da mayor a 0 correr el fix correspondiente al número


-------------------------------------------------------------
-- <b>DEVICE </b>----------------------------------------------------
-------------------------------------------------------------

	-- Cantidad de things OWNER	
	select count(seq_id) from "DEVICE".SMPR_TTHING; 
	

	-- Cantidad de things Alias OWNER
	select count(seq_id) from "DEVICE".SMPR_TTHING_ALIAS; 
	

	--3) no pueden existir thing alias activos cuyo campo SEQ_PARENT_ID no sea una
	-- referencia a otro thing alias (correcto) o a un thing (esto seria incoreccto, pero ya por otor tema)
	SELECT COUNT(1) FROM "DEVICE".SMPR_TTHING_ALIAS sta
	WHERE sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "DEVICE".SMPR_TTHING_ALIAS)
	AND sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "DEVICE".SMPR_TTHING);
	

	-- 4) que NO halla replica de Installations Activas y que el en OWNER esté INACTIVAS
	SELECT count(1) FROM "DEVICE".SMPR_TINSTALLATION sta 
	WHERE sta.FYH_DELETED IS NULL 
	AND sta.SEQ_ID IN 
	(SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION inst WHERE inst.FYH_DELETED IS NOT NULL);
	
	
	-- 5) Que no halla installationAlias que apunten a installation que ya no exiten
	-- <orange/>
	SELECT count(1) FROM "DEVICE".SMPR_TINSTALLATION_ALIAS sta
	WHERE FYH_DELETED IS null 
	AND sta.SEQ_INSTALLATION_ID NOT IN (SELECT SEQ_ID FROM "DEVICE".SMPR_TINSTALLATION st);
		
		-- Estamos en una replica
		-- <blue/>
		SELECT count(*) FROM "DEVICE".SMPR_TINSTALLATION WHERE FYH_DELETED IS null;
		-- <yellow/>
		SELECT count(*) FROM "DEVICE".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT null;
		--<red/>
		SELECT count(*) FROM "DEVICE".SMPR_TINSTALLATION_ALIAS;
		
	--6) InstallationAlias ACTIVAS que apuntan a Installaion NO ACTIVAS   
	SELECT count(1) FROM "DEVICE".SMPR_TINSTALLATION_ALIAS sta 
	WHERE FYH_DELETED IS null 
	AND sta.SEQ_INSTALLATION_ID IN 
	(SELECT SEQ_ID FROM "DEVICE".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL);
	
	-- 6.1) Usuarios para estas InstallationAlias ACTIVAS que apuntan a Installaion NO ACTIVAS
	SELECT sta.SEQ_ID, sta.SEQ_USER_ID, usr.DES_NICK_NAME, usr.BOL_ADMINISTRATOR, sta.DES_ALIAS, sta.DES_IMAGE, sta.SEQ_INSTALLATION_ID, sta.DES_PANEL_USER, sta.FYH_UPDATE_DATE, sta.FYH_DELETED
	     FROM "DEVICE".SMPR_TINSTALLATION_ALIAS sta, "ACCOUNT-MANAGEMENT".SMPR_TUSER usr   
	    WHERE sta.SEQ_USER_ID = usr.SEQ_ID  
	    AND sta.FYH_DELETED IS null  AND sta.SEQ_INSTALLATION_ID IN      
	   (SELECT SEQ_ID FROM "DEVICE".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL);
		
	--***************************************************************
	
	-- Fix para 3) SOLO PARA EL OWNER, EN EL RESTO HAY QUE AGREGAR EL BOL_ACTIVE
	--DELETE FROM "DEVICE".SMPR_TTHING_ALIAS sta
	--WHERE sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "DEVICE".SMPR_TTHING_ALIAS)
	--AND sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "DEVICE".SMPR_TTHING)

	-- Fix para 5) Borrado de installation Alias que apuntan a INSTALLATIONS QUE YA NO EXISTEN
	--DELETE FROM "DEVICE".SMPR_TINSTALLATION_ALIAS sta 
	--WHERE FYH_DELETED IS null 
	--AND sta.SEQ_INSTALLATION_ID NOT IN (SELECT SEQ_ID FROM "DEVICE".SMPR_TINSTALLATION st)
	
	-- Fix para 6) 
	--UPDATE SMPR_TINSTALLATION_ALIAS SET FYH_DELETED = SYSDATE 
	--WHERE FYH_DELETED IS null 
	--AND SEQ_INSTALLATION_ID IN 
	--(SELECT SEQ_ID FROM SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL)
	
-------------------------------------------------------------	
--<b> ACCOUNT-MANAGEMENT </b>---------------------------------------
-------------------------------------------------------------

	-- Installation OWNER
	select COUNT(seq_id) from "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION;	
	select COUNT(seq_id) from "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION WHERE FYH_DELETED IS NULL; 	
	select COUNT(seq_id) from "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL; 
	
				
	-- InstallationAlias OWNER
	select COUNT(seq_id) from "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION_ALIAS;  


	--1) Things que no existen mas en Device
	select COUNT(seq_id) from "ACCOUNT-MANAGEMENT".SMPR_TTHING WHERE BOL_IS_ACTIVE = 1 AND SEQ_ID NOT IN 
	(select seq_id from "DEVICE".SMPR_TTHING);
	
	
	-- 1.1 Generador de updates si 1) result > 0 (Correr con teleprecence)
		--SELECT concat(CONCAT('UPDATE SMPR_TTHING SET BOL_IS_ACTIVE = 0 WHERE SEQ_ID = ', seq_id), ';') 
		--from "ACCOUNT-MANAGEMENT".SMPR_TTHING 
		--WHERE BOL_IS_ACTIVE = 1 AND SEQ_ID NOT IN 
		--(select seq_id from "DEVICE".SMPR_TTHING);
		
	--2) thing alias activos para thing borrados lógicos
	select count(1) from "ACCOUNT-MANAGEMENT".SMPR_TTHING_ALIAS  WHERE BOL_IS_ACTIVE = 1 AND SEQ_THING_ID IN 
	(select seq_id from "ACCOUNT-MANAGEMENT".SMPR_TTHING WHERE BOL_IS_ACTIVE = 0 );	

	
	--3) no pueden existir thing alias activos cuyo campo SEQ_PARENT_ID no sea una
	-- referencia a otro thing alias (correcto) o a un thing (esto seria incoreccto, pero ya por otor tema)
	SELECT COUNT(1) FROM "ACCOUNT-MANAGEMENT".SMPR_TTHING_ALIAS sta 
	WHERE sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TTHING_ALIAS)
	AND sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TTHING)
	AND sta.BOL_IS_ACTIVE = 1;		
	
	
	-- 4) Que no halla installationalias que apunten a installation qeu no exiten
	SELECT count(1) FROM "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION_ALIAS sta 
	WHERE sta.SEQ_INSTALLATION_ID NOT IN (SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION);	
				
	--***************************************************************
		
	-- Fix para 1) correr los updates generados
		
		
	-- Fix para 2) Borrado Logico de thingsAlias para los cuales el thing está desactivado
	--UPDATE "ACCOUNT-MANAGEMENT".SMPR_TTHING_ALIAS SET BOL_IS_ACTIVE = 0
	--WHERE BOL_IS_ACTIVE = 1 AND SEQ_THING_ID IN 
	--(SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TTHING WHERE BOL_IS_ACTIVE = 0)
	
		
	
-------------------------------------------------------------	
--<b> EVENT-PROCESSOR </b>--------------------------------------
-------------------------------------------------------------
	--1) Things que no existen mas en Device
	select COUNT(seq_id) from "EVENT-PROCESSOR".SMPR_TTHING WHERE BOL_IS_ACTIVE = 1 AND SEQ_ID NOT IN 
	(select seq_id from "DEVICE".SMPR_TTHING);
		
		-- 1.1 Generador de updates si 1) result > 0
		--SELECT concat(CONCAT('UPDATE SMPR_TTHING SET BOL_IS_ACTIVE = 0 WHERE SEQ_ID = ', seq_id), ';') 
		--from "EVENT-PROCESSOR".SMPR_TTHING 
		--WHERE BOL_IS_ACTIVE = 1 AND SEQ_ID NOT IN 
		--(select seq_id from "DEVICE".SMPR_TTHING)
	
		-- Chequeo previo al update
		--select * from "DEVICE".SMPR_TTHING WHERE SEQ_ID = 6564
		--select * from "EVENT-PROCESSOR".SMPR_TTHING WHERE SEQ_ID = 6564
	
	--2) thing alias activos para thing borrados lógicos 
	select count(1) from "EVENT-PROCESSOR".SMPR_TTHING_ALIAS  WHERE BOL_IS_ACTIVE = 1 AND SEQ_THING_ID  IN 
	(select seq_id from "EVENT-PROCESSOR".SMPR_TTHING WHERE BOL_IS_ACTIVE = 0 );	
	
		--select count(SEQ_ID), BOL_IS_ACTIVE from "EVENT-PROCESSOR".SMPR_TTHING
		--GROUP BY BOL_IS_ACTIVE 

	--3) no pueden existir thing alias activos cuyo campo SEQ_PARENT_ID no sea una
	-- referencia a otro thing alias (correcto) o a un thing (esto seria incoreccto, pero ya por otor tema)
	SELECT COUNT(1) FROM "EVENT-PROCESSOR".SMPR_TTHING_ALIAS sta 
	WHERE sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "EVENT-PROCESSOR".SMPR_TTHING_ALIAS)
	AND sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "EVENT-PROCESSOR".SMPR_TTHING)
	AND sta.BOL_IS_ACTIVE = 1;	
	
	
	-- 4) que NO halla replica de Installations Activas y que el en OWNER esté INACTIVAS
	SELECT count(1) FROM "EVENT-PROCESSOR".SMPR_TINSTALLATION sta
	WHERE sta.FYH_DELETED IS NULL 
	AND sta.SEQ_ID IN 
	(SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION inst WHERE inst.FYH_DELETED IS NOT NULL);
	
	
	-- 5) Que no halla installationAlias que apunten a installation que ya no exiten
	-- No existe FYH_DELETED 
	SELECT count(1) FROM "EVENT-PROCESSOR".SMPR_TINSTALLATION_ALIAS sta 
	WHERE sta.SEQ_INSTALLATION_ID NOT IN 
	(SELECT SEQ_ID FROM "EVENT-PROCESSOR".SMPR_TINSTALLATION st);	
	
		-- Estamos en una replica
		--SELECT count(*) FROM "EVENT-PROCESSOR".SMPR_TINSTALLATION;
		SELECT count(*) FROM "EVENT-PROCESSOR".SMPR_TINSTALLATION WHERE FYH_DELETED IS null;
		SELECT count(*) FROM "EVENT-PROCESSOR".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT null;
		SELECT count(*) FROM "EVENT-PROCESSOR".SMPR_TINSTALLATION_ALIAS;
		
	--6) InstallationAlias ACTIVAS que apuntan a Installaion NO ACTIVAS 
	-- No existe FYH_DELETED   
	SELECT count(1) FROM "EVENT-PROCESSOR".SMPR_TINSTALLATION_ALIAS sta
	WHERE sta.SEQ_INSTALLATION_ID IN 
	(SELECT SEQ_ID FROM "EVENT-PROCESSOR".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL);	 
	
	
	-- ******************************************************
	
	-- Fix para 5) Borrado de installation Alias que apuntan a INSTALLATIONS QUE YA NO EXISTEN
	--DELETE FROM "EVENT-PROCESSOR".SMPR_TINSTALLATION_ALIAS sta 
	--WHERE sta.SEQ_INSTALLATION_ID NOT IN (SELECT SEQ_ID FROM "EVENT-PROCESSOR".SMPR_TINSTALLATION st)
	
	-- Fix para 6) Borrado de installationales que apuntan a Intallations con BORRADO LOGICO
	--DELETE FROM "EVENT-PROCESSOR".SMPR_TINSTALLATION_ALIAS sta -- 
	--WHERE sta.SEQ_INSTALLATION_ID IN 
	--(SELECT SEQ_ID FROM "EVENT-PROCESSOR".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL)

-------------------------------------------------------------	
--<b> PANIC-BUTTON </b>--------------------------------------
-------------------------------------------------------------

	--1) Things que no existen mas en Device
	select COUNT(seq_id) from "PANIC-BUTTON".SMPR_TTHING WHERE BOL_IS_ACTIVE = 1 AND SEQ_ID NOT IN
	(select seq_id from "DEVICE".SMPR_TTHING);	

		-- 1.1 Generador de updates si 1) result > 0
		--SELECT concat(CONCAT('UPDATE SMPR_TTHING SET BOL_IS_ACTIVE = 0 WHERE SEQ_ID = ', seq_id), ';') 
		--from "PANIC-BUTTON".SMPR_TTHING 
		--WHERE BOL_IS_ACTIVE = 1 AND SEQ_ID NOT IN 
		--(select seq_id from "DEVICE".SMPR_TTHING)
	
	
	--2) thing alias activos para thing borrados lógicos 
	select count(1) from "PANIC-BUTTON".SMPR_TTHING_ALIAS  WHERE BOL_IS_ACTIVE = 1 AND SEQ_THING_ID  IN
	(select seq_id from "PANIC-BUTTON".SMPR_TTHING WHERE BOL_IS_ACTIVE = 0 );
	
	
		--select count(SEQ_ID), BOL_IS_ACTIVE from "PANIC-BUTTON".SMPR_TTHING
		--GROUP BY BOL_IS_ACTIVE 

	
	--3) no pueden existir thing alias activos cuyo campo SEQ_PARENT_ID no sea una
	-- referencia a otro thing alias (correcto) o a un thing (esto seria incoreccto, pero ya por otor tema)
	SELECT COUNT(1) FROM "PANIC-BUTTON".SMPR_TTHING_ALIAS sta
	WHERE sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "PANIC-BUTTON".SMPR_TTHING_ALIAS)
	AND sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "PANIC-BUTTON".SMPR_TTHING)
	AND sta.BOL_IS_ACTIVE = 1;
		
	
	-- 4) que NO halla replica de Installations Activas y que el en OWNER esté INACTIVAS
	SELECT count(1) FROM "PANIC-BUTTON".SMPR_TINSTALLATION sta
	WHERE sta.FYH_DELETED IS NULL 
	AND sta.SEQ_ID IN 
	(SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION inst WHERE inst.FYH_DELETED IS NOT NULL);
	
	
	-- 5) Que no halla installationAlias que apunten a installation que ya no exiten
	-- No existe FYH_DELETED 
	SELECT count(1) FROM "PANIC-BUTTON".SMPR_TINSTALLATION_ALIAS sta
	WHERE sta.SEQ_INSTALLATION_ID NOT IN 
	(SELECT SEQ_ID FROM "PANIC-BUTTON".SMPR_TINSTALLATION st);
	
	
		-- Estamos en una replica
		--SELECT count(*) FROM "PANIC-BUTTON".SMPR_TINSTALLATION;
		SELECT count(*) FROM "PANIC-BUTTON".SMPR_TINSTALLATION WHERE FYH_DELETED IS null;
		SELECT count(*) FROM "PANIC-BUTTON".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT null;
		SELECT count(*) FROM "PANIC-BUTTON".SMPR_TINSTALLATION_ALIAS;
		
	--6) InstallationAlias ACTIVAS que apuntan a Installaion NO ACTIVAS   
	-- No existe FYH_DELETED 
	SELECT count(1) FROM "PANIC-BUTTON".SMPR_TINSTALLATION_ALIAS sta
	WHERE sta.SEQ_INSTALLATION_ID IN 
	(SELECT SEQ_ID FROM "PANIC-BUTTON".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL);
	
	

	--7) Usuarios que tienen thingAlias y thing donde el SEQ_ENTITY_TYPE corresponde a CONTIGO y no estan en SMPR_TPANIC_BUTTON_USER
	SELECT SEQ_ID, DES_NICK_NAME, BOL_ADMINISTRATOR FROM "ACCOUNT-MANAGEMENT".SMPR_TUSER WHERE SEQ_ID IN ( 
	SELECT thna.SEQ_USER_ID FROM "DEVICE".SMPR_TTHING_ALIAS thna  
	INNER JOIN "DEVICE".SMPR_TTHING thn 
	ON (thn.SEQ_ID = thna.SEQ_THING_ID)
	WHERE thn.SEQ_ENTITY_TYPE = 20 
	AND thna.SEQ_USER_ID NOT IN 
	(SELECT SEQ_USER_ID FROM "PANIC-BUTTON".SMPR_TPANIC_BUTTON_USER stbu));
	
	-- ******************************************************
	
	-- Fix para 5) Borrado de installation Alias que apuntan a INSTALLATIONS QUE YA NO EXISTEN
	--DELETE FROM "PANIC-BUTTON".SMPR_TINSTALLATION_ALIAS sta 
	--WHERE sta.SEQ_INSTALLATION_ID NOT IN (SELECT SEQ_ID FROM "PANIC-BUTTON".SMPR_TINSTALLATION st)
	
	-- Fix para 6) Borrado de installationales que apuntan a Intallations con BORRADO LOGICO
	--DELETE FROM "PANIC-BUTTON".SMPR_TINSTALLATION_ALIAS sta -- 
	--WHERE sta.SEQ_INSTALLATION_ID IN 
	--(SELECT SEQ_ID FROM "PANIC-BUTTON".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL)
	
	
-------------------------------------------------------------
--<b> FILTER-DISPATCHER </b>--------------------------------------
-------------------------------------------------------------

	--1) Things que no existen mas en Device
	select COUNT(seq_id) from "FILTER-DISPATCHER".SMPR_TTHING WHERE BOL_IS_ACTIVE = 1 AND SEQ_ID NOT IN
	(select seq_id from "DEVICE".SMPR_TTHING);
	 
	
		-- 1.1 Generador de updates si 1) result > 0
		--SELECT concat(CONCAT('UPDATE SMPR_TTHING SET BOL_IS_ACTIVE = 0 WHERE SEQ_ID = ', seq_id), ';') 
		--from "FILTER-DISPATCHER".SMPR_TTHING 
		--WHERE BOL_IS_ACTIVE = 1 AND SEQ_ID NOT IN 
		--(select seq_id from "DEVICE".SMPR_TTHING)
	
	--2) thing alias activos para thing borrados lógicos 
	select count(1) from "FILTER-DISPATCHER".SMPR_TTHING_ALIAS  WHERE BOL_IS_ACTIVE = 1 AND SEQ_THING_ID  IN
	(select seq_id from "FILTER-DISPATCHER".SMPR_TTHING WHERE BOL_IS_ACTIVE = 0 );
	
	
		--select count(SEQ_ID), BOL_IS_ACTIVE from "FILTER-DISPATCHER".SMPR_TTHING
		--GROUP BY BOL_IS_ACTIVE 
	
	--3) no pueden existir thing alias activos cuyo campo SEQ_PARENT_ID no sea una
	-- referencia a otro thing alias (correcto) o a un thing (esto seria incoreccto, pero ya por otor tema)
	SELECT COUNT(1) FROM "FILTER-DISPATCHER".SMPR_TTHING_ALIAS sta
	WHERE sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "FILTER-DISPATCHER".SMPR_TTHING_ALIAS)
	AND sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "FILTER-DISPATCHER".SMPR_TTHING)
	AND sta.BOL_IS_ACTIVE = 1;
	
	
	
	-- 4) Que no halla installationalias que apunten a installation qeu no exiten
	-- NO tiene replica de SMPR_TINSTALLATION_ALIAS
-------------------------------------------------------------	
--<b> RULE-ENGINE </b>--------------------------------------
-------------------------------------------------------------

	--1) Things que no existen mas en Device
	select COUNT(seq_id) from "RULE-ENGINE".SMPR_TTHING WHERE BOL_IS_ACTIVE = 1 AND SEQ_ID NOT IN
	(select seq_id from "DEVICE".SMPR_TTHING);
	 

		-- 1.1 Generador de updates si 1) result > 0
		--SELECT concat(CONCAT('UPDATE SMPR_TTHING SET BOL_IS_ACTIVE = 0 WHERE SEQ_ID = ', seq_id), ';') 
		--from "RULE-ENGINE".SMPR_TTHING 
		--WHERE BOL_IS_ACTIVE = 1 AND SEQ_ID NOT IN 
		--(select seq_id from "DEVICE".SMPR_TTHING)
	
	--2) thing alias activos para thing borrados lógicos 
	select count(1) from "RULE-ENGINE".SMPR_TTHING_ALIAS  WHERE BOL_IS_ACTIVE = 1 AND SEQ_THING_ID  IN
	(select seq_id from "RULE-ENGINE".SMPR_TTHING WHERE BOL_IS_ACTIVE = 0 );
	
	
		--select count(SEQ_ID), BOL_IS_ACTIVE from "RULE-ENGINE".SMPR_TTHING
		--GROUP BY BOL_IS_ACTIVE 
		
		--select count(SEQ_ID), BOL_IS_ACTIVE from "RULE-ENGINE".SMPR_TTHING_ALIAS
		--GROUP BY BOL_IS_ACTIVE 
	
	
	--3) no pueden existir thing alias activos cuyo campo SEQ_PARENT_ID no sea una
	-- referencia a otro thing alias (correcto) o a un thing (esto seria incoreccto, pero ya por otor tema)
	SELECT COUNT(1) FROM "RULE-ENGINE".SMPR_TTHING_ALIAS sta
	WHERE sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "RULE-ENGINE".SMPR_TTHING_ALIAS)
	AND sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "RULE-ENGINE".SMPR_TTHING)
	AND sta.BOL_IS_ACTIVE = 1;
	
	
	
	-- 4) que NO halla replica de Installations Activas y que el en OWNER esté INACTIVAS
	SELECT count(1) FROM "RULE-ENGINE".SMPR_TINSTALLATION sta
	WHERE sta.FYH_DELETED IS NULL 
	AND sta.SEQ_ID IN 
	(SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION inst WHERE inst.FYH_DELETED IS NOT NULL);
	
	
	-- 5) Que no halla installationAlias que apunten a installation que ya no exiten
	-- No existe FYH_DELETED 
	SELECT count(1) FROM "RULE-ENGINE".SMPR_TINSTALLATION_ALIAS sta
	WHERE sta.SEQ_INSTALLATION_ID NOT IN 
	(SELECT SEQ_ID FROM "RULE-ENGINE".SMPR_TINSTALLATION st);
	 
	
		-- Estamos en una replica
		--SELECT count(*) FROM "RULE-ENGINE".SMPR_TINSTALLATION;
		SELECT count(*) FROM "RULE-ENGINE".SMPR_TINSTALLATION WHERE FYH_DELETED IS null;
		SELECT count(*) FROM "RULE-ENGINE".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT null;
		SELECT count(*) FROM "RULE-ENGINE".SMPR_TINSTALLATION_ALIAS;
		
	--6) InstallationAlias ACTIVAS que apuntan a Installaion NO ACTIVAS   
	-- No existe FYH_DELETED 
	SELECT count(1) FROM "RULE-ENGINE".SMPR_TINSTALLATION_ALIAS sta
	WHERE sta.SEQ_INSTALLATION_ID IN 
	(SELECT SEQ_ID FROM "RULE-ENGINE".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL);

	
	-- ******************************************************
	
	-- Fix para 5) Borrado de installation Alias que apuntan a INSTALLATIONS QUE YA NO EXISTEN
	--DELETE FROM "RULE-ENGINE".SMPR_TINSTALLATION_ALIAS sta 
	--WHERE sta.SEQ_INSTALLATION_ID NOT IN (SELECT SEQ_ID FROM "RULE-ENGINE".SMPR_TINSTALLATION st)
	
	-- Fix para 6) Borrado de installationales que apuntan a Intallations con BORRADO LOGICO
	--DELETE FROM "RULE-ENGINE".SMPR_TINSTALLATION_ALIAS sta -- 
	--WHERE sta.SEQ_INSTALLATION_ID IN 
	--(SELECT SEQ_ID FROM "RULE-ENGINE".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL)
	
	
-------------------------------------------------------------	
--<b> SYSLOG </b>--------------------------------------
-------------------------------------------------------------

	--1) Things que no existen mas en Device
	select COUNT(seq_id) from "SYSLOG".SMPR_TTHING WHERE BOL_IS_ACTIVE = 1 AND SEQ_ID NOT IN
	(select seq_id from "DEVICE".SMPR_TTHING);
	
	
		-- 1.1 Generador de updates si 1) result > 0
		--SELECT concat(CONCAT('UPDATE SMPR_TTHING SET BOL_IS_ACTIVE = 0 WHERE SEQ_ID = ', seq_id), ';') 
		--from "SYSLOG".SMPR_TTHING 
		--WHERE BOL_IS_ACTIVE = 1 AND SEQ_ID NOT IN 
		--(select seq_id from "DEVICE".SMPR_TTHING)
	
	--2) thing alias activos para thing borrados lógicos 
	select count(1) from "SYSLOG".SMPR_TTHING_ALIAS  WHERE BOL_IS_ACTIVE = 1 AND SEQ_THING_ID IN
	(select seq_id from "SYSLOG".SMPR_TTHING WHERE BOL_IS_ACTIVE = 0 );
	
	
		--select count(SEQ_ID), BOL_IS_ACTIVE from "SYSLOG".SMPR_TTHING
		--GROUP BY BOL_IS_ACTIVE 
	
	
	--3) no pueden existir thing alias activos cuyo campo SEQ_PARENT_ID no sea una
	-- referencia a otro thing alias (correcto) o a un thing (esto seria incoreccto, pero ya por otor tema)
	SELECT COUNT(1) FROM "SYSLOG".SMPR_TTHING_ALIAS sta
	WHERE sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "SYSLOG".SMPR_TTHING_ALIAS)
	AND sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "SYSLOG".SMPR_TTHING)
	AND sta.BOL_IS_ACTIVE = 1;
	
	
	
	-- 4) que NO halla replica de Installations Activas y que el en OWNER esté INACTIVAS
	SELECT count(1) FROM "SYSLOG".SMPR_TINSTALLATION sta
	WHERE sta.FYH_DELETED IS NULL 
	AND sta.SEQ_ID IN 
	(SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION inst WHERE inst.FYH_DELETED IS NOT NULL);
	
	
	-- 5) Que no halla installationAlias que apunten a installation que ya no exiten
	-- No existe FYH_DELETED 
	SELECT count(1) FROM "SYSLOG".SMPR_TINSTALLATION_ALIAS sta
	WHERE sta.SEQ_INSTALLATION_ID NOT IN 
	(SELECT SEQ_ID FROM "SYSLOG".SMPR_TINSTALLATION st);
	
	
		-- Estamos en una replica
		-- SELECT count(*) FROM "SYSLOG".SMPR_TINSTALLATION;
		SELECT count(*) FROM "SYSLOG".SMPR_TINSTALLATION WHERE FYH_DELETED IS null;
		SELECT count(*) FROM "SYSLOG".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT null;
		SELECT count(*) FROM "SYSLOG".SMPR_TINSTALLATION_ALIAS;
		
	--6) InstallationAlias ACTIVAS que apuntan a Installaion NO ACTIVAS   
	-- No existe FYH_DELETED 
	SELECT count(1) FROM "SYSLOG".SMPR_TINSTALLATION_ALIAS sta
	WHERE sta.SEQ_INSTALLATION_ID IN 
	(SELECT SEQ_ID FROM "SYSLOG".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL);
	

	--***************************************************************
	
	-- Fix para 3)	
	--DELETE FROM "SYSLOG".SMPR_TTHING_ALIAS sta
	--WHERE sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "SYSLOG".SMPR_TTHING_ALIAS)
	--AND sta.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM "SYSLOG".SMPR_TTHING)
	--AND sta.BOL_IS_ACTIVE = 1
	
	-- Fix para 4) Borrado de installation Alias que apuntan a INSTALLATIONS QUE YA NO EXISTEN
	--DELETE FROM "SYSLOG".SMPR_TINSTALLATION_ALIAS sta 
	--WHERE sta.SEQ_INSTALLATION_ID NOT IN (SELECT SEQ_ID FROM "SYSLOG".SMPR_TINSTALLATION st)
	
	-- Fix para 5) Borrado de installation Alias que apuntan a INSTALLATIONS QUE YA NO EXISTEN
	--DELETE FROM "SYSLOG".SMPR_TINSTALLATION_ALIAS sta 
	--WHERE sta.SEQ_INSTALLATION_ID NOT IN (SELECT SEQ_ID FROM "SYSLOG".SMPR_TINSTALLATION st)
	
	-- Fix para 6) Borrado de installationales que apuntan a Intallations con BORRADO LOGICO
	--DELETE FROM "SYSLOG".SMPR_TINSTALLATION_ALIAS sta -- 
	--WHERE sta.SEQ_INSTALLATION_ID IN 
	--(SELECT SEQ_ID FROM "SYSLOG".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL)

-------------------------------------------------------------	
--<b> SCENE </b>--------------------------------------
-------------------------------------------------------------

	--1) Things que no existen mas en Device
	select COUNT(seq_id) from "SCENE".SMPR_TTHING WHERE BOL_IS_ACTIVE = 1 AND SEQ_ID NOT IN
	(select seq_id from "DEVICE".SMPR_TTHING);
	 
	
		-- 1.1 Generador de updates si 1) result > 0
		--SELECT concat(CONCAT('UPDATE SMPR_TTHING SET BOL_IS_ACTIVE = 0 WHERE SEQ_ID = ', seq_id), ';') 
		--from "SCENE".SMPR_TTHING 
		--WHERE BOL_IS_ACTIVE = 1 AND SEQ_ID NOT IN 
		--(select seq_id from "DEVICE".SMPR_TTHING)
	
	
	--2)
	--NO exiet SMPR_TTHING_ALIAS 	
		
	-- 4) No tiene replica de SMPR_TINSTALLATION_ALIAS

-------------------------------------------------------------	
--<b> CAMPAINGS </b>---------------------
-------------------------------------------------------------

	--2) 
	--NO exiet SMPR_TTHING_ALIAS 
	
	--NO TIENE FYH_DELETED
	
	-- 4) que NO halla replica de Installations Activas y que el en OWNER esté INACTIVAS
	SELECT count(1) FROM "CAMPAIGNS".SMPR_TINSTALLATION sta
	WHERE sta.FYH_DELETED IS NULL 
	AND sta.SEQ_ID IN 
	(SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION inst WHERE inst.FYH_DELETED IS NOT NULL);
	
	
	-- 5) Que no halla installationAlias que apunten a installation que ya no exiten
	-- No existe FYH_DELETED 
	SELECT count(1) FROM "CAMPAIGNS".SMPR_TINSTALLATION_ALIAS sta 
	WHERE sta.SEQ_INSTALLATION_ID NOT IN 
	(SELECT SEQ_ID FROM "CAMPAIGNS".SMPR_TINSTALLATION st);
	
		-- Estamos en una replica
		SELECT count(*) FROM "CAMPAIGNS".SMPR_TINSTALLATION;
		SELECT count(*) FROM "CAMPAIGNS".SMPR_TINSTALLATION_ALIAS;
		
	--6) InstallationAlias ACTIVAS que apuntan a Installaion NO ACTIVAS   
	-- No existe FYH_DELETED 
	SELECT count(1) FROM "CAMPAIGNS".SMPR_TINSTALLATION_ALIAS sta
	WHERE sta.SEQ_INSTALLATION_ID IN 
	(SELECT SEQ_ID FROM "CAMPAIGNS".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL);
	

	--***************************************************************
		
	-- Fix para 6) Borrado de installationales que apuntan a Intallations con BORRADO LOGICO
	--DELETE FROM "CAMPAIGNS".SMPR_TINSTALLATION_ALIAS sta -- 
	--WHERE sta.SEQ_INSTALLATION_ID IN 
	--(SELECT SEQ_ID FROM "CAMPAIGNS".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL)
	
-------------------------------------------------------------	
--<b> USER-ENROLLMENT </b>------------------
-------------------------------------------------------------	
	--2) 
	--NO exiet SMPR_TTHING_ALIAS 
	--NO TIENE FYH_DELETED
	
	-- 4) que NO halla replica de Installations Activas y que el en OWNER esté INACTIVAS
	-- count: 0	
	SELECT count(1) FROM "USER-ENROLLMENT".SMPR_TINSTALLATION sta
	WHERE sta.FYH_DELETED IS NULL 
	AND sta.SEQ_ID IN 
	(SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION inst WHERE inst.FYH_DELETED IS NOT NULL);
	
	
	-- 5) Que no halla installationAlias que apunten a installation que ya no exiten
	-- No existe FYH_DELETED 
	SELECT count(1) FROM "USER-ENROLLMENT".SMPR_TINSTALLATION_ALIAS sta
	WHERE sta.SEQ_INSTALLATION_ID NOT IN 
	(SELECT SEQ_ID FROM "USER-ENROLLMENT".SMPR_TINSTALLATION st);	 
	
		-- Estamos en una replica
		--SELECT count(*) FROM "USER-ENROLLMENT".SMPR_TINSTALLATION;
		SELECT count(*) FROM "USER-ENROLLMENT".SMPR_TINSTALLATION WHERE FYH_DELETED IS null;
		SELECT count(*) FROM "USER-ENROLLMENT".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT null;
		SELECT count(*) FROM "USER-ENROLLMENT".SMPR_TINSTALLATION_ALIAS;
		
	--6) InstallationAlias ACTIVAS que apuntan a Installaion NO ACTIVAS   
	-- No existe FYH_DELETED 
	SELECT count(1) FROM "USER-ENROLLMENT".SMPR_TINSTALLATION_ALIAS sta
	WHERE sta.SEQ_INSTALLATION_ID IN 
	(SELECT SEQ_ID FROM "USER-ENROLLMENT".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL);
	
	
	--***************************************************************

	-- Fix para 4) Borrado de installation Alias que apuntan a INSTALLATIONS QUE YA NO EXISTEN
	--DELETE FROM "USER-ENROLLMENT".SMPR_TINSTALLATION_ALIAS sta
	--WHERE sta.SEQ_INSTALLATION_ID NOT IN (SELECT SEQ_ID FROM "USER-ENROLLMENT".SMPR_TINSTALLATION st)
	
	-- Fix para 5) Borrado de installation Alias que apuntan a INSTALLATIONS QUE YA NO EXISTEN
	--DELETE FROM "USER-ENROLLMENT".SMPR_TINSTALLATION_ALIAS sta 
	--WHERE sta.SEQ_INSTALLATION_ID NOT IN (SELECT SEQ_ID FROM "USER-ENROLLMENT".SMPR_TINSTALLATION st)
	
	-- Fix para 6) Borrado de installationales que apuntan a Intallations con BORRADO LOGICO
	--DELETE FROM "USER-ENROLLMENT".SMPR_TINSTALLATION_ALIAS sta -- 
	--WHERE sta.SEQ_INSTALLATION_ID IN 
	--(SELECT SEQ_ID FROM "USER-ENROLLMENT".SMPR_TINSTALLATION WHERE FYH_DELETED IS NOT NULL)
