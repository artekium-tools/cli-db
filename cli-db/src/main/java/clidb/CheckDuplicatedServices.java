package clidb;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CheckDuplicatedServices extends QryDB {

    static Integer ocurrences = 0;
    static ArrayList<Long> arrInstallationIds = new ArrayList<>();

    public int fixRepeatedServices(String env, String datafixReportFile, String reportDate) {

        loadExternalFileProperties();

        DataSource dataSource = null;

        try {

            if (env == null) env = getProperty("env");
            String connectionType = getProperty("connectionType");

            String jdbcUrl = getProperty(connectionType + "." + env + ".url");
            String dbUsername = getProperty(env + ".username");
            String password = getProperty(env + ".password");

            dataSource = getDataSource(jdbcUrl, dbUsername, password);

            System.out.println("Executing step 1: check repeated services in device-identification  ..." );

            appendToFile(datafixReportFile,
                  "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-43569'>" +
                        "10) MC " +
                        "https://jira.prosegur.com/browse/AM-43569</a> - " +
                        "Eliminar servicios duplicados<br>\n");

            checkRepeatedServices(dataSource, datafixReportFile, reportDate);

            generateUserInfoReports(dataSource, env);

            System.out.println("Total repeated services found: " + ocurrences.toString());

        } catch (Exception e){
            printStackTrace(e);
        }


        return ocurrences;
    }

    private static void checkRepeatedServices(final DataSource dataSource, String datafixReportFile, String reportDate) {

        try {
            Connection con = dataSource.getConnection();
            Statement stmt = null;
            ResultSet rs = null;

            try {

                int cant = 0;

                String sqlSelectCant = "SELECT SEQ_INSTALLATION_ID,SEQ_ENTITY_TYPE, COUNT(*) AS repeticion_count " +
                      "FROM DEVICE.SMPR_TTHING " +
                      "WHERE SEQ_INSTALLATION_ID IS NOT NULL AND SEQ_PARENT_ID IS NULL " +
                      "GROUP BY SEQ_INSTALLATION_ID,SEQ_ENTITY_TYPE HAVING COUNT(*) > 1 ";

                stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                rs = stmt.executeQuery(sqlSelectCant);

                String instalations = "";
                Set<String> types = new HashSet<String>();

                while (rs.next()) {
                    instalations = instalations + rs.getString(1) + ",";
                    types.add(rs.getString(2));
                }

                String idsToDelete = "";

                if (!"".equals(instalations)){
                    instalations = instalations.substring(0, instalations.length() - 1);
                    cant = instalations.split(",").length;

                    // generate delete statements
                    String sqlSelectToDeletes = "SELECT MIN(SEQ_ID), SEQ_INSTALLATION_ID, COUNT(*) " +
                          "FROM DEVICE.SMPR_TTHING " +
                          "WHERE SEQ_ENTITY_TYPE IN (" + String.join(",", types) + ") AND SEQ_INSTALLATION_ID IN (" + instalations + ") " +
                          "GROUP BY SEQ_INSTALLATION_ID HAVING COUNT(*) > 1";

                    stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                    rs = stmt.executeQuery(sqlSelectToDeletes);

                    int cont = 0;
                    String fileName = "fixRepeatedServies.sql";

                    while (rs.next()) {
                        cont++;
                        idsToDelete = idsToDelete + rs.getString(1) + ",";

                        arrInstallationIds.add(rs.getLong(2));

                        if (cont == 1000) {
                            idsToDelete = idsToDelete.substring(0, idsToDelete.length() - 1);
                            appendToFile(fileName, "schemas:DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG\n");
                            appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_PARENT_ID IN (SELECT SEQ_ID FROM SMPR_TTHING_ALIAS sta2 WHERE SEQ_THING_ID IN (" + idsToDelete + "));\n");
                            appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_THING_ID IN (" + idsToDelete + ");\n");
                            appendToFile(fileName, "schemas:DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SCENE,SYSLOG\n");
                            appendToFile(fileName, "DELETE FROM SMPR_TTHING sta WHERE SEQ_PARENT_ID IN (" + idsToDelete + ");\n");
                            appendToFile(fileName, "DELETE FROM SMPR_TTHING sta WHERE SEQ_ID IN (" + idsToDelete + ");\n");
                            idsToDelete = "";
                            cont = 0;
                        }

                    }
                    if (!idsToDelete.isEmpty()) {
                        idsToDelete = idsToDelete.substring(0, idsToDelete.length() - 1);
                        appendToFile(fileName, "schemas:DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG\n");
                        appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_PARENT_ID IN (SELECT SEQ_ID FROM SMPR_TTHING_ALIAS sta2 WHERE SEQ_THING_ID IN (" + idsToDelete + "));\n");
                        appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_THING_ID IN (" + idsToDelete + ");\n");
                        appendToFile(fileName, "schemas:DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SCENE,SYSLOG\n");
                        appendToFile(fileName, "DELETE FROM SMPR_TTHING sta WHERE SEQ_PARENT_ID IN (" + idsToDelete + ");\n");
                        appendToFile(fileName, "DELETE FROM SMPR_TTHING sta WHERE SEQ_ID IN (" + idsToDelete + ");\n");
                    }
                }

                if (cant > 0) {
                    ocurrences = cant;
                    appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                          ", se fixearon " + cant +
                          " casos. <br><br>\n");

                    appendToFile(datafixReportFile, idsToDelete +"<br>\n" );

                } else {
                    appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate +
                          ", no se encontraron casos a fixear <br>\n");
                    System.out.println("Duplicated services not found!");
                }

                System.out.println("Process statement generation.");

            } catch (Exception e) {
                printStackTrace(e);

            } finally {
                try {
                    if (rs != null) rs.close();
                    if (stmt != null) stmt.close();
                    if (con != null) con.close();
                } catch (Exception e) {
                    System.out.println(" ");
                    System.out.println(e.getMessage());
                    printStackTrace(e);
                }
            }

        } catch (Exception e1) {
            System.out.println(" ");
            System.out.println(e1.getMessage());
            e1.printStackTrace();
        }
    }

    private  void generateUserInfoReports(final DataSource dataSource, String env) {

        try {
            Connection con = dataSource.getConnection();
            Statement stmt = null;
            ResultSet rs = null;

            try {

                if (arrInstallationIds.size() > 0){
                    String sqlSearchUser = "SELECT DES_NICK_NAME  FROM  \"ACCOUNT-MANAGEMENT\".SMPR_TUSER " +
                          "WHERE seq_id = (SELECT seq_admin FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION " +
                          "WHERE seq_id =:installtionId)";

                    stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

                    for (Long installationId:arrInstallationIds){
                        rs = stmt.executeQuery(sqlSearchUser.replaceAll(":installtionId", ""+installationId) );
                        if (rs.next()) {
                            generateAndSendUserReport(dataSource, env, rs.getString(1), "martin.cruz@artekium.com");
                        }
                    }
                }

                System.out.println("End userInfo report generation.");

            } catch (Exception e) {
                printStackTrace(e);

            } finally {
                try {
                    if (rs != null) rs.close();
                    if (stmt != null) stmt.close();
                    if (con != null) con.close();
                } catch (Exception e) {
                    System.out.println(" ");
                    System.out.println(e.getMessage());
                    printStackTrace(e);
                }
            }

        } catch (Exception e1) {
            System.out.println(" ");
            System.out.println(e1.getMessage());
            e1.printStackTrace();
        }
    }
}
