package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class CheckPBUserDeleted extends QryDB {

    private static final Logger LOGGER = LogManager.getLogger(CheckPBUserDeleted.class);
    static int recordCount = 0;

    public int main(String env, String datafixReportFile, String reportDate) {

        loadExternalFileProperties();
        DataSource dataSource = null;

        try {

            if (env == null) env = getProperty("env");
            String connectionType = getProperty("connectionType");

            String jdbcUrl = getProperty(connectionType + "." + env + ".url");
            String dbUsername = getProperty(env + ".username");
            String password = getProperty(env + ".password");

            dataSource = getDataSource(jdbcUrl, dbUsername, password);

            System.out.println("Executing step 1: check PB users records ..." );
            checkPBUsers(dataSource);

            System.out.println("Total record found: " + recordCount);

            appendToFile(datafixReportFile,
                    "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-40173'>" +
                          "18) CS " +
                            "https://jira.prosegur.com/browse/AM-40173</a> - " +
                            "Borrado de PB User - DESCONTINUAR VIENRES 14 FEB<br>\n");

            if (recordCount > 0) {
                appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                        ", ERROR se encontraron " + recordCount + " PB User sin borrar. <br>\n");
            } else {
                appendToFile(datafixReportFile,  "Ejecucion del dia : " + reportDate+
                        ", OK, no se encontraron PB Users sin borrar <br>\n");
            }

        } catch (Exception e){
            printStackTrace(e);
        }

        return recordCount;
    }

    public static void checkPBUsers(final DataSource dataSource) {

        try {
            Connection con = dataSource.getConnection();
            Statement stmt = null;
            ResultSet rs = null;

            try {

                String sql1 = "SELECT count(1) " +
                      "FROM \"PANIC-BUTTON\".SMPR_TPANIC_BUTTON_USER pb " +
                      "JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usu " +
                      "ON pb.SEQ_USER_ID = usu.SEQ_ID " +
                      "AND USU.FYH_DELETED IS NOT NULL";

                stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                rs = stmt.executeQuery(sql1);

                if (rs.next()) {
                    recordCount = rs.getInt(1);
                }

            } catch (Exception e) {
                System.out.println(" ");
                System.out.println(e.getMessage());
                printStackTrace(e);
            } finally {
                try {
                    if (rs != null) rs.close();
                    if (stmt != null) stmt.close();
                    if (con != null) con.close();
                } catch (Exception e) {
                    System.out.println(" ");
                    System.out.println(e.getMessage());
                    printStackTrace(e);
                }
            }

        } catch (Exception e1) {
            System.out.println(" ");
            System.out.println(e1.getMessage());
            printStackTrace(e1);
        }
    }
}
