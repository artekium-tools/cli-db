package clidb;

import clidb.entities.Installation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


public class RepetedInstallations extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(RepetedInstallations.class);

   static boolean UPDATE_IN_TEST_MODE = false; // true=olny shows logs  false=execute updates

   static Properties prop = new Properties();
   static Properties scriptProperties = new Properties();
   static ArrayList<Long> arr_seq_user_id = new ArrayList();
   static int totalUserToFix = 0;


   public String checkRepetedInstalationsCCNull(String env, String userToLoginFile, String datafixReportFile, String reportDate) {

      totalUserToFix =0;

      loadExternalFileProperties();

      DataSource dataSource = null;

      String sqlToCheckCCNull = null;
      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         LOGGER.info("Init Process: " );
         // Usuarios que posean instalaciones repetidas,
         // Donde se repite ContractId y ContractNumber
         System.out.println("Executing step 1: select user with repeted installations ...." );
         selectUsers(dataSource);

         //1) Mismo contrato DES_CONTRACT_ID y DES_CONTRACT_NUMBER asignado a 2 usuarios distintos
         //2) Usuario con 2 instalaciones y en las dos se repite DES_CONTRACT_ID y DES_CONTRACT_NUMBER
            // a) Una de las instalaciones tiene panel con CC Null (Producido por la migracion, fix IN PROGRESS)
            // b) Una de ellas no tiene arbol de thing ni de thingAlias (No sabemos la causa aun )
         //3) Usuario con más de 2 instalaciones, dos de ellas se repite DES_CONTRACT_ID y DES_CONTRACT_NUMBER
         // Correccion la instalacion repetida ya pertenece a otro usuario
         String listUsersIdCCNull = "";
         //String listUsersIdNoTree = "";
         ArrayList<Long> arr_inst_to_be_deleted = new ArrayList();

         System.out.println("Executing step 2: check installations has Panel With CCNull...." );
         int inx = 1;
         for (Long userId:arr_seq_user_id){

//System.out.println("item " + inx + " of " + arr_seq_user_id.size());
            inx ++;
            // Consultar las instalaciones para el seq_admin
            // Ver si es caso 1, 2 o 3

            // Seleccionamos las instalciones activas para el usuario
            ArrayList<Installation> arrInstallation = selectInstallationByUserId(dataSource, userId);

            if (arrInstallation.size() == 1) {
               //appendToFile("fixLimpiezaErronea.txt",  userId + "\n");
            } else if (arrInstallation.size() == 2) {
               boolean repetedInstFound = false;
               boolean isCCNull = false;
               for (Installation installation:arrInstallation){

                  if (hasPanelWithCCNull(dataSource, installation.getId()) &&
                        !arr_inst_to_be_deleted.contains(installation.getId())){
                     generateDeleteStatements(dataSource, "deleteRepetedInstallationsCCNull.sql", installation.getId(), userId);
                     arr_inst_to_be_deleted.add(installation.getId());
                     repetedInstFound = true;
                     isCCNull = true;
                     break;
                  }


               }

               if (repetedInstFound && isCCNull){
                  appendToFile("deleteRepetedInstallationsCCNull.sql", "-- End user: " + userId + " --------------------------------------------------\n");
                  listUsersIdCCNull += userId + ",";
               }


            } else if (arrInstallation.size() > 2) {
               boolean repetedInstFound = false;
               boolean isCCNull = false;
               for (Installation installation: arrInstallation){
                  ArrayList<Installation> arrInstallationDiffUser = selectInstallationByContract(dataSource, installation.getContractId(), installation.getContractNumber());

                  for (Installation installationDiffUser:arrInstallationDiffUser){
                     if (hasPanelWithCCNull(dataSource, installationDiffUser.getId()) &&
                           !arr_inst_to_be_deleted.contains(installationDiffUser.getId())){
                        generateDeleteStatements(dataSource, "deleteRepetedInstallationsCCNull.sql", installationDiffUser.getId(), userId);
                        arr_inst_to_be_deleted.add(installationDiffUser.getId());
                        repetedInstFound = true;
                        isCCNull = true;
                        break;
                     }
                  }

                  if (repetedInstFound) break;
               }

               if (repetedInstFound && isCCNull) {
                  appendToFile("deleteRepetedInstallationsCCNull.sql", "-- End user: " + userId + " --------------------------------------------------\n");
                  listUsersIdCCNull += userId + ",";
               }

            }

            // Si el resultado es 1 registro, es caso 1)
               //Accion: consultar por DES_CONTRACT_ID y DES_CONTRACT_NUMBER para obtener las instalaciones duplicadas
               //Accion: Generar datos para evaluar cual hay que borrar - generar un reporte

            // Si el resultado es 2 registros, es caso 2)

            // Si el resultado es > 2 registros es caso 3
               //Accion: consultar por DES_CONTRACT_ID y DES_CONTRACT_NUMBER para obtener las instalaciones duplicadas

               // una vez identificadas las instalaciones duplicadas para 2) y 3)
                  // Consulta 1: el COD_CONNECION_ID de Thing para ver si uno de ellos es NULL y es es el candidato a ser Borrado
                  // Accion: generar scripts de borrado de instalacion duplicada, y arbol de thing, thignAlias y sus replicas.

                  // Consulta 2: que no Posea arbol de thing ni de thingAlias
                  // Accion: generar scripts de borrado de instalacion duplicada, y sus replicas.



         }

         appendToFile(datafixReportFile,
               "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-38370'>" +
                     "1)  CS " +
                     "https://jira.prosegur.com/browse/AM-38370</a> - " +
                     "Instalaciones Duplicadas CC Null <br>\n");

         if (listUsersIdCCNull.length() > 0 ) {
            sqlToCheckCCNull = "SELECT usr.SEQ_ID AS userId, usr.DES_NICK_NAME, usr.DES_COUNTRY, inst.SEQ_ID AS instalaltionId, inst.DES_LOCATION, thn.SEQ_ID AS panelId, thn.COD_CONNECTION_ID  \n" +
                  "FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr \n" +
                  "INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION inst ON (usr.SEQ_ID = inst.SEQ_ADMIN)  \n" +
                  "INNER JOIN \"DEVICE\".SMPR_TTHING thn ON (thn.SEQ_INSTALLATION_ID = inst.SEQ_ID AND thn.SEQ_ENTITY_TYPE = 13) \n" +
                  "INNER JOIN \"DEVICE\".SMPR_TSERVICE_TYPE stt ON (thn.SEQ_TYPE_ID = stt.SEQ_ID) \n" +
                  "WHERE thn.COD_CONNECTION_ID IS NULL \n" +
                  "AND usr.SEQ_ID IN \n";

            listUsersIdCCNull = listUsersIdCCNull.substring(0, listUsersIdCCNull.lastIndexOf(","));
            sqlToCheckCCNull += "(" + listUsersIdCCNull + ");";


            appendToFile(userToLoginFile,  "-- checkRepetedInstalations AM-38370 \n");

            String listUsers = showUsersNames(dataSource, listUsersIdCCNull, userToLoginFile, env);

            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+ ", se fixearon " +
                  totalUserToFix + " casos. <br>\n");

            appendToFile(datafixReportFile, listUsers);

         }  else {
            System.out.println("No result for CC Null ");
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+ ", no se encontraron casos a fixear<br>\n");
         }


      } catch (Exception e){
         printStackTrace(e);
      }


      return sqlToCheckCCNull;
   }

   public String checkRepetedInstalationsNoTree(String env, String userToLoginFile, String datafixReportFile, String reportDate) {

      totalUserToFix =0;

      loadExternalFileProperties();

      DataSource dataSource = null;
      String sqlToCheckNoTree = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         LOGGER.info("Init Process: " );
         // Usuarios que posean instalaciones repetidas,
         // Donde se repite ContractId y ContractNumber
         System.out.println("Executing step 1: select user with repeted installations ...." );
         selectUsers(dataSource);

         //1) Mismo contrato DES_CONTRACT_ID y DES_CONTRACT_NUMBER asignado a 2 usuarios distintos
         //2) Usuario con 2 instalaciones y en las dos se repite DES_CONTRACT_ID y DES_CONTRACT_NUMBER
         // a) Una de las instalaciones tiene panel con CC Null (Producido por la migracion, fix IN PROGRESS)
         // b) Una de ellas no tiene arbol de thing ni de thingAlias (No sabemos la causa aun )
         //3) Usuario con más de 2 instalaciones, dos de ellas se repite DES_CONTRACT_ID y DES_CONTRACT_NUMBER
         // Correccion la instalacion repetida ya pertenece a otro usuario
         //String listUsersIdCCNull = "";
         String listUsersIdNoTree = "";
         ArrayList<Long> arr_inst_to_be_deleted = new ArrayList();

         System.out.println("Executing step 2: check installations has no thing alias tree ..." );
         for (Long userId:arr_seq_user_id){

            // Consultar las instalaciones para el seq_admin
            // Ver si es caso 1, 2 o 3

            // Seleccionamos las instalciones activas para el usuario
            ArrayList<Installation> arrInstallation = selectInstallationByUserId(dataSource, userId);

            if (arrInstallation.size() == 1) {
               //appendToFile("fixLimpiezaErronea.txt",  userId + "\n");
            } else if (arrInstallation.size() == 2) {
               boolean repetedInstFound = false;
               boolean isCCNull = false;
               for (Installation installation:arrInstallation){


                  if (hasNoTree(dataSource, installation.getId(), userId) &&
                        !arr_inst_to_be_deleted.contains(installation.getId())){
                     generateDeleteStatements(dataSource, "deleteRepetedInstallationsNoTree.sql", installation.getId(), userId);
                     arr_inst_to_be_deleted.add(installation.getId());
                     repetedInstFound = true;
                     isCCNull = false;
                     break;
                  }

               }


               if (repetedInstFound && !isCCNull){
                  appendToFile("deleteRepetedInstallationsNoTree.sql", "-- End user: " + userId + " --------------------------------------------------\n");
                  listUsersIdNoTree += userId + ",";
               }

            } else if (arrInstallation.size() > 2) {
               boolean repetedInstFound = false;
               boolean isCCNull = false;
               for (Installation installation: arrInstallation){
                  ArrayList<Installation> arrInstallationDiffUser = selectInstallationByContract(dataSource, installation.getContractId(), installation.getContractNumber());

                  for (Installation installationDiffUser:arrInstallationDiffUser){

                     if (hasNoTree(dataSource, installationDiffUser.getId(), userId) &&
                           !arr_inst_to_be_deleted.contains(installationDiffUser.getId())){
                        generateDeleteStatements(dataSource, "deleteRepetedInstallationsNoTree.sql", installationDiffUser.getId(), userId);
                        arr_inst_to_be_deleted.add(installationDiffUser.getId());
                        repetedInstFound = true;
                        isCCNull = false;
                        break;
                     }
                  }
                  if (repetedInstFound) break;
               }


               if (repetedInstFound && !isCCNull) {
                  appendToFile("deleteRepetedInstallationsNoTree.sql", "-- End user: " + userId + " --------------------------------------------------\n");
                  listUsersIdNoTree += userId + ",";
               }
            }

            // Si el resultado es 1 registro, es caso 1)
            //Accion: consultar por DES_CONTRACT_ID y DES_CONTRACT_NUMBER para obtener las instalaciones duplicadas
            //Accion: Generar datos para evaluar cual hay que borrar - generar un reporte

            // Si el resultado es 2 registros, es caso 2)

            // Si el resultado es > 2 registros es caso 3
            //Accion: consultar por DES_CONTRACT_ID y DES_CONTRACT_NUMBER para obtener las instalaciones duplicadas

            // una vez identificadas las instalaciones duplicadas para 2) y 3)
            // Consulta 1: el COD_CONNECION_ID de Thing para ver si uno de ellos es NULL y es es el candidato a ser Borrado
            // Accion: generar scripts de borrado de instalacion duplicada, y arbol de thing, thignAlias y sus replicas.

            // Consulta 2: que no Posea arbol de thing ni de thingAlias
            // Accion: generar scripts de borrado de instalacion duplicada, y sus replicas.



         }


         appendToFile(datafixReportFile,
               "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-40608'>" +
                     "2) CS " +
                     "https://jira.prosegur.com/browse/AM-40608</a> - " +
                     "Instalaciones Duplicadas Sin Arbol de ThingAlias<br>\n");


         if (listUsersIdNoTree.length() > 0 ) {
            listUsersIdNoTree = listUsersIdNoTree.substring(0, listUsersIdNoTree.lastIndexOf(","));

            sqlToCheckNoTree = "SELECT usr.SEQ_ID AS userId, usr.DES_NICK_NAME, usr.DES_COUNTRY, inst.SEQ_ID AS instalaltionId, \n" +
                  "(SELECT count(1) FROM DEVICE.SMPR_TTHING_ALIAS WHERE SEQ_INSTALLATION_ALIAS_ID = ( \n" +
                  "SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS WHERE SEQ_INSTALLATION_ID = inst.SEQ_ID AND SEQ_USER_ID = usr.SEQ_ID \n" +
                  ")) AS cntThnAlias \n" +
                  "FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr \n" +
                  "INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION inst ON (usr.SEQ_ID = inst.SEQ_ADMIN) \n" +
                  "WHERE \n" +
                  "(SELECT count(1) FROM DEVICE.SMPR_TTHING_ALIAS WHERE SEQ_INSTALLATION_ALIAS_ID = ( \n" +
                  " SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS WHERE SEQ_INSTALLATION_ID = inst.SEQ_ID AND SEQ_USER_ID = usr.SEQ_ID \n" +
                  ")) = 0 \n" +
                  "AND usr.SEQ_ID IN (" + listUsersIdNoTree + ")";

            appendToFile(userToLoginFile,  "-- checkRepetedInstalations AM-40608  \n");

            String listUser = showUsersNames(dataSource, listUsersIdNoTree,userToLoginFile, env);

            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+ ", se fixearon " + totalUserToFix +
                  " casos. <br>\n");

            appendToFile(datafixReportFile, listUser);

         } else {
            System.out.println("No result for No Tree");
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+ ", no se encontraron casos a fixear <br>\n");
         }


      } catch (Exception e){
         printStackTrace(e);
      }

      return sqlToCheckNoTree;
   }

   public void generateDeleteIntallationStmnt(String env, Long installationId, Long userId) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         //System.out.println("env: " + env);
         //System.out.println("connectionType: " + connectionType);

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         generateDeleteStatements(dataSource, "deleteInstallationsAndTree.sql",  installationId, userId);

         System.out.println("End.");
      } catch (Exception e){
         printStackTrace(e);
      }



      LOGGER.info("End process.");

      return;
   }

   public static void selectUsers(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT DISTINCT SEQ_ADMIN " +
                     "FROM  \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION " +
                     "WHERE DES_CONTRACT_NUMBER IN ( " +
                     "      SELECT 	DES_CONTRACT_NUMBER " +
                     "     FROM ( " +
                     "            SELECT count(DES_CONTRACT_NUMBER), DES_CONTRACT_NUMBER " +
                     "            FROM  \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION " +
                     "            WHERE " +
                     "            FYH_DELETED IS NULL " +
                     "            AND SEQ_ADMIN IN ( " +
                     "                  SELECT DISTINCT SEQ_ADMIN FROM  \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION " +
                     "                  WHERE DES_CONTRACT_ID IN ( " +
                     "                        SELECT DES_CONTRACT_ID FROM " +
                     "                              ( " +
                     "                                    SELECT count(DES_CONTRACT_ID), DES_CONTRACT_ID " +
                     "                                    FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION " +
                     "                                    WHERE " +
                     "                                    FYH_DELETED IS NULL " +
                     "                                    GROUP BY DES_CONTRACT_ID " +
                     "                                    HAVING count(DES_CONTRACT_ID) > 1 " +
                     "                                    ORDER BY count(DES_CONTRACT_ID) DESC " +
                     "                              ) " +
                     "                  ) " +
                     "            ) " +
                     "            GROUP BY DES_CONTRACT_NUMBER " +
                     "            HAVING count(DES_CONTRACT_NUMBER) > 1 " +
                     "      ) " +
                     ") " ;

//System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
               arr_seq_user_id.add(rs.getLong(1));
               //printInLine(rs.getLong(1) + " User Added");
               //System.out.println(rs.getLong(1) + " User Added");
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }

   public static ArrayList<Installation> selectInstallationByUserId(final DataSource dataSource, Long seqUserId) {

      ArrayList<Installation> arrInstallation = new ArrayList<>();

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql = "SELECT * FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION WHERE " +
                  "FYH_DELETED IS NULL " +
                  "AND SEQ_ADMIN = " + seqUserId;
            //System.out.println(sql);
            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            while (rs.next()) {

               Installation installation = new Installation();

               installation.setId(rs.getLong("SEQ_ID"));
               installation.setAdmin(rs.getLong("SEQ_ADMIN"));

               installation.setAccountManager(rs.getString("DES_ACCOUNT_MANAGER"));
               installation.setContractId(rs.getString("DES_CONTRACT_ID"));
               installation.setContractNumber(rs.getString("DES_CONTRACT_NUMBER"));
               installation.setCountry(rs.getString("DES_COUNTRY"));
               installation.setCustomerReference(rs.getString("DES_CUSTOMER_REFERENCE"));
               installation.setLatitude(rs.getString("DES_LATITUDE"));
               installation.setLocation(rs.getString("DES_LOCATION"));
               installation.setLocationCity(rs.getString("DES_LOCATION_CITY"));
               installation.setLocationPostcode(rs.getString("DES_LOCATION_POSTCODE"));
               installation.setLocationRegion(rs.getString("DES_LOCATION_REGION"));
               installation.setLocationStreet(rs.getString("DES_LOCATION_STREET"));
               installation.setLocationStreetNumber(rs.getString("DES_LOCATION_STREET_NUMBER"));
               installation.setLocationStreetType(rs.getString("DES_LOCATION_STREET_TYPE"));
               installation.setLongitude(rs.getString("DES_LONGITUDE"));
               installation.setContractType(rs.getString("DES_CONTRACT_TYPE"));
               installation.setContractStatus(rs.getString("DES_CONTRACT_STATUS"));

               //installation.setUpdateDate(rs.getString("FYH_UPDATE_DATE"));
               //installation.setDeletedDate(rs.getString("FYH_DELETED"));

               installation.setFlagSynchronized(rs.getInt("BOL_SYNCHRONIZED"));
               installation.setActive(rs.getInt("BOL_ACTIVE")==1?true:false);

               arrInstallation.add(installation);

               //printInLine(rs.getLong(1) + " Installation Added");
               //System.out.println(rs.getLong(1) + " Installation Added");
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return arrInstallation;

   }

   /**
    * Has panel with CC Null: return True
    *
    *
    * @param dataSource
    * @param instalationId
    * @return
    */
   public static boolean hasPanelWithCCNull(final DataSource dataSource, Long instalationId) {


      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql = "SELECT seq_id, cod_connection_id FROM \"DEVICE\".smpr_tthing WHERE " +
                  "SEQ_ENTITY_TYPE = 13 AND seq_installation_id = " + instalationId;

            //System.out.println("   " + sql);
            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
               if (rs.getString(2) == null) {
                 // System.out.println("For installation:  " + instalationId + " the Panel: " +
                  //      rs.getLong(1) + " has CC Null. - TRUE");
                  return true;
               } else {
                  //System.out.println("For installation:  " + instalationId + " the Panel: " +
                  //      rs.getLong(1) + " has CC:" + rs.getString(2) + " - FALSE");
                  return false;
               }
            } /*else {
               System.out.println("For installation:  " + instalationId + " has NO PANEL - TRUE");
               return true;
            }*/

         } catch (Exception e) {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception ex) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               ex.printStackTrace();
            }

            return false;
            //System.out.println(" ");
            //System.out.println(e.getMessage());
            //printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return false;

   }

   public static boolean hasNoTree(final DataSource dataSource, Long instalationId, Long userId) {

      Boolean result = null;

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql = "SELECT count(1) FROM DEVICE.SMPR_TTHING_ALIAS WHERE SEQ_INSTALLATION_ALIAS_ID = ( " +
                  "SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS " +
                  "WHERE SEQ_INSTALLATION_ID = " +  instalationId +
                  " AND SEQ_USER_ID = " + userId +
                  ")" ;

            //System.out.println("   " + sql);
            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
               if (rs.getInt(1) == 0) {
                  //System.out.println("For installation:  " + instalationId + " and user: " +
                  //      userId + " result: " + rs.getLong(1) + " has No Tree - TRUE");
                  result = true;
               } else {
                  //System.out.println("For installation:  " + instalationId + " and user: " +
                  //      userId + " result: " + rs.getLong(1) + " has Tree - FALSE");
                  result  = false;
               }
            }  else {
               result = true;
            }

         } catch (Exception e) {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception ex) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               ex.printStackTrace();
            }
            return false;
            //System.out.println(" ");
            //System.out.println(e.getMessage());
            //printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return result;

   }

   public static ArrayList<Installation> selectInstallationByContract(final DataSource dataSource, String contractId, String contractNumber) {

      ArrayList<Installation> arrInstallation = new ArrayList<>();

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql = "SELECT * FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION WHERE " +
                  "DES_CONTRACT_ID = '" + contractId + "'" +
                  " AND DES_CONTRACT_NUMBER = '" + contractNumber + "'";

            //System.out.println("      " + sql);
            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            while (rs.next()) {

               Installation installation = new Installation();

               installation.setId(rs.getLong("SEQ_ID"));
               installation.setAdmin(rs.getLong("SEQ_ADMIN"));

               installation.setAccountManager(rs.getString("DES_ACCOUNT_MANAGER"));
               installation.setContractId(rs.getString("DES_CONTRACT_ID"));
               installation.setContractNumber(rs.getString("DES_CONTRACT_NUMBER"));
               installation.setCountry(rs.getString("DES_COUNTRY"));
               installation.setCustomerReference(rs.getString("DES_CUSTOMER_REFERENCE"));
               installation.setLatitude(rs.getString("DES_LATITUDE"));
               installation.setLocation(rs.getString("DES_LOCATION"));
               installation.setLocationCity(rs.getString("DES_LOCATION_CITY"));
               installation.setLocationPostcode(rs.getString("DES_LOCATION_POSTCODE"));
               installation.setLocationRegion(rs.getString("DES_LOCATION_REGION"));
               installation.setLocationStreet(rs.getString("DES_LOCATION_STREET"));
               installation.setLocationStreetNumber(rs.getString("DES_LOCATION_STREET_NUMBER"));
               installation.setLocationStreetType(rs.getString("DES_LOCATION_STREET_TYPE"));
               installation.setLongitude(rs.getString("DES_LONGITUDE"));
               installation.setContractType(rs.getString("DES_CONTRACT_TYPE"));
               installation.setContractStatus(rs.getString("DES_CONTRACT_STATUS"));

               //installation.setUpdateDate(rs.getString("FYH_UPDATE_DATE"));
               //installation.setDeletedDate(rs.getString("FYH_DELETED"));

               installation.setFlagSynchronized(rs.getInt("BOL_SYNCHRONIZED"));
               installation.setActive(rs.getInt("BOL_ACTIVE")==1?true:false);

               arrInstallation.add(installation);

               //System.out.println("        " + rs.getLong(1) + " Installation Added");
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return arrInstallation;

   }

   public static void generateDeleteStatements(final DataSource dataSource, String fileName, Long installationId, Long userId){

      /*
         replicas.installation=CAMPAIGNS,DEVICE,EVENT-PROCESSOR,PANIC-BUTTON,RULE-ENGINE
         replicas.installationAias=CAMPAIGNS,DEVICE,EVENT-PROCESSOR,PANIC-BUTTON,RULE-ENGINE
         replicas.thing=ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SCENE
         replicas.thingAlias=ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE
       */
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {


            String sqlThingAlias = "SELECT concat( 'DELETE FROM SMPR_TTHING_ALIAS WHERE SEQ_ID = ' , SEQ_ID) " +
                  " FROM \"ACCOUNT-MANAGEMENT\".SMPR_TTHING_ALIAS WHERE SEQ_INSTALLATION_ALIAS_ID = " +
                  " (SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS WHERE SEQ_INSTALLATION_ID = " + installationId + " AND SEQ_USER_ID = " + userId +" )" +
                  " UNION " +
                  " SELECT concat( 'DELETE FROM SMPR_TTHING_ALIAS WHERE SEQ_ID = ' , SEQ_ID) " +
                  " FROM \"ACCOUNT-MANAGEMENT\".SMPR_TTHING_ALIAS WHERE SEQ_PARENT_ID IN (" +
                  " SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TTHING_ALIAS WHERE SEQ_INSTALLATION_ALIAS_ID = " +
                  " (SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS WHERE SEQ_INSTALLATION_ID = " + installationId + " AND SEQ_USER_ID = " + userId +")" +
                  ")";

            String sqlThing = "SELECT concat( 'DELETE FROM SMPR_TTHING WHERE SEQ_ID = ' , SEQ_ID) " +
                  "FROM \"ACCOUNT-MANAGEMENT\".SMPR_TTHING WHERE SEQ_INSTALLATION_ID = " + installationId +
                  " UNION " +
                  " SELECT concat( 'DELETE FROM SMPR_TTHING WHERE SEQ_ID = ' , SEQ_ID) " +
                  "FROM \"ACCOUNT-MANAGEMENT\".SMPR_TTHING WHERE SEQ_PARENT_ID IN (" +
                  " SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TTHING WHERE SEQ_INSTALLATION_ID = " + installationId +
                  ")";

            String sqlInstAlias = "SELECT concat( 'DELETE FROM SMPR_TINSTALLATION_ALIAS WHERE SEQ_ID = ' , SEQ_ID) FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS " +
                  "WHERE SEQ_INSTALLATION_ID = " + installationId;

            String sqlInstallations = "SELECT concat( 'DELETE FROM SMPR_TINSTALLATION WHERE SEQ_ID = ' , SEQ_ID) FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION WHERE SEQ_id = " + installationId;

            // Thing Alias -------------------------------------------------------------------------
            //System.out.println("   " + sqlThingAlias);
            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sqlThingAlias);

            String deleteStmnt = "";

            String schemas = "schemas:DEVICE,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG\n";
            appendToFile(fileName, "-- Thing Alias for user: " + userId + "\n");
            appendToFile(fileName, schemas);
            while (rs.next()) {
               deleteStmnt = rs.getString(1);
               appendToFile(fileName, deleteStmnt + ";\n");
            }

            // Thing -------------------------------------------------------------------------
            //System.out.println("   " + sqlThing);
            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sqlThing);

            schemas = "schemas:DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SCENE,SYSLOG\n";
            appendToFile(fileName, "-- Thing for user: " + userId + "\n");
            appendToFile(fileName, schemas);
            while (rs.next()) {
               deleteStmnt = rs.getString(1);
               appendToFile(fileName, deleteStmnt + ";\n");
            }

            // Installation Alias -------------------------------------------------------------------
            //System.out.println("   " + sqlInstAlias);
            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sqlInstAlias);

               schemas = "schemas:ACCOUNT-MANAGEMENT,CAMPAIGNS,DEVICE,DIGITAL-SECURITY,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG,USER-ENROLLMENT\n";

            appendToFile(fileName, "-- Installation Alias for user: " + userId + "\n");
            appendToFile(fileName, schemas);
            while (rs.next()) {
               deleteStmnt = rs.getString(1);
               appendToFile(fileName, deleteStmnt + ";\n");
            }

            // Installation -------------------------------------------------------------------
            //System.out.println("   " + sqlInstallations);
            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sqlInstallations);

            schemas = "schemas:ACCOUNT-MANAGEMENT,CAMPAIGNS,DEVICE,DIGITAL-SECURITY,EVENT-PROCESSOR,FEATURE-FLAG,INBOX,INBOX-MESSAGE-PROCESSOR,PANIC-BUTTON,RULE-ENGINE,SYSLOG,USER-ENROLLMENT,VIRTUAL-ASSISTANT,WEBSOCKET-ADAPTER\n";
            appendToFile(fileName, "-- Installation for user: " + userId + "\n");
            appendToFile(fileName, schemas);
            while (rs.next()) {
               deleteStmnt = rs.getString(1);
               appendToFile(fileName, deleteStmnt + ";\n");
            }

         } catch (Exception e) {
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }
      } catch (Exception e) {
         printStackTrace(e);
      }
   }


   public String showUsersNames(final DataSource dataSource, String listUserId, String userToLoginFile, String env) {

      String result = "";

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT DES_NICK_NAME " +
               "FROM  \"ACCOUNT-MANAGEMENT\".SMPR_TUSER " +
               "WHERE SEQ_ID IN ( " + listUserId + ") " ;

         //System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
               System.out.println(rs.getString(1));

               generateAndSendUserReport(dataSource, env, rs.getString(1), "cristian.salcedo@artekium.com");

               appendToFile(userToLoginFile, rs.getString(1) + "\n");
               result +=  rs.getString(1) + "<br>\n";
               totalUserToFix ++;
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return result;
   }




}
