package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class CheckUnsynchronizedScene extends QryDB {

    private static final Logger LOGGER = LogManager.getLogger(CheckRuleEngineScene.class);
    static ArrayList<String> arr_sceneUnsynchronized = new ArrayList<>();
    static Set<String> arr_userSceneUnsynchronized = new HashSet<>();

    public int findUnsynchronizedScene(String env, String userToLoginFile, String datafixReportFile, String reportDate) {

        loadExternalFileProperties();
        DataSource dataSource = null;

        try {

            if (env == null) env = getProperty("env");
            String connectionType = getProperty("connectionType");

            String jdbcUrl = getProperty(connectionType + "." + env + ".url");
            String dbUsername = getProperty(env + ".username");
            String password = getProperty(env + ".password");

            dataSource = getDataSource(jdbcUrl, dbUsername, password);

            System.out.println("Executing step 1: check missing scene in rule engine ..." );
            checkSceneUnsynchronized(dataSource);

            System.out.println("Total record found: " + arr_sceneUnsynchronized.size());

            appendToFile(datafixReportFile,
                    "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-41474'>" +
                            "15) YH " +
                            "https://jira.prosegur.com/browse/AM-41474</a> - " +
                            "RULE-ENGINE - Desincronización entre SCENE y RULE-ENGINE<br>\n");



            if (arr_sceneUnsynchronized.size() > 0) {
                appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                        ", se fixearon " + arr_sceneUnsynchronized.size() + " casos. <br>\n");

                for (String sceneData:arr_sceneUnsynchronized){

                    appendToFile(datafixReportFile, sceneData + "<br>\n");
                }

                appendToFile(userToLoginFile, "-- synchronizeSceneBetweenSceneAndRseuleEngine AM-41474 \n");
                for (String username:arr_userSceneUnsynchronized) {
                    appendToFile(userToLoginFile, username);
                }
            } else {
                appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                        ", no se encontraron casos a fixear<br>\n");
            }

        } catch (Exception e){
            e.printStackTrace();
        }

        return arr_sceneUnsynchronized.size();
    }

    public static void checkSceneUnsynchronized(final DataSource dataSource) {

        try {
            Connection con = dataSource.getConnection();
            Statement stmt = null;
            ResultSet rs = null;

            try {

                String sql1 = "SELECT su.DES_NICK_NAME,ss.SEQ_ID,ss.SEQ_USER_ID,ss.BOL_ACTIVE,ss.BOL_OBSOLETE,SS.DES_COUNTRY " +
                        " FROM \"SCENE\".SMPR_TSCENE ss " +
                        " JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TUSER su ON ss.SEQ_USER_ID = su.SEQ_ID " +
                        " WHERE ss.SEQ_ID NOT IN (SELECT SEQ_ID FROM \"RULE-ENGINE\".SMPR_TSCENE) " +
                        " ORDER BY su.DES_NICK_NAME desc ";

                stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                rs = stmt.executeQuery(sql1);


                while (rs.next()) {

                    String insertStmt = "INSERT INTO SMPR_TSCENE (SEQ_ID,SEQ_USER_ID, BOL_ACTIVE, BOL_OBSOLETE, DES_COUNTRY) " +
                            " VALUES ('" + rs.getLong(2)+"','"+rs.getString(3)+
                            "','"+rs.getInt(4)+"','"+rs.getInt(5)+"','"+rs.getString(6)+"');";
                    appendToFile("createMissingScene.sql",  insertStmt + "\n");

                    arr_sceneUnsynchronized.add("SceneId : ".concat(rs.getString(2).concat(" Nickname : ").concat(rs.getString(1))));
                    arr_userSceneUnsynchronized.add(rs.getString(1).concat("\n"));
                }

            } catch (Exception e) {
                System.out.println(" ");
                printStackTrace(e);
            } finally {
                try {
                    if (rs != null) rs.close();
                    if (stmt != null) stmt.close();
                    if (con != null) con.close();
                } catch (Exception e) {
                    System.out.println(" ");
                    printStackTrace(e);
                }
            }

        } catch (Exception e1) {
            System.out.println(" ");
            printStackTrace(e1);
        }
    }
}
