package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

public class CheckPrivacyModeStatus extends QryDB {

    private static final Logger LOGGER = LogManager.getLogger(CheckCMSession.class);

    static ArrayList<String> arr_userId = new ArrayList<>();
    static ArrayList<String> arr_ThingId = new ArrayList<>();

    public int fixMPStatusDevices(String env, String datafixReportFile, String reportDate, String usersToTurnOnCamerasFile) {

        loadExternalFileProperties();

        DataSource dataSource = null;

        try {

            if (env == null) env = getProperty("env");
            String connectionType = getProperty("connectionType");

            String jdbcUrl = getProperty(connectionType + "." + env + ".url");
            String dbUsername = getProperty(env + ".username");
            String password = getProperty(env + ".password");

            dataSource = getDataSource(jdbcUrl, dbUsername, password);

            System.out.println("Executing step 1: check MP status  ..." );

            appendToFile(usersToTurnOnCamerasFile, "-- fixMPStatusDevices AM-38079 \n");

            appendToFile(datafixReportFile,
                  "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-38079'>" +
                        "7) MC " +
                        "https://jira.prosegur.com/browse/AM-38079</a> - " +
                        "Clientes de tipo negocio se crean con Modo Privacidad activo<br>\n");

            checkMpStatusDevicesInRuleEngine(dataSource, datafixReportFile, reportDate);


            writeUsersToTurnOnCamera(dataSource, usersToTurnOnCamerasFile);

            System.out.println("Total devices found: " + arr_ThingId.size());

        } catch (Exception e){
            printStackTrace(e);
        }


        return arr_ThingId.size();
    }

    public static void checkMpStatusDevicesInRuleEngine(final DataSource dataSource, String datafixReportFile, String reportDate) {

        try {
            Connection con = dataSource.getConnection();
            Statement stmt = null;
            ResultSet rs = null;

            try {

                String sql1 = "SELECT rule.SEQ_THING_ID, instal.SEQ_ADMIN FROM \"RULE-ENGINE\".SMPR_TINSTALLATION instal" +
                      " INNER JOIN \"RULE-ENGINE\".SMPR_TTHING service ON instal.SEQ_ID = service.SEQ_INSTALLATION_ID" +
                      " INNER JOIN \"RULE-ENGINE\".SMPR_TTHING thing ON service.SEQ_ID = thing.SEQ_PARENT_ID" +
                      " INNER JOIN \"RULE-ENGINE\".SMPR_TPRIVACY_MODE rule ON rule.SEQ_THING_ID = thing.SEQ_ID" +
                      " WHERE instal.SEQ_ID IN (SELECT DISTINCT instal.SEQ_ID FROM \"RULE-ENGINE\".SMPR_TUSER usr" +
                      "     INNER JOIN \"RULE-ENGINE\".SMPR_TINSTALLATION instal ON usr.SEQ_ID = instal.SEQ_ADMIN" +
                      "     WHERE instal.DES_CONTRACT_TYPE IN ('V', 'VIVIENDA','N','NEGOCIO')" +
                      "     AND instal.SEQ_ADMIN IN (SELECT SEQ_ADMIN FROM \"RULE-ENGINE\".SMPR_TINSTALLATION" +
                      "     WHERE DES_CONTRACT_TYPE IN   ('N', 'NEGOCIO')))" +
                      " AND thing.SEQ_ENTITY_TYPE IN (2, 24) AND rule.BOL_PRIVACY_MODE_STATUS = 1";

                // Query that finaly will be executed
                stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                rs = stmt.executeQuery(sql1);

                String fileName = "fixDevicesMPStatus.sql";
                appendToFile(fileName, "schemas:RULE-ENGINE\n");
                String thingIdsToUpdate = "";

                while (rs.next()) {
                    arr_ThingId.add(rs.getString(1));

                    if (!isInList(rs.getString(2), arr_userId)){
                        arr_userId.add(rs.getString(2));
                    }

                    thingIdsToUpdate = thingIdsToUpdate + rs.getString(1) + ",";
                    String updateSql = "UPDATE SMPR_TPRIVACY_MODE stm SET BOL_PRIVACY_MODE_STATUS = 0 WHERE SEQ_THING_ID = " + rs.getString(1) + ";\n" ;
                    appendToFile(fileName, updateSql);

                }

                if (!thingIdsToUpdate.isEmpty()) {
                    thingIdsToUpdate = thingIdsToUpdate.substring(0, thingIdsToUpdate.length() - 1);
                    System.out.println("ThingIds to fix: " + thingIdsToUpdate);

                    appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                          ", se fixearon " + arr_ThingId.size() +
                          " casos. <br><br>\n");

                    appendToFile(datafixReportFile,  "Se fixearon los siguientes thingId: " + thingIdsToUpdate +
                          " <br>\n");

                } else {
                    System.out.println("Things to fix don't exist");
                    appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+ ", no se encontraron casos a fixear <br>\n");
                }

                System.out.println("Process end.");

            } catch (Exception e) {
                System.out.println(" ");
                System.out.println(e.getMessage());
                printStackTrace(e);
            } finally {
                try {
                    if (rs != null) rs.close();
                    if (stmt != null) stmt.close();
                    if (con != null) con.close();
                } catch (Exception e) {
                    System.out.println(" ");
                    System.out.println(e.getMessage());
                    printStackTrace(e);
                }
            }

        } catch (Exception e1) {
            System.out.println(" ");
            System.out.println(e1.getMessage());
            e1.printStackTrace();
        }
    }

    public void writeUsersToTurnOnCamera(final DataSource dataSource, String userToTurnOnCamera) {

        String result = "";

        try {
            Connection con = dataSource.getConnection();
            Statement stmt = null;
            ResultSet rs = null;

            String sql = "SELECT DES_NICK_NAME " +
                  "FROM  \"ACCOUNT-MANAGEMENT\".SMPR_TUSER " +
                  "WHERE SEQ_ID = " ;

            //System.out.println(sql);

            try {

                // Query that finaly will be executed
                stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

                for (String userId: arr_userId){
                    rs = stmt.executeQuery(sql + userId);

                    if (rs.next()){
                        appendToFile(userToTurnOnCamera, rs.getString(1) + "\n");
                    }
                }

            } catch (Exception e) {
                System.out.println(" ");
                System.out.println(e.getMessage());
                printStackTrace(e);
            } finally {
                try {
                    if (rs != null) rs.close();
                    if (stmt != null) stmt.close();
                    if (con != null) con.close();
                } catch (Exception e) {
                    System.out.println(" ");
                    System.out.println(e.getMessage());
                    printStackTrace(e);
                }
            }

        } catch (Exception e1) {
            System.out.println(" ");
            System.out.println(e1.getMessage());
            e1.printStackTrace();
        }
    }
}