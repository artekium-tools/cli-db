package clidb;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class CheckAliasesReplicaVsOwner extends QryDB {

    static int totalThingAliasCount=0;

    public int fixWrongThingAliases(String env, String datafixReportFile, String reportDate, int waitTimeInSeconds) {

        loadExternalFileProperties();

        DataSource dataSource = null;

        try {

            if (env == null) env = getProperty("env");
            String connectionType = getProperty("connectionType");

            String jdbcUrl = getProperty(connectionType + "." + env + ".url");
            String dbUsername = getProperty(env + ".username");
            String password = getProperty(env + ".password");

            dataSource = getDataSource(jdbcUrl, dbUsername, password);

            System.out.println("at: " + getFormattedDate(new Date()) + " Executing step 1: check thingAlias that do not exist in the owner..." );

            appendToFile(datafixReportFile,
                  "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-40193'>" +
                        "16) MC " +
                        "https://jira.prosegur.com/browse/AM-40193</a> - " +
                        "Inconsistencias en datos - no existen en el owner<br>\n");

            checkThingAliasNotInOwner(dataSource, datafixReportFile, reportDate, waitTimeInSeconds);

            System.out.println("Total thingAlias found: " + totalThingAliasCount);

        } catch (Exception e){
            printStackTrace(e);
        }

        return totalThingAliasCount;
    }

    public synchronized void checkThingAliasNotInOwner(final DataSource dataSource, String datafixReportFile, String reportDate, int waitTimeInSeconds) {

        try {
            Connection con = dataSource.getConnection();
            Statement stmt = null;
            ResultSet rs = null;

            try {

                // Get schemas list for thingAlias replicas
                String thingAliasSchemas = getProperty("replicas.thingAlias");
                String[] arrThingAliasSchemas = thingAliasSchemas.split(",");

                List<String> replicasLogicalDelete = Arrays.asList("ACCOUNT-MANAGEMENT", "EVENT-PROCESSOR");

                appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate + "<br>\n");

                int schemaCount = 0;
                for (String schemaThingAlias : arrThingAliasSchemas) {
                    if (schemaThingAlias.equals("DEVICE")) {
                        continue;
                    }

                    String logicalDeleteClause = replicasLogicalDelete.contains(schemaThingAlias)? " AND sta.BOL_IS_ACTIVE = 1" : "";

                    /*String sqlSelectAliases = "SELECT SEQ_ID FROM \"" + schemaThingAlias + "\".SMPR_TTHING_ALIAS sta"
                          + " WHERE NOT EXISTS (SELECT std.SEQ_ID FROM DEVICE.SMPR_TTHING_ALIAS std"
                          + " WHERE sta.SEQ_ID = std.SEQ_ID)" + logicalDeleteClause;*/

                    String sqlSelectAliases = "SELECT SEQ_ID FROM \"" + schemaThingAlias + "\".SMPR_TTHING_ALIAS sta " +
                           " WHERE sta.SEQ_ID NOT IN" +
                           " (SELECT std.SEQ_ID FROM DEVICE.SMPR_TTHING_ALIAS std) " + logicalDeleteClause;

                    System.out.println("Executing at " + getFormattedDate(new Date()) + ": " + sqlSelectAliases);

                    // Execute query
                    stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                    rs = stmt.executeQuery(sqlSelectAliases);

                    String fileName = "fixWrongThingAliases.sql";
                    String idsToDelete = "";
                    int cont = 0;

                    while (rs.next()) {

                        idsToDelete = idsToDelete + rs.getString(1) + ",";
                        totalThingAliasCount++;
                        cont++;
                        schemaCount++;

                        if (cont == 500) {
                            System.out.println("at: " + getFormattedDate(new Date()) + " adding 500 ids to delete " +
                                  "statement for schema: " + schemaThingAlias);

                            idsToDelete = idsToDelete.substring(0, idsToDelete.length() - 1);
                            appendToFile(fileName, "schemas:" + schemaThingAlias + "\n");
                            appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta " +
                                  "WHERE SEQ_ID IN (" + idsToDelete + ");\n");
                            idsToDelete = "";
                            cont = 0;
                        }
                    }

                    if (!idsToDelete.isEmpty()) {
                        System.out.println("at: " + getFormattedDate(new Date()) + " adding " + cont + " ids to delete " +
                              "statement for schema: " + schemaThingAlias);

                        idsToDelete = idsToDelete.substring(0, idsToDelete.length() - 1);
                        appendToFile(fileName, "schemas:" + schemaThingAlias + "\n");
                        appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta " +
                              "WHERE SEQ_ID IN (" + idsToDelete + ");\n");
                    }

                    if (schemaCount > 0) {
                        appendToFile(datafixReportFile,  "SCHEMA: " + schemaThingAlias +
                              ", se fixearon " + schemaCount +
                              " casos. <br><br>\n");

                        schemaCount = 0;
                    }

                    System.out.println("wait for " + waitTimeInSeconds + " seconds ");
                    wait(waitTimeInSeconds * 1000);
                }

                System.out.println("Process end.");

            } catch (Exception e) {
                System.out.println(" ");
                System.out.println(e.getMessage());
                printStackTrace(e);
            } finally {
                try {
                    if (rs != null) rs.close();
                    if (stmt != null) stmt.close();
                    if (con != null) con.close();
                } catch (Exception e) {
                    System.out.println(" ");
                    System.out.println(e.getMessage());
                    printStackTrace(e);
                }
            }

        } catch (Exception e1) {
            System.out.println(" ");
            System.out.println(e1.getMessage());
            e1.printStackTrace();
        }
    }
}