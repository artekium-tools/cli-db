package clidb;

import clidb.entities.InstallationAlias;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;


public class SynchronizeReplicasInstallationAlias extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(SynchronizeReplicasInstallationAlias.class);


   public void checkInstalationsAlias(String env, String schema) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         String insertUpdateFileName = "insertUpdateFileNameInstallationAlias.sql";

         System.out.println("Start record (default=1): ");
         Scanner scanner2 = new Scanner(new InputStreamReader(System.in));
         String startRecStr = scanner2.nextLine();
         System.out.println(" ");

         int from = 1;

         try {
            from = Integer.valueOf(startRecStr);
         } catch (Exception e) {
            from = 1;
         }

         int chunk = 999;
         int to = from + chunk;
         int total = countInstallationsAlias(dataSource);


         while (true) {

            System.out.println("\nLoading Owner InstallationAlias from: " + from + " to: " + to + " of " + total);

            ArrayList<InstallationAlias> arrInstallationAlias = selectInstallationsAlias(dataSource, schema, from, to);

            System.out.println("\n InstallationAlias loaded: " + arrInstallationAlias.size());

            for (InstallationAlias installationAlias: arrInstallationAlias) {

               InstallationAlias replica = getInstallationsAliasReplica(dataSource, schema, installationAlias.getId());

               if (replica == null) {
                  printInLine("INSERT for seqId: " + installationAlias.getId());
                  String sqlInsert = installationAlias.getInsertStatement(schema);
                  appendToFile(insertUpdateFileName, sqlInsert + ";\n");
               } else {
                  if (!installationAlias.equals(schema, replica)) {
                     printInLine("UPDATE for seqId: " + installationAlias.getId());
                     String sqlUpdate = installationAlias.getUpdateStatement(schema);

                     appendToFile(insertUpdateFileName, sqlUpdate + ";\n");

                  } else {
                     printInLine("EQUALS for seqId: " + installationAlias.getId());
                  }
               }

            }

            from = to + 1;
            to = from + chunk;
         }


      } catch (Exception e){
         printStackTrace(e);
      }


   }

   public static int countInstallationsAlias(final DataSource dataSource) {

      int count = 0;
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql = "SELECT COUNT(1) " +
                  "FROM  \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS instAlias " ;

            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
               count = rs.getInt(1);
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return count;

   }

   public static ArrayList<InstallationAlias> selectInstallationsAlias(final DataSource dataSource,String schema, int from, int to) {

      ArrayList<InstallationAlias> arrInstallationAlias = new ArrayList<>();

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql = "SELECT c.* " +
            "FROM (SELECT c.*, ROWNUM as rnum " +
            "      FROM ( " +
            "            SELECT SEQ_ID, SEQ_INSTALLATION_ID, SEQ_USER_ID, FYH_UPDATE_DATE, DES_ALIAS, DES_PANEL_USER " +
            "            FROM  \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS instAlias " +
            "            ORDER BY instAlias.SEQ_ID " +
            "      ) c) c " +
            "WHERE c.rnum BETWEEN " + from + " AND " + to;

            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
               InstallationAlias installationAlias = new InstallationAlias(schema, rs);
               arrInstallationAlias.add(installationAlias);
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return arrInstallationAlias;

   }


   public static InstallationAlias getInstallationsAliasReplica(final DataSource dataSource, String schema, Long seqId) {
      InstallationAlias replica = null;
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql =
                  "SELECT SEQ_ID, SEQ_INSTALLATION_ID, SEQ_USER_ID, FYH_UPDATE_DATE, DES_ALIAS, DES_PANEL_USER " +
                  "FROM  \""+ schema +"\".SMPR_TINSTALLATION_ALIAS instAlias " +
                  "WHERE instAlias.SEQ_ID = " + seqId;

            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
               replica = new InstallationAlias(schema, rs);
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return replica;

   }

}
