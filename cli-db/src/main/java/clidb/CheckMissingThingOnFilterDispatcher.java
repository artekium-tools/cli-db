package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


public class CheckMissingThingOnFilterDispatcher extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(CheckMissingThingOnFilterDispatcher.class);

   static int countCases = 0;

   public synchronized int main(String env, String datafixReportFile, String reportDate, String updateStatementFileName, int waitTimeInSeconds) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         appendToFile(datafixReportFile,
               "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-44490'>" +
                     "19) MB " +
                     "https://jira.prosegur.com/browse/AM-44490</a> - " +
                     "Replicas faltantes de SMPR_TTHING y SMPR_TINSTALLATION_ALIAS en FILTER-DISPATCHER<br>\n");

         System.out.println("Executing step 1: looking for cases ..." );

         String listCases = lookingForCases(dataSource, updateStatementFileName);

         if (!listCases.contains("Empty Result")){

            System.out.println("Waiting for " + waitTimeInSeconds + " seconds." );
            wait(waitTimeInSeconds * 1000);

            System.out.println("Executing step 2: generate insert InstallationAlias statements ..." );
            generateInsertInstallationAliasStatements(dataSource, updateStatementFileName);

            System.out.println("Waiting for " + waitTimeInSeconds + " seconds." );
            wait(waitTimeInSeconds * 1000);

            System.out.println("Executing step 3: generate insert Thing statements ..." );
            generateInsertThingStatements(dataSource, updateStatementFileName);

         }

         if (countCases > 0) {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", se fixearon " + countCases + " casos. <br>\n");

            appendToFile(datafixReportFile,    listCases  + "<br>\n");
         } else {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", no se encontraron casos a fixear<br>\n");
         }

      } catch (Exception e){
         printStackTrace(e);
      }

      return countCases;
   }



   public String lookingForCases(final DataSource dataSource, String repotFile) {

      String start = "<table border='1'>\n";
      String end = "</table>";
      String body = "";

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT SEQ_ID,DES_CLIENT_ID,DES_COUNTRY,DES_NICK_NAME " +
               "FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER WHERE SEQ_ID IN ( " +
               "SELECT SEQ_ADMIN FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION WHERE SEQ_ID IN ( " +
               "SELECT SEQ_INSTALLATION_ID FROM \"DEVICE\".SMPR_TTHING WHERE SEQ_ENTITY_TYPE IN (2,16,24,25) AND seq_id NOT IN " +
               "(SELECT seq_id FROM \"FILTER-DISPATCHER\".SMPR_TTHING WHERE SEQ_ENTITY_TYPE IN (2,16,24,25)) " +
               ")) " +
               "UNION " +
               "SELECT SEQ_ID,DES_CLIENT_ID,DES_COUNTRY,DES_NICK_NAME " +
               "FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER WHERE SEQ_ID IN ( " +
               "SELECT SEQ_ADMIN FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION WHERE SEQ_ID IN " +
               "  (SELECT SEQ_INSTALLATION_ID FROM \"DEVICE\".SMPR_TTHING WHERE SEQ_ID IN ( " +
               "SELECT SEQ_parent_ID FROM \"DEVICE\".SMPR_TTHING WHERE SEQ_ENTITY_TYPE IN (2,16,24,25) AND seq_id NOT IN " +
               "(SELECT seq_id FROM \"FILTER-DISPATCHER\".SMPR_TTHING WHERE SEQ_ENTITY_TYPE IN (2,16,24,25)) " +
               "))) " +
               "UNION " +
               "SELECT SEQ_ID,DES_CLIENT_ID,DES_COUNTRY,DES_NICK_NAME FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER WHERE SEQ_ID IN ( " +
               "SELECT SEQ_ADMIN FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION WHERE SEQ_ID IN ( " +
               "SELECT SEQ_INSTALLATION_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS WHERE SEQ_ID NOT IN " +
               "(SELECT seq_id FROM \"FILTER-DISPATCHER\".SMPR_TINSTALLATION_ALIAS)))";


         //System.out.println(sql);

         try {


            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            body = selectToHTML(dataSource, sql, "grey");


            System.out.println("\nEnd looking for cases." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return start + body + end;
   }

   public void generateInsertInstallationAliasStatements(final DataSource dataSource, String updateStatementFileName) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT " +
               "'INSERT INTO SMPR_TINSTALLATION_ALIAS " +
               "(SEQ_ID, SEQ_INSTALLATION_ID, SEQ_USER_ID, FYH_UPDATE_DATE, DES_ALIAS, DES_PANEL_USER) ' || " +
               "    'VALUES(' || " +
               "    SEQ_ID || ', ' || " +
               "    SEQ_INSTALLATION_ID || ', ' || " +
               "    SEQ_USER_ID || ', TO_DATE(''' || " +
               "    TO_CHAR(FYH_UPDATE_DATE, 'DD/MM/RRRR') || ''', ''DD/MM/RRRR''), ''' || " +
               "    COALESCE(DES_ALIAS, '') || ''', ''' || " +
               "    COALESCE(DES_PANEL_USER, '') || ''')' " +
               "FROM " +
               "\"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS " +
               "WHERE " +
               "SEQ_ID NOT IN ( " +
               " SELECT SEQ_ID FROM \"FILTER-DISPATCHER\".SMPR_TINSTALLATION_ALIAS " +
               ")";


         //System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            while (rs.next()) {
               appendToFile(updateStatementFileName,    rs.getString(1) + ";\n");
               countCases++;
            }

            System.out.println("\nEnd insert Installation Alias statement generation .." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }

   public void generateInsertThingStatements(final DataSource dataSource, String updateStatementFileName) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT " +
               "'INSERT INTO SMPR_TTHING ' || " +
               "'(SEQ_ID, NUM_CHANNEL, DES_CODE, DES_CONTRACT_NUMBER, SEQ_DOM_CATALOG_DEV_ID, DES_INDEX, DES_NAME, SEQ_ROOM_ID, ' || " +
               "'STATE_1, STATE_2, STATE_3, STATE_4, STATE_5, STATE_6, STATE_7, STATE_8, STATE_9, STATE_10, STATE_11, STATE_12, ' || " +
               "'STATE_13, STATE_14, STATE_15, STATE_16, STATE_17, STATE_18, STATE_19, STATE_20, FYH_UPDATE_DATE, SEQ_ENTITY_TYPE, ' || " +
               "'SEQ_INSTALLATION_ID, SEQ_PARENT_ID, SEQ_TYPE_ID, SEQ_DEVICE_TYPE, COD_CONNECTION_ID, COD_COUNTRY, STATE_21, STATE_22, ' || " +
               "'BOL_PRIVACY_MODE, STATE_23, DES_MODEL, DES_STATE, FYH_START_TIMER, STATE_27, STATE_24, STATE_25, STATE_26) VALUES (' || " +
               "NVL(SEQ_ID, 0) || ', ' || " +
               "NVL(NUM_CHANNEL, 0) || ', ''' || " +
               "NVL(DES_CODE, '') || ''', ''' || " +
               "NVL(DES_CONTRACT_NUMBER, '') || ''', ' || " +
               "NVL(SEQ_DOM_CATALOG_DEV_ID, 0) || ', ''' || " +
               "NVL(DES_INDEX, '') || ''', ''' || " +
               "NVL(DES_NAME, '') || ''', ' || " +
               "NVL(SEQ_ROOM_ID, 0) || ', ''' || " +
               "NVL(STATE_1, '') || ''', ''' || " +
               "NVL(STATE_2, '') || ''', ''' || " +
               "NVL(STATE_3, '') || ''', ''' || " +
               "NVL(STATE_4, '') || ''', ''' || " +
               "NVL(STATE_5, '') || ''', ''' || " +
               "NVL(STATE_6, '') || ''', ''' || " +
               "NVL(STATE_7, '') || ''', ''' || " +
               "NVL(STATE_8, '') || ''', ''' || " +
               "NVL(STATE_9, '') || ''', ''' || " +
               "NVL(STATE_10, '') || ''', ''' || " +
               "NVL(STATE_11, '') || ''', ''' || " +
               "NVL(STATE_12, '') || ''', ''' || " +
               "NVL(STATE_13, '') || ''', ''' || " +
               "NVL(STATE_14, '') || ''', ''' || " +
               "NVL(STATE_15, '') || ''', ''' || " +
               "NVL(STATE_16, '') || ''', ''' || " +
               "NVL(STATE_17, '') || ''', ''' || " +
               "NVL(STATE_18, '') || ''', ''' || " +
               "NVL(STATE_19, '') || ''', ''' || " +
               "NVL(STATE_20, '') || ''', SYSDATE, ' || " +
               "NVL(SEQ_ENTITY_TYPE, 0) || ', ' || " +
               "NVL(SEQ_INSTALLATION_ID, 0) || ', ' || " +
               "NVL(SEQ_PARENT_ID, 0) || ', ' || " +
               "NVL(SEQ_TYPE_ID, 0) || ', ' || " +
               "NVL(SEQ_DEVICE_TYPE, 0) || ', ''' || " +
               "NVL(COD_CONNECTION_ID, '') || ''', ''' || " +
               "NVL(COD_COUNTRY, '') || ''', ''' || " +
               "NVL(STATE_21, '') || ''', ''' || " +
               "NVL(STATE_22, '') || ''', ' || " +
               "NVL(BOL_PRIVACY_MODE, 0) || ', ''' || " +
               "NVL(STATE_23, '') || ''', ''' || " +
               "NVL(DES_MODEL, '') || ''', NULL, NULL, ''' || " +
               "NVL(STATE_27, '') || ''', ''' || " +
               "NVL(STATE_24, '') || ''', ''' || " +
               "NVL(STATE_25, '') || ''', ''' || " +
               "NVL(STATE_26, '') || ''')' " +
               "FROM " +
               "\"DEVICE\".SMPR_TTHING " +
               "WHERE " +
               "SEQ_ENTITY_TYPE IN (2,16,24,25) " +
               "AND SEQ_ID NOT IN (SELECT SEQ_ID FROM \"FILTER-DISPATCHER\".SMPR_TTHING " +
               "WHERE SEQ_ENTITY_TYPE IN (2,16,24,25))";


         //System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            while (rs.next()) {
               appendToFile(updateStatementFileName,    rs.getString(1) + ";\n");
               countCases++;
            }

            System.out.println("\nEnd insert Thing statement generation .." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }
}
