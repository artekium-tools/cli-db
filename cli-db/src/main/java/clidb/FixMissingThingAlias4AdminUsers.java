package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class FixMissingThingAlias4AdminUsers extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(FixMissingThingAlias4AdminUsers.class);

   static int countCases = 0;

   public synchronized int main(String env, String datafixReportFile, String reportDate, String updateStatementFileName, int waitTimeInSeconds) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         appendToFile(datafixReportFile,
               "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-38396'>" +
                     "21) MB " +
                     "https://jira.prosegur.com/browse/AM-38396</a> - " +
                     "Permisos faltantes a Usuarios Admin<br>\n");

         System.out.println("Executing step 1: looking for cases ..." );

         List<String> outputLines = lookingForCases(dataSource);

         String listCases = "";

         if (outputLines.size() > 0) {
            System.out.println("waiting for 30 seconds ..." );
            wait( 30000);
            System.out.println("Executing step 2: generate insert statements ..." );
            String listUserId = generateInsertStatements(connectionType, env, updateStatementFileName, outputLines);

            if (countCases > 0){
               System.out.println("Executing step 3: generate user list ..." );
               listCases = listCases(dataSource, listUserId);
            }

         }

         if (countCases > 0) {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", se fixearon " + countCases + " casos. <br>\n");

            appendToFile(datafixReportFile,    listCases  + "<br>\n");
         } else {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", no se encontraron casos a fixear<br>\n");
         }

      } catch (Exception e){
         printStackTrace(e);
      }

      return countCases;
   }

   public ArrayList<String> lookingForCases(final DataSource dataSource) {

      ArrayList<String> outputLines = new ArrayList<>();

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "DECLARE " +
               "  CURSOR c_thing_alias_users " +
               "  IS SELECT DISTINCT device.SEQ_ID AS DEVICE_ID, instal.SEQ_ADMIN, serviceAlias.SEQ_ID AS SERVICE_ALIAS_ID " +
               "    FROM \"DEVICE\".SMPR_TTHING device     " +
               "    INNER JOIN \"DEVICE\".SMPR_TTHING service " +
               "        ON service.SEQ_ID = device.SEQ_PARENT_ID " +
               "    INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION instal " +
               "        ON service.SEQ_INSTALLATION_ID = instal.SEQ_ID    " +
               "    INNER JOIN \"DEVICE\".SMPR_TTHING_ALIAS serviceAlias " +
               "        ON device.SEQ_PARENT_ID = serviceAlias.SEQ_THING_ID " +
               "    INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usuario " +
               "        ON instal.SEQ_ADMIN = usuario.SEQ_ID " +
               "    WHERE serviceAlias.SEQ_USER_ID = instal.SEQ_ADMIN  " +
               "    AND device.SEQ_PARENT_ID IS NOT NULL " +
               "    AND instal.FYH_DELETED IS NULL " +
               "    GROUP BY instal.SEQ_ADMIN, device.SEQ_ID, serviceAlias.SEQ_ID; " +
               "    " +
               "    vrow c_thing_alias_users%ROWTYPE; " +
               "    existe_alias NUMBER(1) := 0; BEGIN  " +
               "    FOR vrow IN c_thing_alias_users LOOP " +
               "        SELECT COUNT(1) INTO existe_alias FROM \"DEVICE\".SMPR_TTHING_ALIAS " +
               "        WHERE SEQ_THING_ID = vrow.DEVICE_ID AND SEQ_USER_ID = vrow.SEQ_ADMIN; " +
               "        IF existe_alias = 0 THEN " +
               "        DBMS_OUTPUT.PUT_LINE('INSERT INTO SMPR_TTHING_ALIAS(SEQ_ID, SEQ_INSTALLATION_ALIAS_ID, SEQ_THING_ID, SEQ_PARENT_ID, DES_NAME, SEQ_USER_ID) " +
               " VALUES (:thingAliasId, NULL,' || vrow.DEVICE_ID || ',' || vrow.SERVICE_ALIAS_ID || ',NULL,' || vrow.SEQ_ADMIN || ')'); " +
               "        END IF; " +
               "    END LOOP; " +
               "    DBMS_OUTPUT.PUT_LINE('Fin proceso.'); " +
               "END;";

         //System.out.println(sql);

         try {


            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            // Habilitar DBMS_OUTPUT
            stmt.execute("BEGIN DBMS_OUTPUT.ENABLE(NULL); END;");

            stmt.execute(sql);

            // Leer el buffer de DBMS_OUTPUT

            try (CallableStatement call = con.prepareCall("{call DBMS_OUTPUT.GET_LINES(?, ?)}")) {
               call.registerOutParameter(1, Types.ARRAY, "DBMSOUTPUT_LINESARRAY");
               call.registerOutParameter(2, Types.INTEGER);
               call.execute();

               Array array = call.getArray(1);
               if (array != null) {
                  String[] lines = (String[]) array.getArray();
                  for (String line : lines) {

                    // System.out.println(line);

                     if (line != null && !line.contains("Fin proceso.")) {
                        outputLines.add(line);
                     }
                  }
               }
            }

            System.out.println("\nEnd looking for cases." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return outputLines;
   }

   private String generateInsertStatements(String connectionType, String env, String updateStatementFileName, List<String> outputLines) {

      String userId = "";
      String thingId = "";
      String parentThingId = "";
      String seqThingAliasId = "";
      String listUserId = "";

      String thingAliasSchemas = "schemas:DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,SYSLOG";
      String thingAliasSchemasWithoutName = "schemas:PANIC-BUTTON,RULE-ENGINE";

      String jdbcUrl = getProperty(connectionType + "." + env + ".url");
      String dbUsername = "DEVICE";
      String password = getProperty(env + ".DEVICE.password");

      DataSource dataSource = null;

      try {

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sqlNextVal = "SELECT SEQ_THING_ALIAS_ID.nextval FROM DUAL";
            String sqlSelectThing = "SELECT seq_id FROM DEVICE.smpr_tThing WHERE seq_id = :seqThingId";

            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            for (String insertStatement: outputLines){
               //System.out.println(insertStatement);

               thingId = getThingId(insertStatement);

               //System.out.println(sqlSelectThing.replace(":seqThingId", thingId));

               rs = stmt.executeQuery(sqlSelectThing.replace(":seqThingId", thingId));

               // Check if thing exits
               if (rs.next()){

                  rs = stmt.executeQuery(sqlNextVal);

                  // If thing exists, create insertStatement
                  if (rs.next()){

                     seqThingAliasId = rs.getString(1);

                     // DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,SYSLOG
                     appendToFile(updateStatementFileName, thingAliasSchemas + "\n");
                     appendToFile(updateStatementFileName,
                           insertStatement.replace(":thingAliasId", seqThingAliasId) + ";\n");

                     parentThingId = getParentThingId(insertStatement);
                     userId = getUserId(insertStatement);

                     // PANIC-BUTTON,RULE-ENGINE (Has no DES_NAME field)
                     appendToFile(updateStatementFileName, thingAliasSchemasWithoutName + "\n");
                     appendToFile(updateStatementFileName,
                           getInsertWithoutNameStatement(seqThingAliasId, thingId, parentThingId, userId) + ";\n");

                     if("".equals(listUserId)){
                        listUserId = userId;
                     } else {
                        listUserId = listUserId + ", " + userId;
                     }

                     countCases ++;
                  }
               } else {
                  System.out.println("Falso positivo para :seqThingId="  + thingId);
               }

            }


         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return listUserId;
   }

   public String listCases(final DataSource dataSource, String listUserId) {



      String start = "<table border='1'>\n";
      String end = "</table>";
      String body = "";

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT SEQ_ID, DES_CLIENT_ID,DES_COUNTRY,DES_EMAIL FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER " +
               "WHERE SEQ_ID IN (" + listUserId + ")";

         //System.out.println(sql);

         try {


            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            body = selectToHTML(dataSource, sql, "grey");

            System.out.println("\nEnd list cases." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return start + body + end;
   }


   private String getThingId(String sql){
      String[] arrLine = sql.split(",");
      return arrLine[7];
   }

   private String getParentThingId(String sql){
      String[] arrLine = sql.split(",");
      return arrLine[8];
   }

   private String getUserId(String sql){
      return sql.substring(sql.lastIndexOf(",") + 1, sql.lastIndexOf(")"));
   }

   private String getInsertWithoutNameStatement(String thingAliasId, String thingId, String parentThingId, String userId){
      String sqlInsertWithoutName = "INSERT INTO SMPR_TTHING_ALIAS(SEQ_ID, SEQ_INSTALLATION_ALIAS_ID, SEQ_THING_ID, SEQ_PARENT_ID, SEQ_USER_ID) VALUES (";

      return sqlInsertWithoutName + thingAliasId + ", NULL, " + thingId + ", " + parentThingId + ", " +  userId  + ")";
   }
 }
