package clidb;

import com.google.common.base.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


public class LargeScript extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(LargeScript.class);

   /**
    * To run lagerScript ready parameters from scriptConfig.properties file
    */
   public void runScript() {

      loadScriptProperties("./scriptConfig.properties");

      String env = scriptProperties.getProperty("env");
      String schema = scriptProperties.getProperty("schema");
      String scriptFile = scriptProperties.getProperty("scriptFile");
      int startLine;
      boolean showSql;
      boolean onErrorExit;
      boolean inBatchMode;
      int recordsByBatch;
      int waitTimeInSeconds;

      try {
         startLine = Integer.valueOf(scriptProperties.getProperty("startLine"));
      } catch (Exception e) {
         startLine = 0;
      }

      if ("Y".equals(scriptProperties.getProperty("showSql").toUpperCase())){
         showSql = true;
      } else {
         showSql = false;
      }

      if ("E".equals(scriptProperties.getProperty("onError").toUpperCase())){
         onErrorExit = true;
      } else {
         onErrorExit = false;
      }

      try {
         inBatchMode = Boolean.valueOf(scriptProperties.getProperty("inBatchMode"));
      } catch (Exception e) {
         inBatchMode = false;
      }

      try {
         recordsByBatch = Integer.valueOf(scriptProperties.getProperty("recordsByBatch"));
      } catch (Exception e) {
         recordsByBatch = 0;
      }

      try {
         waitTimeInSeconds = Integer.valueOf(scriptProperties.getProperty("waitTimeInSeconds"));
      } catch (Exception e) {
         waitTimeInSeconds = 0;
      }

      System.out.println("schema: " + schema);
      System.out.println("scriptFile: " + scriptFile);
      System.out.println("startLine: " + startLine);
      System.out.println("showSql: " + showSql);
      System.out.println("onErrorExit: " + onErrorExit);
      System.out.println("inBatchMode: " + inBatchMode);
      System.out.println("recordsByBatch: " + recordsByBatch);
      System.out.println("waitTimeInSeconds: " + waitTimeInSeconds);
      System.out.println("");
      System.out.println("--------------------------------- ");
      System.out.println(" ");
      System.out.println("Start execution ... ");
      runScript( env,  schema,  scriptFile,  startLine,  showSql,  onErrorExit, inBatchMode,
            recordsByBatch, waitTimeInSeconds, true, null);
   }

   /**
    * Function to run largeScript configuring parameters
    *
    * @param env
    * @param schema
    */
   public void runScript(String env, String schema) {


      int startLine;
      boolean showSql;
      boolean onErrorExit;
      boolean inBatchMode;
      int recordsByBatch = 0;
      int waitTimeInSeconds = 0;

      System.out.println(" ");
      System.out.println("- LargeScript - mode: " + schema);

      String scriptFile = chooseSqlFile();

      System.out.println("Start line (default=1): ");
      Scanner scanner2 = new Scanner(new InputStreamReader(System.in));
      String startLineStr = scanner2.nextLine();
      System.out.println(" ");

      System.out.println("Show sql in console? (default=n) (y/n): ");
      Scanner scanner3 = new Scanner(new InputStreamReader(System.in));
      String showSqlStr = scanner3.nextLine();
      System.out.println(" ");

      System.out.println("On sql error log or exit? (default=l) (l/e): ");
      Scanner scanner4 = new Scanner(new InputStreamReader(System.in));
      String onErrorStr = scanner4.nextLine();
      System.out.println(" ");


      System.out.println("Execute in batch mode (y/n): ");
      String inBatchModeStr = scanner4.nextLine();
      System.out.println(" ");


      if (inBatchModeStr != null &&
            "Y".equals(inBatchModeStr.toUpperCase())){
         inBatchMode = true;

         System.out.println("Records by batch (default=10000): ");
         String recByBatchStr = scanner4.nextLine();
         System.out.println(" ");

         try {
            recordsByBatch = Integer.valueOf(recByBatchStr);
         } catch (Exception e) {
            recordsByBatch = 10000;
         }

         System.out.println("Wait time in seconds: (default=60): ");
         String waitTimeStr = scanner4.nextLine();
         System.out.println(" ");

         try {
            waitTimeInSeconds = Integer.valueOf(waitTimeStr);
         } catch (Exception e) {
            waitTimeInSeconds = 60;
         }

      } else {
         inBatchMode = false;
      }

      try {
         startLine = Integer.valueOf(startLineStr);
      } catch (Exception e) {
         startLine = 1;
      }

      if (showSqlStr != null &&
            "Y".equals(showSqlStr.toUpperCase())){
         showSql = true;
      } else {
         showSql = false;
      }

      if (onErrorStr != null &&
            "E".equals(onErrorStr.toUpperCase())){
         onErrorExit = true;
      } else {
         onErrorExit = false;
      }

      System.out.println("Confirm execution (y/n)?: ");
      String confimStr = scanner4.nextLine();
      System.out.println(" ");


      if (confimStr != null &&
            "Y".equals(confimStr.toUpperCase())){

         SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SS");
         Date resultdate = new Date(Math.abs(System.currentTimeMillis()));
         System.out.println("Start time: " + sdf.format(resultdate));

         runScript( env,  schema,  scriptFile,  startLine,  showSql,  onErrorExit, inBatchMode, recordsByBatch,
               waitTimeInSeconds, true, null);
      }

   }

   /**
    * Function to be called from another functions like datafixes
    *
    * @param env
    * @param schema
    * @param scriptFile
    * @param exit
    */
   public void runScript(String env, String schema, String scriptFile, boolean exit, String datafixReportFileName) {
      runScript( env,  schema,  scriptFile,  0,  false,  false, false,
            0, 0, exit, datafixReportFileName);
   }

   /**
    * Function to run in Multi Update Mode.
    * scriptFile must have "schemas:" at first line
    *
    * waitTimeInSeconds indicate time to wait between each sql statement execution
    *
    * @param env
    * @param scriptFile
    * @param exit
    * @param waitTimeInSeconds
    */
   public void runMultiUpdate(String env, String scriptFile, boolean exit, int waitTimeInSeconds, String datafixReportFileName){
      runScript( env,  null,  scriptFile,  0,  false,  false, false,
            0, waitTimeInSeconds, exit, datafixReportFileName);
   }

   /**
    * To call runMultiUpdate task from main menu
    *
    * @param env
    */
   public void runMultiUpdate(String env) {

      System.out.println(" ");
      System.out.println("- MultiUpdate -");
      System.out.println("UPDATE_IN_TEST_MODE = " + UPDATE_IN_TEST_MODE);

      String scriptFile = chooseSqlFile();

      runMultiUpdate(env, scriptFile, true, 0, null);

   }

   public synchronized void runScript(String env, String schema, String scriptFile, int startLine, boolean showSql, boolean onErrorExit,
                         boolean inBatchMode, int recordsByBatch, int waitTimeInSeconds, boolean exit, String datafixReportFileName) {

      loadExternalFileProperties();

      DataSource dataSource = null;
      String password = "";

      try {

         boolean multiUpdateMode = schema == null ? true:false;

         String strDate = new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date());
         String batchReportFileName = "batchReport-" + strDate + ".txt";

         ChronoMeter chronoMeter = new ChronoMeter();
         chronoMeter.start();

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);
         System.out.println("connectionType: " + connectionType);

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");

         if (!multiUpdateMode) {
            password = getProperty(env + "." + schema + ".password");
            dataSource = getDataSource(jdbcUrl, schema, password);
         }

         BufferedReader br = new BufferedReader(new FileReader(scriptFile));

         String sCurrentLine;
         int line = 1;
         int totalLines = 0;
         int totalSqlStmnt =0;
         int totalOK = 0;
         int totalError = 0;
         boolean isFirstBatch = true;

         String sql = "";
         String[] arrSchemas = null;

         String success;

         System.out.println("Counting lines ... " );
         while ((br.readLine()) != null) {
            totalLines++;
         }

         System.out.println("Total lines: " + totalLines);

         if (datafixReportFileName == null){
            System.out.println("Confirm execution for script " + scriptFile +" ? (y/n): ");
            Scanner scanner1 = new Scanner(new InputStreamReader(System.in));
            String confirm = scanner1.nextLine();

            if (confirm != null &&
                  !"Y".equals(confirm.toUpperCase())){
               return;
            }
         }

         System.out.println("Processing ...");

         br.close();

         br = new BufferedReader(new FileReader(scriptFile));

         String logMsg = "";
         String logERRORMsg = "";

         int batchFromLine=startLine;
         int batchToLine= batchFromLine + recordsByBatch;

         if (inBatchMode){
            System.out.println("processing from line " + batchFromLine + " to " + batchToLine);
         }

         while ((sCurrentLine = br.readLine()) != null) {
            // always read first line
            if (sCurrentLine.startsWith("schemas:")){
               System.out.println(sCurrentLine);
               if (!sCurrentLine.contains(",")){
                  arrSchemas = new String[]{sCurrentLine.substring(sCurrentLine.indexOf("schemas:") + 8)};
               } else {
                  arrSchemas = sCurrentLine.substring(sCurrentLine.indexOf("schemas:") + 8).split(",");
               }
            }

            line++;

            if (line >= startLine) {

               // for batch mode = true
               if (inBatchMode && line == batchToLine){

                  appendToFile(batchReportFileName, "Lines from " + batchFromLine + " to " +
                        batchToLine + " proceseed OK.\n");

                  // Calculate al show estimated time
                  if (!multiUpdateMode && isFirstBatch){
                     isFirstBatch = false;
                     //calculateAndShowEstimatedTime(chronoMeter, totalLines/recordsByBatch, waitTimeInSeconds);
                  }

                  // wait time will be apply at the end of each batch
                  if (waitTimeInSeconds > 0){
                     System.out.println(" ");
                     System.out.println("wait for " + waitTimeInSeconds + " seconds - inBatchMode = true");
                     wait(waitTimeInSeconds * 1000);
                  }

                  batchFromLine = batchToLine +1;
                  batchToLine= batchFromLine + recordsByBatch;

                  System.out.println("processing from line " + batchFromLine + " to " + batchToLine + " of " + totalLines);
               }

               // chunk of sql line
               if (!Strings.isNullOrEmpty(sCurrentLine) &&
                     !sCurrentLine.trim().startsWith("--") &&
                     !sCurrentLine.trim().startsWith("schemas:") &&
                     sCurrentLine.trim().length() > 0) {

                  sql = sql + sCurrentLine + " ";
               }

               // En of sql line
               if (sql.trim().endsWith(";")) {
                  sql = sql.substring(0, sql.lastIndexOf(";"));

                  // Block: multiple mode ---------------------------------------------------------------------------------
                  if (multiUpdateMode) {
                     // For each schema
                     for (String schemaMulti: arrSchemas) {
                        String passwordMulti = getProperty(env + "." + schemaMulti + ".password");
                        dataSource = getDataSource(jdbcUrl, schemaMulti, passwordMulti);

                        logMsg = scriptFile + " line: " + line + " of " + totalLines + " on " + schemaMulti;

                        if (showSql) LOGGER.info("Execute on: " + schemaMulti + " sql: " + sql);
                        if (showSql) System.out.println("Execute on: " + schemaMulti + " Sql: " + sql + ";");

                        success = updateBatch(dataSource, sql);
                        totalSqlStmnt++;

                        if (!SUCCESS.equals(success) && onErrorExit){
                           logMsg = "Exit on error in line: " + line + " of " + totalLines + " - " + success;
                           System.out.println(logMsg);
                           LOGGER.info(logMsg);
                           return;
                        }

                        if (SUCCESS.equals(success)) {
                           totalOK ++;
                           LOGGER.info(logMsg + " - Executed OK");
                        } else {
                           totalError ++;
                           LOGGER.error("Error executing: " + logMsg + " - " + success);
                           logERRORMsg += "Error executing: " + logMsg + " - " + success + "\n";
                        }

                        printInLine(logMsg + " processed             ");

                        // time to wait between each sql statement execution.
                        if (waitTimeInSeconds > 0){
                           System.out.println(" ");
                           System.out.println("wait for " + waitTimeInSeconds + " seconds - multiUpdateMode = true");
                           wait(waitTimeInSeconds * 1000);
                        }

                     }

                     // Calculate al show estimated time
                     if (isFirstBatch){
                        isFirstBatch = false;
                     }

                  // Block: for one schema mode --------------------------------------------------------------------------
                  } else {
                     logMsg = "Line: " + line + " of " + totalLines;

                     if (showSql) LOGGER.info("sql: " + sql);
                     success = updateBatch(dataSource, sql);

                     totalSqlStmnt++;

                     if (!SUCCESS.equals(success) && onErrorExit){
                        logMsg = "Exit on error in line: " + line + " of " + totalLines + " - " + success;
                        System.out.println(logMsg);
                        LOGGER.info(logMsg);
                        return;
                     }

                     if (SUCCESS.equals(success)) {
                        totalOK ++;
                        LOGGER.info(logMsg + " - Executed OK");
                     } else {
                        totalError ++;
                        LOGGER.error("Error executing: " + logMsg + " - " + success);
                        logERRORMsg += "Error executing: " + logMsg + " - " + success + "\n";
                     }

                     printInLine("line: " + line + " of " + totalLines + " processed");

                     if (isFirstBatch &&
                           !inBatchMode &&
                              !multiUpdateMode){
                        isFirstBatch = false;

                     }
                  }

                  sql = "";
               }
            }
         }

         System.out.println("\nFile processed: " + scriptFile);

         chronoMeter.stop();

         System.out.println(logMsg);
         LOGGER.info(logMsg);

         // If is call from some datafix, add Summary to datafix report
         if (datafixReportFileName != null){
            String htmlBody = "<br>File processed: <b>" + scriptFile + "</b><br>\n" +
                  "Total lines: " + totalLines + "<br>\n" +
                  "Start line: " + startLine + "<br>\n" +
                  "Total sql statements: " + totalSqlStmnt + "<br><br>\n" +
                  " \n" +
                  "Total sql processed OK: <b>" + totalOK + "</b><br>\n" +
                  "Total sql processed with Error: <b>" + totalError + "</b><br><br>\n" +
                  " \n" +
                  "Start Time: " + chronoMeter.getStartTime() + "<br>\n" +
                  "End Time: " + chronoMeter.getEndTime() + "<br>\n" +
                  "Time elapsed: " + chronoMeter.getElapseTime() + "<br>\n";

             if (!"".equals(logERRORMsg) ){
                htmlBody += logERRORMsg.replaceAll("\n", "<br>\n");
             }

            htmlBody += "<br>";

            appendToFile(datafixReportFileName, htmlBody);

         } else {

            String body = "File processed: " + scriptFile + "\n" +
                  "Total lines: " + totalLines + "\n" +
                  "Start line: " + startLine + "\n" +
                  "Total sql statements: " + totalSqlStmnt + "\n" +
                  " \n" +
                  "Total sql processed OK: " + totalOK + "\n" +
                  "Total sql processed with Error: " + totalError + "\n" +
                  " \n" +
                  "Start Time: " + chronoMeter.getStartTime() + "\n" +
                  "End Time: " + chronoMeter.getEndTime() + "\n" +
                  "Time elapsed: " + chronoMeter.getElapseTime() + "\n\n" +

                  logERRORMsg + "\n";

            sendEmail(env, "Script " + scriptFile + " report", body, null, null);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      if (exit) return;
   }





}
