package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import java.util.UUID;


public class EventIndividualGenerator extends QryDB {

   private static final Logger LOGGER = LogManager.getLogger(EventIndividualGenerator.class);

   public synchronized void main() {
      loadExternalFileProperties();
      try {
         UPDATE_IN_TEST_MODE = false;

         String jdbcUrl = getProperty("telepresence.pre.url");
         String schema = "EVENT-PROCESSOR";
         String password = getProperty("pre.EVENT-PROCESSOR." + "password");

         DataSource dataSource;
         dataSource = getDataSource(jdbcUrl, schema, password);

         System.out.println("- EventGenerator -");
         System.out.println("UPDATE_IN_TEST_MODE = " + UPDATE_IN_TEST_MODE);

         Scanner scanner1 = new Scanner(new InputStreamReader(System.in));

         System.out.println("userName: ");
         String userName = scanner1.nextLine();


         // Busqueda de usuario,
         String sqlSearch = "SELECT SEQ_ID, DES_NICK_NAME, DES_CLIENT_ID," +
               " DES_COUNTRY, BOL_ADMINISTRATOR, BOL_MULTISITE " +
               "FROM SMPR_TUSER WHERE DES_NICK_NAME = '" + userName + "'";

         System.out.println("user data ----- ");
         select(dataSource, sqlSearch);

         System.out.println("userId: ");
         String userId = scanner1.nextLine();

         System.out.println("country: ");
         String country = scanner1.nextLine();

         sqlSearch = "SELECT * FROM SMPR_TINSTALLATION_ALIAS instalias\n" +
               "WHERE SEQ_USER_ID = (\n" +
               "SELECT SEQ_ID FROM SMPR_TUSER WHERE DES_NICK_NAME = '" + userName + "')";

         System.out.println("installation alias data ----- ");
         select(dataSource, sqlSearch);

         System.out.println("InstallationId: ");
         String installationId = scanner1.nextLine();


         sqlSearch = "SELECT thn.SEQ_ID, thn.COD_CONNECTION_ID, THNA.SEQ_INSTALLATION_ALIAS_ID " +
               "FROM SMPR_TTHING thn, SMPR_TTHING_ALIAS thna " +
               "WHERE thn.SEQ_ENTITY_TYPE = 13 " +
               "AND thn.SEQ_INSTALLATION_ID = " + installationId + " " +
               "AND THNA.SEQ_THING_ID = thn.SEQ_ID " +
               "AND thna.SEQ_USER_ID = ( " +
               "SELECT SEQ_ID FROM SMPR_TUSER WHERE DES_NICK_NAME = '" + userName + "')";

         System.out.println("panel data ----- ");
         select(dataSource, sqlSearch);

         sqlSearch = "SELECT thn.SEQ_ID, thn.des_name, thn.SEQ_ENTITY_TYPE " +
               "FROM SMPR_TTHING thn " +
               "WHERE thn.SEQ_PARENT_ID = (" +
               "         SELECT thn.SEQ_ID " +
               "FROM SMPR_TTHING thn, SMPR_TTHING_ALIAS thna " +
               "WHERE thn.SEQ_ENTITY_TYPE = 13 " +
               "AND thn.SEQ_INSTALLATION_ID = " + installationId + " " +
               "AND THNA.SEQ_THING_ID = thn.SEQ_ID " +
               "AND thna.SEQ_USER_ID = ( " +
               "               SELECT SEQ_ID FROM SMPR_TUSER WHERE DES_NICK_NAME = '" + userName + "') " +
               "         )";

         System.out.println("partion data ----- ");
         select(dataSource, sqlSearch);

         System.out.println("panelId (parent_thing_id en eventos): ");
         String parentThingId = scanner1.nextLine();

         System.out.println("thing_id de la particion: BasePartion va el id del panel/ FullPartition va el id de la particion  : ");
         String thingId = scanner1.nextLine();

         System.out.println("is Full Partition (1/0): ");
         String isFullPartition = scanner1.nextLine();

         while (true) {
            System.out.println(" ");
            System.out.println("1|Panel armado parcialmente [SOURCE] por Usuario");
            System.out.println("2|Panel armado totalmente [SOURCE] por Usuario");
            System.out.println("3|Panel desarmado [SOURCE] por Usuario");
            System.out.println("6|Error en armado total");
            System.out.println("7|Error en desarmado");
            System.out.println("8|Error en armado parcial");
            System.out.println("15|Fallo línea de comunicación");
            System.out.println("Tipo de evento: ");

            String eventTypeId = scanner1.nextLine();

            System.out.println("Cantidad de eventos a generar: ");
            String totalStr = scanner1.nextLine();

            System.out.println("Fecha desde de eventos a generar yyyy-MM-dd: ");
            String eventDate = scanner1.nextLine();

            System.out.println("Cantidad de dias: ");
            String totalDaysStr = scanner1.nextLine();

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date date = df.parse(eventDate);

            int totalEvents = Integer.valueOf(totalStr);
            int totalDays = Integer.valueOf(totalDaysStr);

            Scanner scanner5 = new Scanner(new InputStreamReader(System.in));
            System.out.println("Se van a generar " + totalEvents + " eventos en " + totalDays + " dias, " + totalEvents / totalDays + " x dia desde el " + eventDate + " confirma (y/n):");
            String confirm = scanner5.nextLine();

            if (!"y".equals(confirm)) return;

            int day = 1;
            int i = 1;

            while (true) {

               if ("1".equals(eventTypeId)) {
                  // 1 - Armado Parcial -  AP
                  System.out.println("event " + i + " of " + totalEvents + " day " + day);
                  if (i == (totalEvents / totalDays * day)) day++;
                  insertEvent(dataSource, date, i, day - 1, 1,
                        userId, "'" + userName + "'", country, installationId, parentThingId, thingId, isFullPartition);
                  i++;
                  if (i == totalEvents) break;
               }

               if ("2".equals(eventTypeId)) {
                  // 2 - Armmado Total - AT
                  System.out.println("event " + i + " of " + totalEvents + " day " + day);
                  if (i == (totalEvents / totalDays * day)) day++;
                  insertEvent(dataSource, date, i, day - 1, 2,
                        userId, "'" + userName + "'", country, installationId, parentThingId, thingId, isFullPartition);
                  i++;
                  if (i == totalEvents) break;
               }


               if ("3".equals(eventTypeId)) {
                  // 3 - Desarmado - DA
                  System.out.println("event " + i + " of " + totalEvents + " day " + day);
                  if (i == (totalEvents / totalDays * day)) day++;
                  insertEvent(dataSource, date, i, day - 1, 3,
                        userId, "'" + userName + "'", country, installationId, parentThingId, thingId, isFullPartition);
                  i++;
                  if (i == totalEvents) break;
               }

               if ("6".equals(eventTypeId)) {
                  // 6 - Error en armado total
                  System.out.println("event " + i + " of " + totalEvents + " day " + day);
                  if (i == (totalEvents / totalDays * day)) day++;
                  insertEvent(dataSource, date, i, day - 1, 6,
                        userId, "'" + userName + "'", country, installationId, parentThingId, thingId, isFullPartition);
                  i++;
                  if (i == totalEvents) break;
               }

               if ("7".equals(eventTypeId)) {
                  //7 - Error en desarmado                                                                                     |
                  System.out.println("event " + i + " of " + totalEvents + " day " + day);
                  if (i == (totalEvents / totalDays * day)) day++;
                  insertEvent(dataSource, date, i, day - 1, 7,
                        userId, "'" + userName + "'", country, installationId, parentThingId, thingId, isFullPartition);
                  i++;
                  if (i == totalEvents) break;
               }

               if ("8".equals(eventTypeId)) {
                  // 8 - Error en armado parcial
                  System.out.println("event " + i + " of " + totalEvents + " day " + day);
                  if (i == (totalEvents / totalDays * day)) day++;
                  insertEvent(dataSource, date, i, day - 1, 8,
                        userId, "'" + userName + "'", country, installationId, parentThingId, thingId, isFullPartition);
                  i++;
                  if (i == totalEvents) break;
               }


               if ("15".equals(eventTypeId)) {
                  // 15 - Fallo de comunicacion
                  System.out.println("event " + i + " of " + totalEvents + " day " + day);
                  if (i == (totalEvents / totalDays * day)) day++;
                  insertEvent(dataSource, date, i, day - 1, 15,
                        userId, "NULL", country, installationId, parentThingId, "", isFullPartition);
                  i++;
                  if (i == totalEvents) break;
               }

            }

            System.out.println("End process.");
         }



      } catch(Exception e) {
         printStackTrace(e);
      }


   }


   public void insertEvent(DataSource dataSource, Date date, int secondToAdd, int dayToAdd, int seqEventTypeId,
                           String userId, String desPanelUser, String country, String installationId,
                           String parentThingId, String thingId, String isFullPartition) {

      /*
      *
      * INSERT INTO SMPR_TEVENT
(SEQ_ID, SEQ_PARENT_THING_ID, SEQ_EVENT_TYPE_ID, DES_TYPE, DES_MESSAGE, FYH_CREATION_DATE, FYH_EVENT_DATE, SEQ_USER_ID, SEQ_LOG_ID, BOL_SMART_EVENT, BOL_MASTERMIND_EVENT, BOL_EXTERNAL_PROVIDER_EVENT, BOL_DOMOTIC_EVENT, DES_DEVICE_NAME, DES_PANEL_USER, DES_CONTRACT, DES_PRIORITY, DES_COLOR, DES_EVENT_ID, DES_LOGIN_AS_USER, DES_SCENE_NAME, DES_SITE_NAME, DES_ZONE_ID, DES_AREA, COD_ACUDA_CASE, DES_COMMENTS, DES_DM_VARIABLE_NAME, DES_DM_VARIABLE_VALUE, SEQ_DEVICE_ID, DES_PARTITION_ID, DES_JOB_ID, BOL_INCLUDE_VIDEO, SEQ_INSTALLATION_ID, DES_USER, DES_USER_ALIAS, DES_PARTITION_NAME, SEQ_SCENE_ID, DES_COUNTRY, BOL_IS_FULL_PARTITION, SEQ_THING_ID)
VALUES ('dbf3218b-7d72-4af2-9528-8f618ab178d8', 30023, 15, NULL, ' ', TIMESTAMP '2024-06-14 17:51:44.613000', TIMESTAMP '2024-06-14 17:51:44.607000', NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 7822, NULL, NULL, NULL, NULL, 'ES', 0, NULL);
      *
      * */

      try {


         Calendar calendar = Calendar.getInstance();
         calendar.setTime(date);
         calendar.add(Calendar.SECOND, secondToAdd * 10);
         calendar.add(Calendar.DAY_OF_MONTH, dayToAdd);
         Date modifiedDate = calendar.getTime();

         SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
         String dateFormatted = formatter.format(modifiedDate);

         String sql = "INSERT INTO SMPR_TEVENT " +
               "(SEQ_ID, " +
               "SEQ_PARENT_THING_ID, " +
               "SEQ_EVENT_TYPE_ID, " +
               "DES_TYPE, " +
               "DES_MESSAGE, " +
               "FYH_CREATION_DATE, " +
               "FYH_EVENT_DATE, " +
               "SEQ_USER_ID, " +
               "SEQ_LOG_ID, " +
               "BOL_SMART_EVENT, " +
               "BOL_MASTERMIND_EVENT, " +
               "BOL_EXTERNAL_PROVIDER_EVENT" +
               ", BOL_DOMOTIC_EVENT" +
               ", DES_DEVICE_NAME" +
               ", DES_PANEL_USER" +
               ", DES_CONTRACT" +
               ", DES_PRIORITY" +
               ", DES_COLOR" +
               ", DES_EVENT_ID" +
               ", DES_LOGIN_AS_USER" +
               ", DES_SCENE_NAME" +
               ", DES_SITE_NAME" +
               ", DES_ZONE_ID" +
               ", DES_AREA" +
               ", COD_ACUDA_CASE" +
               ", DES_COMMENTS" +
               ", DES_DM_VARIABLE_NAME" +
               ", DES_DM_VARIABLE_VALUE" +
               ", SEQ_DEVICE_ID" +
               ", DES_PARTITION_ID" +
               ", DES_JOB_ID" +
               ", BOL_INCLUDE_VIDEO" +
               ", SEQ_INSTALLATION_ID" +
               ", DES_USER" +
               ", DES_USER_ALIAS" +
               ", DES_PARTITION_NAME" +
               ", SEQ_SCENE_ID" +
               ", DES_COUNTRY" +
               ", BOL_IS_FULL_PARTITION" +
               ", SEQ_THING_ID) " +
               "VALUES " +
               "('" + UUID.randomUUID().toString() + "'" +
               ", " + parentThingId +
               ", "+ seqEventTypeId +"" +
               ", NULL" +
               ", 'generated by EventIndividualGenerator'" +
               ", TIMESTAMP '" + dateFormatted +".613000'" +
               ", TIMESTAMP '"+ dateFormatted +".607000'" +
               ", " + userId +
               ", NULL" +
               ", 1" +
               ", 0" +
               ", 0" +
               ", 0" +
               ", NULL, " +
                desPanelUser +
               ", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL" +
               ", 0" +
               ", " + installationId +
               ", NULL" +
               ", NULL" +
               ", NULL" +
               ", NULL" +
               ", '"+ country +"'" +
               ", " + isFullPartition +
               ", " + thingId +")";

         //System.out.println(dateFormatted);
         //System.out.println(sql);
         //System.out.println("## ----------------------------------------------------------------");
         updateBatch(dataSource, sql);


      } catch (Exception e) {
         printStackTrace(e);
      }

   }


   public static void waitFor(int ms) {
      try
      {
         Thread.sleep(ms);
      }
      catch(InterruptedException ex)
      {
         Thread.currentThread().interrupt();
      }
   }

}
