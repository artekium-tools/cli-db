package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


public class CheckWhiteScreenUsers extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(CheckWhiteScreenUsers.class);

   static int countCases = 0;

   public synchronized int main(String env, String datafixReportFile, String reportDate, String updateStatementFileName, int waitTimeInSeconds) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         appendToFile(datafixReportFile,
               "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-43566'>" +
                     "20) MB " +
                     "https://jira.prosegur.com/browse/AM-43566</a> - " +
                     "Usuarios de ES con al menos 1 instalación inactiva, pantalla en blanco<br>\n");

         System.out.println("Executing step 1: looking for cases ..." );

         String listCases = lookingForCases(dataSource, updateStatementFileName);

         if (!listCases.contains("Empty Result")){

            System.out.println("Waiting for " + waitTimeInSeconds + " seconds." );
            wait(waitTimeInSeconds * 1000);

            System.out.println("Executing step 2: generate delete statements ..." );
            generateDeleteStatements(dataSource, updateStatementFileName);

         }

         if (countCases > 0) {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", se fixearon " + countCases + " casos. <br>\n");

            appendToFile(datafixReportFile,    listCases  + "<br>\n");
         } else {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", no se encontraron casos a fixear<br>\n");
         }

      } catch (Exception e){
         printStackTrace(e);
      }

      return countCases;
   }



   public String lookingForCases(final DataSource dataSource, String repotFile) {

      String start = "<table border='1'>\n";
      String end = "</table>";
      String body = "";

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT SEQ_ID, DES_CLIENT_ID, DES_COUNTRY, DES_NICK_NAME " +
               "FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER " +
               "WHERE (SEQ_ID IN (SELECT SEQ_admin FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION " +
               "WHERE FYH_DELETED IS NOT NULL) " +
               "AND " +
               "SEQ_ID IN (SELECT seq_user_id FROM \"ACCOUNT-MANAGEMENT\".SMPR_TPREBOARDING_VIEWED " +
               "WHERE bol_viewed = 0)) " +
               "AND DES_COUNTRY = 'ES'";

         //System.out.println(sql);

         try {


            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            body = selectToHTML(dataSource, sql, "grey");

            System.out.println("\nEnd looking for cases." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return start + body + end;
   }

   public void generateDeleteStatements(final DataSource dataSource, String updateStatementFileName) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION " +
               "WHERE FYH_DELETED IS NOT NULL AND SEQ_ADMIN IN ( " +
               "SELECT seq_user_id FROM \"ACCOUNT-MANAGEMENT\".SMPR_TPREBOARDING_VIEWED " +
               "WHERE bol_viewed=0) AND DES_COUNTRY IN ('ES')";


         String sqlDeleteThing = "DELETE FROM SMPR_TTHING st WHERE seq_id IN ( " +
               "SELECT SEQ_ID FROM SMPR_TTHING st WHERE SEQ_PARENT_ID IN ( " +
               "SELECT SEQ_ID FROM SMPR_TTHING st WHERE SEQ_INSTALLATION_ID =:installationID) " +
               "UNION " +
               "SELECT SEQ_ID FROM SMPR_TTHING st WHERE SEQ_INSTALLATION_ID =:installationID)";

         String sqlDeleteThingAlias = "DELETE FROM SMPR_TTHING_ALIAS sta WHERE seq_thing_id " +
               "IN (SELECT SEQ_ID FROM SMPR_TTHING st WHERE SEQ_PARENT_ID IN ( " +
               "SELECT SEQ_ID FROM SMPR_TTHING st WHERE SEQ_INSTALLATION_ID =:installationID) " +
               "UNION " +
               "SELECT SEQ_ID FROM SMPR_TTHING st WHERE SEQ_INSTALLATION_ID=:installationID)";

         String sqlDeleteInstallation = "DELETE FROM SMPR_TINSTALLATION st WHERE seq_id=:installationID" ;

         String sqlDeleteInstallationAlias = "DELETE FROM SMPR_TINSTALLATION_ALIAS sta WHERE seq_installation_id=:installationID";

         //System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            // For installationID
            String installationId = "";
            while (rs.next()) {
               installationId = ""+ rs.getLong(1);

               appendToFile(updateStatementFileName,    sqlDeleteThing.replaceAll(":installationID", installationId) + ";\n");
               appendToFile(updateStatementFileName,    sqlDeleteThingAlias.replaceAll(":installationID", installationId) + ";\n");
               appendToFile(updateStatementFileName,    sqlDeleteInstallation.replaceAll(":installationID", installationId) + ";\n");
               appendToFile(updateStatementFileName,    sqlDeleteInstallationAlias.replaceAll(":installationID", installationId) + ";\n");
               countCases++;
            }

            System.out.println("\nEnd Delete statement generation .." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }

 }
