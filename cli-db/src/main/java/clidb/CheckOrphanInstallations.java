package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class CheckOrphanInstallations extends QryDB {

    private static final Logger LOGGER = LogManager.getLogger(CheckOrphanInstallations.class);

    static ArrayList<String> contractInstallationList = new ArrayList<>();

    public int fixOrphanInstallations(String env, String datafixReportFile, String reportDate, String instalWithoutAdminFile) {

        loadExternalFileProperties();

        DataSource dataSource = null;

        try {

            if (env == null) env = getProperty("env");
            String connectionType = getProperty("connectionType");

            String jdbcUrl = getProperty(connectionType + "." + env + ".url");
            String dbUsername = getProperty(env + ".username");
            String password = getProperty(env + ".password");

            dataSource = getDataSource(jdbcUrl, dbUsername, password);

            System.out.println("Executing step 1: check installation without valid admin user  ..." );

            appendToFile(datafixReportFile,
                  "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-40939'>" +
                        "8) MC " +
                        "https://jira.prosegur.com/browse/AM-40939</a> - " +
                        "Instalaciones huerfanas - asociadas a usuarios que no existen<br>\n");

            checkInstallationWithoutAdminInAccount(dataSource, datafixReportFile, reportDate, instalWithoutAdminFile );

            System.out.println("Total installation found: " + contractInstallationList.size());

        } catch (Exception e){
            printStackTrace(e);
        }

        return contractInstallationList.size();
    }

    public void checkInstallationWithoutAdminInAccount(final DataSource dataSource, String datafixReportFile,
                                                              String reportDate, String instalWithoutAdminFile) {

        try {
            Connection con = dataSource.getConnection();
            Statement stmt = null;
            ResultSet rs = null;

            try {

                // Get SEQ_ID of orphan installation
                String sqlSelectInstal = "SELECT SEQ_ID, DES_CONTRACT_ID " +
                      " FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION instal"
                    + " WHERE NOT EXISTS (SELECT * FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usuario"
                    + " WHERE usuario.SEQ_ID = instal.SEQ_ADMIN) "
                    + " AND ROWNUM <= 1000";

                // Execute query
                stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                rs = stmt.executeQuery(sqlSelectInstal);

                String fileName = "fixOrphanInstallations.sql";
                String installationsToFix = "";
                String contratsNumbers = "";
                String thingsToFix = "";

                while (rs.next()) {
                    contractInstallationList.add(rs.getString(2));
                    installationsToFix = installationsToFix + rs.getString(1) + ",";
                    contratsNumbers = contratsNumbers + rs.getString(2) + "\n";
                }


                if (!installationsToFix.isEmpty()) {
                    installationsToFix = installationsToFix.substring(0, installationsToFix.length() - 1);
                    contratsNumbers = contratsNumbers.substring(0, contratsNumbers.length() - 1);

                    appendToFile(instalWithoutAdminFile, "-- fixOrphanInstallations AM-40939 \n");
                    appendToFile(instalWithoutAdminFile, contratsNumbers);

                    //Get SEQ_ID in Things for orphan installation
                    String sqlSelectThings = "SELECT SEQ_ID FROM \"DEVICE\".SMPR_TTHING" +
                            " WHERE SEQ_INSTALLATION_ID IN (" + installationsToFix + ")";

                    // Execute query
                    stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                    rs = stmt.executeQuery(sqlSelectThings);

                    int cont = 0;

                    String thingAliasSchemas ="schemas:DEVICE," + getProperty("replicas.thingAlias");
                    String thingSchemas = "schemas:DEVICE," + getProperty("replicas.thing");
                    String installationSchemas = "schemas:ACCOUNT-MANAGEMENT," + getProperty("replicas.installation");
                    String installationAliasSchemas = "schemas:ACCOUNT-MANAGEMENT," + getProperty("replicas.installationAias");


                    while (rs.next()) {
                        cont++;
                        thingsToFix = thingsToFix + rs.getString(1) + ",";
                        if (cont == 1000) {
                            thingsToFix = thingsToFix.substring(0, thingsToFix.length() - 1);
                            // THING_ALIAS
                            appendToFile(fileName, "--DELETE THING_ALIAS AND THINGS FOR ORPHAN INSTALLATION\n");
                            appendToFile(fileName, thingAliasSchemas + "\n");
                            appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_PARENT_ID IN (SELECT SEQ_ID FROM SMPR_TTHING_ALIAS sta2 WHERE SEQ_THING_ID IN ("+ thingsToFix +"));\n");
                            appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_THING_ID IN ("+ thingsToFix +");\n");

                            // THING
                            appendToFile(fileName, thingSchemas + "\n");
                            appendToFile(fileName, "DELETE FROM SMPR_TTHING sta WHERE SEQ_PARENT_ID IN ("+ thingsToFix +");\n");
                            appendToFile(fileName, "DELETE FROM SMPR_TTHING WHERE SEQ_ID IN (" + thingsToFix + ");\n");
                            cont = 0;
                            thingsToFix = "";
                        }
                    }

                    if (!thingsToFix.isEmpty()) {
                        thingsToFix = thingsToFix.substring(0, thingsToFix.length() - 1);
                        // THING_ALIAS
                        appendToFile(fileName, "--DELETE THING_ALIAS AND THINGS FOR ORPHAN INSTALLATION\n");
                        appendToFile(fileName,  thingAliasSchemas + "\n");
                        appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_PARENT_ID IN (SELECT SEQ_ID FROM SMPR_TTHING_ALIAS sta2 WHERE SEQ_THING_ID IN ("+ thingsToFix +"));\n");
                        appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_THING_ID IN ("+ thingsToFix +");\n");

                        // THING
                        appendToFile(fileName,  thingSchemas + "\n");
                        appendToFile(fileName, "DELETE FROM SMPR_TTHING sta WHERE SEQ_PARENT_ID IN ("+ thingsToFix +");\n");
                        appendToFile(fileName, "DELETE FROM SMPR_TTHING WHERE SEQ_ID IN (" + thingsToFix + ");\n");
                    }

                    // INSTALLATION_ALIAS
                    appendToFile(fileName, "--DELETE INSTALLATION_ALIAS AND INSTALLATION FOR ORPHAN INSTALLATION\n");
                    appendToFile(fileName,  installationAliasSchemas + "\n");
                    appendToFile(fileName, "DELETE FROM SMPR_TINSTALLATION_ALIAS WHERE SEQ_INSTALLATION_ID IN (" + installationsToFix + ");\n");
                    // INSTALLATION
                    appendToFile(fileName,  installationSchemas + "\n");
                    appendToFile(fileName, "DELETE FROM SMPR_TINSTALLATION WHERE SEQ_ID IN (" + installationsToFix + ");\n");

                    appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                          ", se fixearon " + contractInstallationList.size() +
                          " casos. <br><br>\n");

                } else {
                    appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate +
                          ", no se encontraron casos a fixear <br>\n");
                    System.out.println("Orphan installations not found!");
                }

                System.out.println("Process end.");

            } catch (Exception e) {
                System.out.println(" ");
                System.out.println(e.getMessage());
                printStackTrace(e);
            } finally {
                try {
                    if (rs != null) rs.close();
                    if (stmt != null) stmt.close();
                    if (con != null) con.close();
                } catch (Exception e) {
                    System.out.println(" ");
                    System.out.println(e.getMessage());
                    printStackTrace(e);
                }
            }

        } catch (Exception e1) {
            System.out.println(" ");
            System.out.println(e1.getMessage());
            e1.printStackTrace();
        }
    }
}
