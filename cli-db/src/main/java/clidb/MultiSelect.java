package clidb;

import com.google.common.base.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Scanner;


public class MultiSelect extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(MultiSelect.class);

   public void runMultiSelect(String env) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);
         System.out.println("connectionType: " + connectionType);

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         System.out.println(" ");
         System.out.println("- MultiSelect -");

         String scriptFile = chooseSqlFile();


         System.out.println("1) html 2) csv: ");
         Scanner scanner2 = new Scanner(new InputStreamReader(System.in));
         String op = scanner2.nextLine();

         if ("2".equals(op)) {
            generateCSVResult(dataSource, scriptFile);
         } else {
            generateHTMLResult(dataSource, scriptFile);
         }



      } catch (Exception e) {
         printStackTrace(e);
      }

      return;
   }

   private void generateHTMLResult(DataSource dataSource, String scriptFile) throws IOException {
      BufferedReader br = new BufferedReader(new FileReader(scriptFile));

      String sCurrentLine;
      String exportData = "";
      int line = 0;
      String sql = "";
      String color = "orange";
      while ((sCurrentLine = br.readLine()) != null) {
         line++;

         if (!Strings.isNullOrEmpty(sCurrentLine) &&
               !sCurrentLine.trim().startsWith("--") &&
               sCurrentLine.trim().length() > 0) {

            sql = sql + sCurrentLine + " ";
            exportData = exportData + "&nbsp;&nbsp;&nbsp;&nbsp;" + sCurrentLine + " \n";

         } else if (Strings.isNullOrEmpty(sCurrentLine)) {

            exportData = exportData + "&nbsp;<br>\n";

         } else if (!Strings.isNullOrEmpty(sCurrentLine) &&
               sCurrentLine.trim().startsWith("--")) {

            if (sCurrentLine.contains("<") && sCurrentLine.contains("/>")) {
               color = sCurrentLine.substring(sCurrentLine.indexOf("<") + 1, sCurrentLine.indexOf("/>"));

            } else {
               System.out.println(sCurrentLine);

               sCurrentLine = sCurrentLine.replaceAll("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");

               if (sCurrentLine.contains("<img")){
                  exportData = exportData + sCurrentLine + "<br> \n";
               } else {
                  exportData = exportData + sCurrentLine.replaceAll(" ", "&nbsp;") + "<br> \n";
               }

            }
         }

         if (sql.trim().endsWith(";")) {
            sql = sql.substring(0, sql.indexOf(";"));
            System.out.println(sql + ";");

            exportData = exportData + selectToScript(dataSource, sql, color);
            sql = "";
         }
      }

      String start = "<html><body>\n<h1> Result for: " + scriptFile + " </h1>\n ";
      String end = "</body></html>";

      System.out.println(" ");
      String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
      String resultFile = scriptFile.replaceAll(".sql", "-result-" + timeStamp + ".html");

      System.out.println("File processed: " + scriptFile);
      writeToFile(resultFile, start + exportData + end);

      runCommandLine("google-chrome " + resultFile);
   }

   private void generateCSVResult(DataSource dataSource, String scriptFile) throws IOException {
      BufferedReader br = new BufferedReader(new FileReader(scriptFile));

      String sCurrentLine;
      String exportData = "";
      String sql = "";
      boolean showHeaders = true;
      while ((sCurrentLine = br.readLine()) != null) {

         if (!Strings.isNullOrEmpty(sCurrentLine) &&
               !sCurrentLine.trim().startsWith("--") &&
               sCurrentLine.trim().length() > 0) {

            sql = sql + sCurrentLine + " ";
         }

         if (sCurrentLine.contains("<space/>")){
            exportData = exportData + "\n";
         }

         if (sCurrentLine.startsWith("--<comment/>")) {
            exportData = exportData + sCurrentLine.substring("--<comment/>".length()) + "\n";
         }

         if (sCurrentLine.contains("<HideHeaders/>")) {
            showHeaders = false;
         }

         if (sCurrentLine.contains("<ShowHeaders/>")) {
            showHeaders = true;
         }

         if (sql.trim().endsWith(";")) {
            sql = sql.substring(0, sql.indexOf(";"));
            System.out.println(sql + ";");

            exportData = exportData + selectToCSV(dataSource, sql, showHeaders);
            sql = "";
         }
      }


      System.out.println(" ");
      String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
      String resultFile = scriptFile.replaceAll(".sql", "-result-" + timeStamp + ".csv");

      System.out.println("File processed: " + scriptFile);
      writeToFile(resultFile, exportData );


   }
}
