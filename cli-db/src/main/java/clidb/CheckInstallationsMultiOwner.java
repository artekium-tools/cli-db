package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Properties;


public class CheckInstallationsMultiOwner extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(CheckInstallationsMultiOwner.class);

   static ArrayList<Long> arr_seq_installation_id = new ArrayList<>();

   public int main(String env, String datafixReportFile, String reportDate) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         System.out.println("Executing step 1: select Installation with multiples ownres ..." );
         selectInstallations(dataSource);
         int count = 0;


         System.out.println("Executing step 2: generate delete statements ..." );
         for (Long installationId : arr_seq_installation_id) {
            count ++;

            // printInLine("checking nro: " + count + " of " + arr_seq_installation_id.size() +" installation : " + installationId);

            generateDeleteStatements(dataSource, installationId, count + " of " + arr_seq_installation_id.size());

            //appendToFile("analisis.txt", installationId + "\n");

         }

         appendToFile(datafixReportFile,
               "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-38657'>" +
                     "4) CS " +
                     "https://jira.prosegur.com/browse/AM-38657</a> - " +
                     "Instalaciones con multiples owners<br>\n");


         if (arr_seq_installation_id.size() > 0) {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", se fixearon " + arr_seq_installation_id.size() + " casos. <br>\n");
         } else {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", no se encontraron casos a fixear<br>\n");
         }



      } catch (Exception e){
         printStackTrace(e);
      }



      return arr_seq_installation_id.size();
   }



   public static void selectInstallations(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT instAlias.SEQ_INSTALLATION_ID , count(usr.BOL_ADMINISTRATOR) " +
               "FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS instAlias " +
               "INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr " +
               "ON (INSTALIAS.SEQ_USER_ID = usr.SEQ_ID AND usr.BOL_ADMINISTRATOR = 1 ) " +
               "GROUP BY instAlias.SEQ_INSTALLATION_ID " +
               "HAVING count(usr.BOL_ADMINISTRATOR) > 1" ;

         //System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            //System.out.println("Adding installations .."  );
            int rec = 0;
            while (rs.next()) {
               arr_seq_installation_id.add(rs.getLong(1));
               rec ++;
               //printInLine("record: " + rec);
            }

            System.out.println("\nEnd Adding installaitons .." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }



   public static void generateDeleteStatements(final DataSource dataSource, Long installationId, String order) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            // Se consultan los installationAlias a borrar
            // Osea cuyo userId NO sea:
            //    El que esté indicado en la intalacion
            //    Los dependientes del anterior
            String sql1 = "SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS " +
            "WHERE SEQ_INSTALLATION_ID = " + installationId +
            "AND SEQ_USER_ID NOT IN ( " +
                  // El admin de la instalacion
            "      SELECT SEQ_admin FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION WHERE SEQ_ID  = " + installationId +
            "      UNION " +
                  // Dependientes del admin declarado
            "      SELECT seq_id FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER " +
                  "     WHERE BOL_ADMINISTRATOR = 0 " +
                  "      AND DES_COUNTRY = (SELECT DES_COUNTRY FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER WHERE SEQ_ID = " +
                  "        (SELECT SEQ_admin FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION WHERE SEQ_ID  = " + installationId +" )) " +
                  "      AND des_client_id = (SELECT des_client_id FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER WHERE SEQ_ID = " +
                  "        (SELECT SEQ_admin FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION WHERE SEQ_ID  = " + installationId + " ))" +
                  ")";

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql1);

            String fileName = "purgeInstallationMultiOwners.sql";
            String deleteInstAlias = "DELETE FROM SMPR_TINSTALLATION_ALIAS WHERE SEQ_ID = ";
            String deleteThingAlias = "DELETE FROM SMPR_TTHING_ALIAS WHERE SEQ_INSTALLATION_ALIAS_ID = ";
            String instAliasSchemas =  "schemas:ACCOUNT-MANAGEMENT,CAMPAIGNS,DEVICE,DIGITAL-SECURITY,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG,USER-ENROLLMENT";
            String thingAliasSchemas = "schemas:DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG";


            appendToFile(fileName,  "-- Purge installation:  " + installationId + "\n");

            while (rs.next()) {
                  appendToFile(fileName,  instAliasSchemas + "\n");
                  appendToFile(fileName,  deleteInstAlias +  rs.getLong(1) + ";\n");

                  appendToFile(fileName,  thingAliasSchemas + "\n");
                  appendToFile(fileName,  deleteThingAlias +  rs.getLong(1) + ";\n");
            }

            appendToFile(fileName,  "-- End " + order + " installation:  " + installationId + "\n");

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }
      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }


    public void analisis(String env) {

      loadExternalFileProperties();
       DataSource dataSource = null;
       UserInfo userInfo = new UserInfo();
       try {

          if (env == null) env = getProperty("env");
          String connectionType = getProperty("connectionType");

          System.out.println("env: " + env);
          System.out.println("connectionType: " + connectionType);

          String jdbcUrl = getProperty(connectionType + "." + env + ".url");
          String dbUsername = getProperty(env + ".username");
          String password = getProperty(env + ".password");

          dataSource = getDataSource(jdbcUrl, dbUsername, password);

          System.out.println("Init Process: " );

          System.out.println("Checking on ACCOUNT-MANAGEMENT " );
          selectInstallations(dataSource);
          int count = 0;


          for (Long installationId : arr_seq_installation_id) {
             count ++;


             printInLine("checking nro: " + count + " of " + arr_seq_installation_id.size() +" installation : " + installationId);

             //getUserInfo(dataSource, userInfo, env, installationId);
             getData(dataSource, env, userInfo, installationId);

             printInLine("checking nro: " + count);

          }

          System.out.println("End Process");
       } catch (Exception e){
          printStackTrace(e);
       }

       LOGGER.info("End process.");

       return;

   }
   // 1268555 | re3movecenter@gmail.com |Last Access:  2024-07-19 12:49:02.963439 | Terminales: SI | Instalacion: SI | crmMail: SI
   public static void getData(final DataSource dataSource, String env, UserInfo userInfo, Long installationId) {


      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT usr.SEQ_ID, " +
               "usr.DES_COUNTRY, " +
               "usr.DES_CLIENT_ID, " +
               "usr.DES_NICK_NAME , " +
               "usr.des_name, " +
               "usr.des_surnames, " +
               "usr.fyh_last_access, " +
               "(select count(1) FROM \"NOTIFICATION\".SMPR_TTERMINAL where SEQ_USER_ID = usr.SEQ_ID) AS cntTerminal, "+
               "(SELECT count(1) FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION where SEQ_admin = usr.SEQ_ID ) AS cntInstallation, " +
               "(SELECT count(1) FROM \"USER-ENROLLMENT\".SMPR_TUSER_EMAIL_CHANGE WHERE DES_OLD_EMAIL = usr.DES_EMAIL) AS cntEmailChange, " +
               "(SELECT count(1) FROM \"MIGRATION-ORCHESTRATOR\".SMPR_TACCOUNT WHERE lower(DES_USERNAME) = usr.DES_EMAIL ) AS cntMigra " +
               "FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS instAlias " +
               "INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr " +
               "ON (INSTALIAS.SEQ_USER_ID = usr.SEQ_ID AND usr.BOL_ADMINISTRATOR = 1 ) " +
               "WHERE  instAlias.SEQ_INSTALLATION_ID = " + installationId;

         System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            String row = "";
            appendToFile("analisisMultiOwner.html", "Instalacion: " + installationId + "<br>\n");
            appendToFile("analisisMultiOwner.html", "\t<b>ID | NickName | Name | Surenames| Last Access | Terminales | Owner | EmailChange | Migration </b><br>\n");

            String clientId = "";
            String country = "";

            Long maxId = 0L;

            while( rs.next()){
               System.out.println("Result for installationId: " + installationId + " -> " + rs.getString(1) );
               row = "";
               row = row + "\t";

               Long id = rs.getLong(1);
               if (id.longValue() > maxId.longValue()){
                  maxId = id;
               }

               row = row + rs.getLong(1) + " | ";
               row = row + rs.getString(4) + " | ";
               row = row + rs.getString(5) + " | ";
               row = row + rs.getString(6) + " | ";
               row = row + rs.getString(7) + " | ";
               row = row + ("0".equals(rs.getString(8)) ? "NO":"SI")+ " | ";
               row = row + "<b>"+("0".equals(rs.getString(9)) ? "NO":"SI")+ " </b>| ";
               row = row + ("0".equals(rs.getString(10)) ? "NO":"SI")+ " | ";
               row = row + ("0".equals(rs.getString(11)) ? "NO":"SI")+ " | ";
               //row = row + " ## | ## | ";
               appendToFile("analisisMultiOwner.html", row + "<br>\n");

               country = rs.getString(2);
               clientId = rs.getString(3);
            }

            appendToFile("analisisMultiOwner.html", "\t mayor: " + maxId + "<br>\n");

            userInfo.setDesClientId(clientId);
            userInfo.setDesCountry(country);
            String contractData = userInfo.getCRMContract(env);

            appendToFile("analisisMultiOwner.html", contractData + "<br><br>\n");

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }


  /* public static void getUserInfo(final DataSource dataSource, UserInfo userInfo, String env, Long installationId) {


      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT usr.DES_NICK_NAME " +
               "FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS instAlias " +
               "INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr " +
               "ON (INSTALIAS.SEQ_USER_ID = usr.SEQ_ID AND usr.BOL_ADMINISTRATOR = 1 ) " +
               "WHERE  instAlias.SEQ_INSTALLATION_ID = " + installationId;

         System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);
            String start = "<html><body>\n";
            String end = "</body></html>";
            String content = "";
            while( rs.next()){
               System.out.println("Result for installationId: " + installationId + " -> " + rs.getString(1) );

               content = content + userInfo.generateUserReport(dataSource, env,  rs.getString(1));

               content = content + "<br><br>";
            }

            appendToFile("report-"+installationId+"-.html", start + content + end);

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }*/
}
