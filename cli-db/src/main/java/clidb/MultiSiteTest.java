package clidb;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.core.MultivaluedMap;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;


public class MultiSiteTest extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(MultiSiteTest.class);



   public void main() {

      /*loadExternalFileProperties();
      DataSource dataSource = null;

      String jdbcUrl = "";
      String dbUsername = "";
      String password = "";

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);


         // select jdbc url
         if ("vpn".equals(connectionType)) {
            jdbcUrl = getProperty("vpn." + env + ".url");
         } else {
            jdbcUrl = getProperty("telepresence." + env + ".url");
         }

         dbUsername = getProperty(env + ".username");
         password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);



      } catch (Exception e) {
         printStackTrace(e);
         return;
      }*/

      try {

         BufferedReader br = new BufferedReader(new FileReader("/home/cristian/workspace/prosegur/prod/soporte/MultiSedeRefactor/userytokenProd.csv"));

         String sCurrentLine;

         int line = 0;

         runCommandLine("rm ./downladEventsResponse.sh");
         //runCommandLine("rm ./get40Responses.sh");
         //runCommandLine("rm ./testMultiSiteTimes.sh");


         
         while ((sCurrentLine = br.readLine()) != null) {

            line++;

            if (line > 2){

               String userName = sCurrentLine.substring(20, 64).trim();
               String token =  sCurrentLine.substring(84);

               String from = "1722880315994";
               String to = "1725472316001";


               getResponses("P" + (line -2), userName, token, from, to);
               //getTimeResponses("P" + (line -2), userName, token, from, to);


              /* appendToFile("compareMultiSite.sh","echo \"  \"\n");
               appendToFile("compareMultiSite.sh","echo \"userName: " +userName +"\"\n");
               appendToFile("compareMultiSite.sh","diff -q -s -u actualMultiSiteP" +"P" + (line -2)
                     +".json nuevoMultiSiteP"+ "P" + (line -2) +".json \n");*/

            }

         }

         System.out.println("end.");

      } catch (Exception e){
         printStackTrace(e);
      }




     // getCorporateToken();

    /*  Scanner scanner = new Scanner(new InputStreamReader(System.in));
      System.out.println("accessToken: ");
      String accessToken = scanner.nextLine();



      getUserToken("soporte.red@carglass.es", accessToken);
      getUserToken("camila.figueredo@biggie.com.py", accessToken);
      getUserToken("cesar.vera@puntofarma.com.py", accessToken);
      getUserToken("rgomezl@adeslasdental.com", accessToken);
      getUserToken("javier.piscoya@prosegur.com", accessToken);
      getUserToken("gperez@losheroes.cl", accessToken);
      getUserToken("michael.fernandez@pjchile.com", accessToken);

      runCommandLine("chmod +x ./generateTokens.sh");*/

      /*while (true) {
         try {



         } catch (Exception e) {
            printStackTrace(e);
         }


      }*/
   }


   public void compareMultiSiteJson() {


      try {


         BufferedReader br;
         String fileNameActual ;
         String fileNameNuevo;


        /* Scanner scanner = new Scanner(new InputStreamReader(System.in));
         System.out.println("corrida: ");
         String corrida = scanner.nextLine();*/

         for (int i=1; i<=40; i++){

            fileNameActual = "actualMultiSiteP"+ i+".json";
            fileNameNuevo = "nuevoMultiSiteP"+ i+".json";;

            System.out.println(fileNameActual + " - " + fileNameNuevo);

            try {
               //br = new BufferedReader(new FileReader("./Pruebas/" + corrida + "/" + fileNameActual));
               br = new BufferedReader(new FileReader(fileNameActual));
               String sCurrentLine;

               ArrayList<String> arrIdActual = new ArrayList<>();

               while ((sCurrentLine = br.readLine()) != null) {
                  if (sCurrentLine.contains("{\"id\":\"")) {
                     String id = sCurrentLine.substring(7, sCurrentLine.length() - 2);
                     arrIdActual.add(id);
                     //System.out.println(id);
                  }
               }
               br.close();

               //br = new BufferedReader(new FileReader("./Pruebas/"+ corrida + "/"  + fileNameNuevo));
               br = new BufferedReader(new FileReader(fileNameNuevo));

               ArrayList<String> arrIdNuevo = new ArrayList<>();

               while ((sCurrentLine = br.readLine()) != null) {
                  if (sCurrentLine.contains("{\"id\":\"")) {
                     String id = sCurrentLine.substring(7, sCurrentLine.length() - 2);
                     arrIdNuevo.add(id);
                  }
               }

               System.out.println("total events in actual: " + arrIdActual.size());
               System.out.println("total events in nuevo: " + arrIdNuevo.size());

               if (arrIdActual.size() == 0 && arrIdNuevo.size() == 0){
                  System.out.println("VACIO");
               } else {
                  ArrayList<String> arrIdEnActualNoenNuevo = new ArrayList<>();
                  for (String idActual : arrIdActual) {
                     if (!arrIdNuevo.contains(idActual)) {
                        arrIdEnActualNoenNuevo.add(idActual);
                     }
                  }

                  ArrayList<String> arrIdEnNuevoNoenActual = new ArrayList<>();
                  for (String idNuevo : arrIdNuevo) {
                     if (!arrIdActual.contains(idNuevo)) {
                        arrIdEnNuevoNoenActual.add(idNuevo);
                     }
                  }
                  System.out.println("total events in actual not in nuevo: " + arrIdEnActualNoenNuevo.size());
                  System.out.println("total events in nuevo not in actual: " + arrIdEnNuevoNoenActual.size());

                  if (arrIdEnActualNoenNuevo.size() == 0 && arrIdEnNuevoNoenActual.size() == 0 ){
                     System.out.println("IDENTICOS");
                  } else {
                     System.out.println("DIFERENCES");

                     if (arrIdEnActualNoenNuevo.size()>0) {
                        System.out.println("1) ids in Actual not in Nuevo, only 10");
                        int count = 1;
                        for (String value : arrIdEnActualNoenNuevo) {
                           count++;
                           System.out.println("     " + value);
                           if (count == 10) break;
                        }
                     }

                     if (arrIdEnNuevoNoenActual.size()>0) {
                        System.out.println("2) ids in Nuevo not in Actual, only 10");
                        int count = 1;
                        for (String value : arrIdEnNuevoNoenActual) {
                           count++;
                           System.out.println("     " + value);
                           if (count == 10) break;
                        }
                     }
                  }
               }


               System.out.println("------------------------------------------");
               System.out.println(" ");
            } catch (Exception e){
               System.out.println(e.getMessage());
            }
         }


         System.out.println("end.");

      } catch (Exception e){
         printStackTrace(e);
      }




      // getCorporateToken();

    /*  Scanner scanner = new Scanner(new InputStreamReader(System.in));
      System.out.println("accessToken: ");
      String accessToken = scanner.nextLine();



      getUserToken("soporte.red@carglass.es", accessToken);
      getUserToken("camila.figueredo@biggie.com.py", accessToken);
      getUserToken("cesar.vera@puntofarma.com.py", accessToken);
      getUserToken("rgomezl@adeslasdental.com", accessToken);
      getUserToken("javier.piscoya@prosegur.com", accessToken);
      getUserToken("gperez@losheroes.cl", accessToken);
      getUserToken("michael.fernandez@pjchile.com", accessToken);

      runCommandLine("chmod +x ./generateTokens.sh");*/

      /*while (true) {
         try {



         } catch (Exception e) {
            printStackTrace(e);
         }


      }*/
   }

   public String getCorporateToken(){


      String loginCorp="https://api-admin-smart.prosegur.cloud/smart-admin/ws/access/corporate/login";

      Client client = Client.create();
      WebResource webResource1 = client.resource(loginCorp);
      webResource1.header("accept", "application/json, text/plain, * /*");
      webResource1.header("accept-language", "es-419,es;q=0.5");
      webResource1.header("content-type", "application/json;charset=UTF-8");
      webResource1.header("origin", "https://admin-smart.prosegur.cloud");
      webResource1.header("priority", "u=1, i");
      webResource1.header("referer", "https://admin-smart.prosegur.cloud/");
      webResource1.header("sec-ch-ua", "\"Chromium\";v=\"124\", \"Brave\";v=\"124\", \"Not-A.Brand\";v=\"99\"");
      webResource1.header("sec-ch-ua-mobile", "?0");
      webResource1.header("sec-ch-ua-platform", "Linux");
      webResource1.header("sec-fetch-dest", "empty");
      webResource1.header("sec-fetch-mode", "cors");
      webResource1.header("sec-fetch-site", "same-site");
      webResource1.header("sec-gpc", 1 );
      webResource1.header("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36");


      MultivaluedMap formData = new MultivaluedMapImpl();
      formData.add("user", "ESX0005218");
      formData.add("password", "Anda#202407");
      formData.add("domain", "emea");
      formData.add("language", "es_ES");
      formData.add("loginAs", "true");


      ClientResponse response1 = webResource1.post(ClientResponse.class, formData);
      String tokenResponse = response1.getEntity(String.class);
      String tokenValue = tokenResponse.substring(17,71);

      System.out.println("tokenResponse: " + tokenResponse);



      return  tokenValue;

   }

   private void getResponses( String test,String userName, String token, String from, String to) {
      String scriptFile = "downladEventsResponse.sh";
      
      appendToFile(scriptFile,"echo \"  \"\n");
      appendToFile(scriptFile,"echo \" " + test +  " - " +userName +"\"\n");
      appendToFile(scriptFile,"echo \"  \"\n");
      appendToFile(scriptFile,"curl --request GET \\\n");
      appendToFile(scriptFile,"--url 'http://localhost:8118/activity-stream/event/from/" + from +"/to/" + to +"?limit=5000' \\\n");
      appendToFile(scriptFile,"--header 'Authorization: " + token+ "' \\\n");
      appendToFile(scriptFile,"--header 'x-smart-platform: Web' > nuevoMultiSite.json\n\n");

      appendToFile(scriptFile,"curl --request GET \\\n");
      appendToFile(scriptFile,"--url 'http://localhost:9119/activity-stream/event/from/" + from +"/to/" + to +"?limit=5000' \\\n");
      appendToFile(scriptFile,"--header 'Authorization: " + token+ "' \\\n");
      appendToFile(scriptFile,"--header 'x-smart-platform: Web' > actualMultiSite.json\n\n");

      appendToFile(scriptFile,"sed -e \"s/,/,\\n/g\" nuevoMultiSite.json > nuevoMultiSite" + test+ ".json\n");
      appendToFile(scriptFile,"sed -e \"s/,/,\\n/g\" actualMultiSite.json > actualMultiSite" + test+ ".json\n\n");



   }


   private void getTimeResponses( String test,String userName, String token, String from, String to) {
      String scriptFile = "testMultiSiteTimes.sh";

      appendToFile(scriptFile,"echo \"" + test + " - " +userName +";\"\n");

      appendToFile(scriptFile,"curl -o /dev/null -s -w '%{time_total}' \\\n");
      appendToFile(scriptFile,"--request GET \\\n");
      appendToFile(scriptFile,"--url 'http://localhost:8118/activity-stream/event/from/" + from +"/to/" + to +"?limit=5000' \\\n");
      appendToFile(scriptFile,"--header 'Authorization: " + token+ "' \\\n");
      appendToFile(scriptFile,"--header 'x-smart-platform: Web' \n\n");

      appendToFile(scriptFile,"echo \";\"\n");

   }

   public void testCrono() {

      ChronoMeter cronoMeter = new ChronoMeter();


      while (true) {
         cronoMeter.start();

         Scanner scannerOP = new Scanner(new InputStreamReader(System.in));
         String userName = scannerOP.nextLine();

         cronoMeter.stop();

         System.out.println(cronoMeter.getElapseTime() + " - " + cronoMeter.getElapseTimeLong());

         String userNames = scannerOP.nextLine();
      }

   }

}
