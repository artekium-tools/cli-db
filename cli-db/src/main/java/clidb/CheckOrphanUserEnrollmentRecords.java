package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class CheckOrphanUserEnrollmentRecords extends QryDB {

    private static final Logger LOGGER = LogManager.getLogger(CheckOrphanUserEnrollmentRecords.class);
    static int recordCount = 0;

    public int main(String env, String datafixReportFile, String reportDate) {

        loadExternalFileProperties();
        DataSource dataSource = null;

        try {

            if (env == null) env = getProperty("env");
            String connectionType = getProperty("connectionType");

            String jdbcUrl = getProperty(connectionType + "." + env + ".url");
            String dbUsername = getProperty(env + ".username");
            String password = getProperty(env + ".password");

            dataSource = getDataSource(jdbcUrl, dbUsername, password);

            System.out.println("Executing step 1: check orphan user enrollment records ..." );
            checkOrphanRecords(dataSource);

            System.out.println("Total record found: " + recordCount);

            appendToFile(datafixReportFile,
                    "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-44796'>" +
                          "17) CS " +
                            "https://jira.prosegur.com/browse/AM-44796</a> - " +
                            "USER-ENROLLMENT - Borrado de registros huerfanos<br>\n");

            if (recordCount > 0) {
                appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                        ", se fixearon " + recordCount + " casos. <br>\n");
            } else {
                appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                        ", no se encontraron casos a fixear<br>\n");
            }

        } catch (Exception e){
            printStackTrace(e);
        }

        return recordCount;
    }

    public static void checkOrphanRecords(final DataSource dataSource) {

        try {
            Connection con = dataSource.getConnection();
            Statement stmt = null;
            ResultSet rs = null;

            try {

                String sql1 = "SELECT enroll.SEQ_ID " +
                    "FROM \"USER-ENROLLMENT\".SMPR_TUSER_ENROLLMENT enroll " +
                    "      WHERE " +
                    "NOT EXISTS ( " +
                    "      SELECT * FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER st " +
                    "       WHERE st.des_nick_name = enroll.DES_USER_NAME AND " +
                    "       st.DES_CLIENT_ID = enroll.DES_CLIENT_ID AND " +
                    "       st.DES_COUNTRY = enroll.DES_COUNTRY_ID)" +
                    "AND " +
                    "NOT EXISTS (" +
                    "       SELECT * " +
                    "       FROM \"USER-ENROLLMENT\".SMPR_TUSER_EMAIL_CHANGE echange " +
                    "       WHERE echange.DES_OLD_EMAIL = enroll.DES_USER_NAME " +
                    "       OR echange.DES_NEW_EMAIL = enroll.DES_USER_NAME)";

                stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                rs = stmt.executeQuery(sql1);

                while (rs.next()) {
                    String deleteStmt = "DELETE FROM SMPR_TUSER_ENROLLMENT WHERE SEQ_ID = " + rs.getLong(1) + ";";
                    appendToFile("deleteOrphanUserEnrollment.sql",  deleteStmt + "\n");

                    recordCount ++;
                }

            } catch (Exception e) {
                System.out.println(" ");
                System.out.println(e.getMessage());
                printStackTrace(e);
            } finally {
                try {
                    if (rs != null) rs.close();
                    if (stmt != null) stmt.close();
                    if (con != null) con.close();
                } catch (Exception e) {
                    System.out.println(" ");
                    System.out.println(e.getMessage());
                    printStackTrace(e);
                }
            }

        } catch (Exception e1) {
            System.out.println(" ");
            System.out.println(e1.getMessage());
            printStackTrace(e1);
        }
    }
}
