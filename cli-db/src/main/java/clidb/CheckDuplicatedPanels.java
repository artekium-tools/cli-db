package clidb;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import javax.ws.rs.core.MultivaluedMap;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Properties;


public class CheckDuplicatedPanels extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(CheckDuplicatedPanels.class);

   static ArrayList<Long> arr_seq_installation_id = new ArrayList<>();
   String lista_instal = "";
   String lista_cod_connection = "";

   public int main(String env, String datafixReportFile, String reportDate) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         System.out.println("Executing step 1: select duplicated panels ...");
         selectDuplicatedPanels(dataSource);

         String token = getCRMToken(env);

         String strDate = new SimpleDateFormat("YYYYMMdd").format(new java.util.Date());

         String reportFileName = "clientes_procesados_" + strDate + ".csv";
         appendToFile(reportFileName, "COUNTRY,CLIENT_ID,CONTRACT_NUMBER\n");

         System.out.println("\nExecuting step 2: select installations data ...");
         for (Long installationId : arr_seq_installation_id) {
            // Buscar la data de cada instalacion
            // Pegarle a siebel para ver cual corresponde borrar
            checkContract(dataSource, installationId, env, token, reportFileName);
         }

         if (!"".equals(lista_instal)) {

            lista_instal = lista_instal.substring(0, lista_instal.lastIndexOf(","));
            lista_cod_connection = lista_cod_connection.substring(0, lista_cod_connection.lastIndexOf(","));

            // Generar delete stamtement
            System.out.println("\nExecuting step 3: generate delete statements ...");
            generateDeleteStatements(dataSource);
            System.out.println("\nReport file generated: " + reportFileName);

         }

         appendToFile(datafixReportFile,
               "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-41075'>" +
                     "6)  " +
                     "https://jira.prosegur.com/browse/AM-41075 </a><br>\n");


         if (arr_seq_installation_id.size() > 0){
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", se fixearon " + arr_seq_installation_id.size() + " casos. <br>\n");
            appendToFile(datafixReportFile,  "NOTA: cargar en jira el .csv del dia<br>\n");
         } else {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", no se encontraron casos a fixear<br>\n");
         }



      } catch (Exception e){
         printStackTrace(e);
      }

      LOGGER.info("End process.");

      return arr_seq_installation_id.size();
   }



   public static void selectDuplicatedPanels(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT SEQ_INSTALLATION_ID, SEQ_ENTITY_TYPE, COUNT(*) AS cantidad_repeticiones " +
               "FROM DEVICE.SMPR_TTHING st " +
               "WHERE SEQ_ENTITY_TYPE = 13 " +
               "GROUP BY SEQ_INSTALLATION_ID, SEQ_ENTITY_TYPE " +
               "HAVING COUNT(*) > 1";

         //System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            //System.out.println("Adding installations .."  );
            int rec = 0;
            while (rs.next()) {
               arr_seq_installation_id.add(rs.getLong(1));
               rec ++;
               printInLine("record: " + rec);
            }

            //System.out.println("\nEnd Adding installaitons .." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }



   public void checkContract(final DataSource dataSource, Long installationId, String env, String token, String reportFileName) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT su.DES_CLIENT_ID, si.DES_COUNTRY, si.DES_CONTRACT_NUMBER " +
               "FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER su " +
               "INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION si ON si.SEQ_ADMIN = su.SEQ_ID " +
               "WHERE si.FYH_DELETED IS NULL AND si.SEQ_ID = " + installationId;

         //System.out.println(sql);

         try {
            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            String clientId = null;
            String desCountry = null;
            String desContractNumber = null;
            String codConnectionId = null;

            boolean poseeElContrato = false;

            if (rs.next()) {
               clientId = rs.getString(1);
               desCountry = rs.getString(2);
               desContractNumber = rs.getString(3);

               printInLine("checking contract: " + desContractNumber + "               ");

               String json = getCRMInstallation(env, token, desCountry, clientId);

               if (json!= null) {
                  String[] arrlines = json.split("\n");
                  for (String line: arrlines){
                     if (line.trim().contains("contractNumber")){
                        String siebelContractNumber = line.substring(line.indexOf(":") + 1, line.length() -1);
                        siebelContractNumber = siebelContractNumber.replaceAll("\"", "").trim();

                        if (desContractNumber.equals(siebelContractNumber)){
                           poseeElContrato = true;
                        }
                        //System.out.println("siebelContractNumber: @" + siebelContractNumber + "@");
                     }

                     if (poseeElContrato){
                        if (line.trim().contains("connectionId")) {
                           codConnectionId = line.substring(line.indexOf(":") + 1, line.length() - 1);
                           codConnectionId = codConnectionId.replaceAll("\"", "").trim();
                           break;
                        }
                     }
                  }
               }

               if (poseeElContrato){
                  lista_instal += installationId + ",";
                  lista_cod_connection += "'" + codConnectionId + "'"+ ",";

                  //System.out.println(desCountry +  "," + clientId + "," + desContractNumber + "," + installationId + "," + codConnectionId);

                  appendToFile(reportFileName, desCountry +"," +  clientId + "," + desContractNumber + "\n");
               }

               //System.out.println("lista_instal: @" + lista_instal + "@");
               //System.out.println("lista_cod_connection: @" + lista_cod_connection + "@");


            } else {
               System.out.println("No result for installation: " + installationId);
            }


         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }


   public void generateDeleteStatements(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql1 = "SELECT SEQ_ID, SEQ_INSTALLATION_ID,COD_CONNECTION_ID, SEQ_TYPE_ID " +
                  "FROM DEVICE.SMPR_TTHING st "+
                  "WHERE SEQ_ENTITY_TYPE = 13 " +
                  "AND SEQ_INSTALLATION_ID IN (" +lista_instal +")" +
                  "AND COD_CONNECTION_ID NOT IN (" + lista_cod_connection +")";

            //System.out.println(sql1);

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql1);

            String fileName = "purgeDuplicatedPanels.sql";

            String lista_paneles = "";
            String lista_mv = "";

            while (rs.next()) {
               lista_paneles += rs.getLong(1) + ","; // thing.SEQ_ID
               Long installationId = rs.getLong(2);

               lista_mv += getMotionViewer(dataSource, installationId);
            }

            lista_paneles = lista_paneles.substring(0, lista_paneles.lastIndexOf(","));
            lista_mv = lista_mv.substring(0, lista_mv.lastIndexOf(","));

           // System.out.println("lista_paneles: " + lista_paneles);
           // System.out.println("lista_mv: " + lista_mv);




            appendToFile(fileName, "--DELETE THING_ALIAS AND THING FOR PANELS AND CHILDS (DEVICE, FILTER-DISPATCHER, PANIC-BUTTON, RULE-ENGINE, SCENE, SYSLOG)\n");
            appendToFile(fileName, "schemas:DEVICE,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SCENE,SYSLOG\n");
            appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_PARENT_ID IN (SELECT SEQ_ID FROM SMPR_TTHING_ALIAS sta2 WHERE SEQ_THING_ID IN ("+ lista_paneles +"));\n");
            appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_THING_ID IN ("+ lista_paneles +");\n");
            appendToFile(fileName, "DELETE FROM SMPR_TTHING sta WHERE SEQ_PARENT_ID IN ("+ lista_paneles +");\n");
            appendToFile(fileName, "DELETE FROM SMPR_TTHING sta WHERE SEQ_ID IN ("+ lista_paneles +");\n");
            appendToFile(fileName, "--DELETE THING_ALIAS AND THING FOR MVs AND CHILDS (DEVICE, FILTER-DISPATCHER, PANIC-BUTTON, RULE-ENGINE, SCENE, SYSLOG)\n");
            appendToFile(fileName, "schemas:DEVICE,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SCENE,SYSLOG\n");
            appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_PARENT_ID IN (SELECT SEQ_ID FROM SMPR_TTHING_ALIAS sta2 WHERE SEQ_THING_ID IN ("+ lista_mv +"));\n");
            appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_THING_ID IN ("+ lista_mv +");\n");
            appendToFile(fileName, "DELETE FROM SMPR_TTHING sta WHERE SEQ_PARENT_ID IN ("+ lista_mv +");\n");
            appendToFile(fileName, "DELETE FROM SMPR_TTHING sta WHERE SEQ_ID IN ("+ lista_mv +");\n");

            appendToFile(fileName, "--\n");


            appendToFile(fileName, "--DEACTIVATE THING_ALIAS AND THING FOR PANELS AND CHILDS (ACCOUNT-MANAGEMENT, EVENT-PROCESSOR)\n");
            appendToFile(fileName, "schemas:ACCOUNT-MANAGEMENT,EVENT-PROCESSOR\n");
            appendToFile(fileName, "UPDATE SMPR_TTHING_ALIAS SET BOL_IS_ACTIVE = 0 WHERE SEQ_PARENT_ID IN (SELECT SEQ_ID FROM SMPR_TTHING_ALIAS sta2 WHERE SEQ_THING_ID IN ("+ lista_paneles +"));\n");
            appendToFile(fileName, "UPDATE SMPR_TTHING_ALIAS SET BOL_IS_ACTIVE = 0 WHERE SEQ_THING_ID IN ("+ lista_paneles +");\n");
            appendToFile(fileName, "UPDATE SMPR_TTHING SET BOL_IS_ACTIVE = 0 WHERE SEQ_PARENT_ID IN ("+ lista_paneles +");\n");
            appendToFile(fileName, "UPDATE SMPR_TTHING SET BOL_IS_ACTIVE = 0 WHERE SEQ_ID IN ("+ lista_paneles +");\n");
            appendToFile(fileName, "--DEACTIVATE THING_ALIAS AND THING FOR MVs AND CHILDS (ACCOUNT-MANAGEMENT, EVENT-PROCESSOR)\n");
            appendToFile(fileName, "schemas:ACCOUNT-MANAGEMENT,EVENT-PROCESSOR\n");
            appendToFile(fileName, "UPDATE SMPR_TTHING_ALIAS SET BOL_IS_ACTIVE = 0 WHERE SEQ_PARENT_ID IN (SELECT SEQ_ID FROM SMPR_TTHING_ALIAS sta2 WHERE SEQ_THING_ID IN ("+ lista_mv +"));\n");
            appendToFile(fileName, "UPDATE SMPR_TTHING_ALIAS SET BOL_IS_ACTIVE = 0 WHERE SEQ_THING_ID IN ("+ lista_mv +");\n");
            appendToFile(fileName, "UPDATE SMPR_TTHING SET BOL_IS_ACTIVE = 0 WHERE SEQ_PARENT_ID IN ("+ lista_mv +");\n");
            appendToFile(fileName, "UPDATE SMPR_TTHING SET BOL_IS_ACTIVE = 0 WHERE SEQ_ID IN ("+ lista_mv +");\n");
            appendToFile(fileName, "--Process end.");


         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }
      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }

   public String getMotionViewer(final DataSource dataSource, Long installationId) {

      String retValue = "";
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql1 = "SELECT COUNT(SEQ_ID) AS cant_mv FROM DEVICE.SMPR_TTHING " +
                  "WHERE SEQ_ENTITY_TYPE = 14 AND SEQ_INSTALLATION_ID = " + installationId;

            String sql2 = "SELECT MIN(SEQ_ID) AS thing_id FROM DEVICE.SMPR_TTHING " +
                  "WHERE SEQ_ENTITY_TYPE = 14 AND SEQ_INSTALLATION_ID = " + installationId;


            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql1);


            if (rs.next()) {
               if (rs.getInt(1) > 1){

                     rs = stmt.executeQuery(sql2);

                     if (rs.next()){

                        retValue = rs.getLong(1) + ",";
                     }
               }
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }
      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return retValue;
   }


   public String getCRMToken(String env){
      String tokenValue = null;
      try {
         String host= getProperty(env + ".crm_host");

         String tokenUrl = host + "/api/oauth/token";

         Client client = Client.create();
         WebResource webResource1 = client.resource(tokenUrl);
         webResource1.header("Content-Type", "application/x-www-form-urlencoded");

         MultivaluedMap formData = new MultivaluedMapImpl();
         formData.add("scope", "alarms.write, alarms.read");
         formData.add("client_secret", getProperty(env + ".client_secret"));
         formData.add("client_id", getProperty(env + ".client_id"));
         formData.add("grant_type", "client_credentials");

         ClientResponse response1 = webResource1.post(ClientResponse.class, formData);
         String tokenResponse = response1.getEntity(String.class);
         tokenValue = tokenResponse.substring(17,71);

      } catch (Exception e) {
         printStackTrace(e);
      }

      return  tokenValue;
   }


   public String getCRMInstallation(String env, String tokenValue, String desCountry, String desClientId){
      String content = null;
      try {
         String host= getProperty(env + ".crm_host");

         String url= host + "/alarms/smart";
         String getInstallationUrl = url + "/v1/country/"+desCountry+"/client/"+desClientId+"/installation";

         Client client2 = Client.create();

         //System.out.println("get Installation from crm: " + getInstallationUrl);

         WebResource webResource2 = client2.resource(getInstallationUrl);
         WebResource.Builder builder =  webResource2.getRequestBuilder();

         builder
               .header("authorization", "Bearer " + tokenValue)
               .header("Content-Type", "application/json")
               .header("ExtSys", "smart");

         ClientResponse response2 = builder.get(ClientResponse.class);
         String result = response2.getEntity(String.class);

         content = formatJson(result);
      } catch (Exception e) {
         printStackTrace(e);
      }
      //System.out.println(content);
      return content;
   }


}
