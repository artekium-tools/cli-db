package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;


public class CheckCMSession extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(CheckCMSession.class);

   static boolean UPDATE_IN_TEST_MODE = false; // true=olny shows logs  false=execute updates

   static Properties prop = new Properties();
   static Properties scriptProperties = new Properties();
   static ArrayList<String> arr_userNames = new ArrayList<>();


 /*  public void main(String env) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);
         System.out.println("connectionType: " + connectionType);

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         System.out.println("Init Process: " );

         System.out.println("Checking on CAMERA-MANAGER " );
         selectUserNames(dataSource);
         int count = 0;

         for (String userName : arr_userNames) {
            count ++;

            printInLine("checking nro: " + count + " userName : " + userName);
            checkUserName(dataSource, userName);
         }

         System.out.println("End Process");
      } catch (Exception e){
         printStackTrace(e);
      }

      LOGGER.info("End process.");

      return;
   }

   public void mainCMSession(String env) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);
         System.out.println("connectionType: " + connectionType);

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         System.out.println("Init Process: " );

         System.out.println("Checking on CAMERA-MANAGER " );
         selectUserNamesCMSession(dataSource);
         int count = 0;

         for (String userName : arr_userNames) {
            count ++;

            printInLine("checking nro: " + count + " of " + arr_userNames.size() + " userName : " + userName);
            checkUserNameCMSession(dataSource, userName);
         }

         System.out.println("End Process");
      } catch (Exception e){
         printStackTrace(e);
      }

      LOGGER.info("End process.");

      return;
   }*/




   public int fixCMSession(String env, String userToLoginFile, String datafixReportFile, String reportDate) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         System.out.println("Executing step 1: check CodeUser On CMSession  ..." );
         checkCodeUserOnCMSession(dataSource);

         System.out.println("Total users found: " + arr_userNames.size());

         appendToFile(userToLoginFile, "-- fixCameraManagerSessionClientId AM-39999 \n");

         appendToFile(datafixReportFile,
               "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-39999'>" +
                     "5) CS " +
                     "https://jira.prosegur.com/browse/AM-39999</a> - " +
                     "Credenciales donde no corresponde el clientId <br>\n");


         if (arr_userNames.size() > 0) {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", se fixearon " + arr_userNames.size() + " casos. <br>\n");
            for (String userName:arr_userNames){
               //System.out.println(userName);
               appendToFile(userToLoginFile, userName + "\n");
               appendToFile(datafixReportFile, userName + "<br>\n");
            }
         } else {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", no se encontraron casos a fixear<br>\n");
         }




      } catch (Exception e){
         printStackTrace(e);
      }


      return arr_userNames.size();
   }


  /* public void checkCMSession(String env) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);
         System.out.println("connectionType: " + connectionType);

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         System.out.println("Init Process: " );

         System.out.println("Start line: ");
         Scanner scanner2 = new Scanner(new InputStreamReader(System.in));
         String startLineStr = scanner2.nextLine();
         System.out.println(" ");

         System.out.println("Checking on CAMERA-MANAGER " );
         selectUserNamesCMSessionFromList(dataSource);
         int count = 0;

         int startLine;
         try {
            startLine = Integer.valueOf(startLineStr);
         } catch (Exception e) {
            startLine = 0;
         }

         for (String userName : arr_userNames) {
            count ++;

            if (count >= startLine) {
               printInLine("checking nro: " + count + " of " + arr_userNames.size() + " userName : " + userName);
               checkCodeUserOnCMSession(dataSource, userName);
            }
         }

         System.out.println("End Process");
      } catch (Exception e){
         printStackTrace(e);
      }

      LOGGER.info("End process.");

      return;
   }*/

  /* public static void selectUserNames(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT COD_USER FROM ( " +
               "SELECT upper (COD_USER) AS cod_user, count (upper (COD_USER)) " +
               "FROM  \"CAMERAMANAGER\".SMPR_TTHIRDPARTYSESSION " +
               "GROUP BY upper (COD_USER) " +
               "HAVING count (upper (COD_USER)) > 1)";


         System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            System.out.println("Adding userNames .."  );
            int rec = 0;
            while (rs.next()) {
               arr_userNames.add(rs.getString(1));
               rec ++;
               printInLine("record: " + rec);
            }

            System.out.println("\nEnd Adding userNames .." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }*/

  /* public static void selectUserNamesCMSession(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT COD_USER FROM ( " +
               "SELECT upper (COD_USER) AS cod_user, count (upper (COD_USER)) " +
               "FROM  \"CAMERAMANAGER\".SMPR_TCAMERAMANAGER_SESSION " +
               "GROUP BY upper (COD_USER) " +
               "HAVING count (upper (COD_USER)) > 1)";


         System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            System.out.println("Adding userNames .."  );
            int rec = 0;
            while (rs.next()) {
               arr_userNames.add(rs.getString(1));
               rec ++;
               printInLine("record: " + rec);
            }

            System.out.println("\nEnd Adding userNames .." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }*/

 /*  public static void selectUserNamesCMSessionFromList(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT COD_USER  " +
               "FROM  \"CAMERAMANAGER\".SMPR_TCAMERAMANAGER_SESSION " +
               "WHERE SEQ_ID IN ( " +
               "18680,214572,40123,57342,122387,122171,49063,109011,120597,149095,198077,201825,6330,65815,102591,208501,114786,127595,123573,124543,151060,197879,13604,20219,40100,104998,159325,40100,104998,159325,217196,203920,86008,90914,93016,195531,225116,53046,53881,72349,81198,84346,160892,190371,215260" +
               ")";


         System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            System.out.println("Adding userNames .."  );
            int rec = 0;
            while (rs.next()) {
               arr_userNames.add(rs.getString(1));
               rec ++;
               printInLine("record: " + rec);
            }

            System.out.println("\nEnd Adding userNames .." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }*/

 /*  public static void selectCodUserFromCMSession(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT DISTINCT COD_USER FROM \"CAMERAMANAGER\".SMPR_TCAMERAMANAGER_SESSION";

         / *
         SELECT cm.SEQ_ID, cm.DES_EMAIL, usr.DES_CLIENT_ID , SUBSTR(cm.DES_EMAIL , 3,  INSTR( cm.DES_EMAIL, '@', 1) -3 )
FROM  "CAMERAMANAGER".SMPR_TCAMERAMANAGER_SESSION cm
INNER JOIN "ACCOUNT-MANAGEMENT".SMPR_TUSER usr ON (UPPER(usr.DES_NICK_NAME) = UPPER(cm.cod_user))
WHERE UPPER(usr.DES_CLIENT_ID) <> UPPER(SUBSTR(cm.DES_EMAIL , 3,  INSTR( cm.DES_EMAIL, '@', 1) -3 ))

          *
          /

         System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            System.out.println("Adding userNames .."  );
            int rec = 0;
            while (rs.next()) {
               arr_userNames.add(rs.getString(1));
               rec ++;
               printInLine("record: " + rec);
            }

            System.out.println("\nEnd Adding userNames .." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }*/


 /*  public static void checkUserName(final DataSource dataSource, String userName) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql1 = "SELECT * FROM  \"CAMERAMANAGER\".SMPR_TTHIRDPARTYSESSION WHERE UPPER(COD_USER) = '" + userName + "'";

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql1);

            int size =0;
            if (rs != null){
               rs.beforeFirst();
               rs.last();
               size = rs.getRow();
            }
            rs.beforeFirst();

            //1) Si el primer resultado es un DES_MIGRATION_STATUS = OK - No hacemos nada
            System.out.println("size: "+size);

            int count=0;
            String desToken1 = "";
            while (rs.next()) {
               // SEQ_ID|COD_USER |DES_PLATFORM    |DES_TOKEN    |FYH_EXPIRATION_DATE|
               // Si está en mayuscula lo agrego para borrarlo
               // Si tiene distintos DES_TOKEN lo guardo en un file para analizar
                count ++;

                if (count ==1){
                   desToken1 = rs.getString(4);
                } else {
                   if (!desToken1.equals(rs.getString(4))){
                      appendToFile("casosAnalisis.txt",  userName + "\n");
                   }
                }

                // Si es el registro en mayuscula lo borramos
                if (userName.equals(rs.getString(2))){
                   String deleteStmt = "DELETE FROM SMPR_TTHIRDPARTYSESSION WHERE seq_id = " + rs.getLong(1) + ";";
                   appendToFile("deleteThirPartySession.sql",  deleteStmt + "\n");
                }
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }*/

 /*  public static void checkUserNameCMSession(final DataSource dataSource, String userName) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql1 = "SELECT * FROM  \"CAMERAMANAGER\".SMPR_TCAMERAMANAGER_SESSION WHERE UPPER(COD_USER) = '" + userName + "'";

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql1);

            int size =0;
            if (rs != null){
               rs.beforeFirst();
               rs.last();
               size = rs.getRow();
            }
            rs.beforeFirst();

            //1) Si el primer resultado es un DES_MIGRATION_STATUS = OK - No hacemos nada
            System.out.println("size: "+size);

            int count=0;
            String desToken1 = "";
            while (rs.next()) {
               // SEQ_ID|DES_EMAIL                 |DES_COUNTRY|COD_USER                                 |DES_ADMIN_EMAIL           |DES_TOKEN                                 |DES_ACCOUNT_ID|DES_USER_ID|FYH_EXPIRATION_DATE    |BOL_ADMIN|
               // Si está en mayuscula lo agrego para borrarlo
               // Si tiene distintos DES_TOKEN lo guardo en un file para analizar
               count ++;

               // Si es el registro en mayuscula lo borramos
               if (userName.equals(rs.getString(4))){
                  String deleteStmt = "DELETE FROM SMPR_TCAMERAMANAGER_SESSION WHERE seq_id = " + rs.getLong(1) + ";";
                  appendToFile("deleteCameraManagareSession.sql",  deleteStmt + "\n");
               }
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }*/

   public static void checkCodeUserOnCMSession(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql1 = "SELECT cm.SEQ_ID, usr.DES_NICK_NAME, cm.DES_EMAIL, usr.DES_CLIENT_ID , SUBSTR(cm.DES_EMAIL , 3,  INSTR( cm.DES_EMAIL, '@', 1) -3 ) " +
                  "FROM  \"CAMERAMANAGER\".SMPR_TCAMERAMANAGER_SESSION cm " +
                  "INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr ON (UPPER(usr.DES_NICK_NAME) = UPPER(cm.cod_user)) " +
                  "WHERE UPPER(usr.DES_CLIENT_ID) <> UPPER(SUBSTR(cm.DES_EMAIL , 3,  INSTR( cm.DES_EMAIL, '@', 1) -3 ))";

            //System.out.println(sql1);

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql1);


            while (rs.next()) {
               // SEQ_ID| DES_EMAIL |DES_CLIENT_ID
               // Si tiene des_token NULL lo agrego para borrarlo
               // Si el client_id en des_mail distinto al del user, lo borramos

               //System.out.println(" desEmail: "+rs.getString(3) + " clientId: " +rs.getString(4));

               String deleteStmt = "DELETE FROM SMPR_TCAMERAMANAGER_SESSION WHERE seq_id = " + rs.getLong(1) + ";";
               appendToFile("deleteCameraManagareSessionDiffClientId.sql",  deleteStmt + "\n");

               String delete3PStmt = "DELETE FROM SMPR_TTHIRDPARTYSESSION WHERE UPPER(COD_USER) = UPPER('" + rs.getString(2) + "');";
               appendToFile("deleteThirdPartySessionDiffClientId.sql",  delete3PStmt + "\n");

               arr_userNames.add(rs.getString(2));
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }


}
