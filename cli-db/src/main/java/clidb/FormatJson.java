package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class FormatJson extends QryDB {


   private  final Logger LOGGER = LogManager.getLogger(FormatJson.class);


   public void main(String fileName, String outputFilename) {

      try {

         BufferedReader br = new BufferedReader(new FileReader(fileName));
         String content = "";
         String sCurrentLine;
         int line= 0;
         while ((sCurrentLine = br.readLine()) != null) {
            line++;
            content = content + sCurrentLine;
            printInLine("read line: " + line);
         }

         //sCurrentLine = sCurrentLine.replaceAll(",", ",\n");

         //runCommandLine("rm fileName");

         //System.out.println(formatJSONStr(sCurrentLine, 2));

         String jsonFormatted = formatJSONStr(content, 2);

        /* Scanner scanner1 = new Scanner(new InputStreamReader(System.in));
         System.out.println(" ");
         System.out.println("output filename: ");
         String outputFilename = scanner1.nextLine();*/

         appendToFile(outputFilename + ".json" ,  jsonFormatted);
         System.out.println(outputFilename + ".json formatted OK.");



      } catch (Exception e) {

      }



   }

   public static String formatJSONStr(final String json_str, final int indent_width) {
      final char[] chars = json_str.toCharArray();
      final String newline = System.lineSeparator();

      String ret = "";
      boolean begin_quotes = false;

      for (int i = 0, indent = 0; i < chars.length; i++) {

         printInLine("format char " + i + " of " + chars.length);

         char c = chars[i];

         if (c == '\"') {
            ret += c;
            begin_quotes = !begin_quotes;
            continue;
         }

         if (!begin_quotes) {
            switch (c) {
               case '{':
               case '[':
                  ret += c + newline + String.format("%" + (indent += indent_width) + "s", "");
                  continue;
               case '}':
               case ']':
                  ret += newline + ((indent -= indent_width) > 0 ? String.format("%" + indent + "s", "") : "") + c;
                  continue;
               case ':':
                  ret += c + " ";
                  continue;
               case ',':
                  ret += c + newline + (indent > 0 ? String.format("%" + indent + "s", "") : "");
                  continue;
               default:
                  if (Character.isWhitespace(c)) continue;
            }
         }

         ret += c + (c == '\\' ? "" + chars[++i] : "");
      }

      return ret;
   }

}
