package clidb;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class CheckCMRepeatedCredentials extends QryDB {

    static Integer ocurrences = 0;

    public int fixCMRepeatedCredentials(String env, String datafixReportFile, String reportDate)  {

        loadExternalFileProperties();

        DataSource dataSource = null;

        try {

            if (env == null) env = getProperty("env");
            String connectionType = getProperty("connectionType");

            String jdbcUrl = getProperty(connectionType + "." + env + ".url");
            String dbUsername = getProperty(env + ".username");
            String password = getProperty(env + ".password");

            dataSource = getDataSource(jdbcUrl, dbUsername, password);

            System.out.println("Executing step 1: check repeated credentials in CM  ..." );

            appendToFile(datafixReportFile,
                  "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-43282'>" +
                        "9) MC " +
                        "https://jira.prosegur.com/browse/AM-43282</a> - " +
                        "Usuarios con mas de una credencial en CM<br>\n");

            checkRepeatedCredentials(dataSource, datafixReportFile, reportDate);

            System.out.println("Total duplicated credentials found: " + ocurrences.toString());

        } catch (Exception e){
            printStackTrace(e);
        }


        return ocurrences;
    }

    public static void checkRepeatedCredentials(final DataSource dataSource, String datafixReportFile, String reportDate) {

        try {
            Connection con = dataSource.getConnection();
            Statement stmt = null;
            ResultSet rs = null;

            try {

                String sqlSelectCant = "SELECT COUNT(*) FROM (SELECT DES_EMAIL, COUNT(*) AS CANT " +
                        "FROM CAMERAMANAGER.SMPR_TCAMERAMANAGER_SESSION " +
                        "GROUP BY DES_EMAIL) WHERE CANT > 1";

                stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                rs = stmt.executeQuery(sqlSelectCant);

                rs.next();
                int cant = rs.getInt(1);

                if (cant > 0) {

                    ocurrences = cant;

                    // Get SEQ_ID of repeated credentials
                    String sqlSelectCredentialsIds = "SELECT DISTINCT SEQ_ID " +
                        "FROM (SELECT * FROM CAMERAMANAGER.SMPR_TCAMERAMANAGER_SESSION sts " +
                        "WHERE SEQ_ID NOT IN (SELECT sts.SEQ_ID FROM CAMERAMANAGER.SMPR_TCAMERAMANAGER_SESSION sts " +
                        "INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TUSER st ON sts.COD_USER = st.DES_NICK_NAME " +
                        "WHERE st.BOL_ADMINISTRATOR = 1))";

                    // Execute query
                    stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                    rs = stmt.executeQuery(sqlSelectCredentialsIds);

                    String fileName = "fixCMRepeatedCredentials.sql";
                    appendToFile(fileName, "schemas:CAMERAMANAGER\n");
                    String idsToDelete = "";
                    int cont = 0;

                    while (rs.next()) {
                        cont++;
                        idsToDelete = idsToDelete + rs.getString(1) + ",";
                        if (cont == 1000) {
                            idsToDelete = idsToDelete.substring(0, idsToDelete.length() - 1);
                            appendToFile(fileName, "DELETE FROM SMPR_TCAMERAMANAGER_SESSION sta WHERE SEQ_ID IN ("+ idsToDelete +");\n");
                            idsToDelete = "";
                            cont = 0;
                        }
                    }

                    if (!idsToDelete.isEmpty()) {
                        idsToDelete = idsToDelete.substring(0, idsToDelete.length() - 1);
                        appendToFile(fileName, "DELETE FROM SMPR_TCAMERAMANAGER_SESSION sta WHERE SEQ_ID IN ("+ idsToDelete +");\n");
                    }


                    appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                          ", se fixearon " + ocurrences +
                          " casos. <br><br>\n");

                } else {
                    appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate +
                          ", no se encontraron casos a fixear <br>\n");
                    System.out.println("Duplicated CM credentials not found!");
                }

                System.out.println("Process end.");

            } catch (Exception e) {
                System.out.println(" ");
                System.out.println(e.getMessage());
                printStackTrace(e);
            } finally {
                try {
                    if (rs != null) rs.close();
                    if (stmt != null) stmt.close();
                    if (con != null) con.close();
                } catch (Exception e) {
                    System.out.println(" ");
                    System.out.println(e.getMessage());
                    printStackTrace(e);
                }
            }

        } catch (Exception e1) {
            System.out.println(" ");
            System.out.println(e1.getMessage());
            e1.printStackTrace();
        }
    }
}

