package clidb;

import clidb.entities.EventDaoJdbcImpl;
import clidb.entities.EventIndividualFiltersDto;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TestEventDao extends QryDB {


   private  final Logger LOGGER = LogManager.getLogger(TestEventDao.class);

   public void main() {

      loadExternalFileProperties();

      String jdbcUrl = "";
      String dbUsername = "";
      String password = "";

      String env = "pro";
      if (env == null) env = getProperty("env");
      String connectionType = getProperty("connectionType");


      // select jdbc url
      if ("vpn".equals(connectionType)) {
         jdbcUrl = getProperty("vpn." + env + ".url");
      } else {
         jdbcUrl = getProperty("telepresence." + env + ".url");
      }

      dbUsername = "EVENT-PROCESSOR" ;//getProperty(env + ".username");
      password = getProperty(env + ".EVENT-PROCESSOR" +".password");

      DataSource dataSource;
      try {
         dataSource = getDataSource(jdbcUrl, dbUsername, password);


         EventDaoJdbcImpl eventDaoJdbc = new EventDaoJdbcImpl();

         Long endDate = Math.abs(System.currentTimeMillis());
         long fromDate = LocalDateTime.now().minusDays(30).atZone(ZoneId.of("Z")).toEpochSecond() * 1000;
         Timestamp fromTS = new Timestamp(fromDate);
         Timestamp toTS = new Timestamp(endDate);
         String country = "UY";
         Long userId = 1810803L;
         int limit = 50;

         Scanner scannerOP = new Scanner(new InputStreamReader(System.in));

         ChronoMeter cronoMeter = new ChronoMeter();

         while (true) {
            System.out.println(" ");
            System.out.println("0- Custom");
            System.out.println("1- Rsi Confort Sin Filtro");
            System.out.println("2- Rsi Confort Pircam1");
            System.out.println("3- Rsi Confort Pircam1 y 2");
            System.out.println("4- Rsi Confort Pircam1 y Pircam2 y panel");
            System.out.println("------ ");
            System.out.println("5- RSI sin filtro");
            System.out.println("6- RSI 1 detector");
            System.out.println("7- RSI todos detector");
            System.out.println("8- RSI 1 camara");
            System.out.println("9- RSI todas las camara");
            System.out.println("10- RSI 1 dvr");
            System.out.println("11- RSI todos los dvr");
            System.out.println("12- RSI todo video camara y dvr");
            System.out.println("13 - RSI panel y 1 detector");
            System.out.println("14 - RSI panel y todos los detectores");
            System.out.println("15 - RSI panel y una camara");
            System.out.println("16 - RSI panel y todas las camaras");
            System.out.println("17 - RSI panel y un dvr");
            System.out.println("18 - RSI panel y todos los dvr");
            System.out.println("19 - RSI panel y todos los video");
            System.out.println("20 - RSI panel, todos los detectores y todos los video");
            System.out.println("------ ");
            System.out.println("21 - Climax sin filtro ");
            System.out.println("22 - Climax Panel ");
            System.out.println("23 - Climax detector BIBLIOTECA ");
            System.out.println("24 - Climax Panel y detector BIBLIOTECA ");

            System.out.println("op: ");
            String op = scannerOP.nextLine();

            int opInt = Integer.valueOf(op);

            String sql = "";


            // climax 1396863L | rsi conf 1396864L | neo 1396865L | rsi 1396866L
            switch (opInt) {

               case 0:
                  System.out.println("country: ");
                  country = scannerOP.nextLine();

                  System.out.println("userId: ");
                  String userIdStr = scannerOP.nextLine();

                  System.out.println("installationId: ");
                  String installationIdStr = scannerOP.nextLine();

                  System.out.println("serviceId: ");
                  String serviceIdStr = scannerOP.nextLine();
                  userId = Long.valueOf(userIdStr);
                  Long installationId =Long.valueOf(installationIdStr);

                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, installationId, getServiceIdsFromStr(serviceIdStr),
                        fromTS, toTS, country, userId, limit, getDtoSinFiltro());

                  break;
               case 1:
                  // Rsi Confort Sin Filtro
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396864L, getRsiConfortServiceIds(),
                        fromTS, toTS, country, userId, limit, getDtoSinFiltro());

                 /* System.out.println("----------------------------------------- ");

                  sql2 = eventDaoJdbc2.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396864L, getRsiConfortServiceIds(),
                        fromTS, toTS, country, userId, limit, getDtoSinFiltro());*/

                  break;
               case 2:
                  // Rsi Confort Pircam1
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396864L, getRsiConfortServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiConfortPIRCAM1());
                  break;
               case 3:
                  // Rsi Confort Pircam1 y 2
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396864L, getRsiConfortServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiConfortPIRCAM1y2());
                  break;
               case 4:
                  // Rsi Confort Pircam1 y Pircam2 y panel
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396864L, getRsiConfortServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiConfortPIRCAM1y2yPanel());
                  break;
               case 5:
                  // RSI sin filtro --------------------------------------
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getDtoSinFiltro());
                  break;
               case 6:
                  // RSI 1 detector
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiHAB());
                  break;
               case 7:
                  // RSI todos detector
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiHabYEntrada());
                  break;
               case 8:
                  // RSI 1 camara
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiUnaCamara());
                  break;
               case 9:
                  // RSI todas las camara
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiTodasCamara());
                  break;
               case 10:
                  // RSI 1 dvr
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiUnDVR());
                  break;
               case 11:
                  // RSI todos los dvr
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiTodosDVR());
                  break;
               case 12:
                  // RSI todoS video camara y dvr
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiTodosVideo());
                  break;
               case 13:
                  // RSI panel y 1 detector
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiHAByPanel());
                  break;
               case 14:
                  // RSI  panel y todos los detectores
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiPanelHabYEntrada());
                  break;
               case 15:
                  // RSI panel y 1 camara
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiPanelyUnaCamara());
                  break;
               case 16:
                  // RSI panel y todas las camara
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiPanelyTodasCamara());
                  break;
               case 17:
                  // RSI Panel y 1 dvr
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiPanelyUnDVR());
                  break;
               case 18:
                  // RSI todos los dvr
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiPanelYTodosDVR());
                  break;
               case 19:
                  // RSI Panel todoS video camara y dvr
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiPanelyTodosVideo());
                  break;
               case 20:
                  // RSI Panel, todos detectores y todoS video camara y dvr
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396866L, getRsiServiceIds(),
                        fromTS, toTS, country, userId, limit, getRsiPanelTodosDetectoresyTodosVideo());
                  break;
               case 21:
                  // Climax sin Fitro
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396863L, getClimaxIds(),
                        fromTS, toTS, country, userId, limit, getDtoSinFiltro());
                  break;
               case 22:
                  // Climax Panel
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl,1396863L, getClimaxIds(),
                        fromTS, toTS, country, userId, limit, getClimaxPanel());
                  break;
               case 23:
                  // Climax detector BIBLIOTECA
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl,1396863L,  getClimaxIds(),
                        fromTS, toTS, country, userId, limit, getClimaxDetector());
                  break;

               case 24:
                  // Climax Panel y detector BIBLIOTECA
                  sql = eventDaoJdbc.findByServiceIdsAndRangeDateIndividual(jdbcUrl, 1396863L, getClimaxIds(),
                        fromTS, toTS, country, userId, limit, getClimaxPanelyDetector());
                  break;

               default:
                  System.out.println("error");
            }

            Connection con = null;
            Statement stmt = null;
            ResultSet rs = null;

            try {
               con = dataSource.getConnection();
               stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

               cronoMeter.start();
               System.out.println("exeuting ... ");
               rs = stmt.executeQuery(sql);

               cronoMeter.stop();

               System.out.println(" ");
               System.out.println("@@@@@@@ T1:  " + cronoMeter.getElapseTime());

               // System.out.println("sqlCount: " + sqlCount);

               // Mulstisede
               if (opInt == 25) {
                  rs.next();

                  System.out.println("total events:: " + rs.getInt(1));
               } else {
                  int count = 0;
                  while (rs.next()) {
                     count ++;
                     System.out.println("eventId: " + rs.getString(1));
                  }

                  System.out.println("  " );
                  System.out.println("total events: " + count);
               }


              /* System.out.println("-------------------------------- " );

               cronoMeter.start();
               rs = stmt.executeQuery(sql2);
               cronoMeter.end();
               System.out.println("@@@@@@@ T2:  " + cronoMeter.getEnlapseTime());


               count = 0;
               while (rs.next()) {
                  count ++;
                  System.out.println("eventId: " + rs.getString(1));
               }

               System.out.println("  " );
               System.out.println("total events2: " + count);*/

            } catch (Exception e) {
               printStackTrace(e);
            } finally {
               try {
                  if (rs != null) rs.close();
                  if (stmt != null) stmt.close();
                  if (con != null) con.close();
               } catch (Exception e) {
                  System.out.println(" ");
                  System.out.println(e.getMessage());
                  printStackTrace(e);
               }
            }

         }


      } catch (Exception e) {
         printStackTrace(e);
      }
   }

   public void mainMultiSite() {

      loadExternalFileProperties();

      String jdbcUrl = "";
      String dbUsername = "";
      String password = "";

      String env = "pro";
      if (env == null) env = getProperty("env");
      String connectionType = getProperty("connectionType");


      // select jdbc url
      if ("vpn".equals(connectionType)) {
         jdbcUrl = getProperty("vpn." + env + ".url");
      } else {
         jdbcUrl = getProperty("telepresence." + env + ".url");
      }

      dbUsername = "EVENT-PROCESSOR" ;//getProperty(env + ".username");
      password = getProperty(env + ".EVENT-PROCESSOR" +".password");

      DataSource dataSource;
      Connection con = null;
      Statement stmt = null;
      ResultSet rs = null;

      try {
         dataSource = getDataSource(jdbcUrl, dbUsername, password);


         EventDaoJdbcImpl eventDaoJdbc = new EventDaoJdbcImpl();

         Long endDate = Math.abs(System.currentTimeMillis());
         long fromDate = LocalDateTime.now().minusDays(30).atZone(ZoneId.of("Z")).toEpochSecond() * 1000;
         Timestamp fromTS = new Timestamp(fromDate);
         Timestamp toTS = new Timestamp(endDate);

         int limit = 5000;

         Scanner scannerOP = new Scanner(new InputStreamReader(System.in));

         ChronoMeter cronoMeter = new ChronoMeter();

         con = dataSource.getConnection();
         stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);


         String sqlUsers = "SELECT * FROM (\n" +
               "SELECT usr.SEQ_ID, usr.DES_COUNTRY, usr.DES_NICK_NAME, count(inst.seq_id) \n" +
               "FROM SMPR_TINSTALLATION  inst\n" +
               "INNER JOIN SMPR_TUSER usr ON (usr.SEQ_ID = inst.SEQ_ADMIN)\n" +
               "WHERE usr.BOL_MULTISITE = 1\n" +
               "GROUP BY usr.SEQ_ID, usr.DES_COUNTRY, usr.DES_NICK_NAME\n" +
               "ORDER BY count(inst.seq_id) DESC\n" +
               ") WHERE rownum <= 20";

         rs = stmt.executeQuery(sqlUsers);

         Map<String, String> userMap = new HashMap<>();
         String value = "";
         String newValue = "";

         while (rs.next()){
            value = rs.getLong(1) + " | "+ rs.getString(2) + " | " + rs.getString(3) + " | " + rs.getInt(4) + " | ";
            userMap.put(rs.getString(3), value);
         }


         for (String userName: userMap.keySet()) {
            System.out.println(" ");

            System.out.println("userName: " + userName);
            //String userName = scannerOP.nextLine();


            String sqlServicesId = "SELECT SEQ_THING_ID FROM SMPR_TTHING_ALIAS WHERE SEQ_INSTALLATION_ALIAS_ID IN \n" +
                  "(SELECT SEQ_ID FROM SMPR_TINSTALLATION_ALIAS WHERE SEQ_USER_ID = \n" +
                  "(SELECT seq_id FROM Smpr_tuser WHERE des_nick_name = '" + userName+ "' ) \n" +
                  ")";

            String sqlUser = "SELECT seq_id, des_country FROM Smpr_tuser WHERE des_nick_name = '"+ userName+"'";

            try {


               Long userId = null;
               String country = null;

               System.out.println(sqlUser);
               rs = stmt.executeQuery(sqlUser);

               if (rs.next()) {
                  userId = rs.getLong(1);
                  country = rs.getString(2);
               }

               System.out.println("userId: " + userId + " country: " + country);

               ArrayList<Long> servicesId = new ArrayList<>();

               System.out.println(sqlServicesId);
               rs = stmt.executeQuery(sqlServicesId);

               while (rs.next()) {
                  servicesId.add(rs.getLong(1));
                  printInLine("servicesId: " + rs.getLong(1));
               }

               System.out.println("servicesId.size: " + servicesId.size());
               if (servicesId.size() > 0 ) {
                  System.out.println("  ");
                  System.out.println("-------------------------- ");


                  String sqlCount = eventDaoJdbc.getCountMultiSiteEventsStmnt(servicesId, fromTS, toTS);
                  System.out.println("  ");
                  System.out.println("-------------------------- ");

                  System.out.println("exeuting ... ");
                  rs = stmt.executeQuery(sqlCount);

                  System.out.println(" ");

                  if (rs.next()) {
                     System.out.println("total events: " + rs.getInt(1));
                     newValue = userMap.get(userName);
                     newValue = newValue + rs.getInt(1);
                     userMap.put(userName, newValue);
                  }
               } else {
                  newValue = userMap.get(userName);
                  newValue = newValue + "0";
                  userMap.put(userName, newValue);
               }

            } catch (Exception e) {
               printStackTrace(e);
            }

         }

         // Print final result
         for (String userName: userMap.keySet()) {
            System.out.println(userMap.get(userName));
         }

      } catch (Exception e) {
         printStackTrace(e);
      } finally {
         try {
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (con != null) con.close();
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         }
      }
   }

   /*

   // Climax installation_id = 1396863
SELECT seq_thing_id FROM  "EVENT-PROCESSOR".SMPR_TTHING_ALIAS
WHERE seq_user_id = 1810803 AND seq_installation_alias_id =
(SELECT seq_id FROM "EVENT-PROCESSOR".smpr_tinstallation_alias
	WHERE seq_user_id = 1810803 AND seq_installation_id = 1396863)
AND bol_is_active = 1;

// rsi confort installation_id = 1396864
SELECT seq_thing_id FROM  "EVENT-PROCESSOR".SMPR_TTHING_ALIAS
WHERE seq_user_id = 1810803 AND seq_installation_alias_id =
(SELECT seq_id FROM "EVENT-PROCESSOR".smpr_tinstallation_alias
	WHERE seq_user_id = 1810803 AND seq_installation_id = 1396864)
AND bol_is_active = 1;

// neo installation_id = 1396865
SELECT seq_thing_id FROM  "EVENT-PROCESSOR".SMPR_TTHING_ALIAS
WHERE seq_user_id = 1810803 AND seq_installation_alias_id =
(SELECT seq_id FROM "EVENT-PROCESSOR".smpr_tinstallation_alias
	WHERE seq_user_id = 1810803 AND seq_installation_id = 1396865)
AND bol_is_active = 1;

// rsi installation_id = 1396866
SELECT seq_thing_id FROM  "EVENT-PROCESSOR".SMPR_TTHING_ALIAS
WHERE seq_user_id = 1810803 AND seq_installation_alias_id =
(SELECT seq_id FROM "EVENT-PROCESSOR".smpr_tinstallation_alias
	WHERE seq_user_id = 1810803 AND seq_installation_id = 1396866)
AND bol_is_active = 1;
    */


   public void testLess() {

      try {


         //String host1 ="https://smart.prosegur.com/smart-server/ws/event";
         String host1 ="http://localhost:9119/activity-stream";
         //String headerKey1="X-Smart-Token";
         String headerKey1 ="Authorization";

         String host2 ="http://localhost:8118/activity-stream";
         String headerKey2 ="Authorization";

  //       Scanner scannerOP = new Scanner(new InputStreamReader(System.in));
   //      System.out.println("user token: ");
         String token = "eyJraWQiOiJhMTZ4UnJyT1JlUGl5SllIT01BMCtTMFBMSGJRcTV1RUxvMWJPblhBM1NFPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI4NjA0N2QyZS00OTg4LTRhMDEtYjExYy0wZGM2Yjc0NWIzZjAiLCJjdXN0b206aWRVc2VyIjoiMTgxMDgwMyIsImN1c3RvbTpjbGllbnRJZCI6IjE3ODkzNTkiLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuZXUtY2VudHJhbC0xLmFtYXpvbmF3cy5jb21cL2V1LWNlbnRyYWwtMV84dEFyQmNrZ04iLCJjdXN0b206aW5pdGlhbGl6ZWQiOiJ0cnVlIiwicHJlZmVycmVkX3VzZXJuYW1lIjoibGVpcmEuc290ZWxvQGhvdG1haWwuY29tIiwiY3VzdG9tOmxhbmd1YWdlIjoiZW5fR0IiLCJhdXRoX3RpbWUiOjE3MjQ2Njg5MzYsIm5pY2tuYW1lIjoiREVWLVNNQVJULVVTRVIiLCJleHAiOjE3MjQ2NzI1MzYsImN1c3RvbTpyb2xlIjoiQWRtaW4iLCJpYXQiOjE3MjQ2Njg5MzYsImp0aSI6IjI5YzYzNTNjLWM5MTktNDkwZS1hNWU4LWE3YmViNWNkNmQ4NCIsImVtYWlsIjoibGVpcmEuc290ZWxvQGhvdG1haWwuY29tIiwiY3VzdG9tOmNvdW50cnkiOiJVWSIsImN1c3RvbTpwbGF0Zm9ybSI6IlNNQVJUMiIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwicGhvbmVfbnVtYmVyX3ZlcmlmaWVkIjpmYWxzZSwiY29nbml0bzp1c2VybmFtZSI6ImxlaXJhLnNvdGVsb0Bob3RtYWlsLmNvbSIsIm9yaWdpbl9qdGkiOiI5NGIyYTAzNC1kNTEzLTQ0YjAtYjg2YS02NmU3ZGFkOTc4NmQiLCJhdWQiOiJ2M3FpajUxN2NrZnN2aXJwOHVzNHQ4ODVvIiwiZXZlbnRfaWQiOiIxMjAwZjE3Yi1jNmM2LTRkYmMtOTY4OS05Y2M2MWY1YTI3NWYiLCJ0b2tlbl91c2UiOiJpZCIsImN1c3RvbTphZG1pblVzZXIiOiJBUlgwMDAxMTg5IiwibmFtZSI6IkFyaWVsIiwiY3VzdG9tOm9yaWdpbiI6IldlYiIsImZhbWlseV9uYW1lIjoiU290ZWxvIn0.fNyJSabJGtCWTMgh3Hiy9ODdLzlFiDd5D2_55-ikKFoLIM2rVj7WUUz3SBwWwMq3igFWp0ITV2AYSWZ2HYe20hUNglAWdvRispoKkD18WUAMagLJBuFeWjIEdWc1p8Z8xQp2VccBYf-jsEWjwptdL94ZdNsjTzdMb5IDvxCGTK0hyJLQ42AoghQVQPbfUlk5qcuNfzOPN4cG3CzB4XR3MCtjW9pwhWztBPGv1KAcLc-UB4aQ_dN8mPm4_5JzXx-ay9cH8EwHRN2-g0T2ovfzOB3sF4PeCnnPNCPdXTfYcz6GKYpNJ3pvBITj8f60jsAl7AbFUbjBH8RvRTTF9dajkw";
               //scannerOP.nextLine();

         //rsi conf 1396864
         System.out.println("// 1 Rsi Confort Sin Filtro");
         getLess("p1-actual", host1, headerKey1, token, 1396864L, getDtoSinFiltro());
         getLess("p1-nuevo", host2, headerKey2, token, 1396864L, getDtoSinFiltro());


         System.out.println("// 2 Rsi Confort Pircam1");
         getLess("p2-actual", host1, headerKey1, token, 1396864L, getRsiConfortPIRCAM1());
         getLess("p2-nuevo", host2, headerKey2, token, 1396864L, getRsiConfortPIRCAM1());

         System.out.println("// 3 Rsi Confort Pircam1 y Piercam2");
         getLess("p3-actual", host1, headerKey1, token, 1396864L, getRsiConfortPIRCAM1y2());
         getLess("p3-nuevo", host2, headerKey2, token, 1396864L, getRsiConfortPIRCAM1y2());

         System.out.println("// 4 Rsi Confort Pircam1 y Pircam2 y panel");
         getLess("p4-actual", host1, headerKey1, token, 1396864L, getRsiConfortPIRCAM1y2yPanel());
         getLess("p4-nuevo", host2, headerKey2, token, 1396864L, getRsiConfortPIRCAM1y2yPanel());

         //rsi 1396866
         System.out.println("// 5 RSI sin filtro -----------------------------------");
         getLess("p5-actual", host1, headerKey1, token, 1396866L, getDtoSinFiltro());
         getLess("p5-nuevo", host2, headerKey2, token, 1396866L, getDtoSinFiltro());

         System.out.println("// 6 RSI 1 detector");
         getLess("p6-actual", host1, headerKey1, token, 1396866L, getRsiHAB());
         getLess("p6-nuevo", host2, headerKey2, token, 1396866L, getRsiHAB());

         System.out.println("// 7 RSI todos detector");
         getLess("p7-actual", host1, headerKey1, token, 1396866L, getRsiHabYEntrada());
         getLess("p7-nuevo", host2, headerKey2, token, 1396866L, getRsiHabYEntrada());

         System.out.println("// 8 RSI 1 camara");
         getLess("p8-actual", host1, headerKey1, token, 1396866L, getRsiUnaCamara());
         getLess("p8-nuevo", host2, headerKey2, token, 1396866L, getRsiUnaCamara());

         System.out.println("// 9 RSI todas las camara");
         getLess("p9-actual", host1, headerKey1, token, 1396866L, getRsiTodasCamara());
         getLess("p9-nuevo", host2, headerKey2, token, 1396866L, getRsiTodasCamara());

         System.out.println("// 10 RSI 1 dvr");
         getLess("p10-actual", host1, headerKey1, token, 1396866L, getRsiUnDVR());
         getLess("p10-nuevo", host2, headerKey2, token, 1396866L, getRsiUnDVR());

         System.out.println("// 11 RSI todos los dvr");
         getLess("p11-actual", host1, headerKey1, token, 1396866L, getRsiTodosDVR());
         getLess("p11-nuevo", host2, headerKey2, token, 1396866L, getRsiTodosDVR());

         System.out.println("// 12 RSI todos video camara y dvr");
         getLess("p12-actual", host1, headerKey1, token, 1396866L, getRsiTodosVideo());
         getLess("p12-nuevo", host2, headerKey2, token, 1396866L, getRsiTodosVideo());

         System.out.println("// 13 RSI panel y 1 detector");
         getLess("p13-actual", host1, headerKey1, token, 1396866L, getRsiHAByPanel());
         getLess("p13-nuevo", host2, headerKey2, token, 1396866L, getRsiHAByPanel());

         System.out.println("14 RSI  panel y todos los detectores");
         getLess("p14-actual", host1, headerKey1, token, 1396866L, getRsiPanelHabYEntrada());
         getLess("p14-nuevo", host2, headerKey2, token, 1396866L, getRsiPanelHabYEntrada());

         System.out.println("15 RSI panel y 1 camara");
         getLess("p15-actual", host1, headerKey1, token, 1396866L, getRsiPanelyUnaCamara());
         getLess("p15-nuevo", host2, headerKey2, token, 1396866L, getRsiPanelyUnaCamara());

         System.out.println("// 16 RSI panel y todas las camara");
         getLess("p16-actual", host1, headerKey1, token, 1396866L, getRsiPanelyTodasCamara());
         getLess("p16-nuevo", host2, headerKey2, token, 1396866L, getRsiPanelyTodasCamara());

         System.out.println("// 17 RSI Panel y 1 dvr");
         getLess("p17-actual", host1, headerKey1, token, 1396866L, getRsiPanelyUnDVR());
         getLess("p17-nuevo", host2, headerKey2, token, 1396866L, getRsiPanelyUnDVR());

         System.out.println("// 18 RSI todos los dvr");
         getLess("p18-actual", host1, headerKey1, token, 1396866L, getRsiPanelYTodosDVR());
         getLess("p18-nuevo", host2, headerKey2, token, 1396866L, getRsiPanelYTodosDVR());

         System.out.println("// 19 RSI Panel todos video camara y dvr");
         getLess("p19-actual", host1, headerKey1, token, 1396866L, getRsiPanelyTodosVideo());
         getLess("p19-nuevo", host2, headerKey2, token, 1396866L, getRsiPanelyTodosVideo());

         System.out.println("// 20 RSI Panel, todos detectores y todos video camara y dvr");
         getLess("p20-actual", host1, headerKey1, token, 1396866L, getRsiPanelTodosDetectoresyTodosVideo());
         getLess("p20-nuevo", host2, headerKey2, token, 1396866L, getRsiPanelTodosDetectoresyTodosVideo());

         // climax 1396863
         System.out.println("// 21 Climax sin Fitro ------------------------------");
         getLess("p21-actual", host1, headerKey1, token, 1396863L, getDtoSinFiltro());
         getLess("p21-nuevo", host2, headerKey2, token, 1396863L, getDtoSinFiltro());

         System.out.println("// 22 Climax Panel");
         getLess("p22-actual", host1, headerKey1, token, 1396863L, getClimaxPanel());
         getLess("p22-nuevo", host2, headerKey2, token, 1396863L, getClimaxPanel());

         System.out.println("// 23 Climax detector BIBLIOTECA");
         getLess("p23-actual", host1, headerKey1, token, 1396863L, getClimaxDetector());
         getLess("p23-nuevo", host2, headerKey2, token, 1396863L, getClimaxDetector());

         System.out.println("// 24 Climax Panel y detector BIBLIOTECA");
         getLess("p24-actual", host1, headerKey1, token, 1396863L, getClimaxPanelyDetector());
         getLess("p24-nuevo", host2, headerKey2, token, 1396863L, getClimaxPanelyDetector());


      } catch (Exception e){
         printStackTrace(e);
      }

   }


   public void testLessTimed() {

      try {


         String host1 ="http://localhost:9119/activity-stream";
         String headerKey1="Authorization";

         String host2 ="http://localhost:8118/activity-stream";
         String headerKey2 ="Authorization";

         Scanner scannerOP = new Scanner(new InputStreamReader(System.in));
         System.out.println("token: ");
         String token = scannerOP.nextLine();

         //rsi conf 1396864
         System.out.println("echo \"1 Rsi Confort Sin Filtro\"");
         getTimedUrl("p1-actual", host1, headerKey1, token, 1396864L, getDtoSinFiltro());
         getTimedUrl("p1-nuevo", host2, headerKey2, token, 1396864L, getDtoSinFiltro());


         System.out.println("echo \"2 Rsi Confort Pircam1\"");
         getTimedUrl("p2-actual", host1, headerKey1, token, 1396864L, getRsiConfortPIRCAM1());
         getTimedUrl("p2-nuevo", host2, headerKey2, token, 1396864L, getRsiConfortPIRCAM1());

         System.out.println("echo \"3 Rsi Confort Pircam1 y Piercam2\"");
         getTimedUrl("p3-actual", host1, headerKey1, token, 1396864L, getRsiConfortPIRCAM1y2());
         getTimedUrl("p3-nuevo", host2, headerKey2, token, 1396864L, getRsiConfortPIRCAM1y2());

         System.out.println("echo \"4 Rsi Confort Pircam1 y Pircam2 y panel\"");
         getTimedUrl("p4-actual", host1, headerKey1, token, 1396864L, getRsiConfortPIRCAM1y2yPanel());
         getTimedUrl("p4-nuevo", host2, headerKey2, token, 1396864L, getRsiConfortPIRCAM1y2yPanel());

         //rsi 1396866
         System.out.println("echo \"5 RSI sin filtro -----------------------------------\"");
         getTimedUrl("p5-actual", host1, headerKey1, token, 1396866L, getDtoSinFiltro());
         getTimedUrl("p5-nuevo", host2, headerKey2, token, 1396866L, getDtoSinFiltro());

         System.out.println("echo \"6 RSI 1 detector\"");
         getTimedUrl("p6-actual", host1, headerKey1, token, 1396866L, getRsiHAB());
         getTimedUrl("p6-nuevo", host2, headerKey2, token, 1396866L, getRsiHAB());

         System.out.println("echo \"7 RSI todos detector\"");
         getTimedUrl("p7-actual", host1, headerKey1, token, 1396866L, getRsiHabYEntrada());
         getTimedUrl("p7-nuevo", host2, headerKey2, token, 1396866L, getRsiHabYEntrada());

         System.out.println("echo \"8 RSI 1 camara\"");
         getTimedUrl("p8-actual", host1, headerKey1, token, 1396866L, getRsiUnaCamara());
         getTimedUrl("p8-nuevo", host2, headerKey2, token, 1396866L, getRsiUnaCamara());

         System.out.println("echo 9 RSI todas las camara\"");
         getTimedUrl("p9-actual", host1, headerKey1, token, 1396866L, getRsiTodasCamara());
         getTimedUrl("p9-nuevo", host2, headerKey2, token, 1396866L, getRsiTodasCamara());

         System.out.println("echo \"10 RSI 1 dvr\"");
         getTimedUrl("p10-actual", host1, headerKey1, token, 1396866L, getRsiUnDVR());
         getTimedUrl("p10-nuevo", host2, headerKey2, token, 1396866L, getRsiUnDVR());

         System.out.println("echo \"11 RSI todos los dvr\"");
         getTimedUrl("p11-actual", host1, headerKey1, token, 1396866L, getRsiTodosDVR());
         getTimedUrl("p11-nuevo", host2, headerKey2, token, 1396866L, getRsiTodosDVR());

         System.out.println("echo \"12 RSI todos video camara y dvr\"");
         getTimedUrl("p12-actual", host1, headerKey1, token, 1396866L, getRsiTodosVideo());
         getTimedUrl("p12-nuevo", host2, headerKey2, token, 1396866L, getRsiTodosVideo());

         System.out.println("echo \"13 RSI panel y 1 detector\"");
         getTimedUrl("p13-actual", host1, headerKey1, token, 1396866L, getRsiHAByPanel());
         getTimedUrl("p13-nuevo", host2, headerKey2, token, 1396866L, getRsiHAByPanel());

         System.out.println("echo \"14 RSI  panel y todos los detectores\"");
         getTimedUrl("p14-actual", host1, headerKey1, token, 1396866L, getRsiPanelHabYEntrada());
         getTimedUrl("p14-nuevo", host2, headerKey2, token, 1396866L, getRsiPanelHabYEntrada());

         System.out.println("echo \"15 RSI panel y 1 camara\"");
         getTimedUrl("p15-actual", host1, headerKey1, token, 1396866L, getRsiPanelyUnaCamara());
         getTimedUrl("p15-nuevo", host2, headerKey2, token, 1396866L, getRsiPanelyUnaCamara());

         System.out.println("echo \"16 RSI panel y todas las camara\"");
         getTimedUrl("p16-actual", host1, headerKey1, token, 1396866L, getRsiPanelyTodasCamara());
         getTimedUrl("p16-nuevo", host2, headerKey2, token, 1396866L, getRsiPanelyTodasCamara());

         System.out.println("echo \"17 RSI Panel y 1 dvr\"");
         getTimedUrl("p17-actual", host1, headerKey1, token, 1396866L, getRsiPanelyUnDVR());
         getTimedUrl("p17-nuevo", host2, headerKey2, token, 1396866L, getRsiPanelyUnDVR());

         System.out.println("echo \"18 RSI todos los dvr\"");
         getTimedUrl("p18-actual", host1, headerKey1, token, 1396866L, getRsiPanelYTodosDVR());
         getTimedUrl("p18-nuevo", host2, headerKey2, token, 1396866L, getRsiPanelYTodosDVR());

         System.out.println("echo \"19 RSI Panel todos video camara y dvr\"");
         getTimedUrl("p19-actual", host1, headerKey1, token, 1396866L, getRsiPanelyTodosVideo());
         getTimedUrl("p19-nuevo", host2, headerKey2, token, 1396866L, getRsiPanelyTodosVideo());

         System.out.println("echo \"20 RSI Panel, todos detectores y todos video camara y dvr\"");
         getTimedUrl("p20-actual", host1, headerKey1, token, 1396866L, getRsiPanelTodosDetectoresyTodosVideo());
         getTimedUrl("p20-nuevo", host2, headerKey2, token, 1396866L, getRsiPanelTodosDetectoresyTodosVideo());

         // climax 1396863
         System.out.println("echo \"21 Climax sin Fitro ------------------------------\"");
         getTimedUrl("p21-actual", host1, headerKey1, token, 1396863L, getDtoSinFiltro());
         getTimedUrl("p21-nuevo", host2, headerKey2, token, 1396863L, getDtoSinFiltro());

         System.out.println("echo \"22 Climax Panel\"");
         getTimedUrl("p22-actual", host1, headerKey1, token, 1396863L, getClimaxPanel());
         getTimedUrl("p22-nuevo", host2, headerKey2, token, 1396863L, getClimaxPanel());

         System.out.println("echo \"23 Climax detector BIBLIOTECA\"");
         getTimedUrl("p23-actual", host1, headerKey1, token, 1396863L, getClimaxDetector());
         getTimedUrl("p23-nuevo", host2, headerKey2, token, 1396863L, getClimaxDetector());

         System.out.println("echo \"24 Climax Panel y detector BIBLIOTECA\"");
         getTimedUrl("p24-actual", host1, headerKey1, token, 1396863L, getClimaxPanelyDetector());
         getTimedUrl("p24-nuevo", host2, headerKey2, token, 1396863L, getClimaxPanelyDetector());


      } catch (Exception e){
         printStackTrace(e);
      }

   }


   public void formatCsv(){
      try {
         BufferedReader br = new BufferedReader(new FileReader("timeComparation.txt"));

         String content = "";
         String sCurrentLine;

         while ((sCurrentLine = br.readLine()) != null) {
            content += sCurrentLine;
         }

         content = content.replaceAll("\n", "");
         content = content.replaceAll("#", "\n");

         writeToFile("timeComparation.csv", content);

      } catch (Exception e){
         printStackTrace(e);
      }

   }

   private void getTimedUrl(String test, String host, String tokenKey, String token, Long installationId, EventIndividualFiltersDto filter){

      System.out.println("echo \"" + test + "\"");
      System.out.println(" ");
      String url= "curl -o /dev/null -s -w 'Establish Connection: %{time_connect}s\nTTFB: %{time_starttransfer}s\nTotal: %{time_total}s\n' \\" +
      "--request GET \\" +
      "--url " + host + "/installation/" + installationId + "/less" + getQueryParams(filter)+ " \\" +
      "--header Authorization: $token \\" +
      "--header 'x-smart-platform: Web'";

      System.out.println(url);
      System.out.println(" ");
      System.out.println("echo \" \"");
      //return url;
   }



   public void getLess(String test, String host, String tokenKey, String token, Long installationId, EventIndividualFiltersDto filter){

      try {

         String url= host + "/installation/" + installationId + "/less" + getQueryParams(filter);

         Client client = Client.create();

         System.out.println("less : " + url);

         WebResource webResource2 = client.resource(url);
         WebResource.Builder builder =  webResource2.getRequestBuilder();

         builder
               .header(tokenKey, token)
               .header("x-smart-platform", "Web");

         ClientResponse response2 = builder.get(ClientResponse.class);
         String result = response2.getEntity(String.class);

         writeToFile("testIndiv.json", result);

         FormatJson formatJson = new FormatJson();

         formatJson.main("testIndiv.json", test);

      } catch (Exception e) {
         printStackTrace(e);
      }
   }

   private String getQueryParams(EventIndividualFiltersDto filter){

      /*?limitDate=
      &showPanelEvents=TRUE
      &showDetectorsEvents=TRUE
      &detectorDeviceIds=
      &showDomoticEvents=TRUE
      &domoticDeviceIds=
      &showVideoDeviceEvents=TRUE
      &videoDeviceIds=*/

      String queryParams = "?limitDate=";

      queryParams += "&showPanelEvents=" + filter.isShowPanelEvents();
      queryParams += "&showDetectorsEvents=" + filter.isShowDetectorsEvents();
      queryParams += "&detectorDeviceIds=" + (StringUtils.isNotEmpty(filter.getDetectorDeviceIdsString())?
            filter.getDetectorDeviceIdsString().replaceAll(",", "%2C"): "");
      queryParams += "&showDomoticEvents=" + filter.isShowDomoticEvents();
      queryParams += "&domoticDeviceIds=" ;
      queryParams += "&showVideoDeviceEvents=" + filter.isShowVideoDeviceEvents();
      queryParams += "&videoDeviceIds=" +
            (StringUtils.isNotEmpty(filter.getVideoDeviceIdsString())?
                  filter.getVideoDeviceIdsString().replaceAll(",", "%2C"): "");;

      return queryParams;

   }


   private ArrayList<Long> getServiceIdsFromStr(String serviceIdStr){

      String arrId[] = serviceIdStr.split(",");
      ArrayList<Long> serviceIds = new ArrayList<>();
      for (String id: arrId){
         serviceIds.add(Long.valueOf(id));
      }

      return serviceIds;
   }


   private ArrayList<Long> getRsiConfortServiceIds(){
      ArrayList<Long> serviceIds = new ArrayList<>();
      serviceIds.add(9704437L);
      serviceIds.add(9704425L);
      serviceIds.add(9704426L);
      serviceIds.add(9704438L);
      return serviceIds;
   }

   private ArrayList<Long> getRsiServiceIds(){
      ArrayList<Long> serviceIds = new ArrayList<>();

      serviceIds.add(9704433L);
      serviceIds.add(9704429L);
      serviceIds.add(9704431L);
      serviceIds.add(9704430L);
      serviceIds.add(9704440L);
      serviceIds.add(9704434L);
      serviceIds.add(9704439L);

      return serviceIds;
   }

   private ArrayList<Long> getClimaxIds(){
      ArrayList<Long> serviceIds = new ArrayList<>();

      serviceIds.add(9704422L);
      serviceIds.add(9704442L);
      serviceIds.add(9704441L);
      serviceIds.add(9704423L);

      return serviceIds;
   }


   private EventIndividualFiltersDto getDtoSinFiltro(){
      // Instalacion rsi confort
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(true);
      eventIndividualFiltersDto.setShowDetectorsEvents(true);
      eventIndividualFiltersDto.setShowDomoticEvents(true);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(true);
      eventIndividualFiltersDto.setDetectorDeviceIdsString(null);
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString(null);

      return eventIndividualFiltersDto;
   }

  // PIRCAM1 9704437 PIRCAM2 9704438
   private EventIndividualFiltersDto getRsiConfortPIRCAM1(){
      // Instalacion rsi confort
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(false);
      eventIndividualFiltersDto.setShowDetectorsEvents(true);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(false);
      eventIndividualFiltersDto.setDetectorDeviceIdsString("9704437");
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString(null);

      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiConfortPIRCAM1y2(){
      // Instalacion rsi confort
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(false);
      eventIndividualFiltersDto.setShowDetectorsEvents(true);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(false);
      eventIndividualFiltersDto.setDetectorDeviceIdsString("9704437,9704438");
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString(null);

      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiConfortPIRCAM1y2yPanel(){
      // Instalacion rsi confort
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(true);
      eventIndividualFiltersDto.setShowDetectorsEvents(true);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(false);
      eventIndividualFiltersDto.setDetectorDeviceIdsString("9704437,9704438");
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString(null);

      return eventIndividualFiltersDto;
   }


   private EventIndividualFiltersDto getRsiHAB(){
      // Instalacion rsi
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(false);
      eventIndividualFiltersDto.setShowDetectorsEvents(true);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(false);
      eventIndividualFiltersDto.setDetectorDeviceIdsString("9704440");
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString(null);
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiHAByPanel(){
      // Instalacion rsi
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(true);
      eventIndividualFiltersDto.setShowDetectorsEvents(true);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(false);
      eventIndividualFiltersDto.setDetectorDeviceIdsString("9704440");
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString(null);
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiHabYEntrada(){
      // Instalacion rsi
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(false);
      eventIndividualFiltersDto.setShowDetectorsEvents(true);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(false);
      eventIndividualFiltersDto.setDetectorDeviceIdsString("9704440,9704439");
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString(null);
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiPanelHabYEntrada(){
      // Instalacion rsi
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(true);
      eventIndividualFiltersDto.setShowDetectorsEvents(true);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(false);
      eventIndividualFiltersDto.setDetectorDeviceIdsString("9704440,9704439");
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString(null);
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiUnaCamara(){
      // Instalacion rsi
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(false);
      eventIndividualFiltersDto.setShowDetectorsEvents(false);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(true);
      eventIndividualFiltersDto.setDetectorDeviceIdsString(null);
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString("9704451");
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiPanelyUnaCamara(){
      // Instalacion rsi
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(true);
      eventIndividualFiltersDto.setShowDetectorsEvents(false);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(true);
      eventIndividualFiltersDto.setDetectorDeviceIdsString(null);
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString("9704451");
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiTodasCamara(){
      // Instalacion rsi
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(false);
      eventIndividualFiltersDto.setShowDetectorsEvents(false);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(true);
      eventIndividualFiltersDto.setDetectorDeviceIdsString(null);
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString("9704451,9704452");
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiPanelyTodasCamara(){
      // Instalacion rsi
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(true);
      eventIndividualFiltersDto.setShowDetectorsEvents(false);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(true);
      eventIndividualFiltersDto.setDetectorDeviceIdsString(null);
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString("9704451,9704452");
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiUnDVR(){
      // Instalacion rsi
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(false);
      eventIndividualFiltersDto.setShowDetectorsEvents(false);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(true);
      eventIndividualFiltersDto.setDetectorDeviceIdsString(null);
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString("9704443");
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiPanelyUnDVR(){
      // Instalacion rsi
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(true);
      eventIndividualFiltersDto.setShowDetectorsEvents(false);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(true);
      eventIndividualFiltersDto.setDetectorDeviceIdsString(null);
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString("9704443");
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiTodosDVR(){
      // Instalacion rsi
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(false);
      eventIndividualFiltersDto.setShowDetectorsEvents(false);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(true);
      eventIndividualFiltersDto.setDetectorDeviceIdsString(null);
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString("9704443,9704447,9704448,9704446,9704444,9704449,9704450,9704445");
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiPanelYTodosDVR(){
      // Instalacion rsi
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(true);
      eventIndividualFiltersDto.setShowDetectorsEvents(false);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(true);
      eventIndividualFiltersDto.setDetectorDeviceIdsString(null);
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString("9704443,9704447,9704448,9704446,9704444,9704449,9704450,9704445");
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiTodosVideo(){
      // Instalacion rsi
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(false);
      eventIndividualFiltersDto.setShowDetectorsEvents(false);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(true);
      eventIndividualFiltersDto.setDetectorDeviceIdsString(null);
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString("9704451,9704452,9704443,9704447,9704448,9704446,9704444,9704449,9704450,9704445");
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiPanelyTodosVideo(){
      // Instalacion rsi
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(true);
      eventIndividualFiltersDto.setShowDetectorsEvents(false);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(true);
      eventIndividualFiltersDto.setDetectorDeviceIdsString(null);
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString("9704451,9704452,9704443,9704447,9704448,9704446,9704444,9704449,9704450,9704445");
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getRsiPanelTodosDetectoresyTodosVideo(){
      // Instalacion rsi
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(true);
      eventIndividualFiltersDto.setShowDetectorsEvents(true);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(true);
      eventIndividualFiltersDto.setDetectorDeviceIdsString("9704440,9704439");
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString("9704451,9704452,9704443,9704447,9704448,9704446,9704444,9704449,9704450,9704445");
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getClimaxPanel(){
      // Instalacion climax
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(true);
      eventIndividualFiltersDto.setShowDetectorsEvents(false);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(false);
      eventIndividualFiltersDto.setDetectorDeviceIdsString(null);
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString(null);
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getClimaxDetector(){
      // Instalacion climax
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(false);
      eventIndividualFiltersDto.setShowDetectorsEvents(true);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(false);
      eventIndividualFiltersDto.setDetectorDeviceIdsString("9704441");
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString(null);
      return eventIndividualFiltersDto;
   }

   private EventIndividualFiltersDto getClimaxPanelyDetector(){
      // Instalacion climax
      EventIndividualFiltersDto eventIndividualFiltersDto = new EventIndividualFiltersDto();
      eventIndividualFiltersDto.setShowPanelEvents(true);
      eventIndividualFiltersDto.setShowDetectorsEvents(true);
      eventIndividualFiltersDto.setShowDomoticEvents(false);
      eventIndividualFiltersDto.setShowVideoDeviceEvents(false);
      eventIndividualFiltersDto.setDetectorDeviceIdsString("9704441");
      eventIndividualFiltersDto.setDomoticDeviceIdsString(null);
      eventIndividualFiltersDto.setVideoDeviceIdsString(null);
      return eventIndividualFiltersDto;
   }
}
