package clidb;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class CheckDuplicatedAliases extends QryDB {

    static Integer ocurrences = 0;

    public int fixRepeatedAliases(String env, String datafixReportFile, String reportDate) {

        loadExternalFileProperties();

        DataSource dataSource = null;

        try {

            if (env == null) env = getProperty("env");
            String connectionType = getProperty("connectionType");

            String jdbcUrl = getProperty(connectionType + "." + env + ".url");
            String dbUsername = getProperty(env + ".username");
            String password = getProperty(env + ".password");

            dataSource = getDataSource(jdbcUrl, dbUsername, password);

            System.out.println("Executing step 1: check repeated aliases in device-identification  ..." );
            checkRepeatedAliases(dataSource);

            appendToFile(datafixReportFile,
                  "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-40165'>" +
                        "14) MC " +
                        "https://jira.prosegur.com/browse/AM-40165</a> - " +
                        "Error permisos duplicados para un usuario y dispositivo <br>\n");

            if (ocurrences > 0) {
                appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate +
                      ", se fixearon " + ocurrences + " casos. <br>\n");
            } else {
                appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate +
                      ", no se encontraron casos a fixear<br>\n");
            }

            System.out.println("Total repeated aliases found: " + ocurrences.toString());

        } catch (Exception e){
            printStackTrace(e);
        }


        return ocurrences;
    }

    private static void checkRepeatedAliases(final DataSource dataSource) {

        try {
            Connection con = dataSource.getConnection();
            Statement stmt = null;
            ResultSet rs = null;

            try {

                int cant = 0;

                String sqlSelectCant = "SELECT SEQ_USER_ID, SEQ_THING_ID, COUNT(*) AS CANT " +
                        "FROM DEVICE.SMPR_TTHING_ALIAS " +
                        "GROUP BY SEQ_USER_ID, SEQ_THING_ID HAVING COUNT(*) > 1 ";

                stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                rs = stmt.executeQuery(sqlSelectCant);

                String things = "";
                String users = "";

                while (rs.next()) {
                    users = users + rs.getString(1) + ",";
                    things = things + rs.getString(2) + ",";
                }

                if (users.length() > 0) {
                    users = users.substring(0, users.length() - 1);
                    things = things.substring(0, things.length() - 1);
                    cant = users.split(",").length;

                    // things.length() have equals to things.length()

                    // generate delete statements
                    String sqlSelectToDeletes = "SELECT MIN(SEQ_ID) FROM DEVICE.SMPR_TTHING_ALIAS sta " +
                            "WHERE SEQ_USER_ID IN (" + users + ") AND SEQ_THING_ID IN (" + things + ") " +
                            "GROUP BY SEQ_USER_ID,SEQ_THING_ID HAVING COUNT(*) > 1";

                    stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                    rs = stmt.executeQuery(sqlSelectToDeletes);

                    String idsToDelete = "";
                    int cont = 0;
                    String fileName = "fixRepeatedAliases.sql";

                    while (rs.next()) {
                        cont++;
                        idsToDelete = idsToDelete + rs.getString(1) + ",";
                        if (cont == 1000) {
                            idsToDelete = idsToDelete.substring(0, idsToDelete.length() - 1);
                            appendToFile(fileName, "schemas:DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG\n");
                            appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_PARENT_ID IN (SELECT SEQ_ID FROM SMPR_TTHING_ALIAS sta2 WHERE SEQ_ID IN ("+ idsToDelete +"));\n");
                            appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_ID IN ("+ idsToDelete +");\n");
                            idsToDelete = "";
                            cont = 0;
                        }
                    }
                    if (!idsToDelete.isEmpty()) {
                        idsToDelete = idsToDelete.substring(0, idsToDelete.length() - 1);
                        appendToFile(fileName, "schemas:DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG\n");
                        appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_PARENT_ID IN (SELECT SEQ_ID FROM SMPR_TTHING_ALIAS sta2 WHERE SEQ_ID IN ("+ idsToDelete +"));\n");
                        appendToFile(fileName, "DELETE FROM SMPR_TTHING_ALIAS sta WHERE SEQ_ID IN ("+ idsToDelete +");\n");
                    }
                }

                if (cant > 0) {
                    ocurrences = cant;
                } else {
                    System.out.println("Duplicated aliases not found!");
                }

                System.out.println("Process end.");

            } catch (Exception e) {
                System.out.println(" ");
                printStackTrace(e);
            } finally {
                try {
                    if (rs != null) rs.close();
                    if (stmt != null) stmt.close();
                    if (con != null) con.close();
                } catch (Exception e) {
                    System.out.println(" ");
                    System.out.println(e.getMessage());
                    printStackTrace(e);
                }
            }

        } catch (Exception e1) {
            System.out.println(" ");
            System.out.println(e1.getMessage());
            e1.printStackTrace();
        }
    }
}

