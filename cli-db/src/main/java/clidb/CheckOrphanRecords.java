package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class CheckOrphanRecords extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(CheckOrphanRecords.class);

   static int countThingByThingAliasId = 0;
   static int countThingIdByIntallation = 0;
   static int countThingAliasId = 0;
   static int countInstallationId = 0;
   static int countInstallationAliasId = 0;

   /*
   Consultar los things que apunte a instalacion que NO exist o a instalacioens cuyo USER admin NO exista

      1)-- ThingAlias que apunten a usuarios que no existen
      SELECT DISTINCT(SEQ_USER_ID) FROM DEVICE.SMPR_TTHING_ALIAS thna
      WHERE SEQ_USER_ID NOT IN (SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TUSER usr);

      -- Totales: -- 1363 thingAlias, 203 usuarios
      -- *** CREAR UN DELETE POR USUARIO

      2)-- Things que apuntan a thingAlias cuyo user ya no existe
      SELECT SEQ_ID FROM DEVICE.SMPR_TTHING thn
      WHERE SEQ_ID IN (
         SELECT SEQ_THING_ID FROM DEVICE.SMPR_TTHING_ALIAS thna
         WHERE SEQ_USER_ID NOT IN (SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TUSER usr));

      -- Totales: 907 things
      -- *** CREAR UN DELETE POR CADA THING

      3) -- InstallationAlias que apuntan a usuario que no existe
      SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION_ALIAS instAlias
      WHERE SEQ_USER_ID NOT IN (SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TUSER usr);

      -- Totales: 231 installationAlias
      -- *** CREAR UN DELETE POR INSTALLATIONALIAS

      4)-- Things que apunten a instalaciones cuyo usuario no exista
      SELECT seq_id FROM DEVICE.SMPR_TTHING thn
      INNER JOIN "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION insta ON (thn.SEQ_INSTALLATION_ID = INSTA.SEQ_ID)
      WHERE insta.SEQ_ADMIN NOT IN (SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TUSER usr);

      -- Totale: 0
      --  *** SI EXISTEN CASOS, CREAR UN DELETE POR THING

      5) -- instalaciones cuyo usuario no exista
      SELECT * FROM "ACCOUNT-MANAGEMENT".SMPR_TINSTALLATION insta
      WHERE insta.SEQ_ADMIN NOT IN (SELECT SEQ_ID FROM "ACCOUNT-MANAGEMENT".SMPR_TUSER usr)
      AND FYH_DELETED IS NULL

      -- Total 0
      --  *** SI EXISTEN CASOS, CREAR UN DELETE POR INSTALACION
    */

   /**
    *  Template for datafix method
    */
   public int main(String env, String datafixReportFile, String reportDate) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         System.out.println("Executing step 1: ThingAlias que apunten a usuarios que no existen ..." );
         checkOrphanThingAlias(dataSource);

         System.out.println("Executing step 2: Things que apuntan a thingAlias cuyo user ya no existe ..." );
         checkOrphanThingByThingAlias(dataSource);

         System.out.println("Executing step 3: InstallationAlias que apuntan a usuario que no existe ..." );
         checkOrphanInstallationAlias(dataSource);

         System.out.println("Executing step 4: Things que apunten a instalaciones cuyo usuario no existe ..." );
         checkOrphanThingByInstallation(dataSource);

         System.out.println("Executing step 5: Instalaciones no borradas cuyo usuario no existe ..." );
         checkOrphanInstallation(dataSource);

         appendToFile(datafixReportFile,
               "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-38250'>" +
                     "11) CS " +
                     "https://jira.prosegur.com/browse/AM-38250</a> - " +
                     "Eliminacion de Thing, ThingAlias, InstallationAlias e Installation Huerfanas <br>\n");

         if ((countThingByThingAliasId +
               countThingAliasId +
               countInstallationId +
               countInstallationAliasId +
               countThingIdByIntallation) > 0) {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", se fixearon : <br>" +
                  (countThingByThingAliasId + countThingIdByIntallation) + " Things. <br>" +
                  countThingAliasId + " ThingAlias. <br>" +
                  countInstallationId + " Installations. <br>" +
                  countInstallationAliasId + " InstallationAlias . <br>");

         } else {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", no se encontraron casos a fixear<br>\n");
         }

         checkSessions(dataSource, dbUsername);

      } catch (Exception e){
         printStackTrace(e);
      }

      return countThingByThingAliasId + countThingAliasId + countInstallationId + countInstallationAliasId + countThingIdByIntallation;
   }

   /**
    * Template method to query DB and Generate sql file for INSERT, UPDATE or DELETE
    * @param dataSource
    */
   public static void checkOrphanThingAlias(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql1 = "SELECT DISTINCT(SEQ_USER_ID) FROM DEVICE.SMPR_TTHING_ALIAS thna " +
                  "WHERE SEQ_USER_ID NOT IN (SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr)";

            //System.out.println(sql1);

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql1);

            while (rs.next()) {
               if (countThingAliasId == 0) {
                  String schemas = "schemas:DEVICE,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG\n";
                  appendToFile("deleteOrphanThingAlias.sql", schemas + "\n");
               }
               String deleteStmt = "DELETE FROM SMPR_TTHING_ALIAS WHERE SEQ_USER_ID = " + rs.getLong(1) + ";";
               appendToFile("deleteOrphanThingAlias.sql",  deleteStmt + "\n");

               countThingAliasId ++;
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }

   public static void checkOrphanThingByThingAlias(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql1 = "SELECT SEQ_ID FROM DEVICE.SMPR_TTHING thn " +
                  "      WHERE SEQ_ID IN (" +
                  "         SELECT SEQ_THING_ID FROM DEVICE.SMPR_TTHING_ALIAS thna " +
                  "         WHERE SEQ_USER_ID NOT IN (SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr))";

            //System.out.println(sql1);

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql1);

            while (rs.next()) {
               if (countThingByThingAliasId == 0) {
                  String schemas = "schemas:DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SCENE,SYSLOG\n";
                  appendToFile("deleteOrphanThingByThingAlias.sql", schemas + "\n");
               }
               String deleteStmt = "DELETE FROM SMPR_TTHING WHERE SEQ_ID = " + rs.getLong(1) + ";";
               appendToFile("deleteOrphanThingByThingAlias.sql",  deleteStmt + "\n");

               countThingByThingAliasId++;
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }

   public static void checkOrphanInstallationAlias(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql1 = "SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS instAlias " +
                  "         WHERE SEQ_USER_ID NOT IN (SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr)";

            //System.out.println(sql1);

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql1);

            while (rs.next()) {
               if (countInstallationAliasId == 0) {
                  String schemas = "schemas:ACCOUNT-MANAGEMENT,CAMPAIGNS,DEVICE,DIGITAL-SECURITY,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG,USER-ENROLLMENT\n";
                  appendToFile("deleteOrphanInstallationAlias.sql", schemas + "\n");
               }
               String deleteStmt = "DELETE FROM SMPR_TINSTALLATION_ALIAS WHERE SEQ_ID = " + rs.getLong(1) + ";";
               appendToFile("deleteOrphanInstallationAlias.sql",  deleteStmt + "\n");

               countInstallationAliasId++;
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }


   public static void checkOrphanThingByInstallation(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql1 = "SELECT thn.SEQ_ID FROM DEVICE.SMPR_TTHING thn\n" +
                  "      INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION insta ON (thn.SEQ_INSTALLATION_ID = INSTA.SEQ_ID)\n" +
                  "      WHERE insta.SEQ_ADMIN NOT IN (SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr)";

            //System.out.println(sql1);

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql1);

            while (rs.next()) {
               if (countThingIdByIntallation == 0) {
                  String schemas = "schemas:DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SCENE,SYSLOG\n";
                  appendToFile("deleteOrphanThingByInstallation.sql", schemas + "\n");
               }
               String deleteStmt = "DELETE FROM SMPR_TTHING WHERE SEQ_ID = " + rs.getLong(1) + ";";
               appendToFile("deleteOrphanThingByInstallation.sql",  deleteStmt + "\n");

               countThingIdByIntallation++;
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }

   public static void checkOrphanInstallation(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql1 = "SELECT insta.SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION insta " +
                  "   WHERE insta.SEQ_ADMIN NOT IN (SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr) " +
                  "   AND FYH_DELETED IS NULL";

            //System.out.println(sql1);

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql1);

            while (rs.next()) {
               if (countInstallationId == 0) {
                  String schemas = "schemas:ACCOUNT-MANAGEMENT,CAMPAIGNS,DEVICE,DIGITAL-SECURITY,EVENT-PROCESSOR,FEATURE-FLAG,INBOX,INBOX-MESSAGE-PROCESSOR,PANIC-BUTTON,RULE-ENGINE,SYSLOG,USER-ENROLLMENT,VIRTUAL-ASSISTANT,WEBSOCKET-ADAPTER\n";
                  appendToFile("<deleteOrphanInstallation.sql>", schemas + "\n");
               }
               String deleteStmt = "DELETE FROM SMPR_TINSTALLATION WHERE SEQ_ID = " + rs.getLong(1) + ";";
               appendToFile("deleteOrphanInstallation.sql",  deleteStmt + "\n");

               countInstallationId++;
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }



}
