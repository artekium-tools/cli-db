package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.File;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class DatafixesMenu extends QryDB {


   private  final Logger LOGGER = LogManager.getLogger(DatafixesMenu.class);


   public synchronized void main(String env, String mode) {

// TODO: Agregar comentarios en jira https://developer.atlassian.com/server/jira/platform/jira-rest-api-example-add-comment-8946422/

      boolean autoMode = false;
      boolean onlyOne = false;

      if (mode != null){
         if ("auto".equals(mode)) autoMode = true;
      }

      Scanner scannerOP = new Scanner(new InputStreamReader(System.in));

      String strDate = new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date());
      String strDate4Folder = new SimpleDateFormat("yyyyMMdd").format(new java.util.Date());

      String userToLoginFile = "usersToForceLogin-" + strDate + ".txt";
      String usersToTurnOnCamerasFile = "usersToTurnOnCameras-" + strDate + ".txt";
      String instalWithoutAdminFile = "instalWithoutAdmin-" + strDate + ".txt";
      String datafixReportFile = "datafixReport-" + strDate + ".html";
      String reportDate = new SimpleDateFormat("dd MMM").format(new java.util.Date());
      String datafixFolderName = "./datafixes/" + strDate4Folder;
      String datafixExecutionName = "datafixExecution-" + strDate4Folder + ".log";

      loadExternalFileProperties();

      if (autoMode)
         runCommandLine("rm ./*.sql");

      DataSource dataSource = null;
      String dbUsername = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);
         System.out.println("connectionType: " + connectionType);

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         addCurrentConfigurationOnReport(datafixReportFile, reportDate);

         int autoIndex = 0;
         int datafixOpValue;
         int lastDatafix=0;
         int opInt;
         int waitTimeInSeconds = Integer.valueOf(getProperty("waitTimeInSecondsBetweenDatafixes"));
         String arrDatafixes[] = null;

         // Levanto la configuracion de connetion.properties
         if (autoMode){
            // Levanto el los datafixess a ejecutar para el dia ej: 1,2,3,4,5,7,8,9,10,11
            String cronExpr =  getProperty("datafixes-" + getDayOfWeek());
            arrDatafixes = cronExpr.split(",");
            lastDatafix = arrDatafixes.length;
         }

         // Se ejecutó con mode = el numero de un datafix puntual a ejecutar con nohup
         if (mode != null && isNumeric(mode)) {
            arrDatafixes = new String[1];
            arrDatafixes[0] = mode;
            lastDatafix = 1;
            onlyOne = true;
         }

         while (true) {

            // automatic mode
            if (autoMode || onlyOne) {
               if (autoIndex == lastDatafix) {
                  datafixOpValue =0;
               } else {
                  datafixOpValue = Integer.valueOf(arrDatafixes[autoIndex]);
                  //System.out.println("datafixOpValue: " + datafixOpValue + " autoIndex: " + autoIndex + " lastDatafix: " + lastDatafix);
               }

               autoIndex++;
               opInt = datafixOpValue;

               if (autoIndex > 1 ){
                  System.out.println("Waiting for " + waitTimeInSeconds + " seconds." );
                  wait(waitTimeInSeconds * 1000);
               }

            // Manual mode
            } else {
               showMenuOptions();

               System.out.println("op: ");
               String op = scannerOP.nextLine();

               opInt = Integer.valueOf(op);

            }

            // Finish and send report by e-mail
            if (opInt == 0) {
               finishAndSendReport(env, userToLoginFile, usersToTurnOnCamerasFile, instalWithoutAdminFile, datafixReportFile, reportDate, datafixFolderName, datafixExecutionName);
               return;
            }


            switch (opInt) {
               case 1:
                  runDatafix1(env, userToLoginFile, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
               case 2:
                  runDatafix2(env, userToLoginFile, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
               case 3:
                  runDatafix3(env, userToLoginFile, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
               case 4:
                  runDatafix4(env, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
               case 5:
                  runDatafix5(env, userToLoginFile, datafixReportFile, reportDate, dataSource, dbUsername);
                  break;
               case 6:
                  runDatafix6(env, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
               case 7:
                  runDatafix7(env, usersToTurnOnCamerasFile, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
               case 8:
                  runDatafix8(env, instalWithoutAdminFile, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
               case 9:
                  runDatafix9(env, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
               case 10:
                  runDatafix10(env, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
               case 11:
                  runDatafix11(env, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
               case 12:
                  runDatafix12(env, datafixReportFile, reportDate, dataSource, dbUsername);
                  break;
               case 13:
                  runDatafix13(env, datafixReportFile, reportDate, dataSource, dbUsername);
                  break;
               case 14:
                  runDatafix14(env, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
               case 15:
                  runDatafix15(env, userToLoginFile, datafixReportFile, reportDate, dataSource, dbUsername);
                  break;
               case 16:
                  runDatafix16(env, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
               case 17:
                  runDatafix17(env, datafixReportFile, reportDate, dataSource, dbUsername);
                  break;
               case 18:
                  runDatafix18(env, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
               case 19:
                  runDatafix19(env, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
               case 20:
                  runDatafix20(env, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
               case 21:
                  runDatafix21(env, datafixReportFile, reportDate, dataSource, dbUsername, waitTimeInSeconds);
                  break;
            }
         }
      } catch (Exception e){
         printStackTrace(e);
      }
   }

   public static boolean isNumeric(String str) {
      try {
         Integer.parseInt(str);
         return true;
      } catch(NumberFormatException e){
         return false;
      }
   }

   private void showMenuOptions() {
      System.out.println( " ");
      System.out.println( "Current configuration:");
      System.out.println( "----------------------");
      System.out.println( "datafixes-1=" + getProperty("datafixes-" + 1) );
      System.out.println( "datafixes-2=" + getProperty("datafixes-" + 2) );
      System.out.println( "datafixes-3=" + getProperty("datafixes-" + 3) );
      System.out.println( "datafixes-4=" + getProperty("datafixes-" + 4) );
      System.out.println( "datafixes-5=" + getProperty("datafixes-" + 5) );
      System.out.println( " ");
      System.out.println( "executeFix1=" + getProperty("executeFix1") );
      System.out.println( "executeFix2=" + getProperty("executeFix2") );
      System.out.println( "executeFix3=" + getProperty("executeFix3") );
      System.out.println( "executeFix18=" + getProperty("executeFix18") );

      System.out.println( " ");
      System.out.println("1) checkRepetedInstalations AM-38370");
      System.out.println("2) checkRepetedInstalations AM-40608");
      System.out.println("3) checkRepetedInstalationsAlias AM-40349");
      System.out.println("4) checkInstallationsMultiOwner AM-38618 - <DEPRECADO>");
      System.out.println("5) fixCameraManagerSessionClientId  AM-39999");
      System.out.println("6) checkDuplicatedPanels AM-41075 - <DEPRECADO>");
      System.out.println("7) checkPrivacyModeStatus AM-38079");
      System.out.println("8) checkOrphanInstallations AM-40939");
      System.out.println("9) checkRepeatedCMCredentials AM-43282");
      System.out.println("10) checkDuplicatedServices AM-43569");
      System.out.println("11) CheckOrphanRecords AM-38250");
      System.out.println("12) DeleteSmartisProcessStateObsolete AM-39977");
      System.out.println("13) DeactivateRuleEngineInconsistentScene AM-40876");
      System.out.println("14) checkDuplicatedServices AM-40165");
      System.out.println("15) synchronizeSceneBetweenSceneAndRseuleEngine AM-41474");
      System.out.println("16) CheckReplicaVsOwner AM-40939");
      System.out.println("17) CheckOrphanUserEnrollmentRecords AM-44796");
      System.out.println("18) fix BOL_MULTISITE Flag to kept users AM-44752");
      System.out.println("19) things faltantes en FILTER-DISPATCHER AM-44490");
      System.out.println("20) Usuarios de ES con al menos 1 instalación inactiva, pantalla en blanco AM-43566");
      System.out.println("21) Fix Usuarios admin que le faltan permisos AM-38396");

      System.out.println("0) End. (Close report and send it by e-mail)");
   }

   private void addCurrentConfigurationOnReport(String datafixReportFile, String reportDate) {
      File f = new File("./" + datafixReportFile);
      if(!f.exists() ) {
         appendToFile(datafixReportFile, "<html><br>\n");
         appendToFile(datafixReportFile, "<h1>Datafixes report for: " + reportDate + "</h1> <br>\n");
         appendToFile(datafixReportFile, "<u>Current configuration:</u><br>\n");
         appendToFile(datafixReportFile, "datafixes-1=" + getProperty("datafixes-" + 1) + "<br>\n");
         appendToFile(datafixReportFile, "datafixes-2=" + getProperty("datafixes-" + 2) + "<br>\n");
         appendToFile(datafixReportFile, "datafixes-3=" + getProperty("datafixes-" + 3) + "<br>\n");
         appendToFile(datafixReportFile, "datafixes-4=" + getProperty("datafixes-" + 4) + "<br>\n");
         appendToFile(datafixReportFile, "datafixes-5=" + getProperty("datafixes-" + 5) + "<br><br>\n");
         appendToFile(datafixReportFile, "executeFix1=" + getProperty("executeFix1") + "<br>\n");
         appendToFile(datafixReportFile, "executeFix2=" + getProperty("executeFix2") + "<br>\n");
         appendToFile(datafixReportFile, "executeFix3=" + getProperty("executeFix3") + "<br><br>\n");
      }
   }

   private void finishAndSendReport(String env, String userToLoginFile, String usersToTurnOnCamerasFile, String instalWithoutAdminFile, String datafixReportFile, String reportDate, String datafixFolderName, String datafixExecutionName) {
      appendToFile(datafixReportFile, "</html>\n");

      // Send report
      String body = "Datafixes report for: " + reportDate;

      // On Mondays send report cc
      if (getDayOfWeek() == 1){
         sendEmail(env, "Datafixes report: " + datafixReportFile, body,
               "./" + datafixReportFile, datafixReportFile, true);
      } else {
         sendEmail(env, "Datafixes report: " + datafixReportFile, body,
               "./" + datafixReportFile, datafixReportFile);
      }

      // Send user to force login
      File f = new File("./" + userToLoginFile);
      if(f.exists() ) {
         body = "Users to force login for: " + reportDate;
         sendEmail(env, "Users to force login: " + userToLoginFile, body,
               "./" + userToLoginFile, userToLoginFile);
      } else {
         body = "No users to force login for: " + reportDate;
         sendEmail(env, "Users to force login: " + userToLoginFile, body,
               null, null);
      }

      // Send user to turn on cameras
      File fc = new File("./" + usersToTurnOnCamerasFile);
      if(fc.exists() ) {
         body = "Users to turn on cameras for: " + reportDate;
         sendEmail(env, "Users to turn on cameras: " + usersToTurnOnCamerasFile, body,
               "./" + usersToTurnOnCamerasFile, usersToTurnOnCamerasFile);
      } else {
         body = "No users to turn on cameras for: " + reportDate;
         sendEmail(env, "Users to turn on cameras: " + usersToTurnOnCamerasFile, body,
               null, null);
      }

      // Send installations data without admin
      File fInstal = new File("./" + instalWithoutAdminFile);
      if(fInstal.exists() ) {
         body = "Installations without admin user : " + reportDate;
         sendEmail(env, "Installations without admin user: " + instalWithoutAdminFile, body,
               "./" + instalWithoutAdminFile, instalWithoutAdminFile);
      }

      // Send Execution log
      runCommandLine("mv datafixExecution.log " + datafixExecutionName);

      File log = new File("./" + datafixExecutionName );
      if(log.exists() ) {
         body = "Execution log for: " + reportDate;
         sendEmail(env, "Execution log: datafixExecution.log" , body,
               "./" + datafixExecutionName , datafixExecutionName);
      }

      // Move files to dated folder
      System.out.println("moving files to data fixes folder.");

      runCommandLine("mkdir " + datafixFolderName);
      runCommandLine("mv " + datafixReportFile + " " + datafixFolderName);
      runCommandLine("mv " + userToLoginFile + " " + datafixFolderName);
      runCommandLine("mv " + usersToTurnOnCamerasFile + " " + datafixFolderName);
      runCommandLine("mv " + datafixExecutionName + " " + datafixFolderName);
      runCommandLine("mv " + instalWithoutAdminFile + " " + datafixFolderName);
      runCommandLine("mv cli-db.log " + datafixFolderName);
      runCommandLine("mv *.sql " + datafixFolderName);

      System.out.println("End of data fixes execution.");
   }

   private void runDatafix1(String env, String userToLoginFile, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) {
      LargeScript largeScript = new LargeScript();
      RepetedInstallations repetedInstallations = new RepetedInstallations();
      String sqlToCheck;
      System.out.println("1) checkRepetedInstalations AM-38370");

      sqlToCheck = repetedInstallations.checkRepetedInstalationsCCNull(env, userToLoginFile, datafixReportFile, reportDate);

      if (sqlToCheck != null) {
         System.out.println("Executing step 3: execute sql to check ....");
         System.out.println(sqlToCheck);
         select(dataSource, sqlToCheck);

         boolean executeFix1 = Boolean.valueOf(getProperty("executeFix1"));

         if (executeFix1){
            System.out.println("Executing step 4: execute deleteRepetedInstallationsCCNull.sql to fix....");
            largeScript.runMultiUpdate(env, "deleteRepetedInstallationsCCNull.sql", false, waitTimeInSeconds, datafixReportFile);
         } else {
            System.out.println("No se ejecutan los deletes para analizar casos  ....");
         }

      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix2(String env, String userToLoginFile, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) {
      System.out.println("2) checkRepetedInstalations AM-40608");
      LargeScript largeScript = new LargeScript();
      RepetedInstallations repetedInstallations = new RepetedInstallations();
      String sqlToCheck = repetedInstallations.checkRepetedInstalationsNoTree(env, userToLoginFile,datafixReportFile, reportDate);

      if (sqlToCheck != null) {
         System.out.println("Executing step 3: execute sql to check ....");
         System.out.println(sqlToCheck);
         select(dataSource, sqlToCheck);

         boolean executeFix2 = Boolean.valueOf(getProperty("executeFix2"));

         if (executeFix2) {
            System.out.println("Executing step 4: execute deleteRepetedInstallationsNoTree.sql to fix....");
            largeScript.runMultiUpdate(env, "deleteRepetedInstallationsNoTree.sql", false, waitTimeInSeconds, datafixReportFile);
         } else {
            System.out.println("No se ejecutan los deletes para analizar casos  ....");
         }
      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);

   }

   private void runDatafix3(String env, String userToLoginFile, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) {
      System.out.println("3) checkRepetedInstalationsAlias AM-40349");
      LargeScript largeScript = new LargeScript();
      RepetedInstallationsAlias repetedInstallationsAlias = new RepetedInstallationsAlias();
      String sqlToCheck = repetedInstallationsAlias.checkRepetedInstalationsAlias(env, userToLoginFile, datafixReportFile, reportDate);

      if (sqlToCheck != null) {
         System.out.println("Executing step 3: execute sql to check ....");
         select(dataSource, sqlToCheck);

         boolean executeFix3 = Boolean.valueOf(getProperty("executeFix3"));

         if (executeFix3) {
            System.out.println("Executing step 4: execute deleteRepetedInstallationsAlias.sql to fix....");
            largeScript.runMultiUpdate(env, "deleteRepetedInstallationsAlias.sql", false, waitTimeInSeconds, datafixReportFile);

         } else {
            System.out.println("No se ejecutan los deletes para analizar casos  ....");
         }

      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix4(String env, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) {
      System.out.println("4) checkInstallationsMultiOwner AM-38618");
      LargeScript largeScript = new LargeScript();
      CheckInstallationsMultiOwner checkInstallationsMultiOwner = new CheckInstallationsMultiOwner();
      int total1 = checkInstallationsMultiOwner.main(env, datafixReportFile, reportDate);

      if (total1 > 0) {
         System.out.println("\nThis datafix has no sql to check, has no force login");
         System.out.println("Executing step 3: execute purgeInstallationMultiOwners.sql ...");

         largeScript.runMultiUpdate(env, "purgeInstallationMultiOwners.sql", false, waitTimeInSeconds, datafixReportFile);
      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix5(String env, String userToLoginFile, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername) {
      System.out.println("5) fixCameraManagerSessionClientId  AM-39999");
      LargeScript largeScript = new LargeScript();
      CheckCMSession checkCMSession = new CheckCMSession();
      int total2 = checkCMSession.fixCMSession(env, userToLoginFile, datafixReportFile, reportDate);

      if (total2 > 0) {
         System.out.println("\nThis datafix has no sql to check");
         System.out.println("Executing step 2: deleteCameraManagareSessionDiffClientId.sql ..." );


         largeScript.runScript(env, "CAMERAMANAGER", "deleteCameraManagareSessionDiffClientId.sql", false, datafixReportFile);

         System.out.println("Executing step 3: deleteThirdPartySessionDiffClientId.sql ..." );
         largeScript.runScript(env, "CAMERAMANAGER", "deleteThirdPartySessionDiffClientId.sql", false, datafixReportFile);
      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix6(String env, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) {
      // DEPRECADO
      System.out.println("6) checkDuplicatedPanels AM-41075 - DEPRECADO");
      LargeScript largeScript = new LargeScript();
      CheckDuplicatedPanels checkDuplicatedPanels = new CheckDuplicatedPanels();
      int total3 = checkDuplicatedPanels.main(env, datafixReportFile, reportDate);

      if (total3 > 0) {
         System.out.println("\nThis datafix has no sql to check, has no force login");
         System.out.println("Executing step 4: execute purgeDuplicatedPanels.sql ...");

         largeScript.runMultiUpdate(env, "purgeDuplicatedPanels.sql", false, waitTimeInSeconds, datafixReportFile);
      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix7(String env, String usersToTurnOnCamerasFile, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) {
      System.out.println("7) checkPrivacyModeStatus AM-38079");
      LargeScript largeScript = new LargeScript();
      CheckPrivacyModeStatus checkPrivacyModeStatus = new CheckPrivacyModeStatus();
      int total4 = checkPrivacyModeStatus.fixMPStatusDevices(env, datafixReportFile, reportDate, usersToTurnOnCamerasFile);

      if (total4 > 0) {
         System.out.println("\nThis datafix has no sql to check, has no force login");
         System.out.println("Executing step 4: execute fixDevicesMPStatus.sql ...");

         largeScript.runMultiUpdate(env, "fixDevicesMPStatus.sql", false, waitTimeInSeconds, datafixReportFile);
      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix8(String env, String instalWithoutAdminFile, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) {
      System.out.println("8) checkOrphanInstallations AM-40939");
      LargeScript largeScript = new LargeScript();
      CheckOrphanInstallations checkOrphanInstallations = new CheckOrphanInstallations();
      int total5 = checkOrphanInstallations.fixOrphanInstallations(env, datafixReportFile, reportDate, instalWithoutAdminFile);

      if (total5 > 0) {
         System.out.println("\nThis datafix has no sql to check, has no force login");
         System.out.println("Executing step 4: execute fixOrphanInstallations.sql ...");

         largeScript.runMultiUpdate(env, "fixOrphanInstallations.sql", false, waitTimeInSeconds, datafixReportFile);
      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix9(String env, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) {
      System.out.println("9) checkRepeatedCMCredentials AM-43282 - DISCONTINUADO");
      LargeScript largeScript = new LargeScript();
      CheckCMRepeatedCredentials checkCMRepeatedCredentials = new CheckCMRepeatedCredentials();
      int total6 = checkCMRepeatedCredentials.fixCMRepeatedCredentials(env, datafixReportFile, reportDate);

      if (total6 > 0) {
         System.out.println("\nThis datafix has no sql to check, has no force login");
         System.out.println("Executing step 4: execute fixCMRepeatedCredentials.sql ...");

         largeScript.runMultiUpdate(env, "fixCMRepeatedCredentials.sql", false, waitTimeInSeconds, datafixReportFile);
      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix10(String env, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) {
      System.out.println("10) checkDuplicatedServices AM-43569");
      LargeScript largeScript = new LargeScript();
      CheckDuplicatedServices checkDuplicatedServices = new CheckDuplicatedServices();
      int total7 = checkDuplicatedServices.fixRepeatedServices(env, datafixReportFile, reportDate);

      if (total7 > 0) {
         System.out.println("\nThis datafix has no sql to check, has no force login");
         System.out.println("Executing step 4: execute fixRepeatedServies.sql ...");

         largeScript.runMultiUpdate(env, "fixRepeatedServies.sql", false, waitTimeInSeconds, datafixReportFile);
      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix11(String env, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) throws InterruptedException {
      System.out.println("11) check Orphan Thing, ThingAlias, Installation & InstallationAlias AM-38250");
      LargeScript largeScript = new LargeScript();
      CheckOrphanRecords checkOrphanRecords = new CheckOrphanRecords();
      int total11 = checkOrphanRecords.main(env, datafixReportFile, reportDate);

      if (total11 > 0) {
         System.out.println("\nThis datafix has no sql to check, has no force login");
         System.out.println("\nDESCOMENTAR LAS EJECUCIONES DE MULTIUPDATE");
         System.out.println("Executing step 6: execute deleteOrphanThingAlias.sql ...");

         largeScript.runMultiUpdate(env, "deleteOrphanThingAlias.sql", false, waitTimeInSeconds, datafixReportFile);

         System.out.println("Waiting for " + waitTimeInSeconds + " seconds." );
         wait(waitTimeInSeconds * 1000);

         System.out.println("Executing step 7: execute deleteOrphanThingByThingAlias.sql ...");

         largeScript.runMultiUpdate(env, "deleteOrphanThingByThingAlias.sql", false, waitTimeInSeconds, datafixReportFile);

         System.out.println("Waiting for " + waitTimeInSeconds + " seconds." );
         wait(waitTimeInSeconds * 1000);

         System.out.println("Executing step 8: execute deleteOrphanInstallationAlias.sql ...");

         largeScript.runMultiUpdate(env, "deleteOrphanInstallationAlias.sql", false, waitTimeInSeconds, datafixReportFile);

         System.out.println("Waiting for " + waitTimeInSeconds + " seconds." );
         wait(waitTimeInSeconds * 1000);

         System.out.println("Executing step 9: execute deleteOrphanThingByInstallation.sql ...");

         largeScript.runMultiUpdate(env, "deleteOrphanThingByInstallation.sql", false, waitTimeInSeconds, datafixReportFile);

         System.out.println("Waiting for " + waitTimeInSeconds + " seconds." );
         wait(waitTimeInSeconds * 1000);

         System.out.println("Executing step 10: execute deleteOrphanInstallation.sql ...");

         largeScript.runMultiUpdate(env, "deleteOrphanInstallation.sql", false, waitTimeInSeconds, datafixReportFile);

         System.out.println("Waiting for " + waitTimeInSeconds + " seconds." );
         wait(waitTimeInSeconds * 1000);

      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix12(String env, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername) {
      System.out.println("12) DeleteSmartisProcessStateObsolete AM-39977");
      LargeScript largeScript = new LargeScript();
      CheckSmartisPSObsolete checkSmartisPSObsolete = new CheckSmartisPSObsolete();
      int total12 = checkSmartisPSObsolete.findProcessStateObsolete(env, datafixReportFile, reportDate);

      if (total12 > 0) {
         System.out.println("\nThis datafix has no sql to check");
         System.out.println("Executing step 2: deleteSmartisProcessStateObsoleteById.sql ..." );

         largeScript.runScript(env, "SMARTIS", "deleteSmartisProcessStateObsoleteById.sql", false, datafixReportFile);

      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix13(String env, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername) {
      System.out.println("13) DeactivateRuleEngineInconsistentScene AM-40876");
      LargeScript largeScript = new LargeScript();
      CheckRuleEngineScene checkRuleEngineScene = new CheckRuleEngineScene();
      int total13 = checkRuleEngineScene.findInconsistentSceneRuleEngine(env, datafixReportFile, reportDate);

      if (total13 > 0) {
         System.out.println("\nThis datafix has no sql to check");
         System.out.println("Executing step 2: deactivateInconsistentScene.sql ..." );

         largeScript.runScript(env, "RULE-ENGINE", "deactivateInconsistentScene.sql", false, datafixReportFile);

      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix14(String env, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) {
      System.out.println("14) checkDuplicatedServices AM-40165");
      LargeScript largeScript = new LargeScript();
      CheckDuplicatedAliases checkDuplicatedAliases = new CheckDuplicatedAliases();
      int total14 = checkDuplicatedAliases.fixRepeatedAliases(env, datafixReportFile, reportDate);

      if (total14 > 0) {
         System.out.println("\nThis datafix has no sql to check, has no force login");
         System.out.println("Executing step 4: execute fixRepeatedAliases.sql ...");
         largeScript.runMultiUpdate(env, "fixRepeatedAliases.sql", false, waitTimeInSeconds, datafixReportFile);
      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix15(String env, String userToLoginFile, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername) {
      System.out.println("15) synchronizeSceneBetweenSceneAndRseuleEngine AM-41474");
      LargeScript largeScript = new LargeScript();
      CheckUnsynchronizedScene checkUnsynchronizedScene = new CheckUnsynchronizedScene();
      int total15 = checkUnsynchronizedScene.findUnsynchronizedScene(env,userToLoginFile,datafixReportFile,reportDate);

      if (total15 > 0) {
         System.out.println("\nThis datafix has no sql to check");
         System.out.println("Executing step 2: createMissingScene.sql ..." );

         largeScript.runScript(env, "RULE-ENGINE", "createMissingScene.sql", false, datafixReportFile);

      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix16(String env, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) {
      System.out.println("16) CheckReplicaVsOwner AM-40193");
      LargeScript largeScript = new LargeScript();
      CheckAliasesReplicaVsOwner checkReplicaVsOwner = new CheckAliasesReplicaVsOwner();
      int total16 = checkReplicaVsOwner.fixWrongThingAliases(env, datafixReportFile, reportDate, waitTimeInSeconds);

      if (total16 > 0) {
         System.out.println("\nThis datafix has no sql to check, has no force login");
         System.out.println("Executing step 4: execute fixWrongThingAliases.sql ...");
         largeScript.runMultiUpdate(env, "fixWrongThingAliases.sql", false, waitTimeInSeconds, datafixReportFile);
      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);

      System.out.println("End at: " + getFormattedDate(new Date()));
   }

   private void runDatafix17(String env, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername) {
      System.out.println("17) CheckOrphanUserEnrollmentRecords AM-44796");
      LargeScript largeScript = new LargeScript();
      CheckOrphanUserEnrollmentRecords checkOrphanUserEnrollmentRecords = new CheckOrphanUserEnrollmentRecords();
      int total17 = checkOrphanUserEnrollmentRecords.main(env, datafixReportFile, reportDate);

      if (total17 > 0) {
         System.out.println("\nThis datafix has no sql to check");
         System.out.println("Executing step 2: deleteOrphanUserEnrollment.sql ..." );

         largeScript.runScript(env, "USER-ENROLLMENT", "deleteOrphanUserEnrollment.sql", false, datafixReportFile);

      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix18(String env, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) {
      System.out.println("18) checkMultisiteFlagToKeptUsers AM-44752");
      LargeScript largeScript = new LargeScript();
      String updateStatementFileName  = "fixMultisiteFlagKeptUsers.sql";
      CheckMultisiteFlagToKeptUsers checkMultisiteFlagToKeptUsers = new CheckMultisiteFlagToKeptUsers();
      int total1 = checkMultisiteFlagToKeptUsers.main(env, datafixReportFile, reportDate, updateStatementFileName);

      if (total1 > 0) {
         System.out.println("Executing step 2: execute " + updateStatementFileName +" ...");

         largeScript.runMultiUpdate(env, updateStatementFileName, false, waitTimeInSeconds, datafixReportFile);
      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix19(String env, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) {
      System.out.println("19) check missing Thing on Filter-dispatcher AM-44490");
      LargeScript largeScript = new LargeScript();
      String updateStatementFileName  = "fixMissingThingAndInstAliasOnFilterDispatcher.sql";
      CheckMissingThingOnFilterDispatcher checkMissingThingOnFilterDispatcher = new CheckMissingThingOnFilterDispatcher();
      int total1 = checkMissingThingOnFilterDispatcher.main(env, datafixReportFile, reportDate, updateStatementFileName, waitTimeInSeconds);

      if (total1 > 0) {
         System.out.println("Executing step 4: execute " + updateStatementFileName +" ...");

         largeScript.runScript(env, "FILTER-DISPATCHER", updateStatementFileName, false, datafixReportFile);
      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix20(String env, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) {
      System.out.println("20) check white screen users AM-43566");
      LargeScript largeScript = new LargeScript();
      String updateStatementFileName  = "fixWhiteScreenUsers.sql";
      CheckWhiteScreenUsers checkWhiteScreenUsers = new CheckWhiteScreenUsers();
      int total1 = checkWhiteScreenUsers.main(env, datafixReportFile, reportDate, updateStatementFileName, waitTimeInSeconds);

      if (total1 > 0) {
         System.out.println("Executing step 3: execute " + updateStatementFileName +" ...");

         largeScript.runScript(env, "ACCOUNT-MANAGEMENT", updateStatementFileName, false, datafixReportFile);
      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

   private void runDatafix21(String env, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername, int waitTimeInSeconds) {
      System.out.println("21) fix missing thingAlias for admin users AM-38396");
      LargeScript largeScript = new LargeScript();
      String updateStatementFileName  = "fixMissingThingAlias4AdminUsers.sql";
      FixMissingThingAlias4AdminUsers fixMissingThingAlias4AdminUsers = new FixMissingThingAlias4AdminUsers();
      int total1 = fixMissingThingAlias4AdminUsers.main(env, datafixReportFile, reportDate, updateStatementFileName, waitTimeInSeconds);

      if (total1 > 0) {
         System.out.println("Executing step 4: execute " + updateStatementFileName + " ...");

         largeScript.runMultiUpdate(env, updateStatementFileName, false, waitTimeInSeconds, datafixReportFile);
      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }

}
