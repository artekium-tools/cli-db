package clidb;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClient;
import com.amazonaws.services.cognitoidp.model.*;
import com.amazonaws.util.StringUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import javax.ws.rs.core.MultivaluedMap;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class UserInfo extends QryDB {


   private  final Logger LOGGER = LogManager.getLogger(UserInfo.class);

   int maxEvents = 50;

   private Long seq_user_id;
   private int isAdministrator;
   private int isMultisite;
   private String desClientId;
   private String desCountry;
   private ArrayList<Long> arr_inst_alias_id = new ArrayList();
   private ArrayList<Long> arr_installation_id = new ArrayList();
   private ArrayList<Long> arr_ext_services_id = new ArrayList();
   private Long seq_panel_id;
   private Long seq_installation_id;
   private Long seq_smart_id;
   private Long seq_bath_id;
   private boolean objectTrackerInfo = false;
   private boolean withReplicas = false;

   public void runUserInfo(String env) {

      // https://www.w3schools.com/colors/colors_names.asp

      loadExternalFileProperties();
      DataSource dataSource = null;

      String jdbcUrl = "";
      String dbUsername = "";
      String password = "";
      String userDataFile = "";

      withReplicas = "1".equals(getProperty("withReplicas")) ? true: false;
      objectTrackerInfo = "1".equals(getProperty("withReplicas")) ? true: false;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);
         System.out.println("objectTrackerInfo: " + objectTrackerInfo);
         System.out.println("withReplicas: " + withReplicas);

         // select jdbc url
         if ("vpn".equals(connectionType)) {
            jdbcUrl = getProperty("vpn." + env + ".url");
         } else {
            jdbcUrl = getProperty("telepresence." + env + ".url");
         }

         dbUsername = getProperty(env + ".username");
         password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         Files.createDirectories(Paths.get("./userInfo/dev"));
         Files.createDirectories(Paths.get("./userInfo/pre"));
         Files.createDirectories(Paths.get("./userInfo/pro"));

      } catch (Exception e) {
         printStackTrace(e);
         return;
      }

      int userNameIndex = 0;
      String[] arrUserNames = null;
      int arrUserNamesLength = 0;

      while (true) {
         try {

            String userName = "";

            // Search menu
            while (true) {

               System.out.println("nickNname | e-mail | userId | installationId | thingId | country & clientId ");

               System.out.println(" ");
               Scanner scanner1 = new Scanner(new InputStreamReader(System.in));
               System.out.println("nickNname:  ");
               userName = scanner1.nextLine();


               String email = "";
               String userId= "";
               String installationId = "";
               String thingId = "";
               String country= "";
               String clientId= "";

               if ("".equals(userName)){
                  Scanner scanner1_1 = new Scanner(new InputStreamReader(System.in));
                  System.out.println("e-mail:  ");
                  email = scanner1_1.nextLine();

                  if ("".equals(email)) {
                     Scanner scanner2 = new Scanner(new InputStreamReader(System.in));
                     System.out.println("userId:  ");
                     userId = scanner2.nextLine();

                     if ("".equals(userId)) {

                        Scanner scanner5 = new Scanner(new InputStreamReader(System.in));
                        System.out.println("installationId:  ");
                        installationId = scanner5.nextLine();

                        if ("".equals(installationId)) {
                           Scanner scanner6 = new Scanner(new InputStreamReader(System.in));
                           System.out.println("thingId:  ");
                           thingId = scanner6.nextLine();

                           if ("".equals(thingId)) {

                              Scanner scanner3 = new Scanner(new InputStreamReader(System.in));
                              System.out.println("country:  ");
                              country = scanner3.nextLine();

                              Scanner scanner4 = new Scanner(new InputStreamReader(System.in));
                              System.out.println("clientId:  ");
                              clientId = scanner4.nextLine();
                           }

                        }
                     }

                  }

                  String sqlSearch = "SELECT SEQ_ID, DES_NICK_NAME as NicName, DES_EMAIL as Email, DES_CLIENT_ID as ClientId," +
                        " DES_COUNTRY as Country, BOL_ADMINISTRATOR as Admin, " +
                        "BOL_MULTISITE as Multi " +
                        //", FYH_DELETED, FYH_LAST_ACCESS " +
                        "FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER WHERE ";

                  System.out.println(" ");

                  if (!"".equals(email)) {
                     System.out.println("Query by email: " + email);
                     sqlSearch += "DES_EMAIL = '" + email + "'";
                  }

                  if (!"".equals(userId)) {
                     System.out.println("Query by userId: " + userId);
                     sqlSearch += "SEQ_ID = " + userId;
                  }

                  if (!"".equals(country) && !"".equals(clientId)) {
                     System.out.println("Query by country: " + country + " clientId: " + clientId);
                     sqlSearch += "DES_COUNTRY = '" + country + "' AND DES_CLIENT_ID = '" + clientId + "'";
                  }

                  if (!"".equals(installationId)) {
                     System.out.println("Query by installationId: " + installationId);
                     sqlSearch += "SEQ_ID = (" +
                           "  SELECT inst.SEQ_ADMIN FROM " +
                           "\"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION inst " +
                           "WHERE inst.SEQ_ID = " + installationId +")" ;
                  }

                  if (!"".equals(thingId)) {
                     System.out.println("Query by thingId: " + thingId);
                     sqlSearch += "SEQ_ID = (" +
                           "  SELECT inst.SEQ_ADMIN FROM " +
                           "  \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION inst " +
                           "  WHERE inst.SEQ_ID = " +
                           " ( " +
                           "     SELECT SEQ_INSTALLATION_ID " +
                           "     FROM \"ACCOUNT-MANAGEMENT\".SMPR_TTHING " +
                           "     WHERE " +
                           "     SEQ_INSTALLATION_ID IS NOT NULL " +
                           "     AND (seq_id = "+ thingId +" OR " +
                           "     seq_id = ( " +
                           "     SELECT SEQ_PARENT_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TTHING " +
                           "     WHERE SEQ_ID = "+ thingId +")) " +
                           " ))" ;
                  }

                  sqlSearch += " ORDER BY BOL_ADMINISTRATOR desc";

                  select(dataSource, sqlSearch);

                  Scanner report = new Scanner(new InputStreamReader(System.in));
                  System.out.println(" ");
                  System.out.println("Type userName for report or (n) to another search: " );
                  userName = report.nextLine();
                  if ("n".equals(userName)){
                     continue;
                  } else {
                     break;
                  }

               } else {
                  break;
               }
            }

            if (userName.contains(";")) {
               arrUserNames = userName.split(";");
               arrUserNamesLength = arrUserNames.length;
            }

            if (arrUserNamesLength > 0 && userNameIndex < arrUserNamesLength - 1) {
               userName = arrUserNames[userNameIndex];
               userNameIndex++;
            } else if (userNameIndex > 0) {
               userName = arrUserNames[userNameIndex];
               userNameIndex = 0;
               arrUserNamesLength = 0;
            }

            Scanner scannerOp = new Scanner(new InputStreamReader(System.in));
            System.out.println("ObjectTracker info (y/n):  ");
            String ot = scannerOp.nextLine();

            if ("y".equals(ot)){
               objectTrackerInfo = true;
            }

            Scanner scannerRep = new Scanner(new InputStreamReader(System.in));
            System.out.println("Replicas info (y/n):  ");
            String rep = scannerRep.nextLine();

            if ("y".equals(rep)){
               withReplicas = true;
            }

            userDataFile = generateUserReport(dataSource, env, userName);

            if (userDataFile == null) continue;

         } catch (Exception e) {
            printStackTrace(e);
         }

         //runCommandLine("google-chrome " + userDataFile);
         System.out.println(" ");
         System.out.println(userDataFile.substring(userDataFile.lastIndexOf("/") + 1));
         System.out.println(" ");
      }
   }

   public String generateUserReport(DataSource dataSource, String env, String userName){

      String userDataFile = null;

      try {
         String content = "";
         String start = "<html><body>\n";
         String end = "</body></html>";


         System.out.println("Query data for: " + userName);

         String schema = "\"USER-ENROLLMENT\".";

         String userEnrollInfo = getUserEnrollmentInfo(dataSource, userName, schema);

         if (userEnrollInfo.contains("Empty Result")) {
            System.out.println(" ");
            System.out.println("**** No Result found for :" + userName + " ***");
            return null;
         }

         String userAtEnrollInfo = getUserAtEnrollmentInfo(dataSource, userName, schema);

         if (isMultisite == 1) {
            UserInfoMultisiteReport userInfoMultisiteReport = new UserInfoMultisiteReport();
            userInfoMultisiteReport.setDesClientId(desClientId);
            userInfoMultisiteReport.setDesCountry(desCountry);
            userInfoMultisiteReport.setObjectTrackerInfo(objectTrackerInfo);
            userInfoMultisiteReport.setWithReplicas(withReplicas);
            content = userInfoMultisiteReport.generateMultiSiteUserReport( dataSource, env, userName, userEnrollInfo, userAtEnrollInfo);
         } else {
            UserInfoIndividualReport userInfoIndividualReport = new UserInfoIndividualReport();
            userInfoIndividualReport.setDesClientId(desClientId);
            userInfoIndividualReport.setDesCountry(desCountry);
            userInfoIndividualReport.setObjectTrackerInfo(objectTrackerInfo);
            userInfoIndividualReport.setWithReplicas(withReplicas);
            content = userInfoIndividualReport.generateIndividualUserReport( dataSource, env, userName, userEnrollInfo, userAtEnrollInfo);
         }

         String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
         userDataFile = "./userInfo/" + env + "/userInfo-" + userName + "-" + timeStamp + ".html";

         writeToFile(userDataFile, start + content + end);

      } catch (Exception e){
         printStackTrace(e);
      }

      return userDataFile;
   }


   public void setDesClientId(String desClientId) {
      this.desClientId = desClientId;
   }


   public void setDesCountry(String desCountry) {
      this.desCountry = desCountry;
   }


   public  String selectToInfo(final DataSource dataSource, String sql, String tableName, String color) {
      String row = "";
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            //System.out.println("------------------------------------------------");
            //System.out.println(sql);

            int columns = 0;
            int rowCount = 0;
            if (rs.next()) {
               ResultSetMetaData rsmd = rs.getMetaData();

               row = row + "<th bgcolor='" + color + "'>&nbsp;</th>\n";

               for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                  columns++;
                  if ("tEvents".equals(tableName) &&
                        ("SEQ_ID".equals(rsmd.getColumnName(i)) || rsmd.getColumnName(i).equals("DES_TEXT"))) {
                     row = row + "<th bgcolor='" + color + "'>" + rsmd.getColumnName(i) +
                           "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>\n";
                  } else {
                     row = row + "<th bgcolor='" + color + "'>" + rsmd.getColumnName(i) + "</th>\n";
                  }
               }

               if (rs.last()) rs.beforeFirst();


               String recordStart = "<table border=1>" +
                     "<tr>" +
                     "<th bgcolor=" + color + ">Column</th><th bgcolor=" + color + ">Value</th>" +
                     "</tr>";
               String recordEnd = "</table><br><button onclick=self.close()>Close</button>";

               String recordContent;

               // Get row values to show
               while (rs.next()) {
                  row = row + "<tr>";

                  recordContent = "";

                  for (int i = 1; i <= rsmd.getColumnCount(); i++) {

                     recordContent = recordContent + "<tr><td>" + rsmd.getColumnName(i).toUpperCase() + "</td>" +
                           "<td>" + rs.getString(rsmd.getColumnName(i)) + "</td></tr>";
                  }

                  // Celda para ver registro en vertical
                  row = row + "<td> <button onclick=\"openWindow('" +  recordStart + recordContent + recordEnd + "')\"> # </button></td>";


                  for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                     if ("SEQ_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER".equals(tableName)) {
                        seq_user_id = rs.getLong(rsmd.getColumnName(i));
                     }

                     if ("BOL_ADMINISTRATOR".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER".equals(tableName)) {
                        isAdministrator = rs.getInt(rsmd.getColumnName(i));
                     }

                     if ("BOL_MULTISITE".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER_ENROLLMENT".equals(tableName)) {
                        isMultisite = rs.getInt(rsmd.getColumnName(i));
                     }


                     if ("DES_CLIENT_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER_ENROLLMENT".equals(tableName)) {
                        desClientId = rs.getString(rsmd.getColumnName(i));
                     }
                     if ("DES_COUNTRY_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER_ENROLLMENT".equals(tableName)) {
                        desCountry = rs.getString(rsmd.getColumnName(i));
                     }

                     if ("SEQ_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "InstallationAlias".equals(tableName)) {
                        arr_inst_alias_id.add(rs.getLong(rsmd.getColumnName(i)));
                     }

                     if ("SEQ_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "Panels".equals(tableName)) {
                        seq_panel_id = (rs.getLong(rsmd.getColumnName(i)));
                     }

                     if ("SEQ_INSTALLATION_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SetInstallationId".equals(tableName)) {
                        seq_installation_id = (rs.getLong(rsmd.getColumnName(i)));
                     }

                     if ("SEQ_INSTALLATION_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "getInstallationId".equals(tableName)) {
                        arr_installation_id.add(rs.getLong(rsmd.getColumnName(i)));
                     }

                     if ("DES_SIEBEL_CODE".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER_EXTERNAL_SERVICE".equals(tableName)) {
                        arr_ext_services_id.add(rs.getLong(rsmd.getColumnName(i)));
                     }

                     if ("tAccount".equals(tableName) &&
                           "DES_MIGRATION_STATUS".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "OK".equals(rs.getString(rsmd.getColumnName(i)))
                     ) {
                        seq_smart_id = rs.getLong("SEQ_SMART_USER_ID");
                        seq_bath_id = rs.getLong("SEQ_BATCH_PROCESS_STATE_ID");
                     }

                     // Pintamos el fondo si tiene fecha de borrado
                     if ("FYH_DELETED".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           !StringUtils.isNullOrEmpty(rs.getString(rsmd.getColumnName(i)))){
                        row = row + "<td bgcolor='orange'>" + rs.getString(rsmd.getColumnName(i)) + "</td>";
                        // Pintamos el fondo de fecha de ultimo acceso
                     } else if ("FYH_LAST_ACCESS".equals(rsmd.getColumnName(i).toUpperCase())){
                        row = row + "<td bgcolor='green'>" + rs.getString(rsmd.getColumnName(i)) + "</td>";
                        // resto de las celdas
                     } else {
                        row = row + "<td>" + rs.getString(rsmd.getColumnName(i)) + "</td>";
                     }

                  }

                  row = row + "</tr>\n";
                  rowCount ++;
               }

               row = row + "<tr><td colspan='2'> <b>" + rowCount + " rows.</b> </td>" +
                     "<td colspan='" + (columns -1 ) +"'>" + sql + " </td></tr>\n";

            } else {
               row = row + "<th> Empty Result </th>\n";
               row = row + "<tr><td>" + sql + " </td></tr>\n";
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return row;
   }

   private  String getUserEnrollmentInfo(DataSource dataSource, String userName, String schema) {
      printInLine("getUserEnrollmentInfo...                           ");
      String start = "<h2> User Enrollment | user-enrollment - smpr_tuser_enrollment </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TUSER_ENROLLMENT where upper (DES_USER_NAME) = upper ('" + userName + "')";

         body = selectToInfo(dataSource, sql, "SMPR_TUSER_ENROLLMENT", "yellow");

         try {
            dataSource.getConnection().close();
            ////System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getUserAtEnrollmentInfo(DataSource dataSource, String userName, String schema) {

      printInLine("getUserAtEnrollmentInfo...                           ");
      String start = "<h2> User | user-enrollment - smpr_tuser </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TUSER where upper (DES_NICK_NAME) = upper ('" + userName + "')";

         body = selectToInfo(dataSource, sql, "SMPR_TUSER_ENROLLMENT", "Lavender");

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   public String getCRMContract(String env){

      String start = "<h2> Installations from CRM </h2>\n" +
            "<table border='1'>\n" +
            "<tr width='100%'><td>\n";
      String end = "</td></tr>\n"+ "</table>";


      try {
         String host="https://apiemea.prosegur.com";
         String url= host + "/alarms/smart";

         String tokenUrl = host + "/api/oauth/token";
         String getContractUrl = url + "/v1/country/"+desCountry+"/client/"+desClientId+"/contractsData";

         Client client = Client.create();
         WebResource webResource1 = client.resource(tokenUrl);
         webResource1.header("Content-Type", "application/x-www-form-urlencoded");

         MultivaluedMap formData = new MultivaluedMapImpl();
         formData.add("scope", "alarms.write, alarms.read");
         formData.add("client_secret", getProperty(env + ".client_secret"));
         formData.add("client_id", getProperty(env + ".client_id"));
         formData.add("grant_type", "client_credentials");

         ClientResponse response1 = webResource1.post(ClientResponse.class, formData);
         String tokenResponse = response1.getEntity(String.class);
         String tokenValue = tokenResponse.substring(17,71);

         Client client2 = Client.create();

         //System.out.println("get Installation from crm: " + getInstallationUrl);

         WebResource webResource2 = client2.resource(getContractUrl);
         WebResource.Builder builder =  webResource2.getRequestBuilder();

         builder
               .header("authorization", "Bearer " + tokenValue)
               .header("Content-Type", "application/json")
               .header("ExtSys", "smart");

         ClientResponse response2 = builder.get(ClientResponse.class);
         String result = response2.getEntity(String.class);


         result = result.replaceAll("\n", "<br>\n");
         result = result.replaceAll(" ", "&nbsp;");
         String content = formatJson4Report(result);
         content = content.replaceAll("<br><br>", "<br>");


         //return  start + "<br><br><font color=\"blue\">" + getContractUrl + "</font><br><br>" + content + end;
         return   getContractUrl + "\n" + content ;
      } catch (Exception e) {
         return start + e.getMessage() + end;
      }
   }


   public void setWithReplicas(boolean withReplicas) {
      this.withReplicas = withReplicas;
   }


}
