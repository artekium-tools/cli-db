package clidb;

import clidb.entities.Installation;
import clidb.entities.InstallationAlias;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;


public class RepetedInstallationsAlias extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(RepetedInstallationsAlias.class);

   static boolean UPDATE_IN_TEST_MODE = false; // true=olny shows logs  false=execute updates

   static Properties prop = new Properties();
   static Properties scriptProperties = new Properties();
   static ArrayList<Long> arr_seq_user_id = new ArrayList();

   public String checkRepetedInstalationsAlias(String env, String userToLoginFile, String datafixReportFile, String reportDate) {

      loadExternalFileProperties();

      DataSource dataSource = null;
      String sqlToCheck = null;
      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         // Buscamos las instalaciones con installationAlias duplicadas
         System.out.println("Executing step 1: select repeted installationsAlias ..." );
         ArrayList<Installation> arrInstallation = selectInstallations(dataSource);

         System.out.println("Total Installations: " + arrInstallation.size());
         int count = 0;
         String msg;

         System.out.println("Executing step 2: check repeted installationsAlias to delete ..." );
         for (Installation installation: arrInstallation) {
            count ++;
            //System.out.println("  " );

            msg = "@@@@ " + count + " of " + arrInstallation.size() +
                  " DesNickName: " + installation.getDesNickName()  + "  " +
                  (installation.isAdmin() == true ? "ADMINISTRATOR" : "DEPENDENT" ) + "   Count: " +
                  installation.getCountInstAlias()                   +" -------------------------\n";


            appendToFile("analisis.txt", msg);
            // Buscar los installationAlias
            ArrayList<InstallationAlias> arrInstallationAlias = getInstallationAlias(dataSource, installation);

            // Ver si tiene eventos
            // Si No tiene Borrar IntallationAlias, ThingAlias y Things)
            // Si tiene eventos en ambos, borrar el Id mas alto de installationAlias
            boolean insta1HasEvents = false;
            boolean insta2HasEvents = false;
            Long maxInstAliasId = 0L;
            Long instAliasIdNoEvents = 0L;
            int instaOrder = 1;

            for (InstallationAlias installationAlias: arrInstallationAlias){
               if (installationAlias.getId().longValue() > maxInstAliasId){
                  maxInstAliasId = installationAlias.getId();
               }
               //
               if (hasEvent(dataSource, installationAlias)) {
                  if (instaOrder == 1){
                     insta1HasEvents = true;
                  } else {
                     insta2HasEvents = true;
                  }
               } else {
                  instAliasIdNoEvents = installationAlias.getId();
               }
               instaOrder ++;
            }

/*
            tiene Evento:
            Insta1  SI
            Insta2  SI
            Borrar id + Alto

            Insta1  No
            Insta2  No
            Borrar id + Alto

            Insta1  SI
            Insta2  NO
            Borrar el que no tiene eventos */

            // true true o false false Buscamos el de mayor id para borrar
            if ((insta1HasEvents && insta2HasEvents) || (!insta1HasEvents && !insta2HasEvents) ){
               for (InstallationAlias installationAlias: arrInstallationAlias){
                  if (installationAlias.getId().equals(maxInstAliasId)){
                     generateDeleteStatements(dataSource, installationAlias.getId(), installationAlias.getUserId());
                     appendToFile("analisis.txt", " add InstallationAlias:  " + installationAlias.getId() + " to delete statements\n");
                  }
               }
            // true false o false true
            } else {
               for (InstallationAlias installationAlias: arrInstallationAlias){
                  if (installationAlias.getId().equals(instAliasIdNoEvents)){
                     generateDeleteStatements(dataSource, installationAlias.getId(), installationAlias.getUserId());
                     appendToFile("analisis.txt", " add InstallationAlias:  " + installationAlias.getId() + " to delete statements\n");
                  }
               }
            }

         }

         appendToFile(datafixReportFile,
               "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-40349'>" +
                     "3) CS " +
                     "https://jira.prosegur.com/browse/AM-40349</a> - " +
                     "Installation Alias y ThingAlias repetidas<br>\n");


         if (arrInstallation.size() > 0) {

            sqlToCheck = "SELECT instAlias.SEQ_INSTALLATION_ID, usr.SEQ_ID, usr.DES_NICK_NAME, usr.DES_CLIENT_ID, usr.DES_COUNTRY, usr.BOL_ADMINISTRATOR , COUNT(instAlias.seq_id) AS cnt \n" +
                  "FROM  \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS instAlias \n" +
                  "INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr ON (instAlias.SEQ_USER_ID = usr.SEQ_ID AND usr.FYH_DELETED IS null) \n" +
                  "GROUP BY instAlias.SEQ_INSTALLATION_ID, usr.SEQ_ID, usr.DES_NICK_NAME, usr.DES_CLIENT_ID, usr.DES_COUNTRY, usr.BOL_ADMINISTRATOR \n" +
                  "HAVING count(instAlias.SEQ_ID) > 1 \n" +
                  "ORDER BY usr.DES_CLIENT_ID, usr.DES_COUNTRY \n";

            /*System.out.println(" -----------------------------------------");
            System.out.println("sqlToCheck: " );
            System.out.println(sqlToCheck);

            System.out.println("  ");
            System.out.println(" ");
            System.out.println(" -----------------------------------------");
            System.out.println("List of userNames to force login:");*/

            appendToFile(userToLoginFile,  "-- checkRepetedInstalationsAlias AM-40349\n");
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", se fixearon " + arrInstallation.size() + " casos. <br>\n");

            for (Installation installation:arrInstallation){
               System.out.println(installation.getDesNickName());

               generateAndSendUserReport(dataSource, env, installation.getDesNickName(), "cristian.salcedo@artekium.com");

               appendToFile(userToLoginFile, installation.getDesNickName() + "\n");
               appendToFile(datafixReportFile, installation.getDesNickName() + "<br>\n");

            }

           // System.out.println("1) run sqlToCheck: must return Non empty result");
            //System.out.println("2) run multiUpdate for ./deleteRepetedInstallationsAlias.sql ");
            System.out.println("sqlToCheck: must return empty result (repeat checkRepetedInstalationsAlias if some not fixed)");
            //System.out.println("4) force login for users ");
            //System.out.println(" ");
            //LOGGER.info("End Process");
         } else {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+ ", no se encontraron casos a fixear<br>\n");
         }


      } catch (Exception e){
         printStackTrace(e);
      }

      //LOGGER.info("End process.");

      return sqlToCheck;
   }

   public static ArrayList<Installation> selectInstallations(final DataSource dataSource) {

      ArrayList<Installation> arrInstallation = new ArrayList<>();

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql = "SELECT instAlias.SEQ_INSTALLATION_ID, usr.SEQ_ID, usr.DES_NICK_NAME, usr.DES_CLIENT_ID, usr.DES_COUNTRY, usr.BOL_ADMINISTRATOR , COUNT(instAlias.seq_id) AS cnt " +
                  "FROM  \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS instAlias " +
                  "INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr ON (instAlias.SEQ_USER_ID = usr.SEQ_ID AND usr.FYH_DELETED IS null) " +
                  "GROUP BY instAlias.SEQ_INSTALLATION_ID, usr.SEQ_ID, usr.DES_NICK_NAME, usr.DES_CLIENT_ID, usr.DES_COUNTRY, usr.BOL_ADMINISTRATOR " +
                  "HAVING count(instAlias.SEQ_ID) > 1 " +
                  "ORDER BY usr.DES_CLIENT_ID, usr.DES_COUNTRY";

           // System.out.println(sql);
            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);
/*
*
* SEQ_INSTALLATION_ID|DES_NICK_NAME                        |DES_CLIENT_ID|DES_COUNTRY|BOL_ADMINISTRATOR|COUNT(INSTALIAS.SEQ_ID)|
-------------------+---------------------------------------+-------------+-----------+-----------------+-----------------------+
            1199687|newmotionargentina                     |0239533      |AR         |                1|                      2|
             228600|teresa.mollh@gmail.com                 |0245234      |CL         |                1|                      2|
            1147023|gionferri.alberto@hotmail.com.ar       |0276117      |AR         |                1|                      2|
             358174|mcazachkoff@gmail.com                  |0395315      |AR         |                1|                      2|
*
* */
            while (rs.next()) {

               Installation installation = new Installation();

               installation.setId(rs.getLong("SEQ_INSTALLATION_ID"));
               installation.setSeqUserId(rs.getLong("SEQ_ID"));
               installation.setDesNickName(rs.getString("DES_NICK_NAME"));
               installation.setClientId(rs.getString("DES_CLIENT_ID"));
               installation.setCountry(rs.getString("DES_COUNTRY"));
               installation.setCountInstAlias(rs.getInt("cnt"));
               installation.setAdmin(rs.getInt("BOL_ADMINISTRATOR")==1?true:false);

               arrInstallation.add(installation);

               //System.out.println(rs.getLong(1) + " Installation Added");
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return arrInstallation;

   }


   public static ArrayList<InstallationAlias> getInstallationAlias(final DataSource dataSource, Installation installation) {

      ArrayList<InstallationAlias> arrInstallationAlias = new ArrayList<>();

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql = "SELECT * " +
                  "FROM  \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS instAlias " +
                  "WHERE instAlias.SEQ_INSTALLATION_ID = " + installation.getId() +
                  " AND instAlias.SEQ_USER_ID = " + installation.getSeqUserId();

            //System.out.println(sql);
            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);


            while (rs.next()) {

               InstallationAlias installationAlias = new InstallationAlias();

               installationAlias.setId(rs.getLong("SEQ_ID"));
               installationAlias.setUserId(rs.getLong("SEQ_USER_ID"));
               installationAlias.setInstallationId(rs.getLong("SEQ_INSTALLATION_ID"));

               arrInstallationAlias.add(installationAlias);

               //System.out.println(rs.getLong(1) + " InstallationAlias Added");
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return arrInstallationAlias;

   }

   public static boolean hasEvent(final DataSource dataSource, InstallationAlias installationAlias) {


      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql = "SELECT count(1) " +
                  "FROM \"EVENT-PROCESSOR\".SMPR_TEVENT event " +
                  "WHERE event.SEQ_EVENT_TYPE_ID IS NOT NULL AND " +
                  "event.SEQ_PARENT_THING_ID IN ( SELECT SEQ_THING_ID FROM \"EVENT-PROCESSOR\".SMPR_TTHING_ALIAS " +
                  "WHERE SEQ_INSTALLATION_ALIAS_ID = " + installationAlias.getId() +
                  " AND SEQ_USER_ID = " + installationAlias.getUserId() + " )" ;

           // System.out.println("   " + sql);
            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
               if (rs.getInt(1) == 0) {
                  //System.out.println("    InstallationAlias :  " + installationAlias.getId() + " has No events - @@@ FALSE @@@");
                  appendToFile("analisis.txt", "    InstallationAlias :  " + installationAlias.getId() + " has No events - @@@ FALSE @@@\n");
                  return false;
               } else {
                  //System.out.println("    InstallationAlias :  " + installationAlias.getId() + " has : " + rs.getInt(1) + " events - @@@ TRUE @@@");
                  appendToFile("analisis.txt", "    InstallationAlias :  " + installationAlias.getId() + " has : " +
                        rs.getInt(1) + " events - @@@ TRUE @@@\n" );
                  return true;
               }
            }

         } catch (Exception e) {

            return false;
            //System.out.println(" ");
            //System.out.println(e.getMessage());
            //printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return false;

   }

   public static void generateDeleteStatements(final DataSource dataSource, Long installationAliasId, Long userId){

      /*

# ACCOUNT-MANAGEMENT
      replicas.installationAias=ACCOUNT-MANAGEMENT,CAMPAIGNS,DEVICE,DIGITAL-SECURITY,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG,USER-ENROLLMENT
# DEVICE
      replicas.thing=ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SCENE,SYSLOG
# DEVICE
      replicas.thingAlias=ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG
*/
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {


            String sqlThingAlias = "SELECT concat( 'DELETE FROM SMPR_TTHING_ALIAS WHERE SEQ_ID = ' , SEQ_ID) " +
                  " FROM \"ACCOUNT-MANAGEMENT\".SMPR_TTHING_ALIAS WHERE SEQ_INSTALLATION_ALIAS_ID = " + installationAliasId +
                  " UNION " +
                  " SELECT concat( 'DELETE FROM SMPR_TTHING_ALIAS WHERE SEQ_ID = ' , SEQ_ID) " +
                  " FROM \"ACCOUNT-MANAGEMENT\".SMPR_TTHING_ALIAS WHERE SEQ_PARENT_ID IN (" +
                  " SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TTHING_ALIAS WHERE SEQ_INSTALLATION_ALIAS_ID = " + installationAliasId +
                  " )";

            String sqlInstAlias = "SELECT concat( 'DELETE FROM SMPR_TINSTALLATION_ALIAS WHERE SEQ_ID = ' , SEQ_ID) FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS " +
                  "WHERE SEQ_ID = " + installationAliasId;

            // Thing Alias -------------------------------------------------------------------------
           // System.out.println("   " + sqlThingAlias);
            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sqlThingAlias);

            String deleteStmnt = "";

            String schemas = "schemas:ACCOUNT-MANAGEMENT,DEVICE,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG\n";
            appendToFile("deleteRepetedInstallationsAlias.sql", "-- Thing Alias for user: " + userId + "\n");
            appendToFile("deleteRepetedInstallationsAlias.sql", schemas);
            while (rs.next()) {
               deleteStmnt = rs.getString(1);
               appendToFile("deleteRepetedInstallationsAlias.sql", deleteStmnt + ";\n");
            }

            // Installation Alias -------------------------------------------------------------------
            // .out.println("   " + sqlInstAlias);
            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sqlInstAlias);

            schemas = "schemas:ACCOUNT-MANAGEMENT,CAMPAIGNS,DEVICE,DIGITAL-SECURITY,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG,USER-ENROLLMENT\n";

            appendToFile("deleteRepetedInstallationsAlias.sql", "-- Installation Alias for user: " + userId + "\n");
            appendToFile("deleteRepetedInstallationsAlias.sql", schemas);
            while (rs.next()) {
               deleteStmnt = rs.getString(1);
               appendToFile("deleteRepetedInstallationsAlias.sql", deleteStmnt + ";\n");
            }

         } catch (Exception e) {
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }
      } catch (Exception e) {
         printStackTrace(e);
      }
   }


}
