package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Template extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(Template.class);


   static ArrayList<String> arr_userNames = new ArrayList<>();

   /**
    *  Template for datafix method
    */
   public int main(String env, String userToLoginFile, String datafixReportFile, String reportDate) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         runCommandLine("rm ./*.sql");

         System.out.println("Executing step 1: check CodeUser On CMSession  ..." );
         checkCodeUserOnCMSession(dataSource);

         System.out.println("Total users found: " + arr_userNames.size());

         appendToFile(userToLoginFile, "-- fixCameraManagerSessionClientId AM-39999 \n");

         appendToFile(datafixReportFile,
               "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-39999'>5)  " +
                     "https://jira.prosegur.com/browse/AM-39999</a> - " +
                     "Credenciales donde no corresponde el clientId <br>\n");


         if (arr_userNames.size() > 0) {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", se fixearon " + arr_userNames.size() + " casos. <br>\n");
            for (String userName:arr_userNames){
               //System.out.println(userName);
               appendToFile(userToLoginFile, userName + "\n");
               appendToFile(datafixReportFile, userName + "<br>\n");
            }
         } else {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", no se encontraron casos a fixear<br>\n");
         }

         checkSessions(dataSource, dbUsername);

      } catch (Exception e){
         printStackTrace(e);
      }

      return arr_userNames.size();
   }

   /**
    * Template method to query DB and Generate sql file for INSERT, UPDATE or DELETE
    * @param dataSource
    */
   public static void checkCodeUserOnCMSession(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql1 = "SELECT cm.SEQ_ID, usr.DES_NICK_NAME, cm.DES_EMAIL, usr.DES_CLIENT_ID , SUBSTR(cm.DES_EMAIL , 3,  INSTR( cm.DES_EMAIL, '@', 1) -3 ) " +
                  "FROM  \"CAMERAMANAGER\".SMPR_TCAMERAMANAGER_SESSION cm " +
                  "INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr ON (UPPER(usr.DES_NICK_NAME) = UPPER(cm.cod_user)) " +
                  "WHERE UPPER(usr.DES_CLIENT_ID) <> UPPER(SUBSTR(cm.DES_EMAIL , 3,  INSTR( cm.DES_EMAIL, '@', 1) -3 ))";

            //System.out.println(sql1);

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql1);


            while (rs.next()) {
               // SEQ_ID| DES_EMAIL |DES_CLIENT_ID
               // Si tiene des_token NULL lo agrego para borrarlo
               // Si el client_id en des_mail distinto al del user, lo borramos

               //System.out.println(" desEmail: "+rs.getString(3) + " clientId: " +rs.getString(4));

               String deleteStmt = "DELETE FROM SMPR_TCAMERAMANAGER_SESSION WHERE seq_id = " + rs.getLong(1) + ";";
               appendToFile("deleteCameraManagareSessionDiffClientId.sql",  deleteStmt + "\n");

               String delete3PStmt = "DELETE FROM SMPR_TTHIRDPARTYSESSION WHERE UPPER(COD_USER) = UPPER('" + rs.getString(2) + "');";
               appendToFile("deleteThirdPartySessionDiffClientId.sql",  delete3PStmt + "\n");

               arr_userNames.add(rs.getString(2));
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }

   /**
    * Template method for stand alone method, read file and query DB for each line in file
    */
   public void main(){

      loadExternalFileProperties();

      String jdbcUrl = "";
      String dbUsername = "";
      String password = "";

      String env = "pro";
      if (env == null) env = getProperty("env");
      String connectionType = getProperty("connectionType");

      // select jdbc url
      if ("vpn".equals(connectionType)) {
         jdbcUrl = getProperty("vpn." + env + ".url");
      } else {
         jdbcUrl = getProperty("telepresence." + env + ".url");
      }

      dbUsername = "SCHEDULER" ;
      password = getProperty(env + ".SCHEDULER" +".password");

      DataSource dataSource = null;
      Connection con = null;
      Statement stmt = null;
      ResultSet rs = null;

      try {

         dataSource = getDataSource(jdbcUrl, dbUsername, password);
         con = dataSource.getConnection();
         stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

         ArrayList<String> arrTriggerName = new ArrayList<>();

         String dayNumber = "4"; // 1- dom 2-Lun 3-mar 4-Mie 5-jue 6-vie 7-sab
         String dayDate = "20" ;
         String month = "11";
         String dayName = "Miercoles"; //"Martes"; // ""Domingo";


         String logFile = "/home/cristian/workspace/prosegur/prod/soporte/EscenasNoSeEjecuta/logs-insights-results"+
               dayDate+"-"+month+"-2024.csv";
         String reportFile = "/home/cristian/workspace/prosegur/prod/soporte/EscenasNoSeEjecuta/noEjecutadas" +
               dayName + "-" +dayDate +".txt";
// 2024-11-19 22:02:02.081,scheduled scene: 344026 was fired,info,smart,scheduler

         System.out.println("Reading log file: " + logFile);
         BufferedReader br = new BufferedReader(new FileReader(logFile));

         String sqlCheck = "SELECT CRON_EXPRESSION, TIME_ZONE_ID FROM SCHEDULER.QRTZ_CRON_TRIGGERS WHERE TRIGGER_NAME = ";

         String sCurrentLine;
         int inx = 1;
         int countInLog = 1;
         while ((sCurrentLine = br.readLine()) != null) {

            String triggerName = sCurrentLine.substring(41, sCurrentLine.indexOf(" was"));
            //System.out.println(triggerName);
            printInLine("log file line: " + inx );

            // Solo si corresponde al dia lo agrego al array
            rs = stmt.executeQuery(sqlCheck + "'" + triggerName +"'");

            if (rs.next()) {
               String cronExpr = rs.getString(1);
               String timeZoneId = rs.getString(2);
               // Si corresponde al timezone y al dia Miercoles
               if ("Europe/Madrid".equals(timeZoneId) &&
                     (cronExpr.contains("*") || cronExpr.contains(dayNumber))){
                  arrTriggerName.add(triggerName) ;
                  countInLog ++;
               }

            }

            inx ++;
            //if (inx == 200) break;
         }

         System.out.println("jods added from log: " + inx + " for day: " + dayNumber);

         String sql = "SELECT TRIGGER_NAME, CRON_EXPRESSION FROM SCHEDULER.QRTZ_CRON_TRIGGERS " +
               "WHERE cron_expression LIKE '0 00 23 ? *%' " +
               "AND TIME_ZONE_ID = 'Europe/Madrid' " ;

         rs = stmt.executeQuery(sql);

/*
 cargar en un array los ids levantados del log,

y ver si luego recorrer el resultado la consulta,
 Ver cuales corresponda que se hallan ejecutado el martes
 y ver si existe en el arrary*/

         inx = 0;
         int countDayOk = 0;
         int totalFromDB = 0;
         boolean caseFound = false;
         while (rs.next()) {

            String triggerName = rs.getString(1);
            String cronExpressionDays = rs.getString(2).substring(12);

            /*if (triggerName.equals("184734")){
               System.out.println("##### : 184734 - FOUND in DB " );

               //appendToFile(reportFile, "##### : 184734 - FOUND in DB for: " + dayName + "\n");
            }*/
            //System.out.println("cronExpression: " + cronExpressionDays);
            // Si incluye al martes: expresison contains 3
            // Si incluye al miercoles: expression contains 4

            if (cronExpressionDays.contains("*") || cronExpressionDays.contains(dayNumber)) {
               countDayOk++;
               // Buscamos la escena Filtrada de la DB en el log
               // Si no la encontramos, asumimos que no se ejecutó
              /* List<String> matches = arrTriggerName
                     .stream()
                     .filter(it -> it.contains(triggerName)).collect(Collectors.toList());*/


               // Si está en la DB y no se encontró en el log
               if (!isInList(triggerName, arrTriggerName)) {
                  System.out.println("@ Escena no ejecutada: " + triggerName +  " cronExp: " +
                        rs.getString(2) + " cronExpressionDays: " + cronExpressionDays );

                  if (triggerName.equals("184734")){
                     caseFound = true;
                  }
                  appendToFile(reportFile, triggerName + " cronExp: " + rs.getString(2) + "\n");
                  inx ++;
               }
            }

            totalFromDB ++;
            //if (inx == 200) break;
         }

         System.out.println("Report for : " + dayName + " - " + dayDate + "/" + month);
         System.out.println("    countInLog: linas en log corresponden a timeZone (Europe/Madrid): " + countInLog);
         System.out.println("    totalFromDB: (0 00 23 ? *% && Europe/Madrid): " + totalFromDB);
         System.out.println("    countScenes: escenas from DB que deben ejecutarse en dia (" + dayName+ "): " + countDayOk);
         System.out.println("    ##### : 184734 - FOUND in Not Executed for: " + dayName +" -> " + caseFound);
         System.out.println("    Escena no ejecutadas (en DB, no en log): " + inx );


      } catch (Exception e) {
         printStackTrace(e);
      } finally {
         try {
            checkSessions(dataSource, dbUsername);

            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (con != null) con.close();
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         }
      }

   }

}
