package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class CheckSmartisPSObsolete extends QryDB {

    private static final Logger LOGGER = LogManager.getLogger(CheckSmartisPSObsolete.class);
    static ArrayList<String> arr_psCountryAndConnection = new ArrayList<>();

    public int findProcessStateObsolete(String env, String datafixReportFile, String reportDate) {

        loadExternalFileProperties();
        DataSource dataSource = null;

        try {

            if (env == null) env = getProperty("env");
            String connectionType = getProperty("connectionType");

            String jdbcUrl = getProperty(connectionType + "." + env + ".url");
            String dbUsername = getProperty(env + ".username");
            String password = getProperty(env + ".password");

            dataSource = getDataSource(jdbcUrl, dbUsername, password);

            System.out.println("Executing step 1: check process state obsolete  ..." );
            checkPSObsolete(dataSource);

            System.out.println("Total record found: " + arr_psCountryAndConnection.size());
            
            appendToFile(datafixReportFile,
                    "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-39977'>" +
                            "12) YH " +
                            "https://jira.prosegur.com/browse/AM-39977</a> - " +
                            "SMARTIS - Panel ocupado por procesos colgados<br>\n");

            if (arr_psCountryAndConnection.size() > 0) {
                appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                        ", se fixearon " + arr_psCountryAndConnection.size() + " casos. <br>\n");

                for (String cc:arr_psCountryAndConnection){
                    appendToFile(datafixReportFile, cc + "<br>\n");
                }

            } else {
                appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                        ", no se encontraron casos a fixear<br>\n");
            }

        } catch (Exception e){
            printStackTrace(e);
        }

        return arr_psCountryAndConnection.size();
    }

    public static void checkPSObsolete(final DataSource dataSource) {

        try {
            Connection con = dataSource.getConnection();
            Statement stmt = null;
            ResultSet rs = null;

            try {
                
                String sql1 = "select SEQ_ID,DES_COUNTRY,DES_CONNECTION_ID " + 
                        " FROM \"SMARTIS\".SMPR_PROCESS_STATE ps " +
                        " WHERE ps.des_state in ('changeState','waitResponse') " +
                        " AND FYH_UPDATE_DATE < SYSDATE - 1 / 24 ORDER BY ps.FYH_UPDATE_DATE DESC";

                stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                rs = stmt.executeQuery(sql1);

                while (rs.next()) {

                    String deleteStmt = "DELETE FROM SMPR_PROCESS_STATE WHERE SEQ_ID = " + rs.getLong(1) + ";";
                    appendToFile("deleteSmartisProcessStateObsoleteById.sql",  deleteStmt + "\n");

                    arr_psCountryAndConnection.add((rs.getString(2).concat(" ").concat(rs.getString(3))));
                }

            } catch (Exception e) {
                System.out.println(" ");
                System.out.println(e.getMessage());
                printStackTrace(e);
            } finally {
                try {
                    if (rs != null) rs.close();
                    if (stmt != null) stmt.close();
                    if (con != null) con.close();
                } catch (Exception e) {
                    System.out.println(" ");
                    System.out.println(e.getMessage());
                    printStackTrace(e);
                }
            }

        } catch (Exception e1) {
            System.out.println(" ");
            System.out.println(e1.getMessage());
            e1.printStackTrace();
        }
    }
}
