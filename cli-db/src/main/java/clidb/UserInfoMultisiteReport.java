package clidb;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClient;
import com.amazonaws.util.StringUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import javax.ws.rs.core.MultivaluedMap;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;


public class UserInfoMultisiteReport extends QryDB {


   private  final Logger LOGGER = LogManager.getLogger(UserInfoMultisiteReport.class);

   int maxEvents = 50;
   int maxDependents = 50;
   int maxInstallations = 50;
   private Long seq_user_id;
   private int isAdministrator;
   private int isMultisite;
   private String desClientId;
   private String desCountry;
   private ArrayList<Long> arr_inst_alias_id = new ArrayList();
   private ArrayList<Long> arr_installation_id = new ArrayList();
   private ArrayList<Long> arr_ext_services_id = new ArrayList();
   private Long seq_panel_id;
   private Long seq_installation_id;
   private Long seq_smart_id;
   private Long seq_bath_id;
   private boolean objectTrackerInfo = false;
   private boolean withReplicas = false;
   protected AWSCognitoIdentityProviderClient identityUserPoolProviderClient = null;

   public String generateMultiSiteUserReport(DataSource dataSource, String env, String userName, String userEnrollInfo, String userAtEnrollInfo) {

      String content = "";
      try {
         arr_inst_alias_id = new ArrayList();
         arr_ext_services_id = new ArrayList();
         arr_installation_id  = new ArrayList();
         seq_bath_id = null;
         seq_smart_id = null;
         desClientId = null;
         desCountry = null;

         String isAdmin = "";
         String userInfo = "";
         String migrationData = "";
         String migrationObjectData = "";
         String terminals = "";
         String dashboard = "";
         String dependents = "";
         String adminForDependent = "";
         String installation = "";
         String instAlias = "";
         String thingAlias = "";
         String thingTree = "";
         String panels = "";
         String events = "";
         String externaServices = "";
         String installationCRMData = "";
         String cognitoData = "";
         String installationData = "";


         cognitoData =  "<br><a target='_self' href='#' onclick=\"showHide('cognitoData');return false;\"> <b>&gt;&gt;&nbsp;&nbsp; Cognito Data</b>  </a>";
         cognitoData += "<div style=\"padding: 50px\" id='cognitoData'>";
         //cognitoData += getCognitoData(env, userName);
         cognitoData += "</div>";


         String schema = "\"ACCOUNT-MANAGEMENT\".";

         userInfo = getUserInfo(dataSource, userName, schema);

         if (isAdministrator == 1) {
            dependents = getDependents(dataSource, schema);
            isAdmin = " ** Administrator **";

            installationData = getInstallations(dataSource, schema);
         } else {
            isAdmin = " ** Dependent **";

            adminForDependent = getAdminForDependent(dataSource, schema);

            installationData ="<h2> Installations: Is a dependent user.</h2>\n";
         }

         installationCRMData =  "<br><a target='_self' href='#' onclick=\"showHide('installationCRMData');return false;\"> <b>&gt;&gt;&nbsp;&nbsp; Installations from CRM</b>  </a>";
         installationCRMData += "<div style=\"padding: 50px\" id='installationCRMData'>";
         installationCRMData += getCRMInstallation(env);
         installationCRMData += "</div>";

         //getInstAliasId(dataSource, schema);

         schema = "\"MIGRATION-ORCHESTRATOR\".";
         if (isAdministrator == 1) {

            migrationData = getMigrationData4Admin(dataSource, schema);
         } else {
            //migrationData = "TODO";
         }

         //migrationObjectData = getMigrationObjectData(dataSource, schema);



         String userReplicaData = "";
         String installationReplicaData = "";
         String installationAliasReplicaData = "";
         String thingAliasReplicaData = "";
         String thingReplicaData = "";





         String init = "<h1> Information for: " + userName + isAdmin + " - Multisite user </h1>\n " +
               "<script>\n" +
               "\tfunction showHide(divId) {\n" +
               "\t  var x = document.getElementById(divId);\n" +
               "\t  if (x.style.display === \"none\") {\n" +
               "\t    x.style.display = \"block\";\n" +
               "\t  } else {\n" +
               "\t    x.style.display = \"none\";\n" +
               "\t  }\n" +
               "\t}\n\n" +
               "\tfunction openWindow(text) { \n" +
               "\t   var newtab = window.open(\"\", \"Record\", \"width=500,height=700\"); \n" +
               "\t   newtab.document.write(text); \n" +
               "\t}\n" +
               "</script>";


         content = init +
               userEnrollInfo +
               //cognitoData +
               userAtEnrollInfo +
               userInfo +
               adminForDependent +
               migrationData +
               migrationObjectData +
               dependents +
               terminals +
               installationCRMData +
               installationData +
               externaServices ;

      } catch (Exception e) {
         printStackTrace(e);
      }

      return content;

   }

   public String getDesClientId() {
      return desClientId;
   }

   public void setDesClientId(String desClientId) {
      this.desClientId = desClientId;
   }

   public String getDesCountry() {
      return desCountry;
   }

   public void setDesCountry(String desCountry) {
      this.desCountry = desCountry;
   }

   public boolean isObjectTrackerInfo() {
      return objectTrackerInfo;
   }

   public void setObjectTrackerInfo(boolean objectTrackerInfo) {
      this.objectTrackerInfo = objectTrackerInfo;
   }

   public boolean isWithReplicas() {
      return withReplicas;
   }
   public void setWithReplicas(boolean withReplicas) {
      this.withReplicas = withReplicas;
   }

   public  int selectToInfoCount(final DataSource dataSource, String sqlCount) {

      int numRows = 0;
      try {
         Connection con = dataSource.getConnection();
         Statement countStmt = null;
         ResultSet countRs = null;

         try {

            //System.out.println("sqlCount: " + sqlCount);

            countStmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            countRs = countStmt.executeQuery(sqlCount);

            countRs.next();
            numRows = countRs.getInt(1);

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            //printStackTrace(e);
         } finally {
            try {
               if (countRs != null) countRs.close();
               if (countStmt != null) countStmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               //printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         //e1.printStackTrace();
      }

      return numRows;
   }

   public  String selectToInfo(final DataSource dataSource, String sql, String tableName) {
      return selectToInfo(dataSource, sql, tableName, "orange");
   }

   public  String selectToInfo(final DataSource dataSource, String sql, String tableName, String color) {
      String row = "";
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            //System.out.println("------------------------------------------------");
            //System.out.println(sql);

            int columns = 0;
            int rowCount = 0;
            if (rs.next()) {
               ResultSetMetaData rsmd = rs.getMetaData();

               row = row + "<th bgcolor='" + color + "'>&nbsp;</th>\n";

               for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                  columns++;
                  if ("tEvents".equals(tableName) &&
                        ("SEQ_ID".equals(rsmd.getColumnName(i)) || rsmd.getColumnName(i).equals("DES_TEXT"))) {
                     row = row + "<th bgcolor='" + color + "'>" + rsmd.getColumnName(i) +
                           "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>\n";
                  } else {
                     row = row + "<th bgcolor='" + color + "'>" + rsmd.getColumnName(i) + "</th>\n";
                  }
               }

               if (rs.last()) rs.beforeFirst();


               String recordStart = "<table border=1>" +
                     "<tr>" +
                     "<th bgcolor=" + color + ">Column</th><th bgcolor=" + color + ">Value</th>" +
                     "</tr>";
               String recordEnd = "</table><br><button onclick=self.close()>Close</button>";

               String recordContent;

               // Get row values to show
               while (rs.next()) {
                  row = row + "<tr>";

                  recordContent = "";

                  for (int i = 1; i <= rsmd.getColumnCount(); i++) {

                     recordContent = recordContent + "<tr><td>" + rsmd.getColumnName(i).toUpperCase() + "</td>" +
                           "<td>" + rs.getString(rsmd.getColumnName(i)) + "</td></tr>";
                  }

                  // Celda para ver registro en vertical
                  row = row + "<td> <button onclick=\"openWindow('" +  recordStart + recordContent + recordEnd + "')\"> # </button></td>";


                  for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                     if ("SEQ_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER".equals(tableName)) {
                        seq_user_id = rs.getLong(rsmd.getColumnName(i));
                     }

                     if ("BOL_ADMINISTRATOR".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER".equals(tableName)) {
                        isAdministrator = rs.getInt(rsmd.getColumnName(i));
                     }

                     if ("BOL_MULTISITE".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER_ENROLLMENT".equals(tableName)) {
                        isMultisite = rs.getInt(rsmd.getColumnName(i));
                     }


                     if ("DES_CLIENT_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER_ENROLLMENT".equals(tableName)) {
                        desClientId = rs.getString(rsmd.getColumnName(i));
                     }
                     if ("DES_COUNTRY_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER_ENROLLMENT".equals(tableName)) {
                        desCountry = rs.getString(rsmd.getColumnName(i));
                     }

                     if ("SEQ_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "InstallationAlias".equals(tableName)) {
                        arr_inst_alias_id.add(rs.getLong(rsmd.getColumnName(i)));
                     }

                     if ("SEQ_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "Panels".equals(tableName)) {
                        seq_panel_id = (rs.getLong(rsmd.getColumnName(i)));
                     }

                     if ("SEQ_INSTALLATION_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SetInstallationId".equals(tableName)) {
                        seq_installation_id = (rs.getLong(rsmd.getColumnName(i)));
                     }

                     if ("SEQ_INSTALLATION_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "getInstallationId".equals(tableName)) {
                        arr_installation_id.add(rs.getLong(rsmd.getColumnName(i)));
                     }

                     if ("DES_SIEBEL_CODE".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER_EXTERNAL_SERVICE".equals(tableName)) {
                        arr_ext_services_id.add(rs.getLong(rsmd.getColumnName(i)));
                     }

                     if ("tAccount".equals(tableName) &&
                           "DES_MIGRATION_STATUS".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "OK".equals(rs.getString(rsmd.getColumnName(i)))
                     ) {
                        seq_smart_id = rs.getLong("SEQ_SMART_USER_ID");
                        seq_bath_id = rs.getLong("SEQ_BATCH_PROCESS_STATE_ID");
                     }

                     // Pintamos el fondo si tiene fecha de borrado
                     if ("FYH_DELETED".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           !StringUtils.isNullOrEmpty(rs.getString(rsmd.getColumnName(i)))){
                        row = row + "<td bgcolor='orange'>" + rs.getString(rsmd.getColumnName(i)) + "</td>";
                        // Pintamos el fondo de fecha de ultimo acceso
                     } else if ("FYH_LAST_ACCESS".equals(rsmd.getColumnName(i).toUpperCase())){
                        row = row + "<td bgcolor='green'>" + rs.getString(rsmd.getColumnName(i)) + "</td>";
                        // resto de las celdas
                     } else {
                        row = row + "<td>" + rs.getString(rsmd.getColumnName(i)) + "</td>";
                     }

                  }

                  row = row + "</tr>\n";
                  rowCount ++;
               }

               row = row + "<tr><td colspan='2'> <b>" + rowCount + " rows.</b> </td>" +
                     "<td colspan='" + (columns -1 ) +"'>" + sql + " </td></tr>\n";

            } else {
               row = row + "<th> Empty Result </th>\n";
               row = row + "<tr><td>" + sql + " </td></tr>\n";
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return row;
   }


   private  String getUserInfo(DataSource dataSource, String userName, String schema) {

      printInLine("getUserInfo...                                        ");
      String start = "<h2> User | account-management - smpr_tuser </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TUSER where upper (DES_NICK_NAME) = upper ('" + userName + "')";

         body = selectToInfo(dataSource, sql, "SMPR_TUSER");

         try {
            dataSource.getConnection().close();
            ////System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getDependents(DataSource dataSource, String schema) {
      printInLine("getDependents...                                        ");
      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT * FROM ( " +
               "select * from " + schema + "SMPR_TUSER WHERE BOL_ADMINISTRATOR = 0 AND " +
               "DES_CLIENT_ID = '" + desClientId + "' AND DES_COUNTRY = '" + desCountry + "' " +
               ") WHERE ROWNUM <= " + maxDependents;

         String sqlCount = "select count(1) from " + schema + "SMPR_TUSER WHERE BOL_ADMINISTRATOR = 0 AND " +
               "DES_CLIENT_ID = '" + desClientId + "' AND DES_COUNTRY = '" + desCountry + "' ";

         int total = selectToInfoCount(dataSource, sqlCount);

         body += "<h2> Dependents | account-management - smpr_tuser -" +
               " Total dependents: " + total + " (only " + maxDependents + " are shown) </h2>\n" +
               "<table border='1'>\n";

         body += selectToInfo(dataSource, sql, "Dependents", "LightSkyBlue");

         try {
            dataSource.getConnection().close();
            ////System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getAdminForDependent(DataSource dataSource, String schema) {

      printInLine("getAdminForDependent...                                        ");
      String start = "<h2> User Admin for it dependent | account-management - smpr_tuser </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TUSER WHERE BOL_ADMINISTRATOR = 1 AND " +
               "DES_CLIENT_ID = '" + desClientId + "' AND DES_COUNTRY = '" + desCountry + "' ";

         body = selectToInfo(dataSource, sql, "Dependents", "Green");

         try {
            dataSource.getConnection().close();
            ////System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getMigrationData4Admin(DataSource dataSource, String schema) {
      printInLine("getMigrationData4Admin...                                        ");
      String start = "<h2> Migration data | MIGRATION-ORCHESTRATOR  - SMPR_TACCOUNT </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TACCOUNT WHERE DES_CLIENT_ID = '" + desClientId +
               "' AND DES_COUNTRY = '" + desCountry + "' ORDER BY SEQ_ID DESC";

         body = selectToInfo(dataSource, sql, "tAccount", "red");

         try {
            dataSource.getConnection().close();
            ////System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getUserEnrollmentInfo(DataSource dataSource, String userName, String schema) {
      printInLine("getUserEnrollmentInfo...                           ");
      String start = "<h2> User Enrollment | user-enrollment - smpr_tuser_enrollment </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TUSER_ENROLLMENT where upper (DES_USER_NAME) = upper ('" + userName + "')";

         body = selectToInfo(dataSource, sql, "SMPR_TUSER_ENROLLMENT", "yellow");

         try {
            dataSource.getConnection().close();
            ////System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getUserAtEnrollmentInfo(DataSource dataSource, String userName, String schema) {

      printInLine("getUserAtEnrollmentInfo...                           ");
      String start = "<h2> User | user-enrollment - smpr_tuser </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TUSER where upper (DES_NICK_NAME) = upper ('" + userName + "')";

         body = selectToInfo(dataSource, sql, "SMPR_TUSER_ENROLLMENT", "Lavender");

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getInstallations(DataSource dataSource, String schema) {
      printInLine("getInstallations...                                        ");

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT * FROM ( " +
               "SELECT * FROM "+ schema +"SMPR_TINSTALLATION WHERE SEQ_ADMIN = " + seq_user_id +
               " ORDER BY FYH_DELETED desc " +
               ") WHERE ROWNUM <= " + maxInstallations;

         String sqlCount = "SELECT count(1) FROM "+ schema +"SMPR_TINSTALLATION WHERE SEQ_ADMIN = " + seq_user_id ;

         int total = selectToInfoCount(dataSource, sqlCount);

         body += "<h2> Installations | account-management - smpr_tinstallation -" +
               " Total Installations: " + total + " (only " + maxInstallations + " are shown) </h2>\n" +
               "<table border='1'>\n";

         body += selectToInfo(dataSource, sql, "SMPR_TINSTALLTION_List", "yellow");

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   public String getCRMInstallation(String env){

      String start = "<h2> Installations from CRM </h2>\n" +
            "<table border='1'>\n" +
            "<tr width='100%'><td>\n";
      String end = "</td></tr>\n"+ "</table>";


      try {
         String host= getProperty(env + ".crm_host");


//#urlBase="/alarms/smart/rtf"
//#urlOauth="$/oauth/alarms/smart/rtf"

         String url= host + "/alarms/smart";

         String tokenUrl = host + "/api/oauth/token";
         String getInstallationUrl = url + "/v1/country/"+desCountry+"/client/"+desClientId+"/installation";

         Client client = Client.create();
         WebResource webResource1 = client.resource(tokenUrl);
         webResource1.header("Content-Type", "application/x-www-form-urlencoded");

         MultivaluedMap formData = new MultivaluedMapImpl();
         formData.add("scope", "alarms.write, alarms.read");
         formData.add("client_secret", getProperty(env + ".client_secret"));
         formData.add("client_id", getProperty(env + ".client_id"));
         formData.add("grant_type", "client_credentials");

         ClientResponse response1 = webResource1.post(ClientResponse.class, formData);
         String tokenResponse = response1.getEntity(String.class);
         String tokenValue = tokenResponse.substring(17,71);

         Client client2 = Client.create();

         //System.out.println("get Installation from crm: " + getInstallationUrl);

         WebResource webResource2 = client2.resource(getInstallationUrl);
         WebResource.Builder builder =  webResource2.getRequestBuilder();

         builder
               .header("authorization", "Bearer " + tokenValue)
               .header("Content-Type", "application/json")
               .header("ExtSys", "smart");

         ClientResponse response2 = builder.get(ClientResponse.class);
         String result = response2.getEntity(String.class);

         result = result.replaceAll("\n", "<br>\n");
         result = result.replaceAll(" ", "&nbsp;");

         String content = formatJson4Report(result);
         content = content.replaceAll("<br><br>", "<br>");


         return  start + "<br><font color=\"blue\">" + getInstallationUrl + "</font><br>" + content + end;
      } catch (Exception e) {
         return start + e.getMessage() + end;
      }
   }

   public String getCRMContract(String env){

      String start = "<h2> Installations from CRM </h2>\n" +
            "<table border='1'>\n" +
            "<tr width='100%'><td>\n";
      String end = "</td></tr>\n"+ "</table>";


      try {
         String host="https://apiemea.prosegur.com";
         String url= host + "/alarms/smart";

         String tokenUrl = host + "/api/oauth/token";
         String getContractUrl = url + "/v1/country/"+desCountry+"/client/"+desClientId+"/contractsData";

         Client client = Client.create();
         WebResource webResource1 = client.resource(tokenUrl);
         webResource1.header("Content-Type", "application/x-www-form-urlencoded");

         MultivaluedMap formData = new MultivaluedMapImpl();
         formData.add("scope", "alarms.write, alarms.read");
         formData.add("client_secret", getProperty(env + ".client_secret"));
         formData.add("client_id", getProperty(env + ".client_id"));
         formData.add("grant_type", "client_credentials");

         ClientResponse response1 = webResource1.post(ClientResponse.class, formData);
         String tokenResponse = response1.getEntity(String.class);
         String tokenValue = tokenResponse.substring(17,71);

         Client client2 = Client.create();

         //System.out.println("get Installation from crm: " + getInstallationUrl);

         WebResource webResource2 = client2.resource(getContractUrl);
         WebResource.Builder builder =  webResource2.getRequestBuilder();

         builder
               .header("authorization", "Bearer " + tokenValue)
               .header("Content-Type", "application/json")
               .header("ExtSys", "smart");

         ClientResponse response2 = builder.get(ClientResponse.class);
         String result = response2.getEntity(String.class);


         result = result.replaceAll("\n", "<br>\n");
         result = result.replaceAll(" ", "&nbsp;");
         String content = formatJson4Report(result);
         content = content.replaceAll("<br><br>", "<br>");


         //return  start + "<br><br><font color=\"blue\">" + getContractUrl + "</font><br><br>" + content + end;
         return   getContractUrl + "\n" + content ;
      } catch (Exception e) {
         return start + e.getMessage() + end;
      }
   }





}
