package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CheckSchedulerExecution extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(CheckSchedulerExecution.class);

   HashMap<String, Integer> timeZoneOffsetMap = new HashMap<>();


   /**
    * Template method for stand alone method, read file and query DB for each line in file
    */
   public void main(){

      loadExternalFileProperties();

      String jdbcUrl = "";
      String dbUsername = "";
      String password = "";

      String env = "pro";
      if (env == null) env = getProperty("env");
      String connectionType = getProperty("connectionType");

      // select jdbc url
      if ("vpn".equals(connectionType)) {
         jdbcUrl = getProperty("vpn." + env + ".url");
      } else {
         jdbcUrl = getProperty("telepresence." + env + ".url");
      }

      dbUsername = getProperty(env + ".username");
      password = getProperty(env + ".password");

      DataSource dataSource = null;
      Connection con = null;
      Statement stmt = null;
      ResultSet rs = null;

      try {


         dataSource = getDataSource(jdbcUrl, dbUsername, password);
         con = dataSource.getConnection();
         stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);


         timeZoneOffsetMap.put("Europe/Lisbon", 0); // Checked
         timeZoneOffsetMap.put("Europe/Madrid", +1); // Checked
         timeZoneOffsetMap.put("Europe/Istanbul", +3); // Checked
         timeZoneOffsetMap.put("America/Santiago", -3); // Checked
         timeZoneOffsetMap.put("America/Buenos_Aires", -3); // Checked
         timeZoneOffsetMap.put("America/Montevideo", -3); // Checked
         timeZoneOffsetMap.put("America/Asuncion", -3); // Checked
         timeZoneOffsetMap.put("America/Bogota", -5); // Checked
         timeZoneOffsetMap.put("America/Lima", -5); // Checked

         /*
HECHO
 - Hacer para distintas horas:
            - Para esto habria que bajar logs por hora y por dia- HECHO
            - Que los reportes se guarden por hora - HECHO
      - Hacer para distintos timeZoneIs
            - Para esto necesitariamos que el filtro por timeZoneId sea
               - opcional (que se pueda apagar con una bandera) - HECHO
               - o levantar un distinct de los timeZoneId y hacer reportes por/ hora / timeZoneId
HECHO:
 - cargo en un array los ids levantados del log,filtrando por timeZonId Europe/Madrid)
 - recorro el resultado la consulta de las escenas para el timeZoneId y hora 23:00,
 - ver cuales corresponda que se hallan ejecutado el dia seleccionado
 - ver si existe en el arrary de triggerName (sceneId) filtrado del log */

         ArrayList<String> arrTriggerName = new ArrayList<>();

         String baseFolder = enterValue("baseFolder ej: /home/ec2-user/cli-db/AM-43380-EscenasNoSeEjecutan/ or /home/cristian/workspace/prosegur/prod/soporte/AM-43380-EscenasNoSeEjecuta/  ");

         String runNumber = enterValue("run folder ej: run-1 ");
         System.out.println("folder: " + baseFolder + runNumber);

         String dayNumber = enterValue("dayNumber : // 1=dom 2=Lun 3=mar 4=Mie 5=jue 6=vie 7=sab"); // 1- dom 2-Lun 3-mar 4-Mie 5-jue 6-vie 7-sab
         String dayDate = enterValue("dayDate: ej: 2 for 02/01/2025");
         String month = enterValue("month number: ");
         String time; // = enterValue("time: ej 23 for 23:00 UTC");
         String arrDayNames[] = {"Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"};
         String dayName = arrDayNames[Integer.valueOf(dayNumber) - 1];

         // for each hour
         for (int i=0; i<=23; i++){

            time = "" + i;
            if (time.length() ==1) time = "0" + time;

            String myString = time + ":00 " + dayDate + "/" + month + "/2025";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            Date myDateTime = null;

            //Parse your string to SimpleDateFormat
            try {
               myDateTime = simpleDateFormat.parse(myString);
            } catch (ParseException e) {
               e.printStackTrace();
            }

            /*String strFilterByTimeZoneMadrid = enterValue("Filter by timeZone Europe/Madrid (y/n):");
            boolean filterByTimeZoneMadrid =  ("y".equals(strFilterByTimeZoneMadrid.toLowerCase()));
            boolean filterByTimeZoneMadrid = false;*/


            String logSchedulerFile = baseFolder + runNumber + "/scheduler/logs-insights-results" +
                  dayDate + "-" + month + "-2025-" + time + ":00.csv";

            String logRuleEngineFile = baseFolder + runNumber + "/rule-engine/logs-insights-results" +
                  dayDate + "-" + month + "-2025-" + time + ":00.csv";

            String reportNotExecutedFile = baseFolder + runNumber + "/escenasNoEjecutadas" +
                  dayName + "-" + dayDate + "-" + time + ":00.txt";

            String reportExecutedFile = baseFolder + runNumber + "/escenasEjecutadas" +
                  dayName + "-" + dayDate + "-" + time + ":00.txt";

            String reportOnSchedulerNotInRuleEngineFile = baseFolder + runNumber + "/onSchedulerNotInRuleEngine" +
                  dayName + "-" + dayDate + "-" + time + ":00.txt";
            String reportHasNoActionFile = baseFolder + runNumber + "/hasNoAction" +
                  dayName + "-" + dayDate + "-" + time + ":00.txt";
            String reportHasNoTriggerFile = baseFolder + runNumber + "/hasNoTrigger" +
                  dayName + "-" + dayDate + "-" + time + ":00.txt";
            String reportIsNotificationFile = baseFolder + runNumber + "/isNotification" +
                  dayName + "-" + dayDate + "-" + time + ":00.txt";

            String totalsReportFile = baseFolder + runNumber + "/totalsReportFile.csv";

            String executionLogFile = baseFolder + runNumber + "/executionLogFile.log";

          /*  System.out.println("Reading logSchedulerFile file: " + logSchedulerFile);
            appendToFile(executionLogFile, "Reading logSchedulerFile file: " + logSchedulerFile + "\n");
            BufferedReader br = new BufferedReader(new FileReader(logSchedulerFile));

            String sqlCheckOnRuleEngine = "SELECT scene.seq_id, des_target, count(action.seq_id), count(tigre.SEQ_ID) " +
                  "FROM \"RULE-ENGINE\".SMPR_TSCENE scene " +
                  "INNER JOIN \"RULE-ENGINE\".SMPR_TSCENE_ACTION action ON (scene.SEQ_ID = action.SEQ_SCENE_ID) " +
                  "INNER JOIN \"RULE-ENGINE\".SMPR_TSCENE_TRIGGER tigre ON (SCENE.SEQ_ID = tigre.SEQ_SCENE_ID ) " +
                  "WHERE bol_active = 1 " +
                  "AND scene.seq_id = :sceneId" +
                  "GROUP BY scene.seq_id, des_target, action.seq_id, tigre.SEQ_ID";

            String sCurrentLine;

            int countInLog = 1;
            int countOnSchedulerNotInRuleEngine = 0;
            int countHasNoAction = 0;
            int countHasNoTrigger= 0;
            int countNotification= 0;

            // Recorro el logSchedulerFile y obtengo los triggerNames (los scene id's)
            while ((sCurrentLine = br.readLine()) != null) {

               if (!sCurrentLine.startsWith("@")) {
                  // 2024-11-19 22:02:02.081,scheduled scene: 344026 was fired,info,smart,scheduler
                  // triggerName: es el Id de escena
                  String triggerName = sCurrentLine.substring(41, sCurrentLine.indexOf(" was"));
                  //System.out.println(triggerName);
                  printInLine("log file line: " + countInLog);

                  rs = stmt.executeQuery(sqlCheckOnRuleEngine.replace(":sceneId", triggerName));
                  if (!rs.next()){
                     System.out.println("Escena " +triggerName+" existe en sheduler y no existe en rule-engine." );
                     appendToFile(reportOnSchedulerNotInRuleEngineFile, triggerName + "\n");
                     countOnSchedulerNotInRuleEngine++;
                  } else {

                     if (rs.getInt(3) == 0){
                        System.out.println("Escena " +triggerName +" no posee action.");
                        appendToFile(reportHasNoActionFile, triggerName + "\n");
                        countHasNoAction++;
                     }

                     if (rs.getInt(4) == 0){
                        System.out.println("Escena " +triggerName +" no posee trigger.");
                        appendToFile(reportHasNoTriggerFile, triggerName + "\n");
                        countHasNoAction++;
                     }

                     if ("NOTIFICATION".equals(rs.getString(2))) {
                        System.out.println("Escena " +triggerName +" es NOTIFICATION.");
                        appendToFile(reportIsNotificationFile, triggerName + "\n");
                        countNotification++;
                     }

                     // Only add if has action and trigger and is not a Notification
                     if (!"NOTIFICATION".equals(rs.getString(2)) &&
                           rs.getInt(3) > 0 &&
                           rs.getInt(4) > 0 ) {
                        arrTriggerName.add(triggerName);
                        countInLog++;
                     }
                  }

//if (countInLog == 100) break;
               }
            }

            System.out.println("jods filtered from log: " + countInLog + " for day: " + dayNumber);
            appendToFile(executionLogFile, "jods filtered from log: " + countInLog + " for day: " + dayNumber + "\n");

            String sqlCount = "SELECT Count(1) FROM SCHEDULER.QRTZ_CRON_TRIGGERS ";
            String sql = "SELECT TRIGGER_NAME, CRON_EXPRESSION, TIME_ZONE_ID FROM SCHEDULER.QRTZ_CRON_TRIGGERS ";
            //"WHERE cron_expression LIKE '0 % " + time + " ? *%' ";
            String whereStmnt = getValidTimeZoneWhereSmtn(myDateTime, time);

            //if (filterByTimeZoneMadrid) sql = sql + "AND TIME_ZONE_ID = 'Europe/Madrid' ";
            System.out.println(sqlCount + whereStmnt);

            appendToFile(executionLogFile, sqlCount + whereStmnt + "\n");

            rs = stmt.executeQuery(sqlCount + whereStmnt);

            int totalFromDB = 0;
            if (rs.next()) {
               totalFromDB = rs.getInt(1);
               System.out.println("totalFromDB: " + totalFromDB + " vs count in log: " + countInLog);
               appendToFile(executionLogFile, "totalFromDB: " + totalFromDB + " vs count filtered in log: " + countInLog + "\n");
            }

   //enterValue("CTRL + C");

            rs = stmt.executeQuery(sql + whereStmnt);

            int countNotExecuted = 0;
            int countExecuted = 0;
            int countDayOk = 0;
            int countDayNotOk = 0;
            int rec = 0;

            // Recorro el resultado de la consulta para timeZoneId = Europe/Madrid y hora 23:00
            while (rs.next()) {

               String triggerName = rs.getString(1);
               // Extraigo en cronExpressionDays los dias en que se debe ejecutar
               String cronExpressionDays = rs.getString(2).substring(12);
               String timeZone = rs.getString(3);

               // Si la hora es superior al time del log,
               // ver si corresponde consultar para el dia anterior
               String correctedDayNumber = getCorrectedDayNumber(myDateTime, timeZone, Integer.valueOf(dayNumber));

               // Si incluye al martes: expresison contains 3 // Si incluye al miercoles: expression contains 4
               if (cronExpressionDays.contains("*") || cronExpressionDays.contains(correctedDayNumber)) {
                  countDayOk++;
                  // Buscamos la escena Filtrada de la DB en el log (osea en el array cargado previamente)
                  // Nos aseguramos que corresponda al DIA Y HORA de ejecución
                  // Si no la encontramos, asumimos que no se ejecutó

                  // Si está en la DB y no se encontró en el log
                  if (!isInList(triggerName, arrTriggerName)) {
                     System.out.println("@ Escena no ejecutada: " + triggerName + " cronExp: " +
                           rs.getString(2) + " cronExpressionDays: " + cronExpressionDays + " timeZone: " + timeZone);


                     appendToFile(reportNotExecutedFile, triggerName + " | " + rs.getString(2) + " | " + timeZone + "\n");
                     countNotExecuted++;
                  } else {
                     System.out.println("####### Escena ejecutada: " + triggerName);
                     countExecuted++;
                     appendToFile(reportExecutedFile, triggerName + " | " + rs.getString(2) + " | " + timeZone + "\n");
                  }
               } else {
                  countDayNotOk++;
                  System.out.println("****** Escena No Corresponde por Dia: " + triggerName);
               }

               rec++;
               System.out.println("for logFile: " + logSchedulerFile + " - rec: " + rec + " of " + totalFromDB + " from DB");
               //System.out.println("------------------------------------------- " );
               //if (inx == 200) break;
            }

            // Leer logRuleEngineFile y por cada escena que no sea NOTIFICATION, buscar un evento para la fecha
            // En tEvent y contar si existe o no.

            */

            System.out.println("Reading logRuleEngineFile file: " + logRuleEngineFile);
            appendToFile(executionLogFile, "Reading logRuleEngineFile file: " + logRuleEngineFile + "\n");
            BufferedReader br = new BufferedReader(new FileReader(logRuleEngineFile));

         // 2025-03-11 03:59:01.203,evaluating action target for [sceneId: 233175] - [actionId: 296939] - [actionTarget: PANEL_PARTITION],info,smart,rule-engine
String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {

               if (!sCurrentLine.startsWith("@")) {

                  if (!sCurrentLine.contains("NOTIFICATION")) {

                     String sceneId = sCurrentLine.substring(
                           sCurrentLine.indexOf("sceneId: ") + "sceneId: ".length(),
                           sCurrentLine.indexOf("]"));

                     // TODO Consultar la tEvent aqui a ver si existe evento para la escena y la fecha de hoy()
                     // TODO Checkear que las No ejecutadas efectivamente no estén en el log de rule-engine

                  }


               }
            }

/*
            // Print en pantalla
            System.out.println("Report for : " + dayName + " - " + dayDate + "/" + month + " time: " + time + ":00");
            System.out.println("    countInLog: lineas en log scheduler corresponden al dia filtradas " + dayNumber + ": " + countInLog);
            System.out.println("    total scenes from DB for : (0 00 " + time + " ? *% ): " + totalFromDB);
            System.out.println("    countDayOk: escenas from DB que deben ejecutarse en dia (" + dayName + "): " + countDayOk);
            System.out.println("    countDayNotOk: escenas from DB que NO deben ejecutarse en dia (" + dayName + "): " + countDayNotOk);
            //System.out.println("    ##### : 184734 - FOUND in Not Executed for: " + dayName + " -> " + caseFound);
            System.out.println("    Escena no ejecutadas (encontrado en DB pero no en log): " + countNotExecuted);
            System.out.println("    countOnSchedulerNotInRuleEngine : " + countOnSchedulerNotInRuleEngine);
            System.out.println("    countHasNoAction :" + countHasNoAction);
            System.out.println("    countHasNoTrigger : " + countHasNoTrigger);
            System.out.println("    countHasNoTrigger : " + countNotification);

            // Se agrega una fila al reporte
            appendToFile(totalsReportFile, "Report for : " + dayName + " - " + dayDate + "/" +
                  month + " time:, " + time + ":00hs \n");

            appendToFile(totalsReportFile, "    countInLog: lineas en log:, "
                  + countInLog + "\n");

            appendToFile(totalsReportFile, "    total scenes from DB for: (0 00 " + time + " ? *% ) teniendo en cuenta offset (sin filtrar por dia), " +
                  totalFromDB + "\n");

            appendToFile(totalsReportFile, "    countDayOk: escenas from DB que deben ejecutarse por corresponder al dia ("
                  + dayName + "):, " + countDayOk + "\n");

            appendToFile(totalsReportFile, "    countDayNotOk: escenas from DB que NO deben ejecutarse por no corresponder al dia ("
                  + dayName + "):, " + countDayNotOk + "\n");

            appendToFile(totalsReportFile, "    Escena ejecutadas (en DB y en log para las " + time + ":00 hs):, " + countExecuted + "\n");

            appendToFile(totalsReportFile, "    Escena No ejecutadas (en DB pero No en log para las " + time + ":00 hs):, " + countNotExecuted + "\n");


            appendToFile(totalsReportFile, "    countOnSchedulerNotInRuleEngine:, " + countOnSchedulerNotInRuleEngine + "\n");
            appendToFile(totalsReportFile, "    countHasNoAction:, " + countHasNoAction + "\n");
            appendToFile(totalsReportFile, "    countHasNoTrigger:, " + countHasNoTrigger + "\n");
            appendToFile(totalsReportFile, "    countNotification:, " + countNotification +  "\n");

            appendToFile(totalsReportFile, "---------------------------------------+ \n"); */

         }

        /* formatReport(baseFolder + runNumber + "/" );

         sendEmail( env, "Scene Execution Report for " + dayDate + "-" + month + "-2025" ,
               "Scene Execution Report",
               baseFolder + runNumber + "/totalsReportFileFormatted.csv", "totalsReportFileFormatted.csv",
               "cristian.salcedo@artekium.com"); */

      } catch (Exception e) {
         printStackTrace(e);
      } finally {
         try {

            checkSessions(dataSource, dbUsername);

            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (con != null) con.close();
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         }
      }

   }


   public void formatReport(String folder){

      String fileOrigin = folder + "totalsReportFile.csv";
      String fileFormatted = folder + "totalsReportFileFormatted.csv";
      try {
         BufferedReader br = new BufferedReader(new FileReader(fileOrigin));

         /*

         1 Report for : Jueves - 2/1 time:, 00:00hs
         2 countInLog: lineas en log:, 4182
         3 total scenes from DB for: (0 00 00 ? *% ) teniendo en cuenta offset (sin filtrar por dia), 6072
         4 countDayOk: escenas from DB que deben ejecutarse por corresponder al dia (Jueves):, 4989
         5 countDayNotOk: escenas from DB que NO deben ejecutarse por no corresponder al dia (Jueves):, 1083
         6 Escena ejecutadas (en DB y en log para las 00:00 hs):, 4141
         7 Escena No ejecutadas (en DB pero No en log para las 00:00 hs):, 848
         8 ---------------------------------------+

          */

         String sCurrentLine;

         String header = "hour | linesInLog | countSceneFromDB | countExecutedScenes | countNotExecutedScenes | countOnSchedulerNotInRuleEngine | countHasNoAction | countHasNoTrigger | countNotification \n";



         String value = "";
         appendToFile(fileFormatted, header);
         int inx = 1;
         while ((sCurrentLine = br.readLine()) != null) {
            System.out.println(inx + " -  " + sCurrentLine);

            if (!sCurrentLine.startsWith("--")) {
               String[] arrLine = sCurrentLine.split(",");
               // hour
               if (inx == 1) value = value + arrLine[1].trim().substring(0,5);

               // linesInLog
               if (inx == 2) value = value + "|" + arrLine[1];

               // countSceneFromDB
               if (inx == 4) value = value + "|" + arrLine[1];

               //countExecutedScenes
               if (inx == 6) value = value + "|" + arrLine[1];

               // countNotExecutedScenes
               if (inx == 7) value = value + "|" + arrLine[1];

               // countOnSchedulerNotInRuleEngine
               if (inx == 8) value = value + "|" + arrLine[1];

               // countHasNoAction
               if (inx == 9) value = value + "|" + arrLine[1];

               // countHasNoTrigger
               if (inx == 10) value = value + "|" + arrLine[1];

               // countNotification
               if (inx == 11) value = value + "|" + arrLine[1] + "\n";

            }


            inx++;
            if (inx == 13) {
               appendToFile(fileFormatted, value);
               value = "";
               inx = 1;
            }

         }

      } catch (Exception e){
         e.printStackTrace();
      }
   }

   private String getValidTimeZoneWhereSmtn(Date myDateTime, String time){


      /* Para cada timezone restarle al "time" el offset y

      Consultar por cada timezone para distintas horas en el cron expression
         ej: 22:00
            Europe/Lisbon 22

         String sql = "SELECT TRIGGER_NAME, CRON_EXPRESSION FROM SCHEDULER.QRTZ_CRON_TRIGGERS " +
               "WHERE
               (cron_expression LIKE '0 % 22 ? *%' AND TIME_ZONE_ID = 'Europe/Lisbon')"
               OR
               (cron_expression LIKE '0 % 21 ? *%' AND TIME_ZONE_ID = 'Europe/Madrid')"
               OR
               (cron_expression LIKE '0 % 19 ? *%' AND TIME_ZONE_ID = 'Europe/Istanbul')"
               OR
               (cron_expression LIKE '0 % 01 ? *%' AND TIME_ZONE_ID IN ('America/Santiago', 'America/Buenos_Aires', 'America/Montevideo')
               OR
               (cron_expression LIKE '0 % 02 ? *%' AND TIME_ZONE_ID = 'America/Asuncion')"
               OR
               (cron_expression LIKE '0 % 03 ? *%' AND TIME_ZONE_ID IN ('America/Bogota', 'America/Lima')";


       */


      String sqlWhere = "\nWHERE "+
            "(cron_expression LIKE '0 % " + time + " ? *%' AND TIME_ZONE_ID = 'Europe/Lisbon') \n" +
            "OR " +
            "      (cron_expression LIKE '0 % " + getTimePlusOffset(myDateTime, "Europe/Madrid") + " ? *%' " +
            "AND TIME_ZONE_ID = 'Europe/Madrid') \n" +
            "OR " +
            "      (cron_expression LIKE '0 % " + getTimePlusOffset(myDateTime, "Europe/Istanbul") + " ? *%' " +
            "AND TIME_ZONE_ID = 'Europe/Istanbul') \n" +
            "OR " +
            "      (cron_expression LIKE '0 % " + getTimePlusOffset(myDateTime, "America/Santiago") +" ? *%' " +
            "AND TIME_ZONE_ID = 'America/Santiago') \n" +
            "OR " +
            "      (cron_expression LIKE '0 % " + getTimePlusOffset(myDateTime, "America/Buenos_Aires") +" ? *%' " +
            "AND TIME_ZONE_ID = 'America/Buenos_Aires') \n" +
            "OR " +
            "      (cron_expression LIKE '0 % " + getTimePlusOffset(myDateTime, "America/Montevideo") +" ? *%' " +
            "AND TIME_ZONE_ID = 'America/Montevideo') \n" +
            "OR " +
            "      (cron_expression LIKE '0 % " + getTimePlusOffset(myDateTime, "America/Asuncion") + " ? *%' " +
            "AND TIME_ZONE_ID = 'America/Asuncion') \n" +
            "OR " +
            "      (cron_expression LIKE '0 % " + getTimePlusOffset(myDateTime, "America/Bogota") + " ? *%' " +
            "AND TIME_ZONE_ID = 'America/Bogota') \n" +
            "OR " +
            "      (cron_expression LIKE '0 % " + getTimePlusOffset(myDateTime, "America/Lima") + " ? *%' " +
            "AND TIME_ZONE_ID = 'America/Lima')" ;


      return sqlWhere;
   }

   private String getTimePlusOffset(Date myDateTime, String timeZone){

      //System.out.println("This is the Actual Date:"+myDateTime);
      Calendar cal = new GregorianCalendar();
      cal.setTime(myDateTime);

      String result = "";

      int offset = timeZoneOffsetMap.get(timeZone);
      //System.out.println("offset: " + offset);

      cal.add(Calendar.HOUR_OF_DAY, +offset);
      //System.out.println("This is Hours Added Date:"+cal.getTime());

      result = "" + cal.getTime().getHours();

      if (result.length() == 1) result = "0"+ result;

      return result;

   }

   private String getCorrectedDayNumber(Date myDateTime, String timeZone, int dayNumber){

      //System.out.println("This is the Actual Date:"+myDateTime);
      Calendar cal = new GregorianCalendar();
      cal.setTime(myDateTime);

      String result = "";

      int offset = timeZoneOffsetMap.get(timeZone);
      //System.out.println("offset: " + offset);
      cal.add(Calendar.HOUR_OF_DAY, +offset);
      //System.out.println("This is Hours Added Date:"+cal.getTime());

      // Si el dia del calendar es mayor al de myDateTime
      // Obtener el dia anterior que corresponda en la cronExp:  1=dom 2=Lun 3=mar 4=Mie 5=jue 6=vie 7=sab

      System.out.println("@@ Cal. day:" + cal.getTime().getDay());
      System.out.println("@@ myDateTime. day:" + myDateTime.getDay());

      if (cal.getTime().getDay() < myDateTime.getDay()){
         dayNumber = getPreviousDayNumber(dayNumber);
      }

      System.out.println("######### corrected dayNumber: " + dayNumber);

      result = "" + dayNumber;

      return result;

   }

   private int getPreviousDayNumber(int dayNumber){

      // 1=dom
      // 2=Lun
      // 3=mar
      // 4=Mie
      // 5=jue
      // 6=vie
      // 7=sab

      if (dayNumber > 1)
         return dayNumber -1;
      else
         return 7;

   }

}
