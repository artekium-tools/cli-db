package clidb.entities;

import java.io.Serializable;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

//@Entity
//@Table(name ="SMPR_TTHING_ALIAS")
//@Cacheable
//@Cache(coordinationType = CacheCoordinationType.SEND_NEW_OBJECTS_WITH_CHANGES)
public class ThingAlias implements Serializable {

    //@Id
    //@GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "IdThingAliasSeq")
    //@SequenceGenerator(name = "IdThingAliasSeq", sequenceName = "SEQ_THING_ALIAS_ID", allocationSize = 1)
    //@Column(name = "SEQ_ID")
    private Long id;

    //@Column(name = "SEQ_INSTALLATION_ALIAS_ID")
    private Long installationAliasId;

    //@Column(name = "SEQ_PARENT_ID")
    private Long parentId;

    //@Column(name = "SEQ_USER_ID")
    private Long userId;

    //@Column(name = "SEQ_THING_ID")
    private Long thingId;

    //@Column(name = "DES_NAME")
    private String name;

    public ThingAlias() { super(); }

    public ThingAlias(String schema, ResultSet rs) {
        this();
        try {
            if ("FILTER-DISPATCHER".equals(schema)){
                this.setId(rs.getLong("SEQ_ID"));
                this.setInstallationAliasId(rs.getLong("SEQ_INSTALLATION_ALIAS_ID"));
                this.setParentId(rs.getLong("SEQ_PARENT_ID"));
                this.setUserId(rs.getLong("SEQ_USER_ID"));
                this.setThingId(rs.getLong("SEQ_THING_ID"));
                this.setName(rs.getString("DES_NAME"));
            }

        } catch (Exception e) {

        }

        this.installationAliasId = installationAliasId;
        this.thingId = thingId;
        this.userId = userId;
    }


    // FYH_UPDATE_DATE


    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Long getInstallationAliasId() { return installationAliasId; }

    public void setInstallationAliasId(Long installationAliasId) { this.installationAliasId = installationAliasId; }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) { this.parentId = parentId; }

    public Long getThingId() { return thingId; }

    public void setThingId(Long thingId) { this.thingId = thingId; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public boolean equals(String schema, ThingAlias replica){

        // Id is the key to search, do not compare

        // FILTER-DISPATCHER: SEQ_ID, SEQ_INSTALLATION_ALIAS_ID, SEQ_PARENT_ID, SEQ_USER_ID, SEQ_THING_ID, DES_NAME

        if ("FILTER-DISPATCHER".equals(schema)) {

            if(!equalFields(installationAliasId, replica.getInstallationAliasId())) return false;

            if (!equalFields(parentId, replica.getParentId())) return false;

            if (!equalFields(thingId, replica.getThingId())) return false;

            if (!equalFields(userId, replica.getUserId())) return false;

            if (!equalFields(name, replica.getName())) return false;

        }


        return true;
    }

    private boolean equalFields(Object ownerField, Object replicaField){

        if (ownerField != null && replicaField == null) return false;

        if (ownerField == null && replicaField != null) return false;

        if (ownerField != null && replicaField != null &&
                !ownerField.equals(replicaField)) return false;

        return true;
    }

    public String getInsertStatement(String schema) throws Exception {

        String sql = "INSERT INTO SMPR_TTHING_ALIAS " +
              "(SEQ_ID, SEQ_INSTALLATION_ALIAS_ID, SEQ_PARENT_ID, SEQ_USER_ID, SEQ_THING_ID, DES_NAME, FYH_UPDATE_DATE) " +
              " VALUES (?,?,?,?,?,?,?)";

        String updateDate = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss.SS").format(new java.util.Date());
        String updateDateStr = "to_timestamp('"+updateDate+"', 'YYYY-MM-DD HH24:MI:SS.FF')";

        Object[] parameters = new Object[]{id, installationAliasId, parentId, userId, thingId, getValidString(name), updateDateStr};

        for (Object obj:parameters){
            sql = sql.replaceFirst("\\?", ""+obj);
        }

//System.out.println(sql);

        return sql;
    }

    public String getValidString(String value){
        if (value != null) value = value.replaceAll("'", "''");
        return value == null?""+null:"'"+ value + "'";
    }

    public String getUpdateStatement(String schema) throws Exception {

        String sql = "UPDATE SMPR_TTHING_ALIAS SET " +
              "SEQ_INSTALLATION_ALIAS_ID =?, " +
              "SEQ_PARENT_ID =?, " +
              "SEQ_USER_ID =?, " +
              "SEQ_THING_ID =?, " +
              "DES_NAME =?, " +
              "FYH_UPDATE_DATE =? " +
              "WHERE SEQ_ID =?";

        String updateDate = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss.SS").format(new java.util.Date());
        String updateDateStr = "to_timestamp('"+updateDate+"', 'YYYY-MM-DD HH24:MI:SS.FF')";

        Object[] parameters = new Object[]{installationAliasId, parentId, userId, thingId, getValidString(name), updateDateStr, id};

        for (Object obj:parameters){
            sql = sql.replaceFirst("\\?", ""+obj);
        }

//System.out.println(sql);

        return sql;
    }

    @Override
    public String toString() {
        return "ThingAlias{" +
            "id=" + id +
            ", installationAliasId=" + installationAliasId +
            ", parentId=" + parentId +
            ", userId=" + userId +
            ", thingId=" + thingId +
            ", name='" + name + '\'' +
            '}';
    }
}
