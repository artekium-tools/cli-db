package clidb.entities;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.Timestamp;

//@Entity
//@Table(name = "SMPR_TINSTALLATION_ALIAS")
//@Cacheable
//@Cache(coordinationType = CacheCoordinationType.SEND_NEW_OBJECTS_WITH_CHANGES)
public class InstallationAlias implements Serializable {

    //  @Id
    //@Column(name = "SEQ_ID")
    private Long id;

    //@Column(name = "SEQ_INSTALLATION_ID")
    private Long installationId;

    //@Column(name = "SEQ_USER_ID")
    private Long userId;

    //@Column(name = "DES_ALIAS")
    private String alias;

    //@Column(name = "DES_PANEL_USER")
    private String panelUser;

    //@Column(name = "DES_IMAGE")
    private Integer image;

    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    //@Column(name = "FYH_UPDATE_DATE")
    private Timestamp updateDate;

    public InstallationAlias() { super(); }

    public InstallationAlias(String schema, ResultSet rs) {
        this();
        try {
            if ("FILTER-DISPATCHER".equals(schema)){
                this.setId(rs.getLong("SEQ_ID"));
                this.setInstallationId(rs.getLong("SEQ_INSTALLATION_ID"));
                this.setUserId(rs.getLong("SEQ_USER_ID"));
                this.setAlias(rs.getString("DES_ALIAS"));
                this.setPanelUser(rs.getString("DES_PANEL_USER"));
                this.setUpdateDate(rs.getTimestamp("FYH_UPDATE_DATE"));
            }

        } catch (Exception e) {

        }

    }


    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Long getInstallationId() { return installationId; }

    public void setInstallationId(Long installationId) { this.installationId = installationId; }

    public Long getUserId() { return userId; }

    public void setUserId(Long userId) { this.userId = userId; }

    public String getAlias() { return alias; }

    public void setAlias(String alias) { this.alias = alias; }

    public String getPanelUser() {
        return panelUser;
    }

    public void setPanelUser(String panelUser) {
        this.panelUser = panelUser;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }


    public boolean equals(String schema, InstallationAlias replica){

        // Id is the key to search, do not compare

        // FILTER-DISPATCHER: SEQ_ID, SEQ_INSTALLATION_ID, SEQ_USER_ID, FYH_UPDATE_DATE, DES_ALIAS, DES_PANEL_USER
        if ("FILTER-DISPATCHER".equals(schema)) {
            if(!equalFields(installationId, replica.getInstallationId())) return false;

            if (!equalFields(userId, replica.getUserId())) return false;

            if (!equalFields(alias, replica.getAlias())) return false;

            if (panelUser == null && replica.getPanelUser() != null) return false;

        }


        return true;
    }

    private boolean equalFields(Object ownerField, Object replicaField){

        if (ownerField != null && replicaField == null) return false;

        if (ownerField == null && replicaField != null) return false;

        if (ownerField != null && replicaField != null &&
              !ownerField.equals(replicaField)) return false;

        return true;
    }

    public String getInsertStatement(String schema){

        String sql = "INSERT INTO SMPR_TINSTALLATION_ALIAS (SEQ_ID, SEQ_INSTALLATION_ID, SEQ_USER_ID, DES_ALIAS, DES_PANEL_USER, FYH_UPDATE_DATE) " +
              " VALUES (?,?,?,?,?,?)";
        if (schema.equals("FILTER-DISPATCHER")){
            sql = sql.replaceFirst("\\?", ""+id );
            sql = sql.replaceFirst("\\?", ""+installationId );
            sql = sql.replaceFirst("\\?", ""+userId );
            sql = sql.replaceFirst("\\?", alias == null?""+null:"'"+ alias + "'" );
            sql = sql.replaceFirst("\\?", panelUser == null?""+null:"'"+ panelUser + "'");
            sql = sql.replaceFirst("\\?", updateDate == null?""+null:"to_timestamp('"+updateDate+"', 'YYYY-MM-DD HH24:MI:SS.FF')");



        }
        return sql;
    }

    public String getUpdateStatement(String schema){

        String sql = "UPDATE SMPR_TINSTALLATION_ALIAS SET " +
              "SEQ_INSTALLATION_ID = ?, " +
              "SEQ_USER_ID = ?, " +
              "DES_ALIAS = ?, " +
              "DES_PANEL_USER = ?, " +
              "FYH_UPDATE_DATE = ? " +
              "WHERE SEQ_ID = ?";

        if (schema.equals("FILTER-DISPATCHER")){
            sql = sql.replaceFirst("\\?", ""+installationId );
            sql = sql.replaceFirst("\\?", ""+userId );
            sql = sql.replaceFirst("\\?", alias == null?""+null:"'"+ alias + "'");
            sql = sql.replaceFirst("\\?", panelUser == null?""+null:"'"+ panelUser + "'");
            sql = sql.replaceFirst("\\?", updateDate == null?""+null:"to_timestamp('"+updateDate+"', 'YYYY-MM-DD HH24:MI:SS.FF')");
            sql = sql.replaceFirst("\\?", ""+id );
        }
        return sql;
    }

    @Override
    public String toString() {
        return "InstallationAlias{" +
                "id=" + id +
                ", installationId=" + installationId +
                ", userId=" + userId +
                ", alias=" + alias +
                ", panelUser=" + panelUser +
                ", image=" + image +
                ", updateDate= " + updateDate +
                '}';
    }
}

