package clidb.entities;

import org.apache.commons.lang.StringUtils;


public class EventIndividualFiltersDto {


   private boolean showPanelEvents;
   private boolean showDetectorsEvents;
   private boolean showDomoticEvents;
   private boolean showVideoDeviceEvents;

   private String detectorDeviceIdsString;
   private String domoticDeviceIdsString;
   private String videoDeviceIdsString;
   private String eventTypeIdsString;

   public boolean isMustFilterVideoDeviceEvents() {

      if (showPanelEvents && showDetectorsEvents && showDomoticEvents && showVideoDeviceEvents
            && StringUtils.isEmpty(detectorDeviceIdsString)
            && StringUtils.isEmpty(domoticDeviceIdsString)
            && StringUtils.isEmpty(videoDeviceIdsString)
            && StringUtils.isEmpty(eventTypeIdsString)) {

         // all selected -- just need to filter videoEvents (CM) by userId
         return true;
      } else {
         return false;
      }
   }


   public boolean isShowPanelEvents() {
      return showPanelEvents;
   }

   public void setShowPanelEvents(boolean showPanelEvents) {
      this.showPanelEvents = showPanelEvents;
   }

   public boolean isShowDetectorsEvents() {
      return showDetectorsEvents;
   }

   public void setShowDetectorsEvents(boolean showDetectorsEvents) {
      this.showDetectorsEvents = showDetectorsEvents;
   }

   public boolean isShowDomoticEvents() {
      return showDomoticEvents;
   }

   public void setShowDomoticEvents(boolean showDomoticEvents) {
      this.showDomoticEvents = showDomoticEvents;
   }

   public boolean isShowVideoDeviceEvents() {
      return showVideoDeviceEvents;
   }

   public void setShowVideoDeviceEvents(boolean showVideoDeviceEvents) {
      this.showVideoDeviceEvents = showVideoDeviceEvents;
   }

   public String getDetectorDeviceIdsString() {
      return detectorDeviceIdsString;
   }

   public void setDetectorDeviceIdsString(String detectorDeviceIdsString) {
      this.detectorDeviceIdsString = detectorDeviceIdsString;
   }

   public String getDomoticDeviceIdsString() {
      return domoticDeviceIdsString;
   }

   public void setDomoticDeviceIdsString(String domoticDeviceIdsString) {
      this.domoticDeviceIdsString = domoticDeviceIdsString;
   }

   public String getVideoDeviceIdsString() {
      return videoDeviceIdsString;
   }

   public void setVideoDeviceIdsString(String videoDeviceIdsString) {
      this.videoDeviceIdsString = videoDeviceIdsString;
   }

   public String getEventTypeIdsString() {
      return eventTypeIdsString;
   }

   public void setEventTypeIdsString(String eventTypeIdsString) {
      this.eventTypeIdsString = eventTypeIdsString;
   }

   @Override
   public String toString() {

      final StringBuffer sb = new StringBuffer("EventIndividualFiltersDto{");

      sb.append("mustFilterVideoDeviceEvents=").append(isMustFilterVideoDeviceEvents());
      sb.append(", showPanelEvents=").append(showPanelEvents);
      sb.append(", showDetectorsEvents=").append(showDetectorsEvents);
      sb.append(", showDomoticEvents=").append(showDomoticEvents);
      sb.append(", showVideoDeviceEvents=").append(showVideoDeviceEvents);
      sb.append(", detectorDeviceIds=[").append(detectorDeviceIdsString).append("] ");
      sb.append(", domoticDeviceIds=[").append(domoticDeviceIdsString).append("] ");
      sb.append(", videoDeviceIds=[").append(videoDeviceIdsString).append("] ");
      sb.append(", eventTypeIds=[").append(eventTypeIdsString).append("] ");
      return sb.toString();
    }
}
