package clidb.entities;


import java.sql.ResultSet;
import java.text.SimpleDateFormat;

//@Entity
//@Table(name = "SMPR_TTHING")
//@Cacheable
//@Cache(coordinationType = CacheCoordinationType.SEND_NEW_OBJECTS_WITH_CHANGES)
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//@DiscriminatorColumn(name = "SEQ_ENTITY_TYPE", discriminatorType = DiscriminatorType.INTEGER)
public class Thing  {

    //@Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IdThingSeq")
    //@SequenceGenerator(name = "IdThingSeq", sequenceName = "SEQ_THING_ID", allocationSize = 1)
    //@Column(name = "SEQ_ID")
    private Long id;

    //@Column(name = "SEQ_PARENT_ID")
    private Long parentId;

    //@Column(name = "SEQ_ENTITY_TYPE")
    private Integer entityTpe;

    //@Column(name = "DES_NAME")
    private String name;

    //@Column(name = "DES_CODE")
    private String code;

    //@Column(name = "SEQ_DEVICE_TYPE")
    private Integer deviceType;

    //@Column(name = "SEQ_DOM_CATALOG_DEV_ID")
    private Long domoticCatalogDeviceId;

    //@Column(name = "SEQ_ROOM_ID")
    private Long roomId;

    //@Column(name = "DES_INDEX")
    private Integer index;

    //@Column(name="SEQ_TYPE_ID")
    private Integer serviceTypeId;

    //@Column( name = "SEQ_INSTALLATION_ID")
    private Long installationId;

    //@Column(name = "COD_CONNECTION_ID")
    private String connectionId;

    //@Column(name = "COD_COUNTRY")
    private String country;

    //@Column(name = "DES_CONTRACT_NUMBER")
    private String contractNumber;

    //@Column(name = "DES_MODEL")
    private String model;

    //@Column(name = "NUM_CHANNEL")
    private Long channelId;
    //@Transient
    //private ServiceType serviceType;

    //@Transient
    //private String classType;
    //@Column(name = "BOL_PRIVACY_MODE")
    //@Convert(converter = BooleanToIntegerConverter.class)
    private Integer privacyModeEnabled;

    private String state1;
    private String state2;
    private String state3;
    private String state4;
    private String state5;
    private String state6;
    private String state7;
    private String state8;
    private String state9;
    private String state10;
    private String state11;
    private String state12;
    private String state13;
    private String state14;
    private String state15;
    private String state16;
    private String state17;
    private String state18;
    private String state19;
    private String state20;
    private String state21;
    private String state22;
    private String state23;

    public Thing() {
        super();
    }

    public Thing(String schema, ResultSet rs) {
        this();
        try {
            if ("FILTER-DISPATCHER".equals(schema)){
                this.setId(rs.getLong("SEQ_ID"));
                this.setChannelId(rs.getLong( "NUM_CHANNEL"));
                this.setCode(rs.getString("DES_CODE"));
                this.setContractNumber(rs.getString("DES_CONTRACT_NUMBER"));
                this.setDomoticCatalogDeviceId(rs.getLong("SEQ_DOM_CATALOG_DEV_ID"));
                this.setIndex(rs.getInt("DES_INDEX"));
                this.setName(rs.getString("DES_NAME"));
                this.setRoomId(rs.getLong( "SEQ_ROOM_ID"));
                this.setEntityTpe(rs.getInt("SEQ_ENTITY_TYPE"));
                this.setInstallationId(rs.getLong("SEQ_INSTALLATION_ID"));
                this.setParentId(rs.getLong("SEQ_PARENT_ID"));
                this.setServiceTypeId(rs.getInt("SEQ_TYPE_ID"));
                this.setDeviceType(rs.getInt("SEQ_DEVICE_TYPE"));
                this.setConnectionId(rs.getString("COD_CONNECTION_ID"));
                this.setCountry(rs.getString("COD_COUNTRY"));
                this.setPrivacyModeEnabled(rs.getInt( "BOL_PRIVACY_MODE"));
                this.setState1(rs.getString( "STATE_1"));
                this.setState2(rs.getString("STATE_2"));
                this.setState3(rs.getString("STATE_3"));
                this.setState4(rs.getString("STATE_4"));
                this.setState5(rs.getString("STATE_5"));
                this.setState6(rs.getString("STATE_6"));
                this.setState7(rs.getString("STATE_7"));
                this.setState8(rs.getString("STATE_8"));
                this.setState9(rs.getString("STATE_9"));
                this.setState10(rs.getString("STATE_10"));
                this.setState11(rs.getString("STATE_11"));
                this.setState12(rs.getString("STATE_12"));
                this.setState13(rs.getString("STATE_13"));
                this.setState14(rs.getString("STATE_14"));
                this.setState15(rs.getString("STATE_15"));
                this.setState16(rs.getString("STATE_16"));
                this.setState17(rs.getString("STATE_17"));
                this.setState18(rs.getString("STATE_18"));
                this.setState19(rs.getString("STATE_19"));
                this.setState20(rs.getString("STATE_20"));
                this.setState21(rs.getString("STATE_21"));
                this.setState22(rs.getString("STATE_22"));
                this.setState23(rs.getString("STATE_23"));

            }

        } catch (Exception e) {

        }

    }

    //@Override
    public Long getId() {
        return id;
    }

    //@Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() { return code; }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public Long getDomoticCatalogDeviceId() {
        return domoticCatalogDeviceId;
    }

    public void setDomoticCatalogDeviceId(Long domoticCatalogDeviceId) {
        this.domoticCatalogDeviceId = domoticCatalogDeviceId;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

   // public String getClassType() { return classType; }

    //public void setClassType(String classType) { this.classType = classType; }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    //@Override
    public Integer getServiceTypeId() {
        return serviceTypeId;
    }

    //@Override
    public void setServiceTypeId(Integer serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    //@Override
    public Long getInstallationId() {
        return installationId;
    }

    //@Override
    public void setInstallationId(Long installationId) {
        this.installationId = installationId;
    }

    //@Override
    public String getConnectionId() {
        return connectionId;
    }

    //@Override
    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    //@Override
    public String getCountry() {
        return country;
    }

    //@Override
    public void setCountry(String country) {
        this.country = country;
    }

    //@Override
    public String getContractNumber() {
        return contractNumber;
    }

    //@Override
    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getPrivacyModeEnabled() {
        return privacyModeEnabled;
    }

    public void setPrivacyModeEnabled(Integer privacyModeEnabled) {
        this.privacyModeEnabled = privacyModeEnabled;
    }

    public String getState1() {
        return state1;
    }

    public void setState1(String state1) {
        this.state1 = state1;
    }

    public String getState2() {
        return state2;
    }

    public void setState2(String state2) {
        this.state2 = state2;
    }

    public String getState3() {
        return state3;
    }

    public void setState3(String state3) {
        this.state3 = state3;
    }

    public String getState4() {
        return state4;
    }

    public void setState4(String state4) {
        this.state4 = state4;
    }

    public String getState5() {
        return state5;
    }

    public void setState5(String state5) {
        this.state5 = state5;
    }

    public String getState6() {
        return state6;
    }

    public void setState6(String state6) {
        this.state6 = state6;
    }

    public String getState7() {
        return state7;
    }

    public void setState7(String state7) {
        this.state7 = state7;
    }

    public String getState8() {
        return state8;
    }

    public void setState8(String state8) {
        this.state8 = state8;
    }

    public String getState9() {
        return state9;
    }

    public void setState9(String state9) {
        this.state9 = state9;
    }

    public String getState10() {
        return state10;
    }

    public void setState10(String state10) {
        this.state10 = state10;
    }

    public String getState11() {
        return state11;
    }

    public void setState11(String state11) {
        this.state11 = state11;
    }

    public String getState12() {
        return state12;
    }

    public void setState12(String state12) {
        this.state12 = state12;
    }

    public String getState13() {
        return state13;
    }

    public void setState13(String state13) {
        this.state13 = state13;
    }

    public String getState14() {
        return state14;
    }

    public void setState14(String state14) {
        this.state14 = state14;
    }

    public String getState15() {
        return state15;
    }

    public void setState15(String state15) {
        this.state15 = state15;
    }

    public String getState16() {
        return state16;
    }

    public void setState16(String state16) {
        this.state16 = state16;
    }

    public String getState17() {
        return state17;
    }

    public void setState17(String state17) {
        this.state17 = state17;
    }

    public String getState18() {
        return state18;
    }

    public void setState18(String state18) {
        this.state18 = state18;
    }

    public String getState19() {
        return state19;
    }

    public void setState19(String state19) {
        this.state19 = state19;
    }

    public String getState20() {
        return state20;
    }

    public void setState20(String state20) {
        this.state20 = state20;
    }

    public String getState21() {
        return state21;
    }

    public void setState21(String state21) {
        this.state21 = state21;
    }

    public String getState22() {
        return state22;
    }

    public void setState22(String state22) {
        this.state22 = state22;
    }

    public String getState23() {
        return state23;
    }

    public void setState23(String state23) {
        this.state23 = state23;
    }

    //@Override
    //public ServiceType getServiceType() {
    // }

    //@Override
    //public void setServiceType(ServiceType serviceType) {
    //    this.serviceType = serviceType;
    //}

    //abstract public <T extends device.entities.Thing> T cast();

    public Integer getEntityTpe() {
        return entityTpe;
    }

    public void setEntityTpe(Integer entityTpe) {
        this.entityTpe = entityTpe;
    }


    public static String getFieldNames(String schema){

        String fields ="";
        if (schema.equals("FILTER-DISPATCHER")){
            fields = "SEQ_ID, NUM_CHANNEL, DES_CODE, DES_CONTRACT_NUMBER, SEQ_DOM_CATALOG_DEV_ID, DES_INDEX, DES_NAME, SEQ_ROOM_ID, " +
                  "SEQ_ENTITY_TYPE, SEQ_INSTALLATION_ID, SEQ_PARENT_ID, SEQ_TYPE_ID, SEQ_DEVICE_TYPE, COD_CONNECTION_ID, COD_COUNTRY, " +
                  "BOL_PRIVACY_MODE, STATE_1, STATE_2, STATE_3, STATE_4, STATE_5, STATE_6, STATE_7, STATE_8, STATE_9, STATE_10, " +
                  "STATE_11, STATE_12, STATE_13, STATE_14, STATE_15, STATE_16, STATE_17, STATE_18, STATE_19, STATE_20, STATE_21, " +
                  "STATE_22, STATE_23";
        }



        return fields;
    }

    public boolean equals(String schema, Thing replica){

        // Id is the key to search, do not compare

        // FILTER-DISPATCHER:
        // NUM_CHANNEL, DES_CODE, DES_CONTRACT_NUMBER, SEQ_DOM_CATALOG_DEV_ID, DES_INDEX, DES_NAME, SEQ_ROOM_ID, " +
        // SEQ_ENTITY_TYPE, SEQ_INSTALLATION_ID, SEQ_PARENT_ID, SEQ_TYPE_ID, SEQ_DEVICE_TYPE, COD_CONNECTION_ID, COD_COUNTRY, " +
        // BOL_PRIVACY_MODE, STATE_1, STATE_2, STATE_3, STATE_4, STATE_5, STATE_6, STATE_7, STATE_8, STATE_9, STATE_10, " +
        // STATE_11, STATE_12, STATE_13, STATE_14, STATE_15, STATE_16, STATE_17, STATE_18, STATE_19, STATE_20, STATE_21, " +
        // STATE_22, STATE_23";

        if ("FILTER-DISPATCHER".equals(schema)) {

            if (!equalFields(channelId, replica.getChannelId())) return false;
            if (!equalFields(code, replica.getCode())) return false;
            if (!equalFields(contractNumber, replica.getContractNumber())) return false;
            if (!equalFields(domoticCatalogDeviceId, replica.getDomoticCatalogDeviceId())) return false;
            if (!equalFields(index, replica.getIndex())) return false;
            if (!equalFields(name, replica.getName())) return false;
            if (!equalFields(roomId, replica.getRoomId())) return false;
            if (!equalFields(entityTpe, replica.getEntityTpe())) return false;
            if (!equalFields(installationId, replica.getInstallationId())) return false;
            if (!equalFields(parentId, replica.getParentId())) return false;
            if (!equalFields(serviceTypeId, replica.getServiceTypeId())) return false;
            if (!equalFields(deviceType, replica.getDeviceType())) return false;
            if (!equalFields(connectionId, replica.getContractNumber())) return false;
            if (!equalFields(country, replica.getCountry())) return false;
            if (!equalFields(privacyModeEnabled, replica.getPrivacyModeEnabled())) return false;
            if (!equalFields(state1, replica.getState1())) return false;
            if (!equalFields(state2, replica.getState2())) return false;
            if (!equalFields(state3, replica.getState3())) return false;
            if (!equalFields(state4, replica.getState4())) return false;
            if (!equalFields(state5, replica.getState5())) return false;
            if (!equalFields(state6, replica.getState6())) return false;
            if (!equalFields(state7, replica.getState7())) return false;
            if (!equalFields(state8, replica.getState8())) return false;
            if (!equalFields(state9, replica.getState9())) return false;
            if (!equalFields(state10, replica.getState10())) return false;
            if (!equalFields(state11, replica.getState11())) return false;
            if (!equalFields(state12, replica.getState12())) return false;
            if (!equalFields(state13, replica.getState13())) return false;
            if (!equalFields(state14, replica.getState14())) return false;
            if (!equalFields(state15, replica.getState15())) return false;
            if (!equalFields(state16, replica.getState16())) return false;
            if (!equalFields(state17, replica.getState17())) return false;
            if (!equalFields(state18, replica.getState18())) return false;
            if (!equalFields(state19, replica.getState19())) return false;
            if (!equalFields(state20, replica.getState20())) return false;
            if (!equalFields(state21, replica.getState21())) return false;
            if (!equalFields(state22, replica.getState22())) return false;
            if (!equalFields(state23, replica.getState23())) return false;

        }


        return true;
    }

    private boolean equalFields(Object ownerField, Object replicaField){

        if (ownerField != null && replicaField == null) return false;

        if (ownerField == null && replicaField != null) return false;

        if (ownerField != null && replicaField != null &&
              !ownerField.equals(replicaField)) return false;

        return true;
    }

    public String getStr(String value){
        if (value != null) value = value.replaceAll("'", "''");
        return value == null?""+null:"'"+ value + "'";
    }

    public String getInsertStatement(String schema) throws Exception {

        // FILTER-DISPATCHER:
        // NUM_CHANNEL, DES_CODE, DES_CONTRACT_NUMBER, SEQ_DOM_CATALOG_DEV_ID, DES_INDEX, DES_NAME, SEQ_ROOM_ID, " +
        // SEQ_ENTITY_TYPE, SEQ_INSTALLATION_ID, SEQ_PARENT_ID, SEQ_TYPE_ID, SEQ_DEVICE_TYPE, COD_CONNECTION_ID, COD_COUNTRY, " +
        // BOL_PRIVACY_MODE, STATE_1, STATE_2, STATE_3, STATE_4, STATE_5, STATE_6, STATE_7, STATE_8, STATE_9, STATE_10, " +
        // STATE_11, STATE_12, STATE_13, STATE_14, STATE_15, STATE_16, STATE_17, STATE_18, STATE_19, STATE_20, STATE_21, " +
        // STATE_22, STATE_23, FYH_UPDATE_DATE

        String sql = "INSERT INTO SMPR_TTHING " +
              "(SEQ_ID, NUM_CHANNEL, DES_CODE, DES_CONTRACT_NUMBER, SEQ_DOM_CATALOG_DEV_ID, DES_INDEX, DES_NAME, SEQ_ROOM_ID, " +
              "SEQ_ENTITY_TYPE, SEQ_INSTALLATION_ID, SEQ_PARENT_ID, SEQ_TYPE_ID, SEQ_DEVICE_TYPE, COD_CONNECTION_ID, COD_COUNTRY, " +
              "BOL_PRIVACY_MODE, STATE_1, STATE_2, STATE_3, STATE_4, STATE_5, STATE_6, STATE_7, STATE_8, STATE_9, STATE_10, " +
              "STATE_11, STATE_12, STATE_13, STATE_14, STATE_15, STATE_16, STATE_17, STATE_18, STATE_19, STATE_20, STATE_21, " +
              "STATE_22, STATE_23, FYH_UPDATE_DATE) " +
              "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        String updateDate = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss.SS").format(new java.util.Date());
        String updateDateStr = "to_timestamp('"+updateDate+"', 'YYYY-MM-DD HH24:MI:SS.FF')";

        Object[] parameters = new Object[]{
            id, channelId, getStr(code), getStr(contractNumber), domoticCatalogDeviceId, index, getStr(name), roomId,
            entityTpe, installationId, parentId, serviceTypeId, deviceType, getStr(connectionId), getStr(country),
            privacyModeEnabled, getStr(state1), getStr(state2), getStr(state3), getStr(state4),getStr(state5), getStr(state6),
            getStr(state7), getStr(state8), getStr(state9), getStr(state10), getStr(state11), getStr(state12), getStr(state13),
            getStr(state14), getStr(state15), getStr(state16), getStr(state17), getStr(state18), getStr(state19), getStr(state20),
            getStr(state21), getStr(state22), getStr(state23), updateDateStr};

        for (Object obj:parameters){
            sql = sql.replaceFirst("\\?", ""+obj);
        }

        return sql;
    }

    public String getUpdateStatement(String schema) throws Exception {

        String sql = "UPDATE SMPR_TTHING SET " +
              "NUM_CHANNEL = ?, " +
              "DES_CODE = ?, " +
              "DES_CONTRACT_NUMBER = ?, " +
              "SEQ_DOM_CATALOG_DEV_ID = ?, " +
              "DES_INDEX = ?, " +
              "DES_NAME = ?, " +
              "SEQ_ROOM_ID = ?, " +
              "SEQ_ENTITY_TYPE = ?, " +
              "SEQ_INSTALLATION_ID = ?, " +
              "SEQ_PARENT_ID = ?, " +
              "SEQ_TYPE_ID = ?, " +
              "SEQ_DEVICE_TYPE = ?, " +
              "COD_CONNECTION_ID = ?, " +
              "COD_COUNTRY = ?, " +
              "BOL_PRIVACY_MODE = ?, " +
              "STATE_1 = ?, " +
              "STATE_2 = ?, " +
              "STATE_3 = ?, " +
              "STATE_4 = ?, " +
              "STATE_5 = ?, " +
              "STATE_6 = ?, " +
              "STATE_7 = ?, " +
              "STATE_8 = ?, " +
              "STATE_9 = ?, " +
              "STATE_10 = ?, " +
              "STATE_11 = ?, " +
              "STATE_12 = ?, " +
              "STATE_13 = ?, " +
              "STATE_14 = ?, " +
              "STATE_15 = ?, " +
              "STATE_16 = ?, " +
              "STATE_17 = ?, " +
              "STATE_18 = ?, " +
              "STATE_19 = ?, " +
              "STATE_20 = ?, " +
              "STATE_21 = ?, " +
              "STATE_22 = ?, " +
              "STATE_23 = ?, " +
              "FYH_UPDATE_DATE = ? " +
              "WHERE SEQ_ID = ? ";

        String updateDate = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss.SS").format(new java.util.Date());
        String updateDateStr = "to_timestamp('"+updateDate+"', 'YYYY-MM-DD HH24:MI:SS.FF')";

        Object[] parameters = new Object[]{
              channelId, getStr(code), getStr(contractNumber), domoticCatalogDeviceId, index, getStr(name), roomId,
              entityTpe, installationId, parentId, serviceTypeId, deviceType, getStr(connectionId), getStr(country),
              privacyModeEnabled, getStr(state1), getStr(state2), getStr(state3), getStr(state4),getStr(state5), getStr(state6),
              getStr(state7), getStr(state8), getStr(state9), getStr(state10), getStr(state11), getStr(state12), getStr(state13),
              getStr(state14), getStr(state15), getStr(state16), getStr(state17), getStr(state18), getStr(state19), getStr(state20),
              getStr(state21), getStr(state22), getStr(state23), updateDateStr, id};

        for (Object obj:parameters){
            sql = sql.replaceFirst("\\?", ""+obj);
        }

        return sql;
    }
}
