package clidb.entities;

import org.apache.commons.lang.StringUtils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;

//@Component
public class EventDaoJdbcImpl {

    //private static final Logger LOGGER = LoggerFactory.getLogger(activitystream.dao.impl.EventDaoJdbcImpl.class);

    private static final int MAX_ALLOWED_VALUES = 1000;

    /*@Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired*/
    EventTypeUtils eventTypeUtils = new EventTypeUtils() ;


    public String getCountMultiSiteEventsStmnt(Collection<Long> serviceIds, Timestamp fromDate, Timestamp toDate) {

        String serviceIdPredicate = getServicePredicate(serviceIds);

        String sql = "SELECT count(seq_id) \n" +

              "FROM SMPR_TEVENT event \n" +

              "WHERE event.SEQ_EVENT_TYPE_ID IS NOT NULL \n\n" +

              "-- serviceIds filter \n" +
              "AND (" + serviceIdPredicate + ") \n" +
              "AND event.FYH_EVENT_DATE >= to_timestamp('?', 'YYYY-MM-DD HH24:MI:SS.FF') \n" +
              "AND event.FYH_EVENT_DATE < to_timestamp('?', 'YYYY-MM-DD HH24:MI:SS.FF') \n" +
              // isTheftAlarmConfirmed: Clause to filter if is theaft alarm confirmed
              "AND ((event.SEQ_EVENT_TYPE_ID = 10 AND event.BOL_MASTERMIND_EVENT = 1 ) OR \n" +
              "     (event.SEQ_EVENT_TYPE_ID <> 10)) \n\n" ;


        String fromStr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(fromDate);
        String toStr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(toDate);

        Object[] parameters = new Object[]{fromStr, toStr};

        for (Object obj:parameters){
            sql = sql.replaceFirst("\\?", ""+obj);
        }

        System.out.println(sql);

        return sql;


    }

    public String findByServiceIdsAndRangeDateMultiSite(Collection<Long> serviceIds, Timestamp fromDate, Timestamp toDate, String country, Long userId, int limit) {

        String serviceIdPredicate = getServicePredicate(serviceIds);

        String sql = "SELECT * FROM ( \n" +
              "SELECT DISTINCT event.*, \n" +
              "insta.SEQ_ID AS installationId, \n" +
              "insta.DES_CONTRACT_NUMBER, \n" +
              "insta.DES_LOCATION_STREET_TYPE, \n" +
              "insta.DES_LOCATION_STREET, \n" +
              "insta.DES_LOCATION_STREET_NUMBER, \n" +
              "insta.DES_LOCATION_CITY, \n" +
              "insta.DES_LOCATION_REGION, \n" +
              "insta.DES_CUSTOMER_REFERENCE, \n" +
              "thingPanel.COD_CONNECTION_ID, \n" +
              "thingDevice.DES_NAME AS cameraName, \n" +
              "eventText.COD_FRONT_CODE, \n" +
              "image.SEQ_ID AS imageId, \n" +
              "video.SEQ_ID AS videoId, \n" +
              "video.SEQ_IMAGEPREVIEW_ID, \n" +
              "video.SEQ_DEVICE_ID as videoDeviceId, \n" +
              "thingVideoDevice.DES_NAME AS videoDeviceName, \n" +
              "usr.des_nick_name AS eventUserName, \n" +
              "alias.DES_ALIAS AS eventUserAlias, \n" +
              "panelUserAlias.DES_ALIAS AS panelUserAlias, \n"+
              "CASE event.BOL_IS_FULL_PARTITION \n"+
              " WHEN 1 THEN \n" +
              "  (SELECT count(1) FROM SMPR_TTHING thn WHERE thn.SEQ_PARENT_ID = event.SEQ_PARENT_THING_ID AND thn.SEQ_ENTITY_TYPE = 18 AND thn.BOL_IS_ACTIVE = 1) \n" +
              " ELSE 0 \n" +
              "END AS partitionCount, \n" +
              "partitionUserAlias.DES_NAME AS partitionUserAlias, \n" +
              "partitionEventUserAlias.DES_NAME AS partitionEventUserAlias, \n" +
              "partitionAlias.DES_NAME AS partitionAlias, \n" +
              "partitionAlias.SEQ_ENTITY_TYPE AS partitionEntiyType \n\n"+

              "FROM SMPR_TEVENT event \n\n" +

              // Join to take SEQ_NOTIFICATION_TYPE_ID for event
              "INNER JOIN SMPR_TEVENT_NOTIFICATION_MAP notifMap \n" +
              "     ON (notifMap.SEQ_EVENT_TYPE_ID = event.SEQ_EVENT_TYPE_ID) \n" +
              // isNotificationTypeEnabledForCountry: Join to know if notification is enable by country
              // isEventFilteredByDelay: Multisite event should not delay.
              // multisiteFilter: user.isMultisite() = TRUE is enough
              "-- notifDelay join \n" +
              "INNER JOIN SMPR_TNOTIFICATION_DELAY notifDelay \n" +
              "     ON (notifDelay.SEQ_NOTIFICATION_TYPE_ID = notifMap.SEQ_NOTIFICATION_TYPE_ID ) \n" +

              "-- eventText join \n" +
              "INNER JOIN SMPR_TEVENT_TEXT eventText \n" +
              "     ON  (event.SEQ_EVENT_TYPE_ID = eventText.SEQ_EVENT_TYPE_ID) \n" +

              "-- installation join \n" +
              "LEFT OUTER JOIN SMPR_TINSTALLATION insta \n" +
              "     ON (event.SEQ_INSTALLATION_ID = insta.SEQ_ID) \n" +

              "-- thingPanel join \n" +
              "LEFT OUTER JOIN SMPR_TTHING thingPanel \n" +
              "     ON (thingPanel.SEQ_ID = event.SEQ_PARENT_THING_ID ) \n" +

              "-- thingDevice join \n" +
              "LEFT OUTER JOIN SMPR_TTHING thingDevice \n" +
              "     ON (thingDevice.SEQ_ID = event.SEQ_THING_ID \n" +
              "         AND thingDevice.SEQ_ENTITY_TYPE <> 18 ) \n" +

              "-- image join \n" +
              "LEFT OUTER JOIN SMPR_TIMAGE image \n" +
              "     ON (image.SEQ_EVENT_ID = event.SEQ_ID) \n" +

              "-- video join \n" +
              "LEFT OUTER JOIN SMPR_TVIDEO video \n" +
              "     ON (video.SEQ_EVENT_ID = event.SEQ_ID) \n" +

              "-- thingVideoDevice join \n" +
              "LEFT OUTER JOIN SMPR_TTHING thingVideoDevice \n" +
              "     ON (thingVideoDevice.SEQ_ID = video.SEQ_DEVICE_ID) \n" +

              "-- user join \n" +
              "LEFT OUTER JOIN SMPR_TUSER usr \n" +
              "     ON (event.SEQ_USER_ID IS NOT NULL \n" +
              "         AND usr.SEQ_ID = event.SEQ_USER_ID) \n" +

              "-- alias join \n" +
              "LEFT OUTER JOIN SMPR_TALIAS alias \n" +
              "     ON (event.SEQ_USER_ID IS NOT NULL \n" +
              "         AND alias.SEQ_INSTALLATION_ID = event.SEQ_INSTALLATION_ID \n" +
              " AND alias.DES_USER = \n" +
              "         (SELECT des_nick_name FROM SMPR_TUSER WHERE SEQ_ID = event.SEQ_USER_ID)) \n" +

              "-- panelUserAlias join \n" +
              "LEFT OUTER JOIN SMPR_TALIAS panelUserAlias \n" +
              "     ON (event.DES_PANEL_USER IS NOT NULL \n" +
              "         AND panelUserAlias.SEQ_INSTALLATION_ID = event.SEQ_INSTALLATION_ID) \n" +

              "-- partitionUserAlias join \n" +
              "LEFT OUTER JOIN SMPR_TTHING_ALIAS partitionUserAlias \n" +
              "     ON (partitionUserAlias.SEQ_THING_ID = event.SEQ_THING_ID " +
              "         AND partitionUserAlias.SEQ_USER_ID = ? )\n" +

              "-- partitionEventUserAlias join \n" +
              "LEFT OUTER JOIN SMPR_TTHING_ALIAS partitionEventUserAlias \n" +
              "     ON (event.SEQ_USER_ID IS NOT NULL \n" +
              "         AND partitionEventUserAlias.SEQ_ID = event.SEQ_THING_ID \n" +
              "         AND partitionEventUserAlias.SEQ_USER_ID = event.SEQ_USER_ID) \n" +

              "-- partitionAlias join \n" +
              "LEFT OUTER JOIN SMPR_TTHING partitionAlias \n" +
              "     ON (partitionAlias.SEQ_ID = event.SEQ_THING_ID \n" +
              "         AND partitionAlias.SEQ_ENTITY_TYPE = 18) \n\n" +

              "WHERE event.SEQ_EVENT_TYPE_ID IS NOT NULL \n\n" +

              "-- eventText condition \n" +
              "AND eventText.COD_FRONT_CODE IS NOT NULL \n" +

              "-- notifDelay condition \n" +
              "AND notifDelay.BOL_ENABLED = 1 \n" +
              "AND notifDelay.DES_COUNTRY_ID = '?' \n" +

              "-- image condition \n" +
              "AND (image.SEQ_ID IS NULL OR \n" +
              "     ( image.SEQ_ID IS NOT NULL \n" +
              "         AND image.bol_acuda_image = 0)) \n" +

              "-- thingPanel condition \n" +
              "AND (thingPanel.SEQ_ID IS NULL OR ( \n" +
              "      thingPanel.SEQ_ID IS NOT NULL \n" +
              "      AND thingPanel.SEQ_ENTITY_TYPE = 13 \n" +
              "      AND thingPanel.BOL_IS_ACTIVE = 1)) \n" +

              "-- thingDevice condition \n" +
              "AND (thingDevice.SEQ_ID IS NULL OR ( \n" +
              "      thingDevice.SEQ_ID IS NOT NULL \n" +
              "      AND thingDevice.BOL_IS_ACTIVE = 1)) \n" +

              "-- thingVideoDevice condition \n" +
              "AND (thingVideoDevice.SEQ_ID IS NULL OR \n" +
              "     (thingVideoDevice.SEQ_ID IS NOT NULL \n" +
              "     AND thingVideoDevice.BOL_IS_ACTIVE = 1)) \n" +

              "-- panelUserAlias condition \n" +
              "AND (panelUserAlias.SEQ_ID IS NULL OR \n" +
              "     (panelUserAlias.SEQ_ID IS NOT NULL \n" +
              "         AND panelUserAlias.DES_USER = event.DES_PANEL_USER)) \n" +

              "-- partitionUserAlias condition \n" +
              "AND (partitionUserAlias.SEQ_ID IS NULL OR \n" +
              "     (partitionUserAlias.SEQ_ID IS NOT NULL \n" +
              "         AND partitionUserAlias.BOL_IS_ACTIVE = 1 )) \n" +

              "-- partitionEventUserAlias condition \n" +
              "AND (partitionEventUserAlias.SEQ_ID IS NULL OR \n" +
              "     (partitionEventUserAlias.SEQ_ID IS NOT NULL \n" +
              "     AND partitionEventUserAlias.BOL_IS_ACTIVE = 1)) \n" +

              "-- partitionAlias condition \n" +
              "AND (partitionAlias.SEQ_ID IS NULL OR \n" +
              "     (partitionAlias.SEQ_ID IS NOT NULL \n" +
              "     AND partitionAlias.BOL_IS_ACTIVE = 1)) \n" +

              "-- serviceIds filter \n" +
              "AND (" + serviceIdPredicate + ") \n" +
              "AND event.FYH_EVENT_DATE >= to_timestamp('?', 'YYYY-MM-DD HH24:MI:SS.FF') \n" +
              "AND event.FYH_EVENT_DATE < to_timestamp('?', 'YYYY-MM-DD HH24:MI:SS.FF') \n" +
              // isTheftAlarmConfirmed: Clause to filter if is theaft alarm confirmed
              "AND ((event.SEQ_EVENT_TYPE_ID = 10 AND event.BOL_MASTERMIND_EVENT = 1 ) OR \n" +
              "     (event.SEQ_EVENT_TYPE_ID <> 10)) \n\n" +

              "ORDER BY event.FYH_EVENT_DATE DESC \n\n" +
              ") \n" +
              "WHERE rownum <= ?";

// TODO pasar a DEBUG cuando termine el desarrollo
       // LOGGER.info(sql);
        //LOGGER.info("parameters: [userId: {}] - [country: {}] - [fromDate: {}] - [toDate: {}] - [limit: {}]  ",
        //      userId, country, fromDate, toDate, limit);

        //return jdbcTemplate.query(sql, new EventAdapterRowMapper(),  userId, country, fromDate, toDate, limit);

        String fromStr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(fromDate);
        String toStr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(toDate);

        Object[] parameters = new Object[]{userId, country, fromStr, toStr, limit};

        for (Object obj:parameters){
            sql = sql.replaceFirst("\\?", ""+obj);
        }

        System.out.println(sql);

        return sql;


    }


    public String findByServiceIdsAndRangeDateIndividual(String jdbcUrl, Long installationId, Collection<Long> serviceIds,
                                                         Timestamp fromDate, Timestamp toDate, String country,
                                                         Long userId, int limit, EventIndividualFiltersDto eventIndividualFiltersDto) {

        String serviceIdCommaSeparated = getServicePredicate(serviceIds);

        String sqlSelect = getSelectStmnt();

        String sqlFrom = getFromStmnt();

        String sqlWhere = getWhereStmnt(serviceIdCommaSeparated);

        String sqlDynaFilter = getDynaFilter(eventIndividualFiltersDto, serviceIdCommaSeparated);

        String sqlOrder = getOrderStmnt();

        String sqlFinal = getSqlFinal(jdbcUrl, sqlSelect, sqlFrom, sqlWhere, sqlDynaFilter, sqlOrder);

        String fromStr = new SimpleDateFormat("dd/MM/yyyy").format(fromDate);
        String toStr = new SimpleDateFormat("dd/MM/yyyy").format(toDate);

        Object[] parameters = getDinaParams(eventIndividualFiltersDto, country, userId, fromStr, toStr, limit);

        for (Object obj:parameters){
            sqlFinal = sqlFinal.replaceFirst("\\?", ""+obj);
        }

        System.out.println(sqlFinal);

        return  sqlFinal;
        ///*LOGGER.debug("[installationId: {}] - [country: {}] - [userId: {}] - [fromDate: {}] - [toDate:{}] - [limit: {}] - [eventIndividualFiltersDto: {}] ",
        //      installationId, country, userId, fromDate, toDate, limit, eventIndividualFiltersDto.toString());

       // return jdbcTemplate.query(sqlFinal, new EventAdapterRowMapper(),
        //      getDinaParams(eventIndividualFiltersDto, country, userId, fromDate, toDate, limit));

    }





    private Object[] getDinaParams(EventIndividualFiltersDto eventIndividualFiltersDto, String country,
                                   Long userId, String fromDate, String toDate, int limit) {
        Object[] parameters;

        if (eventIndividualFiltersDto.isMustFilterVideoDeviceEvents()) {
            parameters = new Object[]{country, fromDate, toDate, userId, limit};
        } else {
            parameters = new Object[]{country, fromDate, toDate, limit};
        }

        return parameters;
    }

  /*  private String getServicePredicate(Collection<Long> serviceIds) {

        String serviceIdCommaSeparated = "";
        int count = 0;
        for (Long serviceId:serviceIds) {
            if (count < MAX_ALLOWED_VALUES){
                serviceIdCommaSeparated += "" + serviceId;
                count++;
                if (count < serviceIds.size() && count < MAX_ALLOWED_VALUES) {
                    serviceIdCommaSeparated += ",";
                }
            } else {
                break;
            }
        }
       // LOGGER.debug("serviceIdCommaSeparated: {}", serviceIdCommaSeparated);
        return serviceIdCommaSeparated;
    }*/

    public String getServicePredicate(Collection<Long> serviceIds) {
        String serviceIdCommaSeparated = "";
        String serviceIdPredicate = "";
        try {

            int count = 0;
            int inx = 0;
            for (Long serviceId:serviceIds) {
                inx ++;
                if (count < MAX_ALLOWED_VALUES){
                    serviceIdCommaSeparated += "" + serviceId;
                    count++;
                    if (count < serviceIds.size() && count < MAX_ALLOWED_VALUES && inx < serviceIds.size()) {
                        serviceIdCommaSeparated += ",";
                    }
                    if (count == MAX_ALLOWED_VALUES || inx == serviceIds.size()) {
                        if ("".equals(serviceIdPredicate)) {
                            serviceIdPredicate = "event.SEQ_PARENT_THING_ID IN (" + serviceIdCommaSeparated + ")";
                        } else {
                            serviceIdPredicate += " OR event.SEQ_PARENT_THING_ID IN (" + serviceIdCommaSeparated + ")";
                        }
                        serviceIdCommaSeparated = "";
                        count = 0;
                    }
                }
            }
            //LOGGER.debug("serviceIdPredicate: {}", serviceIdPredicate);
        } catch (Exception e){
            //LOGGER.ERROR("error building serviceIdPredicate: {}", e.getMessage());
        }

        return serviceIdPredicate;
    }

    private String getCommaSeparededIds(List<Integer> listId) {
        String result = "";
        int count = 0;
        for (Integer id:listId){
            result += "" + id;
            count ++;
            if (count< listId.size()){
                result += ",";
            }
        }
        return result;
    }

    private String getCommaSeparededIdsAsString(String listId) {
        return "'" + listId.replaceAll(",", "','") + "'" ;
    }


    private String getSqlFinal(String jdbcUrl, String sqlSelect, String sqlFrom, String sqlWhere, String sqlDynaFilter, String sqlOrder){
        String sqlFinal;
        // Oracle
        if (jdbcUrl.contains("jdbc:oracle")) {
            sqlFinal = "SELECT * FROM ( " + sqlSelect + sqlFrom + sqlWhere + sqlDynaFilter +  sqlOrder +") WHERE rownum <= ?";
            // Postgres
        } else {
            sqlFinal = sqlSelect + sqlFrom + sqlWhere + sqlDynaFilter +  sqlOrder +") LIMIT ?";
        }
        return sqlFinal;
    }


    private String getSelectStmnt(){
        String sqlSelect = "SELECT DISTINCT event.* \n";

        return sqlSelect;
    }

    private String getFromStmnt(){
        String sqlFrom = "FROM SMPR_TEVENT event \n" +
              //"-- Join with SMPR_TEVENT_NOTIFICATION_MAP to have SEQ_NOTIFICATION_TYPE_ID for event \n" +
              "INNER JOIN SMPR_TEVENT_NOTIFICATION_MAP notifMap ON (notifMap.SEQ_EVENT_TYPE_ID = event.SEQ_EVENT_TYPE_ID) \n" +
              //"-- isNotificationTypeEnabledForCountry: Join to know if notification is enable by country \n" +
              "INNER JOIN SMPR_TNOTIFICATION_DELAY notifDelay \n" +
              "     ON (notifDelay.SEQ_NOTIFICATION_TYPE_ID = notifMap.SEQ_NOTIFICATION_TYPE_ID )\n" +
              "LEFT OUTER JOIN SMPR_TIMAGE image \n" +
              "     ON (image.SEQ_EVENT_ID = event.SEQ_ID AND image.bol_acuda_image = 0) \n";

        return sqlFrom;
    }

    private String getWhereStmnt(String serviceIdCommaSeparated){
        String sqlWhere = "WHERE event.SEQ_EVENT_TYPE_ID IS NOT NULL \n" +
              "AND notifDelay.BOL_ENABLED = 1 " +
              "AND notifDelay.DES_COUNTRY_ID = '?' \n" +
              "AND event.SEQ_PARENT_THING_ID IN (" + serviceIdCommaSeparated + ") \n" +
              "AND event.FYH_EVENT_DATE >=  to_date('?','DD/MM/YYYY') \n" +
              "AND event.FYH_EVENT_DATE <=  to_date('?','DD/MM/YYYY') \n" +
              "-- isEventFilteredByDelay: if eventTime + delay is minor than now, event must be filtered \n" +
              "AND (notifDelay.NUM_DELAY = 0 OR \n" +
              "     (notifDelay.NUM_DELAY > 0 \n" +
              "             AND event.FYH_EVENT_DATE + notifDelay.NUM_DELAY * INTERVAL '1' SECOND < Sysdate)) \n";

        return sqlWhere;
    }

    private String getVideoCMByUserFilter(String serviceIdCommaSeparated) {
        String sqlVideoCMByUserFilter = "";

        sqlVideoCMByUserFilter =
              "-- getVideoCMByUserFilter: filter videoEvents (CM) by current user \n" +
              "((event.SEQ_EVENT_TYPE_ID IN (" +
                    getCommaSeparededIds(eventTypeUtils.getVideoDeviceEventTypeIds())+") \n" +
                    "AND event.SEQ_USER_ID = ? ) \n" +
                    "OR event.SEQ_EVENT_TYPE_ID NOT IN (" +
                    getCommaSeparededIds(eventTypeUtils.getVideoDeviceEventTypeIds())
                    +")) \n";

        return sqlVideoCMByUserFilter;
    }

    private String getPanelFilter(){
        String panelFilter = "";
        panelFilter =
              "-- isTheftAlarmConfirmed: Clause to filter if is theft alarm confirmed \n" +
              " ((event.SEQ_EVENT_TYPE_ID = 10 AND event.BOL_MASTERMIND_EVENT = 1 ) " +
                    "      OR (event.SEQ_EVENT_TYPE_ID <> 10)) \n";

        return panelFilter;
    }

    private String getDetectorFilter(EventIndividualFiltersDto eventIndividualFiltersDto) {
        String detectorFilter =
              "-- getDetectorFilter: Clause to filter selected detectors \n" +
              " (\n" +
                    "    (event.SEQ_EVENT_TYPE_ID = " + EventConstants.APPLY_FOR_IMAGE_ID  +
                    "      AND image.SEQ_DEVICE_ID IN (" + eventIndividualFiltersDto.getDetectorDeviceIdsString() + ")) \n" +
                    " OR \n" +
                    "    (event.SEQ_EVENT_TYPE_ID IN (" + getCommaSeparededIds(EventConstants.APPLY_FOR_IMAGE_ERROR_IDS) + ") " +
                    "      AND event.DES_DEVICE_NAME IN (" +
                    getCommaSeparededIdsAsString(eventIndividualFiltersDto.getDetectorDeviceIdsString()) + ")) \n";

        String showPanelEvents = "";

        if (eventIndividualFiltersDto.isShowPanelEvents()){
            showPanelEvents =
                  "-- getDetectorFilter: showPanelEvents=TRUE make detectors filter optional \n" +
                  " OR event.SEQ_EVENT_TYPE_ID NOT IN (" + getCommaSeparededIds(EventConstants.APPLY_FOR_IMAGE_IDS)+") \n";
        }

        return detectorFilter + showPanelEvents + ")\n";
    }

    private String getVideoFilter(EventIndividualFiltersDto eventIndividualFiltersDto){
        String videoFilter = "-- getVideoFilter() \n" +
              " (\n" +
                    "-- isVideoDeviceEvent() \n" +
                    "     event.SEQ_EVENT_TYPE_ID IN ( \n" +
                    getCommaSeparededIds(eventTypeUtils.getVideoDeviceEventTypeIds())
                    +  ") \n" +
                    "-- (videoDeviceIds.isEmpty() || videoDeviceIds.contains(thingId)) \n" +
                    "  AND event.SEQ_THING_ID IN (" + eventIndividualFiltersDto.getVideoDeviceIdsString() + ") \n";

        String showPanelEvents = "";

        if (eventIndividualFiltersDto.isShowPanelEvents()){
            showPanelEvents =
                  "-- getVideoFilter: showPanelEvents=TRUE make video filter optional \n" +
                  " OR event.SEQ_EVENT_TYPE_ID NOT IN ( \n" +
                        getCommaSeparededIds(eventTypeUtils.getVideoDeviceEventTypeIds())
                        + ") \n";
        }

        return videoFilter + showPanelEvents + ")\n";

    }

    private String getExcludeDetectorFilter() {
        String excludeDetectorFilter =
              "-- getExcludeDetectorFilter: showPanelEvents=TRUE and showDetectorsEvents=FALSE \n" +
              "    event.SEQ_EVENT_TYPE_ID NOT IN (" +
                    getCommaSeparededIds(EventConstants.APPLY_FOR_IMAGE_IDS)  + ") \n";

        return excludeDetectorFilter;
    }

    private String getExcludeVideoFilter(){
        String excludeVideoFilter =
              "-- getExcludeVideoFilter: showPanelEvents=TRUE and showVideoDeviceEvents=FALSE \n" +
              "     event.SEQ_EVENT_TYPE_ID NOT IN ( \n" +
                    getCommaSeparededIds(eventTypeUtils.getVideoDeviceEventTypeIds())
                    + ") \n";

        return excludeVideoFilter;
    }

    private String getOrderStmnt(){
        String sqlOrder = "ORDER BY event.FYH_EVENT_DATE DESC \n";
        return sqlOrder;
    }

    private String getDynaFilter(EventIndividualFiltersDto eventIndividualFiltersDto, String serviceIdCommaSeparated) {
        String sqlDynaFilter = "";

        if (eventIndividualFiltersDto.isMustFilterVideoDeviceEvents()){
            // showPanelEvents && showDetectorsEvents && showDomoticEvents && showVideoDeviceEvents are TRUE
            // AND detectorDeviceIds.isEmpty() && domoticDeviceIds.isEmpty() && videoDeviceIds.isEmpty()
            // all selected -- just need to filter videoEvents (CM) by userId
            //events.removeIf(event -> isVideoDeviceEvent(event) && !Objects.equals(userId, event.getUserId()) );
            //sqlDynaFilter += //"-- getDynaFilter: isMustFilterVideoDeviceEvents=TRUE \n" ;
            sqlDynaFilter += " AND " + getVideoCMByUserFilter(serviceIdCommaSeparated);
            sqlDynaFilter += " AND " + getPanelFilter();
        } else {

            //sqlDynaFilter += //"-- getDynaFilter: isMustFilterVideoDeviceEvents=FALSE \n" ;
            // Show Panel events
            if (eventIndividualFiltersDto.isShowPanelEvents()){

                //sqlDynaFilter += //"-- getDynaFilter: isShowPanelEvents=TRUE \n" ;
                sqlDynaFilter += "AND " +  getPanelFilter() + " \n";;

                // Include detectors events
                if (eventIndividualFiltersDto.isShowDetectorsEvents() &&
                      StringUtils.isNotEmpty(eventIndividualFiltersDto.getDetectorDeviceIdsString())) {
                    //sqlDynaFilter += //"-- getDynaFilter: isShowDetectorsEvents=TRUE \n" ;
                    sqlDynaFilter += "AND " + getDetectorFilter(eventIndividualFiltersDto);
                    // Exclude detectors events
                } else if (!eventIndividualFiltersDto.isShowDetectorsEvents()) {
                    //sqlDynaFilter += //"-- getDynaFilter: isShowDetectorsEvents=FALSE \n" ;
                    sqlDynaFilter += "AND " +  getExcludeDetectorFilter() + " \n";
                }

                // Include video events
                if (eventIndividualFiltersDto.isShowVideoDeviceEvents() &&
                      StringUtils.isNotEmpty(eventIndividualFiltersDto.getVideoDeviceIdsString())){
                    //sqlDynaFilter += //"-- getDynaFilter: isShowVideoDeviceEvents=TRUE \n" ;
                    sqlDynaFilter += "AND " + getVideoFilter(eventIndividualFiltersDto);
                    // Exclude video eventos
                } else if (!eventIndividualFiltersDto.isShowVideoDeviceEvents()){
                    //sqlDynaFilter += //"-- getDynaFilter: isShowVideoDeviceEvents=FALSE \n" ;
                    sqlDynaFilter += "AND " +  getExcludeVideoFilter() + " \n";
                }

            } else {
                //sqlDynaFilter += //"-- getDynaFilter: isShowPanelEvents=FALSE \n" ;
                // Not show panel events
                // Include detector events
                if (eventIndividualFiltersDto.isShowDetectorsEvents() &&
                      StringUtils.isNotEmpty(eventIndividualFiltersDto.getDetectorDeviceIdsString())) {
                    //sqlDynaFilter += //"-- getDynaFilter: isShowDetectorsEvents=TRUE \n" ;
                    sqlDynaFilter += getDetectorFilter(eventIndividualFiltersDto);
                }

                // Include viedo events
                if (eventIndividualFiltersDto.isShowVideoDeviceEvents() &&
                      StringUtils.isNotEmpty(eventIndividualFiltersDto.getVideoDeviceIdsString())){
                    //sqlDynaFilter += //"-- getDynaFilter: isShowVideoDeviceEvents=TRUE \n" ;
                    sqlDynaFilter += getOrClause(sqlDynaFilter);
                    sqlDynaFilter += getVideoFilter(eventIndividualFiltersDto);
                }

                sqlDynaFilter = "AND (\n" +  sqlDynaFilter  + ") \n";
            }

        }

        return sqlDynaFilter;
    }

    private String getOrClause(String sqlDynaFilter) {
        if (!"".equals(sqlDynaFilter)){
            return " OR \n";
        } else {
            return "";
        }
    }

 }
