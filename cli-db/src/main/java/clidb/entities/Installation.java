package clidb.entities;

import java.util.Objects;

public class Installation {

    private static final int VALUE_SYNCHRONIZED_TRUE = 1;

    //(name = "SEQ_ID")
    private Long id;

    //(name = "DES_LOCATION")
    private String location;

    //(name = "DES_CONTRACT_ID")
    private String contractId;

    //(name = "DES_CONTRACT_NUMBER")
    private String contractNumber;

    //(name = "SEQ_ADMIN")
    private Long admin; //Administrator user's ID

    //(name = "DES_COUNTRY")
    private String country;

    //(name="BOL_SYNCHRONIZED")
    private Integer flagSynchronized;

    //(name = "DES_LOCATION_STREET")
    private String locationStreet;

    //(name = "DES_LOCATION_STREET_TYPE")
    private String locationStreetType;

    //(name = "DES_LOCATION_STREET_NUMBER")
    private String locationStreetNumber;

    //(name = "DES_LOCATION_CITY")
    private String locationCity;

    //(name = "DES_LOCATION_REGION")
    private String locationRegion;

    //(name = "DES_LOCATION_POSTCODE")
    private String locationPostcode;

    //(name = "DES_LATITUDE")
    private String latitude;

    //(name = "DES_LONGITUDE")
    private String longitude;

    //(name = "DES_ACCOUNT_MANAGER")
    private String accountManager;

    //(name = "DES_CUSTOMER_REFERENCE")
    private String customerReference;

    //(name = "BOL_ACTIVE")
    //@Convert(converter = BooleanToIntegerConverter.class)
    private Boolean active;

    //(name = "DES_CONTRACT_TYPE")
    private String contractType;

    //(name = "DES_CONTRACT_STATUS")
    private String contractStatus;

    //(name = "FYH_DELETED")
    private String deletedDate;

    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    //(name = "FYH_UPDATE_DATE")
    private String updateDate;

    private String clientId;
    private Long seqUserId;
    private String desNickName;
    private int countInstAlias;
    private boolean isAdmin;

    public Installation() {
        //
    }

    public Installation(String location, String contractId, Long admin, String contractNumber, String country) {
        this.location = location;
        this.contractId = contractId;
        this.admin = admin;
        this.contractNumber = contractNumber;
        this.country = country;
    }

    public Installation(String contractId, String contractNumber, Long admin, String country,
                        Integer flagSynchronized, String location, String locationStreet, String locationStreetType,
                        String locationStreetNumber, String locationCity, String locationRegion, String locationPostcode,
                        String latitude, String longitude, String accountManager, String customerReference, Integer active, String contractType) {
        this(location, contractId, admin, contractNumber, country);
        this.flagSynchronized = flagSynchronized;
        this.locationStreet = locationStreet;
        this.locationStreetType = locationStreetType;
        this.locationStreetNumber = locationStreetNumber;
        this.locationCity = locationCity;
        this.locationRegion = locationRegion;
        this.locationPostcode = locationPostcode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.accountManager = accountManager;
        this.customerReference = customerReference;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public Long getSeqUserId() {
        return seqUserId;
    }

    public void setSeqUserId(Long seqUserId) {
        this.seqUserId = seqUserId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getDesNickName() {
        return desNickName;
    }

    public void setDesNickName(String desNickName) {
        this.desNickName = desNickName;
    }

    public int getCountInstAlias() {
        return countInstAlias;
    }

    public void setCountInstAlias(int countInstAlias) {
        this.countInstAlias = countInstAlias;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Long getAdmin() {
        return admin;
    }

    public void setAdmin(Long admin) {
        this.admin = admin;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getFlagSynchronized() {
        return flagSynchronized;
    }

    public void setFlagSynchronized(Integer flagSynchronized) {
        this.flagSynchronized = flagSynchronized;
    }

    public boolean isFlagSynchronized(){
        return ( flagSynchronized != null && VALUE_SYNCHRONIZED_TRUE == flagSynchronized );
    }

    public String getLocationStreet() {
        return locationStreet;
    }

    public void setLocationStreet(String locationStreet) {
        this.locationStreet = locationStreet;
    }

    public String getLocationStreetType() {
        return locationStreetType;
    }

    public void setLocationStreetType(String locationStreetType) {
        this.locationStreetType = locationStreetType;
    }

    public String getLocationStreetNumber() {
        return locationStreetNumber;
    }

    public void setLocationStreetNumber(String locationStreetNumber) {
        this.locationStreetNumber = locationStreetNumber;
    }

    public String getLocationCity() {
        return locationCity;
    }

    public void setLocationCity(String locationCity) {
        this.locationCity = locationCity;
    }

    public String getLocationRegion() {
        return locationRegion;
    }

    public void setLocationRegion(String locationRegion) {
        this.locationRegion = locationRegion;
    }

    public String getLocationPostcode() {
        return locationPostcode;
    }

    public void setLocationPostcode(String locationPostcode) {
        this.locationPostcode = locationPostcode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(String accountManager) {
        this.accountManager = accountManager;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(String delete) {
        this.deletedDate = delete;
    }
    
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "Installation{" +
                "id=" + id +
                ", location='" + location + '\'' +
                ", contractId='" + contractId + '\'' +
                ", contractNumber='" + contractNumber + '\'' +
                ", admin=" + admin +
                ", country='" + country + '\'' +
                ", flagSynchronized=" + flagSynchronized +
                ", locationStreet='" + locationStreet + '\'' +
                ", locationStreetType='" + locationStreetType + '\'' +
                ", locationStreetNumber='" + locationStreetNumber + '\'' +
                ", locationCity='" + locationCity + '\'' +
                ", locationRegion='" + locationRegion + '\'' +
                ", locationPostcode='" + locationPostcode + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", accountManager='" + accountManager + '\'' +
                ", active='" + active + '\'' +
                ", contractType='" + contractType + '\'' +
                ", customerReference='" + customerReference + '\'' +
                ", contractStatus='" + contractStatus + '\'' +
                ", deletedDate=" + deletedDate +
                '}';
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Installation installation = (Installation) obj;
        return Objects.equals(returnNullWhenIsEmpty(this.getLocation()), returnNullWhenIsEmpty(installation.getLocation())) &&
                Objects.equals(returnNullWhenIsEmpty(this.getContractId()), returnNullWhenIsEmpty(installation.getContractId())) &&
                Objects.equals(returnNullWhenIsEmpty(this.getContractNumber()), returnNullWhenIsEmpty(installation.getContractNumber())) &&
                Objects.equals(this.getAdmin(), installation.getAdmin()) &&
                Objects.equals(returnNullWhenIsEmpty(this.getCountry()), returnNullWhenIsEmpty(installation.getCountry())) &&
                Objects.equals(this.getFlagSynchronized(), installation.getFlagSynchronized()) &&
                Objects.equals(returnNullWhenIsEmpty(this.getLocationStreet()), returnNullWhenIsEmpty(installation.getLocationStreet())) &&
                Objects.equals(returnNullWhenIsEmpty(this.getLocationStreetType()), returnNullWhenIsEmpty(installation.getLocationStreetType())) &&
                Objects.equals(returnNullWhenIsEmpty(this.getLocationStreetNumber()), returnNullWhenIsEmpty(installation.getLocationStreetNumber())) &&
                Objects.equals(returnNullWhenIsEmpty(this.getLocationCity()), returnNullWhenIsEmpty(installation.getLocationCity())) &&
                Objects.equals(returnNullWhenIsEmpty(this.getLocationRegion()), returnNullWhenIsEmpty(installation.getLocationRegion())) &&
                Objects.equals(returnNullWhenIsEmpty(this.getLocationPostcode()), returnNullWhenIsEmpty(installation.getLocationPostcode())) &&
                Objects.equals(returnNullWhenIsEmpty(this.getLatitude()), returnNullWhenIsEmpty(installation.getLatitude())) &&
                Objects.equals(returnNullWhenIsEmpty(this.getLongitude()), returnNullWhenIsEmpty(installation.getLongitude())) &&
                Objects.equals(returnNullWhenIsEmpty(this.getAccountManager()), returnNullWhenIsEmpty(installation.getAccountManager())) &&
                Objects.equals(returnNullWhenIsEmpty(this.getCustomerReference()), returnNullWhenIsEmpty(installation.getCustomerReference())) &&
                Objects.equals(this.getActive(), installation.getActive()) &&
                Objects.equals(returnNullWhenIsEmpty(this.getContractType()), returnNullWhenIsEmpty(installation.getContractType()));
    }

    private static String returnNullWhenIsEmpty(String input) {
        if (input == null || input.isEmpty()) {
            return null;
        }
        return input;
    }
}
