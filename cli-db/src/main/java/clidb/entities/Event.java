package clidb.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

//@Entity
//@Table(name = "SMPR_TEVENT")
public class Event implements Serializable, Cloneable {

   //@Id
   //@Column(name = "SEQ_ID")
   private String id;

   //@Column(name = "SEQ_PARENT_THING_ID")
   private Long parentThingId;

   //@Column(name = "SEQ_THING_ID")
   private Long thingId;

   //@Column(name = "SEQ_INSTALLATION_ID")
   private Long installationId;

   //@Column(name = "SEQ_EVENT_TYPE_ID")
   private Integer eventTypeId;

   //@Column(name = "DES_TYPE")
   private String type;

   //@Column(name = "DES_MESSAGE")
   private String message;

   //@Column(name = "FYH_CREATION_DATE")
   private Timestamp creationDate;

   //@Column(name = "FYH_EVENT_DATE")
   private Timestamp eventDate;

   //@Column(name = "SEQ_USER_ID")
   private Long userId;

   //@Column(name = "SEQ_LOG_ID")
   private Integer logId;

   //@Column(name = "BOL_SMART_EVENT")
   //@Convert(converter = BooleanToIntegerConverter.class)
   private Boolean smartEvent;

   //@Column(name = "BOL_MASTERMIND_EVENT")
   //@Convert(converter = BooleanToIntegerConverter.class)
   private Boolean mastermindEvent;

   //@Column(name = "BOL_EXTERNAL_PROVIDER_EVENT")
   //@Convert(converter = BooleanToIntegerConverter.class)
   private Boolean externalProviderEvent;

   //@Column(name = "BOL_DOMOTIC_EVENT")
   //@Convert(converter = BooleanToIntegerConverter.class)
   private Boolean domoticEvent;

   //@Column(name = "DES_DEVICE_NAME")
   private String deviceName;

   //@Column(name = "DES_PANEL_USER")
   private String userName;

   //@Column(name = "DES_USER")
   private String userKey;

   //@Column(name = "DES_USER_ALIAS")
   private String userAlias;

   //@Column(name = "DES_CONTRACT")
   private String contract;

   //@Column(name = "DES_PRIORITY")
   private Integer priority;

   //@Column(name = "DES_COLOR")
   private String color;

   //@Column(name = "DES_EVENT_ID")
   private String eventId;

   //@Column(name = "DES_LOGIN_AS_USER")
   private String loginAsUser;

   //@Column(name = "SEQ_SCENE_ID")
   private Long sceneId;

   //@Column(name = "DES_SCENE_NAME")
   private String sceneName;

   //@Column(name = "DES_SITE_NAME")
   private String siteName;

   //@Column(name = "DES_ZONE_ID")
   private String zoneId;

   //@Column(name = "DES_AREA")
   private String area;

   //@Column(name = "COD_ACUDA_CASE")
   private Long acudaCase;

   //@Column(name = "DES_COMMENTS")
   private String comments;

   //@Column(name = "DES_DM_VARIABLE_NAME")
   private String variableName;

   //@Column(name = "DES_DM_VARIABLE_VALUE")
   private String variableValue;

   //@Column(name = "DES_PARTITION_NAME")
   private String partitionName;

   //@Column(name = "BOL_IS_FULL_PARTITION")
   //@Convert(converter = BooleanToIntegerConverter.class)
   private Boolean isFullPartition;

   //@Column(name = "DES_JOB_ID")
   private String acudaJobId;

   //@Column(name = "BOL_INCLUDE_VIDEO")
   //@Convert(converter = BooleanToIntegerConverter.class)
   private Boolean includeVideo;

   //@Column(name = "DES_COUNTRY")
   private String country;

   //@Column(name = "DES_PARTITION_ID")
   private String partitionId;

   //@Transient
   private Long doorbellRingingExpiration;

   //Constructors
   public Event() {
      this.includeVideo = Boolean.FALSE;
   }

   public Event(String id, Integer eventTypeId, Long installationId, Long parentThingId, Long thingId, Long userId, String message,
                Timestamp eventDate, String eventId, String loginAsUser, Long sceneId, String sceneName) {
      this(id, eventTypeId, installationId, parentThingId,thingId, userId, message, eventDate, null, null, null,
            false, false, null, eventId, loginAsUser,
      sceneId, sceneName, null, null, null, false, null);
   }

   public Event(String id, Integer eventTypeId, Long installationId, Long parentThingId, Long thingId, Long userId, String message,
                Timestamp eventDate, String contract, Integer priority, String color, Boolean mastermindEvent,
                Boolean isDomoticEvent, String deviceName, String eventId, String loginAsUser, Long sceneId,
                String sceneName, String siteName, String zoneId, String area, Boolean includeVideo, Boolean isFullPartition) {

      this();
      this.id = id;
      this.creationDate = new Timestamp(new Date().getTime());
      this.eventTypeId = eventTypeId;
      this.installationId = installationId;
      this.parentThingId = parentThingId;
      this.thingId = thingId;
      this.userId = userId;
      this.message = message;
      this.eventDate = eventDate;
      this.contract = contract;
      this.priority = priority;
      this.color = color;
      this.mastermindEvent = mastermindEvent;
      this.domoticEvent = isDomoticEvent;
      this.deviceName = deviceName;
      this.eventId = eventId;
      this.loginAsUser = loginAsUser;
      this.sceneId = sceneId;
      this.sceneName = sceneName;
      this.siteName = siteName;
      this.zoneId = zoneId;
      this.area = area;
      this.includeVideo = includeVideo;
      this.isFullPartition = isFullPartition;
   }

   public Event(String id, Integer eventTypeId, String userName,
                Long installationId, Long parentThingId, Long thingId,
                String message, Timestamp eventDate, String contract,
                Integer priority, String color, Boolean mastermindEvent,
                String deviceName, String eventId, String siteName,
                String zoneId, String area, Boolean isFullPartition) {

      this();
      this.id = id;
      this.creationDate = new Timestamp(new Date().getTime());
      this.eventTypeId = eventTypeId;
      this.userName = userName;
      this.installationId = installationId;
      this.parentThingId = parentThingId;
      this.thingId = thingId;
      this.deviceName = deviceName;
      this.message = message;
      this.eventDate = eventDate;
      this.contract = contract;
      this.priority = priority;
      this.color = color;
      this.mastermindEvent = mastermindEvent;
      this.eventId = eventId;
      this.siteName = siteName;
      this.zoneId = zoneId;
      this.area = area;
      this.isFullPartition = isFullPartition;

   }
   // Getters & Setters.

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public Long getParentThingId() {
      return parentThingId;
   }

   public void setParentThingId(Long parentThingId) {
      this.parentThingId = parentThingId;
   }

   public Long getThingId() {
      return thingId;
   }

   public void setThingId(Long thingId) {
      this.thingId = thingId;
   }

   public Long getInstallationId() {
      return installationId;
   }

   public void setInstallationId(Long installationId) {
      this.installationId = installationId;
   }

   public Integer getEventTypeId() {
      return eventTypeId;
   }

   public void setEventTypeId(Integer eventTypeId) {
      this.eventTypeId = eventTypeId;
   }

   public String getType() {
      return type;
   }

   public void setType(String type) {
      this.type = type;
   }

   public String getMessage() {
      return message;
   }

   public void setMessage(String message) { this.message = message; }

   public Timestamp getCreationDate() {
      return new Timestamp(creationDate.getTime());
   }

   public void setCreationDate(Timestamp creationDate) {
      this.creationDate = creationDate;
   }

   public Long getUserId() {
      return userId;
   }

   public void setUserId(Long userId) {
      this.userId = userId;
   }

   public Timestamp getEventDate() {
      return new Timestamp(eventDate.getTime());
   }

   public void setEventDate(Timestamp eventDate) {
      this.eventDate = eventDate;
   }

   public Integer getLogId() {
      return logId;
   }

   public void setLogId(Integer logId) {
      this.logId = logId;
   }

   public Boolean getSmartEvent() { return smartEvent; }

   public void setSmartEvent(Boolean smartEvent) { this.smartEvent = smartEvent; }

   public Boolean getMastermindEvent() { return mastermindEvent; }

   public void setMastermindEvent(Boolean mastermindEvent) { this.mastermindEvent = mastermindEvent; }

   public Boolean getExternalProviderEvent() { return externalProviderEvent; }

   public void setExternalProviderEvent(Boolean externalProviderEvent) {
      this.externalProviderEvent = externalProviderEvent;
   }

   public Boolean getDomoticEvent() { return domoticEvent; }

   public void setDomoticEvent(Boolean domoticEvent) { this.domoticEvent = domoticEvent; }

   public String getDeviceName() { return deviceName; }

   public void setDeviceName(String deviceName) {
      this.deviceName = deviceName;
   }

   public String getUserName() {
      return userName;
   }

   public void setUserName(String userName) {
      this.userName = userName;
   }

   public String getUserKey() {
      return userKey;
   }

   public void setUserKey(String userKey) {
      this.userKey = userKey;
   }

   public String getUserAlias() {
      return userAlias;
   }

   public void setUserAlias(String userAlias) { this.userAlias = userAlias; }

   public String getContract() {
      return contract;
   }

   public void setContract(String contract) {
      this.contract = contract;
   }

   public Integer getPriority() { return priority; }

   public void setPriority(Integer priority) {
      this.priority = priority;
   }

   public String getColor() {
      return color;
   }

   public void setColor(String color) {
      this.color = color;
   }

   public String getEventId() {
      return eventId;
   }

   public void setEventId(String eventId) {
      this.eventId = eventId;
   }

   public String getLoginAsUser() {
      return loginAsUser;
   }

   public void setLoginAsUser(String loginAsUser) { this.loginAsUser = loginAsUser; }

   public String getCountry() {
      return country;
   }

   public void setCountry(String country) {
      this.country = country;
   }

   public Long getSceneId() {
      return sceneId;
   }

   public void setSceneId(Long sceneId) {
      this.sceneId = sceneId;
   }

   public String getSceneName() {
      return sceneName;
   }

   public void setSceneName(String sceneName) {
      this.sceneName = sceneName;
   }

   public String getSiteName() {
      return siteName;
   }

   public void setSiteName(String siteName) {
      this.siteName = siteName;
   }

   public String getZoneId() { return zoneId; }

   public void setZoneId(String zoneId) {
      this.zoneId = zoneId;
   }

   public String getArea() {
      return area;
   }

   public void setArea(String area) {
      this.area = area;
   }

   public Long getAcudaCase() {
      return acudaCase;
   }

   public void setAcudaCase(Long acudaCase) { this.acudaCase = acudaCase; }

   public String getComments() {
      return comments;
   }

   public void setComments(String comments) {
      if (!isNullOrEmpty(comments) && comments.length() > 500) {
         comments = comments.substring(0, 500);
      }
      this.comments = comments;
   }

   public String getVariableName() {
      return variableName;
   }

   public void setVariableName(String variableName) {
      this.variableName = variableName;
   }

   public String getVariableValue() {
      return variableValue;
   }

   public void setVariableValue(String variableValue) {
      this.variableValue = variableValue;
   }

   public String getPartitionName() {
      return partitionName;
   }

   public void setPartitionName(String partitionName) {
      this.partitionName = partitionName;
   }

   public String getAcudaJobId() {
      return acudaJobId;
   }

   public void setAcudaJobId(String acudaJobId) {
      this.acudaJobId = acudaJobId;
   }

   public Boolean getIncludeVideo() {
      return includeVideo;
   }

   public void setIncludeVideo(Boolean includeVideo) {
      this.includeVideo = includeVideo;
   }

   public Long getDoorbellRingingExpiration() {
      return doorbellRingingExpiration;
   }

   public void setDoorbellRingingExpiration(Long doorbellRingingExpiration) {
      this.doorbellRingingExpiration = doorbellRingingExpiration;
   }

   public Boolean getFullPartition() {
      return isFullPartition;
   }

   public void setFullPartition(Boolean fullPartition) {
      isFullPartition = fullPartition;
   }

   public String getPartitionId() {
      return partitionId;
   }

   public void setPartitionId(String partitionId) {
      this.partitionId = partitionId;
   }

   @Override
   public String toString() {
      final StringBuffer sb = new StringBuffer("Event{");
      sb.append("id='").append(id).append('\'');
      sb.append(", parentThingId=").append(parentThingId);
      sb.append(", thingId=").append(thingId);
      sb.append(", installationId=").append(installationId);
      sb.append(", eventTypeId=").append(eventTypeId);
      sb.append(", type='").append(type).append('\'');
      sb.append(", message='").append(message).append('\'');
      sb.append(", creationDate=").append(creationDate);
      sb.append(", eventDate=").append(eventDate);
      sb.append(", userId=").append(userId);
      sb.append(", logId=").append(logId);
      sb.append(", smartEvent=").append(smartEvent);
      sb.append(", mastermindEvent=").append(mastermindEvent);
      sb.append(", externalProviderEvent=").append(externalProviderEvent);
      sb.append(", domoticEvent=").append(domoticEvent);
      sb.append(", deviceName='").append(deviceName).append('\'');
      sb.append(", userName='").append(userName).append('\'');
      sb.append(", userKey='").append(userKey).append('\'');
      sb.append(", userAlias='").append(userAlias).append('\'');
      sb.append(", contract='").append(contract).append('\'');
      sb.append(", priority=").append(priority);
      sb.append(", color='").append(color).append('\'');
      sb.append(", eventId='").append(eventId).append('\'');
      sb.append(", loginAsUser='").append(loginAsUser).append('\'');
      sb.append(", sceneId=").append(sceneId);
      sb.append(", sceneName='").append(sceneName).append('\'');
      sb.append(", siteName='").append(siteName).append('\'');
      sb.append(", zoneId='").append(zoneId).append('\'');
      sb.append(", area='").append(area).append('\'');
      sb.append(", acudaCase=").append(acudaCase);
      sb.append(", comments='").append(comments).append('\'');
      sb.append(", variableName='").append(variableName).append('\'');
      sb.append(", variableValue='").append(variableValue).append('\'');
      sb.append(", partitionName='").append(partitionName).append('\'');
      sb.append(", isFullPartition=").append(isFullPartition);
      sb.append(", acudaJobId='").append(acudaJobId).append('\'');
      sb.append(", includeVideo=").append(includeVideo);
      sb.append(", country='").append(country).append('\'');
      sb.append(", partitionId='").append(partitionId).append('\'');
      sb.append(", doorbellRingingExpiration=").append(doorbellRingingExpiration);
      sb.append('}');
      return sb.toString();
   }

   @Override
   public int hashCode() {
      return Objects.hash(
            color,
            installationId,
            contract,
            creationDate,
            deviceName,
            eventDate,
            eventId,
            eventTypeId,
            id,
            externalProviderEvent,
            mastermindEvent,
            smartEvent,
            domoticEvent,
            logId,
            loginAsUser,
            message,
            priority,
            sceneId,
            sceneName,
            parentThingId,
            thingId,
            siteName,
            type,
            userId,
            userName,
            userKey,
            userAlias,
            zoneId,
            area,
            acudaCase,
            comments,
            variableName,
            variableValue,
            partitionName,
            country,
            includeVideo,
            partitionId
      );
   }

  /* @Override
   public boolean equals(Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      activitystream.entities.Event other = (activitystream.entities.Event) obj;
      return Objects.equals(acudaCase, other.acudaCase) &&
      Objects.equals(color, other.color) &&
      Objects.equals(installationId, other.installationId) &&
      Objects.equals(contract, other.contract) &&
      Objects.equals(creationDate, other.creationDate) &&
      Objects.equals(deviceName, other.deviceName) &&
      Objects.equals(eventDate, other.eventDate) &&
      Objects.equals(eventId, other.eventId) &&
      Objects.equals(eventTypeId, other.eventTypeId) &&
      Objects.equals(id, other.id) &&
      Objects.equals(externalProviderEvent, other.externalProviderEvent) &&
      Objects.equals(mastermindEvent, other.mastermindEvent) &&
      Objects.equals(smartEvent, other.smartEvent) &&
      Objects.equals(domoticEvent, other.domoticEvent) &&
      Objects.equals(logId, other.logId) &&
      Objects.equals(loginAsUser, other.loginAsUser) &&
      Objects.equals(message, other.message) &&
      Objects.equals(priority, other.priority) &&
      Objects.equals(sceneId, other.sceneId) &&
      Objects.equals(sceneName, other.sceneName) &&
      Objects.equals(parentThingId, other.parentThingId) &&
      Objects.equals(thingId, other.thingId) &&
      Objects.equals(siteName, other.siteName) &&
      Objects.equals(type, other.type) &&
      Objects.equals(userId, other.userId) &&
      Objects.equals(userName, other.userName) &&
      Objects.equals(userKey, other.userKey) &&
      Objects.equals(userAlias, other.userAlias) &&
      Objects.equals(zoneId, other.zoneId) &&
      Objects.equals(area, other.area) &&
      Objects.equals(comments, other.comments) &&
      Objects.equals(variableName, other.variableName) &&
      Objects.equals(variableValue, other.variableValue) &&
      Objects.equals(partitionName, other.partitionName) &&
      Objects.equals(country, other.country) &&
      Objects.equals(includeVideo, other.includeVideo) &&
      Objects.equals(partitionId, other.partitionId);
   }*/



   private boolean isNullOrEmpty(String string) {
      return string == null || string.length() == 0;
   }

  /* @Override
   public activitystream.entities.Event clone() {
      try {
         activitystream.entities.Event clone = (activitystream.entities.Event) super.clone();
         // TODO: copy mutable state here, so the clone can't change the internals of the original
         return clone;
      } catch (CloneNotSupportedException e) {
         throw new AssertionError();
      }
   }*/
}