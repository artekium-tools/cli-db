package clidb.entities;

import java.util.Arrays;
import java.util.List;

public class EventConstants {

    // TODO Migrates to EventTypeUtils

    public final static int PARTIAL_ARM_ID = 1;
    public final static int TOTAL_ARM_ID = 2;
    public final static int DISARM_ID = 3;
    public final static int APPLY_FOR_IMAGE_ID = 4;
    public final static int APPLY_FOR_IMAGE_ERROR_ID = 5;
    public final static int APPLY_FOR_IMAGE_COMMUNICATION_ERROR_ID = 42;
    public final static int APPLY_FOR_IMAGE_BUSY_PANEL_ERROR_ID = 47;
    public final static int TOTAL_ARM_ERROR_ID = 6;
    public final static int TOTAL_ARM_COMMUNICATION_ERROR_ID = 43;
    public final static int TOTAL_ARM_BUSY_PANEL_ERROR_ID = 48;
    public final static int DISARM_ERROR_ID = 7;
    public final static int DISARM_COMMUNICATION_ERROR_ID = 45;
    public final static int DISARM_BUSY_PANEL_ERROR_ID = 50;
    public final static int PARTIAL_ARM_ERROR_ID = 8;
    public final static int PARTIAL_ARM_COMMUNICATION_ERROR_ID = 44;
    public final static int PARTIAL_ARM_BUSY_PANEL_ERROR_ID = 49;
    public final static int ALARM_IMAGE_ID = 9;
    public final static int BURGLAR_ALARM_ID = 10;
    public final static int FIRE_ALARM_ID = 12;
    public final static int DOCTOR_ALARM_ID = 13;
    public final static int CHANGE_USER_CODE_ID = 19;
    public final static int CHANGE_USER_CODE_ERROR_ID = 20;
    public final static int CHANGE_USER_CODE_COMMUNICATION_ERROR_ID = 46;
    public final static int CHANGE_USER_CODE_BUSY_PANEL_ERROR_ID = 51;

    public final static int ACUDA_OPENING_EVENT_ID = 117;
    public final static int ACUDA_ASSIGNED_EVENT_ID = 118;
    public final static int ACUDA_ON_THE_ROAD_EVENT_ID = 119;
    public final static int ACUDA_ON_INSTALLATION_SITE_EVENT_ID = 120;
    public final static int ACUDA_COMPLETED_EVENT_ID = 121;
    public final static int ACUDA_REFUSED_EVENT_ID = 122;
    public final static int ACUDA_AVAILABLE_IMAGES_EVENT_ID = 123;
    public final static int ACUDA_EVIDENCE_EVENT_ID = 124;


    // Eventos indicadores de estado de panel
    public final static int LOW_BATTERY_ALARM_ID = 14;
    public final static int LOW_BATTERY_RESTORED_ID = 21;
    public final static int POWER_FAILURE_ALARM_ID = 37;
    public final static int POWER_FAILURE_RESTORED_ID = 38;
    public final static int COMMUNICATION_LINE_FAILURE_ID = 15;
    public final static int COMMUNICATION_LINE_FAILURE_RESTORED_ID = 23;

    public static final String TYPE_ALARM = "alarm";
    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_EVENT = "event";

    public static final String SUBTYPE_INTRUDER = "intruder";

    public static final List<Integer> CHANGE_STATUS_EVENT_IDS = Arrays.asList(
            PARTIAL_ARM_ID,
            TOTAL_ARM_ID,
            DISARM_ID);

    public static final List<Integer> CHANGE_USER_CODE_IDS = Arrays.asList(
            CHANGE_USER_CODE_ID,
            CHANGE_USER_CODE_ERROR_ID,
            CHANGE_USER_CODE_COMMUNICATION_ERROR_ID,
            CHANGE_USER_CODE_BUSY_PANEL_ERROR_ID);

    public static final List<Integer> APPLY_FOR_IMAGE_IDS = Arrays.asList(
            APPLY_FOR_IMAGE_ID,
            APPLY_FOR_IMAGE_ERROR_ID,
            APPLY_FOR_IMAGE_COMMUNICATION_ERROR_ID,
            APPLY_FOR_IMAGE_BUSY_PANEL_ERROR_ID);

    public static final List<Integer> APPLY_FOR_IMAGE_ERROR_IDS = Arrays.asList(
            APPLY_FOR_IMAGE_ERROR_ID,
            APPLY_FOR_IMAGE_COMMUNICATION_ERROR_ID,
            APPLY_FOR_IMAGE_BUSY_PANEL_ERROR_ID);

    public static final List<Integer> BUSY_PANEL_IDS = Arrays.asList(
            TOTAL_ARM_BUSY_PANEL_ERROR_ID,
            PARTIAL_ARM_BUSY_PANEL_ERROR_ID,
            DISARM_BUSY_PANEL_ERROR_ID,
            APPLY_FOR_IMAGE_BUSY_PANEL_ERROR_ID,
            CHANGE_USER_CODE_BUSY_PANEL_ERROR_ID);

    public static final List<Integer> ACUDA_IDS = Arrays.asList(
          ACUDA_OPENING_EVENT_ID,
          ACUDA_ASSIGNED_EVENT_ID,
          ACUDA_ON_THE_ROAD_EVENT_ID,
          ACUDA_ON_INSTALLATION_SITE_EVENT_ID,
          ACUDA_COMPLETED_EVENT_ID,
          ACUDA_AVAILABLE_IMAGES_EVENT_ID,
          ACUDA_REFUSED_EVENT_ID,
          ACUDA_EVIDENCE_EVENT_ID);


    public static boolean isStatusType(int eventId) {
        return PARTIAL_ARM_ID == eventId || TOTAL_ARM_ID == eventId || DISARM_ID == eventId;
    }

    public static boolean isApplyForImageEvent(int eventTypeId) {
        return APPLY_FOR_IMAGE_IDS.contains(eventTypeId);
    }

    public static boolean isChangeUserCodeEvent(int eventTypeId) {
        return CHANGE_USER_CODE_IDS.contains(eventTypeId);
    }

}
