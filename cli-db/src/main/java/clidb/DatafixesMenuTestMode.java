package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.File;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class DatafixesMenuTestMode extends QryDB {



   private  final Logger LOGGER = LogManager.getLogger(DatafixesMenu.class);


   public synchronized void main(String env, boolean autoMode) {

// TODO: Agregar comentarios en jira https://developer.atlassian.com/server/jira/platform/jira-rest-api-example-add-comment-8946422/

      Scanner scannerOP = new Scanner(new InputStreamReader(System.in));

      String strDate = new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date());
      String strDate4Folder = new SimpleDateFormat("yyyyMMdd").format(new java.util.Date());

      String userToLoginFile = "usersToForceLogin-" + strDate + ".txt";
      String usersToTurnOnCamerasFile = "usersToTurnOnCameras-" + strDate + ".txt";
      String datafixReportFile = "datafixReport-" + strDate + ".html";
      String reportDate = new SimpleDateFormat("dd MMM").format(new java.util.Date());
      String datafixFolderName = "./datafixes/" + strDate4Folder;
      String datafixExecutionName = "datafixExecution-" + strDate4Folder + ".log";

      loadExternalFileProperties();

      if (autoMode)
         runCommandLine("rm ./*.sql");

      DataSource dataSource = null;
      String dbUsername = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);
         System.out.println("connectionType: " + connectionType);

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         appendToFile(datafixReportFile, "<html><br>\n");
         appendToFile(datafixReportFile, "<h1>Datafixes report for: " + reportDate + "</h1> <br>\n");
         appendToFile(datafixReportFile, "<u>Current configuration:</u><br>\n");
         appendToFile(datafixReportFile, "datafixes-1=" + getProperty("datafixes-" + 1) +  "<br>\n");
         appendToFile(datafixReportFile, "datafixes-2=" + getProperty("datafixes-" + 2) +  "<br>\n");
         appendToFile(datafixReportFile, "datafixes-3=" + getProperty("datafixes-" + 3) +  "<br>\n");
         appendToFile(datafixReportFile, "datafixes-4=" + getProperty("datafixes-" + 4) +  "<br>\n");
         appendToFile(datafixReportFile, "datafixes-5=" + getProperty("datafixes-" + 5) +  "<br><br>\n");

         int autoIndex = 0;
         int datafixOpValue;
         int lastDatafix=0;
         int opInt;
         int waitTimeInSeconds = Integer.valueOf(getProperty("waitTimeInSecondsBetweenDatafixes"));
         String arrDatafixes[] = null;

         if (autoMode){
            // Levanto el los datafixess a ejecutar para el dia ej: 1,2,3,4,5,7,8,9,10,11
            String cronExpr =  getProperty("datafixes-" + getDayOfWeek());
            arrDatafixes = cronExpr.split(",");
            lastDatafix = arrDatafixes.length;
         }

         while (true) {

            // automatic mode
            if (autoMode) {
               if (autoIndex == lastDatafix) {
                  datafixOpValue =0;
               } else {
                  datafixOpValue = Integer.valueOf(arrDatafixes[autoIndex]);
                  //System.out.println("datafixOpValue: " + datafixOpValue + " autoIndex: " + autoIndex + " lastDatafix: " + lastDatafix);
               }

               autoIndex++;
               opInt = datafixOpValue;

               if (autoIndex > 1 ){
                  System.out.println("Waiting for " + waitTimeInSeconds + " seconds." );
                  wait(waitTimeInSeconds * 1000);
               }

               // Manual mode
            } else {
               System.out.println( " ");
               System.out.println( "Current configuration:");
               System.out.println( "----------------------");
               System.out.println( "datafixes-1=" + getProperty("datafixes-" + 1) );
               System.out.println( "datafixes-2=" + getProperty("datafixes-" + 2) );
               System.out.println( "datafixes-3=" + getProperty("datafixes-" + 3) );
               System.out.println( "datafixes-4=" + getProperty("datafixes-" + 4) );
               System.out.println( "datafixes-5=" + getProperty("datafixes-" + 5) );
               System.out.println( " ");
               System.out.println("1) checkRepetedInstalations AM-38370");
               System.out.println("2) checkRepetedInstalations AM-40608");
               System.out.println("3) checkRepetedInstalationsAlias AM-40349");
               System.out.println("4) checkInstallationsMultiOwner AM-38618");
               System.out.println("5) fixCameraManagerSessionClientId  AM-39999");
               System.out.println("6) checkDuplicatedPanels AM-41075 - <DEPRECADO>");
               System.out.println("7) checkPrivacyModeStatus AM-38079");
               System.out.println("8) checkOrphanInstallations AM-40939");
               System.out.println("9) checkRepeatedCMCredentials AM-43282");
               System.out.println("10) checkDuplicatedServices AM-43569");
               System.out.println("11) CheckOrphanRecords AM-38250");
               System.out.println("12) DeleteSmartisProcessStateObsolete AM-39977");
               System.out.println("13) DeactivateRuleEngineInconsistentScene AM-40876");
               System.out.println("14) checkDuplicatedServices AM-40165");
               System.out.println("15) synchronizeSceneBetweenSceneAndRseuleEngine AM-41474");
               System.out.println("16) CheckReplicaVsOwner AM-40939");

               System.out.println("0) End. (Close report and send it by e-mail)");
               System.out.println("op: ");
               String op = scannerOP.nextLine();

               opInt = Integer.valueOf(op);

            }

            LargeScript largeScript = new LargeScript();
            RepetedInstallations repetedInstallations;

            if (opInt == 0) {
               appendToFile(datafixReportFile, "</html>\n");

               // Send report
               String body = "Datafixes report for: " + reportDate;
               sendEmail(env, "Datafixes report: " + datafixReportFile, body,
                     "./" + datafixReportFile, datafixReportFile);

               // Send user to force login
               File f = new File("./" + userToLoginFile);
               if(f.exists() ) {
                  body = "Users to force login for: " + reportDate;
                  sendEmail(env, "Users to force login: " + userToLoginFile, body,
                        "./" + userToLoginFile, userToLoginFile);
               } else {
                  body = "No users to force login for: " + reportDate;
                  sendEmail(env, "Users to force login: " + userToLoginFile, body,
                        null, null);
               }

               // Send user to turn on cameras
               File fc = new File("./" + usersToTurnOnCamerasFile);
               if(fc.exists() ) {
                  body = "Users to turn on cameras for: " + reportDate;
                  sendEmail(env, "Users to turn on cameras: " + usersToTurnOnCamerasFile, body,
                        "./" + usersToTurnOnCamerasFile, usersToTurnOnCamerasFile);
               } else {
                  body = "No users to turn on cameras for: " + reportDate;
                  sendEmail(env, "Users to turn on cameras: " + usersToTurnOnCamerasFile, body,
                        null, null);
               }

               // Send Execution log
               runCommandLine("mv datafixExecution.log " + datafixExecutionName);

               File log = new File("./" + datafixExecutionName );
               if(log.exists() ) {
                  body = "Execution log for: " + reportDate;
                  sendEmail(env, "Execution log: datafixExecution.log" , body,
                        "./" + datafixExecutionName , datafixExecutionName);
               }

               // Move files to dated folder
               System.out.println("moving files to datafixes folder.");

               runCommandLine("mkdir " + datafixFolderName);
               runCommandLine("mv " + datafixReportFile + " " + datafixFolderName);
               runCommandLine("mv " + userToLoginFile + " " + datafixFolderName);
               runCommandLine("mv " + usersToTurnOnCamerasFile + " " + datafixFolderName);
               runCommandLine("mv " + datafixExecutionName + " " + datafixFolderName);
               runCommandLine("mv cli-db.log " + datafixFolderName);
               runCommandLine("mv *.sql " + datafixFolderName);

               return;
            }

            String sqlToCheck;
            switch (opInt) {
               case 1:
                  System.out.println("1) checkRepetedInstalations AM-38370");
                  repetedInstallations = new RepetedInstallations();
                  sqlToCheck = null; //repetedInstallations.checkRepetedInstalationsCCNull(env, userToLoginFile, datafixReportFile, reportDate);

                  if (sqlToCheck != null) {
                     System.out.println("Executing step 3: execute sql to check ....");
                     //select(dataSource, sqlToCheck);

                     System.out.println("Solo se ejecuta el chekeo....");

                     System.out.println("Executing step 4: execute deleteRepetedInstallationsCCNull.sql to fix....");

                     //largeScript.runMultiUpdate(env, "deleteRepetedInstallationsCCNull.sql", false, waitTimeInSeconds);

                     System.out.println("Executing step 4: execute sql to final check.");
                     //select(dataSource, sqlToCheck);
                  } else {
                     System.out.println("Nothing to do.");
                  }

                  //checkSessions(dataSource, dbUsername);

                  break;
               case 2:
                  System.out.println("2) checkRepetedInstalations AM-40608");
                  repetedInstallations = new RepetedInstallations();
                  sqlToCheck = null;//repetedInstallations.checkRepetedInstalationsNoTree(env, userToLoginFile,datafixReportFile, reportDate);

                  if (sqlToCheck != null) {
                     System.out.println("Executing step 3: execute sql to check ....");
                     //select(dataSource, sqlToCheck);

                     System.out.println("Solo se ejecuta el chekeo....");

                     System.out.println("Executing step 4: execute deleteRepetedInstallationsNoTree.sql to fix....");

                     //largeScript.runMultiUpdate(env, "deleteRepetedInstallationsNoTree.sql", false, waitTimeInSeconds);

                     System.out.println("Executing step 4: execute sql to final check.");
                     //select(dataSource, sqlToCheck);
                  } else {
                     System.out.println("Nothing to do.");
                  }

                  //checkSessions(dataSource, dbUsername);

                  break;

               case 3:
                  System.out.println("3) checkRepetedInstalationsAlias AM-40349");
                  RepetedInstallationsAlias repetedInstallationsAlias = new RepetedInstallationsAlias();
                  sqlToCheck = null;//repetedInstallationsAlias.checkRepetedInstalationsAlias(env, userToLoginFile, datafixReportFile, reportDate);

                  if (sqlToCheck != null) {
                     System.out.println("Executing step 3: execute sql to check ....");
                     //select(dataSource, sqlToCheck);

                     System.out.println("Solo se ejecuta el chekeo....");

                     System.out.println("Executing step 4: execute deleteRepetedInstallationsAlias.sql to fix....");

                     //largeScript.runMultiUpdate(env, "deleteRepetedInstallationsAlias.sql", false, waitTimeInSeconds);

                     System.out.println("Executing step 4: execute sql to final check.");
                     //select(dataSource, sqlToCheck);
                  } else {
                     System.out.println("Nothing to do.");
                  }

                  //checkSessions(dataSource, dbUsername);

                  break;
               case 4:
                  System.out.println("4) checkInstallationsMultiOwner AM-38618");
                  CheckInstallationsMultiOwner checkInstallationsMultiOwner = new CheckInstallationsMultiOwner();
                  int total1 = 0;//checkInstallationsMultiOwner.main(env, datafixReportFile, reportDate);

                  if (total1 > 0) {
                     System.out.println("\nThis datafix has no sql to check, has no force login");
                     System.out.println("Executing step 3: execute purgeInstallationMultiOwners.sql ...");

                     //largeScript.runMultiUpdate(env, "purgeInstallationMultiOwners.sql", false, waitTimeInSeconds);
                  } else {
                     System.out.println("Nothing to do.");
                  }

                  //checkSessions(dataSource, dbUsername);

                  break;
               case 5:
                  System.out.println("5) fixCameraManagerSessionClientId  AM-39999");
                  CheckCMSession checkCMSession = new CheckCMSession();
                  int total2 = 0;//checkCMSession.fixCMSession(env, userToLoginFile, datafixReportFile, reportDate);

                  if (total2 > 0) {
                     System.out.println("\nThis datafix has no sql to check");
                     System.out.println("Executing step 2: deleteCameraManagareSessionDiffClientId.sql ..." );


                     //largeScript.runScript(env, "CAMERAMANAGER", "deleteCameraManagareSessionDiffClientId.sql", false);

                     System.out.println("Executing step 3: deleteThirdPartySessionDiffClientId.sql ..." );
                     //largeScript.runScript(env, "CAMERAMANAGER", "deleteThirdPartySessionDiffClientId.sql", false);
                  } else {
                     System.out.println("Nothing to do.");
                  }

                  //checkSessions(dataSource, dbUsername);

                  break;
               case 6:
                  // DEPRECADO
                  System.out.println("6) checkDuplicatedPanels AM-41075 - DEPRECADO");
                  CheckDuplicatedPanels checkDuplicatedPanels = new CheckDuplicatedPanels();
                  int total3 = 0;//checkDuplicatedPanels.main(env, datafixReportFile, reportDate);

                  if (total3 > 0) {
                     System.out.println("\nThis datafix has no sql to check, has no force login");
                     System.out.println("Executing step 4: execute purgeDuplicatedPanels.sql ...");

                     //largeScript.runMultiUpdate(env, "purgeDuplicatedPanels.sql", false, waitTimeInSeconds);
                  } else {
                     System.out.println("Nothing to do.");
                  }

                  //checkSessions(dataSource, dbUsername);

                  break;
               case 7:
                  System.out.println("7) checkPrivacyModeStatus AM-38079");
                  CheckPrivacyModeStatus checkPrivacyModeStatus = new CheckPrivacyModeStatus();
                  int total4 = 0;//checkPrivacyModeStatus.fixMPStatusDevices(env, datafixReportFile, reportDate, usersToTurnOnCamerasFile);

                  if (total4 > 0) {
                     System.out.println("\nThis datafix has no sql to check, has no force login");
                     System.out.println("Executing step 4: execute fixDevicesMPStatus.sql ...");

                     //largeScript.runMultiUpdate(env, "fixDevicesMPStatus.sql", false, waitTimeInSeconds);
                  } else {
                     System.out.println("Nothing to do.");
                  }

                  //checkSessions(dataSource, dbUsername);

                  break;
               case 8:
                  System.out.println("8) checkOrphanInstallations AM-40939");
                  CheckOrphanInstallations checkOrphanInstallations = new CheckOrphanInstallations();
                  int total5 = 0;//checkOrphanInstallations.fixOrphanInstallations(env, datafixReportFile, reportDate, "");

                  if (total5 > 0) {
                     System.out.println("\nThis datafix has no sql to check, has no force login");
                     System.out.println("Executing step 4: execute fixOrphanInstallations.sql ...");

                     //largeScript.runMultiUpdate(env, "fixOrphanInstallations.sql", false, waitTimeInSeconds);
                  } else {
                     System.out.println("Nothing to do.");
                  }

                  //checkSessions(dataSource, dbUsername);

                  break;
               case 9:
                  System.out.println("9) checkRepeatedCMCredentials AM-43282");
                  CheckCMRepeatedCredentials checkCMRepeatedCredentials = new CheckCMRepeatedCredentials();
                  int total6 = 0;//checkCMRepeatedCredentials.fixCMRepeatedCredentials(env, datafixReportFile, reportDate);

                  if (total6 > 0) {
                     System.out.println("\nThis datafix has no sql to check, has no force login");
                     System.out.println("Executing step 4: execute fixOrphanInstallations.sql ...");

                     //largeScript.runMultiUpdate(env, "fixCMRepeatedCredentials.sql", false, waitTimeInSeconds);
                  } else {
                     System.out.println("Nothing to do.");
                  }

                  //checkSessions(dataSource, dbUsername);

                  break;
               case 10:
                  System.out.println("10) checkDuplicatedServices AM-43569");
                  CheckDuplicatedServices checkDuplicatedServices = new CheckDuplicatedServices();
                  int total7 = 0;//checkDuplicatedServices.fixRepeatedServices(env, datafixReportFile, reportDate);

                  if (total7 > 0) {
                     System.out.println("\nThis datafix has no sql to check, has no force login");
                     System.out.println("Executing step 4: execute fixCMRepeatedCredentials.sql ...");

                     //largeScript.runMultiUpdate(env, "fixRepeatedServies.sql", false, waitTimeInSeconds);
                  } else {
                     System.out.println("Nothing to do.");
                  }

                  //checkSessions(dataSource, dbUsername);

                  break;

               case 11:
                  System.out.println("11) check Orphan Thing, ThingAlias, Installation & InstallationAlias AM-38250");
                  CheckOrphanRecords checkOrphanRecords = new CheckOrphanRecords();
                  int total11 = 0;//checkOrphanRecords.main(env, datafixReportFile, reportDate);

                  if (total11 > 0) {
                     System.out.println("\nThis datafix has no sql to check, has no force login");
                     System.out.println("\nDESCOMENTAR LAS EJECUCIONES DE MULTIUPDATE");
                     System.out.println("Executing step 6: execute deleteOrphanThingAlias.sql ...");

                     /*largeScript.runMultiUpdate(env, "deleteOrphanThingAlias.sql", false, waitTimeInSeconds);

                     System.out.println("Waiting for " + waitTimeInSeconds + " seconds." );
                     wait(waitTimeInSeconds * 1000);

                     System.out.println("Executing step 7: execute deleteOrphanThingByThingAlias.sql ...");

                     largeScript.runMultiUpdate(env, "deleteOrphanThingByThingAlias.sql", false, waitTimeInSeconds);

                     System.out.println("Waiting for " + waitTimeInSeconds + " seconds." );
                     wait(waitTimeInSeconds * 1000);

                     System.out.println("Executing step 8: execute deleteOrphanInstallationAlias.sql ...");

                     largeScript.runMultiUpdate(env, "deleteOrphanInstallationAlias.sql", false, waitTimeInSeconds);

                     System.out.println("Waiting for " + waitTimeInSeconds + " seconds." );
                     wait(waitTimeInSeconds * 1000);

                     System.out.println("Executing step 9: execute deleteOrphanThingByInstallation.sql ...");

                     largeScript.runMultiUpdate(env, "deleteOrphanThingByInstallation.sql", false, waitTimeInSeconds);

                     System.out.println("Waiting for " + waitTimeInSeconds + " seconds." );
                     wait(waitTimeInSeconds * 1000);

                     System.out.println("Executing step 10: execute deleteOrphanInstallation.sql ...");

                     largeScript.runMultiUpdate(env, "deleteOrphanInstallation.sql", false, waitTimeInSeconds);

                     System.out.println("Waiting for " + waitTimeInSeconds + " seconds." );
                     wait(waitTimeInSeconds * 1000);*/

                  } else {
                     System.out.println("Nothing to do.");
                  }

                  //checkSessions(dataSource, dbUsername);

                  break;
               case 12:
                  System.out.println("12) DeleteSmartisProcessStateObsolete AM-39977");
                  CheckSmartisPSObsolete checkSmartisPSObsolete = new CheckSmartisPSObsolete();
                  int total12 = 0;//checkSmartisPSObsolete.findProcessStateObsolete(env, datafixReportFile, reportDate);

                  if (total12 > 0) {
                     System.out.println("\nThis datafix has no sql to check");
                     System.out.println("Executing step 2: deleteSmartisProcessStateObsoleteById.sql ..." );

                     //largeScript.runScript(env, "SMARTIS", "deleteSmartisProcessStateObsoleteById.sql", false);

                  } else {
                     System.out.println("Nothing to do.");
                  }

                  //checkSessions(dataSource, dbUsername);

                  break;
               case 13:
                  System.out.println("13) DeactivateRuleEngineInconsistentScene AM-40876");
                  CheckRuleEngineScene checkRuleEngineScene = new CheckRuleEngineScene();
                  int total13 = 0; //checkRuleEngineScene.findInconsistentSceneRuleEngine(env, datafixReportFile, reportDate);

                  if (total13 > 0) {
                     System.out.println("\nThis datafix has no sql to check");
                     System.out.println("Executing step 2: deactivateInconsistentScene.sql ..." );

                     //largeScript.runScript(env, "RULE-ENGINE", "deactivateInconsistentScene.sql", false);

                  } else {
                     System.out.println("Nothing to do.");
                  }

                  //checkSessions(dataSource, dbUsername);

                  break;

               case 14:
                  System.out.println("14) checkDuplicatedServices AM-40165");
                  CheckDuplicatedAliases checkDuplicatedAliases = new CheckDuplicatedAliases();
                  int total14 = 0;//checkDuplicatedAliases.fixRepeatedAliases(env, datafixReportFile, reportDate);

                  if (total14 > 0) {
                     System.out.println("\nThis datafix has no sql to check, has no force login");
                     System.out.println("Executing step 4: execute fixRepeatedAliases.sql ...");
                     //largeScript.runMultiUpdate(env, "fixRepeatedAliases.sql", false, waitTimeInSeconds);
                  } else {
                     System.out.println("Nothing to do.");
                  }

                  //checkSessions(dataSource, dbUsername);

                  break;

               case 15:
                  System.out.println("15) synchronizeSceneBetweenSceneAndRseuleEngine AM-41474");
                  CheckUnsynchronizedScene checkUnsynchronizedScene = new CheckUnsynchronizedScene();
                  int total15 = 0;//checkUnsynchronizedScene.findUnsynchronizedScene(env,userToLoginFile,datafixReportFile,reportDate);

                  if (total15 > 0) {
                     System.out.println("\nThis datafix has no sql to check");
                     System.out.println("Executing step 2: createMissingScene.sql ..." );

                     //largeScript.runScript(env, "RULE-ENGINE", "createMissingScene.sql", false);

                  } else {
                     System.out.println("Nothing to do.");
                  }

                  //checkSessions(dataSource, dbUsername);

                  break;
               case 16:
                  executeDatafix16(env, datafixReportFile, reportDate, dataSource, dbUsername);

                  break;

            }
         }
      } catch (Exception e){
         printStackTrace(e);
      }
   }

   private void executeDatafix16(String env, String datafixReportFile, String reportDate, DataSource dataSource, String dbUsername) {
      System.out.println("16) CheckReplicaVsOwner AM-40193");
      CheckAliasesReplicaVsOwner checkReplicaVsOwner = new CheckAliasesReplicaVsOwner();
      int total16 = checkReplicaVsOwner.fixWrongThingAliases(env, datafixReportFile, reportDate, 0);

      if (total16 > 0) {
         System.out.println("\nThis datafix has no sql to check, has no force login");
         System.out.println("Executing step 4: execute fixWrongThingAliases.sql ...");
         //largeScript.runMultiUpdate(env, "fixWrongThingAliases.sql", false, waitTimeInSeconds);
      } else {
         System.out.println("Nothing to do.");
      }

      checkSessions(dataSource, dbUsername);
   }


}
