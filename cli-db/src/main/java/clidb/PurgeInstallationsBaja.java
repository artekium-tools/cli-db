package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;


public class PurgeInstallationsBaja extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(PurgeInstallationsBaja.class);

   static boolean UPDATE_IN_TEST_MODE = false; // true=olny shows logs  false=execute updates

   static Properties prop = new Properties();
   static Properties scriptProperties = new Properties();
   static ArrayList<Long> arr_seq_installation_id = new ArrayList();

   public void main(String env) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);
         System.out.println("connectionType: " + connectionType);

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         LOGGER.info("Init Process: " );
         // Instalaciones borradas que aún poseen paneles en THING
         selectInstallations(dataSource);

         for (Long installationId:arr_seq_installation_id){
               createCheckStatement(dataSource, installationId);
               generateDeleteStatements(installationId);
         }

         System.out.println("Execute multiUpdate for: ./deleteInstallationsBaja.sql");

         LOGGER.info("End Process");
      } catch (Exception e){
         printStackTrace(e);
      }

      LOGGER.info("End process.");

      return;
   }


   public static void selectInstallations(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT inst.SEQ_ID, usr.DES_NICK_NAME FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION inst " +
                     "INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr ON (usr.SEQ_ID = inst.SEQ_ADMIN) " +
                     "WHERE inst.FYH_DELETED IS NOT NULL " +
                     "AND inst.seq_admin IN ( " +
                     "      SELECT DISTINCT usr.SEQ_ID " +
                     "      FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr WHERE SEQ_ID IN ( " +
                     "            SELECT SEQ_ADMIN FROM DEVICE.SMPR_TINSTALLATION st WHERE SEQ_ID IN ( " +
                     "                  SELECT SEQ_INSTALLATION_ID FROM DEVICE.SMPR_TTHING thn " +
                     "                  WHERE thn.SEQ_ENTITY_TYPE=13 AND thn.COD_CONNECTION_ID IS NOT NULL " +
                     "                  AND thn.SEQ_ID NOT IN " +
                     "                        (SELECT SEQ_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TTHING st " +
                     "                              WHERE SEQ_ENTITY_TYPE=13 ) " +
                     "            )" +
                     "      ))";


         System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
               arr_seq_installation_id.add(rs.getLong(1));
               System.out.println(rs.getLong(1) + " - " + rs.getString(2) + " Installation Added");
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }

   public static void createCheckStatement(final DataSource dataSource, Long installationId) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql1 = "Select seq_id FROM DEVICE.SMPR_TTHING st WHERE seq_id IN ( " +
                  " SELECT SEQ_ID FROM DEVICE.SMPR_TTHING st WHERE SEQ_PARENT_ID IN (" +
                  "      SELECT SEQ_ID FROM DEVICE.SMPR_TTHING st WHERE SEQ_INSTALLATION_ID = " + installationId + ")" +
            " UNION " +
            " SELECT SEQ_ID FROM DEVICE.SMPR_TTHING st WHERE SEQ_INSTALLATION_ID=" + installationId + ")";

            String sql2 = "Select seq_id FROM DEVICE.SMPR_TTHING_ALIAS sta WHERE seq_thing_id IN (SELECT SEQ_ID FROM DEVICE.SMPR_TTHING st WHERE SEQ_PARENT_ID IN (" +
            "      SELECT SEQ_ID FROM DEVICE.SMPR_TTHING st WHERE SEQ_INSTALLATION_ID ="+ installationId + ")" +
            " UNION" +
            " SELECT SEQ_ID FROM DEVICE.SMPR_TTHING st WHERE SEQ_INSTALLATION_ID="+ installationId + ")";

            String sql3 = "Select seq_id FROM DEVICE.SMPR_TINSTALLATION st WHERE seq_id="+ installationId;

            String sql4 = "Select seq_id FROM DEVICE.SMPR_TINSTALLATION_ALIAS sta WHERE seq_installation_id="+ installationId;


            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql1);

            System.out.println("installation ID: "+ installationId +"----------------------------------------");

            //System.out.println("sql1: " + sql1);
            while (rs.next()) {
               System.out.println("SELECT * FROM DEVICE.SMPR_TTHING WHERE SEQ_ID = " + rs.getLong(1) + " # THING TO BE DELETED");
            }
            System.out.println("----------------------------------------");
            rs = stmt.executeQuery(sql2);
            //System.out.println("sql2: " + sql2);
            while (rs.next()) {
               System.out.println("SELECT * FROM DEVICE.SMPR_THING_ALIAS WHERE SEQ_ID = " +rs.getLong(1) + " # THING_ALIAS TO BE DELETED");
            }
            System.out.println("----------------------------------------");
            rs = stmt.executeQuery(sql3);
            //System.out.println("sql3: " + sql3);
            while (rs.next()) {
               System.out.println("SELECT * FROM DEVICE.SMPR_TINSTALLATION WHERE SEQ_ID = " + rs.getLong(1) + " # TINSTALLATION TO BE DELETED");
            }
            System.out.println("----------------------------------------");
            rs = stmt.executeQuery(sql4);

            //System.out.println("sql4: " + sql4);
            while (rs.next()) {
               System.out.println("SELECT * FROM DEVICE.SMPR_TINSTALLATION_ALIAS WHERE SEQ_ID = " + rs.getLong(1) + " # TINSTALLATION_ALIAS TO BE DELETED");
            }




         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }


   }


   public static void generateDeleteStatements(Long installationId){

      /*
         replicas.installation=ACCOUNT-MANAGEMENT,CAMPAIGNS,DEVICE,DIGITAL-SECURITY,EVENT-PROCESSOR,INBOX,PANIC-BUTTON,RULE-ENGINE,SYSLOG,USER-ENROLLMENT
         replicas.installationAias=ACCOUNT-MANAGEMENT,CAMPAIGNS,DEVICE,DIGITAL-SECURITY,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG,USER-ENROLLMENT
         replicas.thing=DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SCENE,SYSLOG
         replicas.thingAlias=DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG
       */
      try {

         try {

            String sqlThing = "DELETE FROM SMPR_TTHING st WHERE seq_id IN ( " +
                  " SELECT SEQ_ID FROM SMPR_TTHING st WHERE SEQ_PARENT_ID IN (" +
                  "      SELECT SEQ_ID FROM SMPR_TTHING st WHERE SEQ_INSTALLATION_ID = " + installationId + ")" +
                  " UNION " +
                  " SELECT SEQ_ID FROM SMPR_TTHING st WHERE SEQ_INSTALLATION_ID=" + installationId + ");";

            String sqlThingAlias = "DELETE FROM SMPR_TTHING_ALIAS sta WHERE seq_thing_id IN (SELECT SEQ_ID FROM SMPR_TTHING st WHERE SEQ_PARENT_ID IN (" +
                  "      SELECT SEQ_ID FROM SMPR_TTHING st WHERE SEQ_INSTALLATION_ID ="+ installationId + ")" +
                  " UNION" +
                  " SELECT SEQ_ID FROM SMPR_TTHING st WHERE SEQ_INSTALLATION_ID="+ installationId + ");";

            String sqlInstallations = "DELETE FROM SMPR_TINSTALLATION st WHERE seq_id="+ installationId + ";";

            String sqlInstAlias = "DELETE FROM SMPR_TINSTALLATION_ALIAS sta WHERE seq_installation_id="+ installationId + ";";

            // Thing Alias -------------------------------------------------------------------------
            //System.out.println("   " + sqlThingAlias);

            String schemas = "schemas:DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG\n";
            appendToFile("deleteInstallationsBaja.sql", "-- Thing Alias for Installation: " + installationId + "\n");
            appendToFile("deleteInstallationsBaja.sql", schemas);
            appendToFile("deleteInstallationsBaja.sql", sqlThingAlias+ "\n");

            // Thing -------------------------------------------------------------------------
            //System.out.println("   " + sqlThing);

            schemas = "schemas:DEVICE,ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SCENE,SYSLOG\n";
            appendToFile("deleteInstallationsBaja.sql", "-- Thing for for Installation: " + installationId + "\n");
            appendToFile("deleteInstallationsBaja.sql", schemas);
            appendToFile("deleteInstallationsBaja.sql", sqlThing + "\n");

            // Installation Alias -------------------------------------------------------------------
            //System.out.println("   " + sqlInstAlias);
            // Query that finaly will be executed

            schemas = "schemas:ACCOUNT-MANAGEMENT,CAMPAIGNS,DEVICE,DIGITAL-SECURITY,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SYSLOG,USER-ENROLLMENT\n";

            appendToFile("deleteInstallationsBaja.sql", "-- Installation Alias for Installation: " + installationId + "\n");
            appendToFile("deleteInstallationsBaja.sql", schemas);
            appendToFile("deleteInstallationsBaja.sql", sqlInstAlias + "\n");

            // Installation -------------------------------------------------------------------
            //System.out.println("   " + sqlInstallations);
            // Query that finaly will be executed

            schemas = "schemas:ACCOUNT-MANAGEMENT,CAMPAIGNS,DEVICE,DIGITAL-SECURITY,EVENT-PROCESSOR,INBOX,PANIC-BUTTON,RULE-ENGINE,SYSLOG,USER-ENROLLMENT\n";
            appendToFile("deleteInstallationsBaja.sql", "-- Installation for Installation: " + installationId + "\n");
            appendToFile("deleteInstallationsBaja.sql", schemas);
            appendToFile("deleteInstallationsBaja.sql", sqlInstallations + "\n");

         } catch (Exception e) {
            printStackTrace(e);
         }
      } catch (Exception e) {
         printStackTrace(e);
      }
   }



}
