package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Scanner;


public class ThingAliasIssue extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(ThingAliasIssue.class);

   public void runCheckThingAlias(String env) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);
         System.out.println("connectionType: " + connectionType);

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);


         String scriptFile;
         int startLine;
         boolean showSql;
         boolean onErrorExit;

         while (true) {
            Scanner scanner1 = new Scanner(new InputStreamReader(System.in));
            System.out.println(" ");
            System.out.println("Script path: ");
            scriptFile = scanner1.nextLine();

            Path path = Paths.get(scriptFile);

            if (Files.exists(path)) {
               break;
            } else {
               System.out.println("File Not Found.");
            }
         }


         BufferedReader br = new BufferedReader(new FileReader(scriptFile));

         String sql = "";
         String analisis = "";
         int line = 0;
         LOGGER.info("Init Process: " );
         while ((sql = br.readLine()) != null) {

            analisis = getResult4Analisis( dataSource,  sql,  "Result = 0", "orange");
            line ++;



            if (analisis.contains("0 rows.")){
               LOGGER.info("result found: " + analisis);
               appendToFile("checkThingAlias.html", analisis);
            } else {
               LOGGER.info("line : " + line + " OK.");
            }
            //System.out.println(analisis);
            //System.out.println("-------------------");
         }

         LOGGER.info("End Process");
      } catch (Exception e){
         printStackTrace(e);
      }



      LOGGER.info("End process.");

      return;
   }

   public void runCheckThingAliasHuerfana(String env) {

      loadExternalFileProperties();

      DataSource dataSource = null;
      LargeScript multiUpdate = new LargeScript();
      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);
         System.out.println("connectionType: " + connectionType);

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         System.out.println("start process ...");
         System.out.println("executing query ...");

         HashMap<Long, String> userMap = getResult1Users(dataSource);

         //HashMap<Long, String> userMap = new HashMap<Long, String>();
         //userMap.put(1237058L, "andira2812@gmail.com");

         String content = "";
         String userData = "";
         String sql4analisis = "";

         int count = 0;
         for (Long userId: userMap.keySet()) {
            count ++;
            System.out.println(count + " ) --------------------------------------------");
            userData = "index: " + count +  " UserId : " + userId + " desNickName: " + userMap.get(userId);
            System.out.println(userData);
            // Levanto el thingAlias huerfano
            // SEQ_ID, SEQ_THING_ID
            HashMap<Long, Long> thingAlias = getResult2ThingAlias(dataSource, userId);

            // Chequeo si no existe el Parent del Thing Asociado
            for (Long thingAliasId: thingAlias.keySet()) {
               Long thingId = thingAlias.get(thingAliasId);

               /*String sqlCheck = "SELECT seq_id,  " +
                     "   CASE SEQ_ENTITY_TYPE " +
                     "   WHEN 1 THEN 'VIDEO_DETECTOR' " +
                     "   WHEN 2 THEN 'VIDEO_CAMERA' " +
                     "   WHEN 12 THEN 'DVR_CAMERA' " +
                     "   WHEN 13 THEN 'PANEL' " +
                     "   WHEN 14 THEN 'MOTION_VIEWER' " +
                     "   WHEN 15 THEN 'HIKVISON_DVR' " +
                     "   WHEN 16 THEN 'CAMERA_MANAGER' " +
                     "   WHEN 17 THEN 'BASE_PARTITION' " +
                     "   WHEN 18 THEN 'FULL_PARTITION' " +
                     "   WHEN 19 THEN 'TVT_DVR' " +
                     "   WHEN 20 THEN 'CONTIGO' " +
                     "   WHEN 21 THEN 'DAHUA_DVR' " +
                     "   WHEN 22 THEN 'MULTISITE_EVENT_CONSOLE' " +
                     "   WHEN 23 THEN 'OperateInstallation' " +
                     "   WHEN 24 THEN 'VMS_VIDEO' " +
                     "   WHEN 25 THEN 'VMS' " +
                     "   ELSE 'UNKNOWN' " +
                     "   END AS DEVICE_TYPE, " +
                     "   SEQ_PARENT_ID," +
                     "   DES_NAME" +
                     "   FROM \"DEVICE\".SMPR_TTHING WHERE seq_id = " + thingId;

               content += getResult4Analisis(dataSource, sqlCheck, "ThingCheck", "orange"); */

               boolean existeParentThing = getResult3Thing(dataSource, thingId);
               // Si no existe p
               System.out.println("   # existeParentThing for thingAliasId: " + thingAliasId +
                     " thingId: " + thingId
                     + " -> " + existeParentThing );

               // Si NO existe parent (Ej: panel no existe) Borramos el thingAlias y thing
               if (!existeParentThing){
                  System.out.println("  # BORRAR thingAliasId: " + thingAliasId + " y thingId: "  + thingId);
                  System.out.println("  # BORRAR Réplicas de thingAlias y Thing x Id");

                  // Deletes on Device
                  String sqlDeleteOnDevices = "schemas:DEVICE\n" +
                        "DELETE FROM SMPR_TTHING_ALIAS WHERE SEQ_ID = " + thingAliasId +";\n" +
                        "DELETE FROM SMPR_TTHING WHERE SEQ_ID = " + thingId +";";

                  writeToFile("./deleteOnDevice-" + thingAliasId + ".sql", sqlDeleteOnDevices);
                  multiUpdate.runMultiUpdate(env, "./deleteOnDevice-" + thingAliasId + ".sql", false, 0,null);

                  // Thing Replicas
                  String sqlDeactivateThingReplicas =
                        "schemas:ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE,SCENE\n" +
                              "UPDATE SMPR_TTHING SET BOL_IS_ACTIVE=0 WHERE SEQ_ID = "+ thingId +";";

                  writeToFile("./deactivateThingReplica-" + thingId + ".sql", sqlDeactivateThingReplicas);
                  multiUpdate.runMultiUpdate(env, "./deactivateThingReplica-" + thingId + ".sql", false, 0, null);

                  // Thing Alias Replicas
                  String sqlDeactivateThingAliasReplicas =
                        "schemas:ACCOUNT-MANAGEMENT,EVENT-PROCESSOR,FILTER-DISPATCHER,PANIC-BUTTON,RULE-ENGINE\n" +
                              "UPDATE SMPR_TTHING_ALIAS SET BOL_IS_ACTIVE=0 WHERE SEQ_ID = "+ thingAliasId +";";

                  writeToFile("./deactivateThingAliasReplica-" + thingAliasId + ".sql", sqlDeactivateThingAliasReplicas);
                  multiUpdate.runMultiUpdate(env, "./deactivateThingAliasReplica-" + thingAliasId + ".sql", false, 0, null);

               } else {

                  // Generar reporte para analizar
                  content += "<h1> " + userData + " </h1>";

                  sql4analisis = "SELECT * FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION WHERE seq_admin = " + userId;
                  content += getResult4Analisis(dataSource, sql4analisis, "Installation", "orange");

                  sql4analisis = "SELECT * FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS WHERE seq_user_id = " + userId;
                  content += getResult4Analisis(dataSource, sql4analisis, "Installation Alias", "blue");

                  sql4analisis = "SELECT * FROM \"DEVICE\".SMPR_TTHING_ALIAS WHERE seq_user_id = " + userId;
                  content += getResult4Analisis(dataSource, sql4analisis, "Thing Alias", "yellow");
               }
            }
         }

         if (!"".equals(content)){
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
            String analisisDataFile = "./checkThingAliasHuerfanas-" + timeStamp + ".html";

            writeToFile(analisisDataFile, content);
         }

      } catch (Exception e) {
         LOGGER.error("Error: " + e.getMessage());
         printStackTrace(e);
      }

      LOGGER.info("End process.");

      return;
   }


   private HashMap<Long, String> getResult1Users(DataSource dataSource ){
      HashMap<Long, String> userMap = new HashMap<>();
      String sql = "SELECT DISTINCT usr.SEQ_ID AS userId, usr.DES_NICK_NAME, usr.BOL_ADMINISTRATOR" +
            "       FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr " +
            "       INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION inst ON (usr.SEQ_ID = inst.SEQ_ADMIN) " +
            "       INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS instAlias ON (inst.SEQ_ID = instAlias.SEQ_INSTALLATION_ID) " +
            "       INNER JOIN \"DEVICE\".SMPR_TTHING_ALIAS thna ON (thna.SEQ_INSTALLATION_ALIAS_ID = instAlias.SEQ_ID) " +
            "       WHERE instAlias.SEQ_USER_ID = usr.SEQ_ID" +
            "       AND thna.SEQ_PARENT_ID IS NOT NULL " +
            "       AND thna.SEQ_PARENT_ID NOT IN ( " +
            "       SELECT SEQ_ID FROM \"DEVICE\".SMPR_TTHING_ALIAS " +
            "       WHERE seq_user_id = usr.SEQ_ID" +
            "       )";

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         // Query that finaly will be executed
         stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
         rs = stmt.executeQuery(sql);

         // Get row values to show
         while (rs.next()) {
            userMap.put(rs.getLong(1), rs.getString(2));
         }

         try {
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (con != null) con.close();
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         }

      } catch (Exception e) {
         System.out.println(" ");
         System.out.println(e.getMessage());
         printStackTrace(e);
      }

      return userMap;
   }


   private HashMap<Long, Long> getResult2ThingAlias(DataSource dataSource, Long seq_user_id){
      HashMap<Long, Long> thingAlias = new HashMap<>();

      String sql = "SELECT thna.SEQ_ID, thna.SEQ_THING_ID, thna.SEQ_PARENT_ID FROM \"DEVICE\".SMPR_TTHING_ALIAS thna " +
            "    INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS instAlias ON (thna.SEQ_INSTALLATION_ALIAS_ID = instAlias.SEQ_ID)  " +
            "    INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION inst ON (inst.SEQ_ID = instAlias.SEQ_INSTALLATION_ID) " +
            "       WHERE inst.seq_admin = " + seq_user_id +
            "       AND instAlias.SEQ_USER_ID = "  + seq_user_id +
            "       AND thna.SEQ_PARENT_ID IS NOT NULL " +
            "       AND thna.SEQ_PARENT_ID NOT IN (" +
            "           SELECT SEQ_ID FROM \"DEVICE\".SMPR_TTHING_ALIAS " +
            "           WHERE seq_user_id = " + seq_user_id +
            "       )";

      System.out.println("   # " + sql);

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         // Query that finaly will be executed
         stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
         rs = stmt.executeQuery(sql);

         // Get row values to show
         while (rs.next()) {
            thingAlias.put(rs.getLong(1), rs.getLong(2));
         }

         try {
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (con != null) con.close();
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         }

      } catch (Exception e) {
         System.out.println(" ");
         System.out.println(e.getMessage());
         printStackTrace(e);
      }

      return thingAlias;
   }

   private boolean getResult3Thing(DataSource dataSource, Long seq_thing_id){

      String sql = "SELECT count(1) FROM \"DEVICE\".SMPR_TTHING WHERE seq_Id =  (" +
            "  SELECT SEQ_PARENT_ID FROM \"DEVICE\".SMPR_TTHING WHERE seq_Id = " + seq_thing_id + ")";
      boolean result = true;

      System.out.println("   # " + sql);

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         // Query that finaly will be executed
         stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
         rs = stmt.executeQuery(sql);


         // Get row values to show
         if (rs.next()) {
            if (rs.getInt(1) == 0){
               result = false;
            } else {
               result = true;
            }
         }

         try {
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (con != null) con.close();
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         }

      } catch (Exception e) {
         System.out.println(" ");
         System.out.println(e.getMessage());
         printStackTrace(e);
      }

      return result;
   }

   private boolean getResult3ThingDetail(DataSource dataSource, Long seq_thing_id){

      String sql = "SELECT * FROM \"DEVICE\".SMPR_TTHING WHERE seq_Id = " + seq_thing_id;
      boolean result = true;

      System.out.println("   # " + sql);

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         // Query that finaly will be executed
         stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
         rs = stmt.executeQuery(sql);


         // Get row values to show
         if (rs.next()) {
            if (rs.getInt(1) == 0){
               result = false;
            } else {
               result = true;
            }
         }

         try {
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (con != null) con.close();
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         }

      } catch (Exception e) {
         System.out.println(" ");
         System.out.println(e.getMessage());
         printStackTrace(e);
      }

      return result;
   }

   private static String getResult4Analisis(DataSource dataSource, String sql, String title, String color) {

      String start = "<h2> " + title + " </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         body = selectToReport(dataSource, sql, color);

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   public synchronized void runMonitorCheckThingAlias(String env) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);
         System.out.println("connectionType: " + connectionType);

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);
         String exportData = "";
         String sql = "SELECT DISTINCT usr.SEQ_ID AS userId, usr.DES_NICK_NAME " +
               "FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr " +
               "INNER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION inst ON (usr.SEQ_ID = inst.SEQ_ADMIN) " +
               "INNER JOIN \"DEVICE\".SMPR_TTHING thn ON (thn.SEQ_INSTALLATION_ID = inst.SEQ_ID) " +
               "WHERE thn.SEQ_ID IN ( " +
               "SELECT SEQ_PARENT_ID " +
               "FROM \"DEVICE\".SMPR_TTHING " +
               "WHERE SEQ_ID IN ( " +
               "      SELECT SEQ_THING_ID " +
               "      FROM \"DEVICE\".SMPR_TTHING_ALIAS thna " +
               "      WHERE thna.SEQ_PARENT_ID NOT IN (SELECT SEQ_ID FROM \"DEVICE\".SMPR_TTHING_ALIAS WHERE seq_user_id = usr.SEQ_ID ) " +
               "      AND thna.SEQ_PARENT_ID IN (SELECT SEQ_ID FROM \"DEVICE\".SMPR_TTHING WHERE SEQ_INSTALLATION_ID = inst.SEQ_ID )" +
               "      AND thna.seq_user_id =  usr.SEQ_ID " +
               "))" ;

         String timeStamp = "";

         String exit = "0";
         System.out.println("start process ...");

         while("0".equals(exit)) {

            timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());

            LOGGER.info("executing query at : " + timeStamp);
            exportData = selectToScript(dataSource, sql, "orange");

            if (!exportData.contains("Empty Result")) {
               LOGGER.info("Result founded.");

               String date = "<h1>" + timeStamp + "</h1>";
               appendToFile("checkThingAlias.html", date + exportData);

               sendEmail(env, "Thing Alias Report" , "Report Found",  "./checkThingAlias.html", "checkThingAlias.html");

               exit = "1";

               continue;

            } else {
               LOGGER.info("Empty Result .");
            }

            int t = 300000; // 5min 300000

            LOGGER.info("waiting for: " + Double.valueOf(t/ 60000) + " minutes");
            this.waitFor(t);
            try {
               loadExternalFileProperties();
               exit = getProperty("exit");
            } catch (Exception e){
               exit = "0";
            }
         }

      } catch (Exception e) {
         LOGGER.error("Error: " + e.getMessage());
         printStackTrace(e);
      }

      LOGGER.info("End process.");

      return;
   }




   public static void waitFor(int ms) {
      try
      {
         Thread.sleep(ms);
      }
      catch(InterruptedException ex)
      {
         Thread.currentThread().interrupt();
      }
   }
}
