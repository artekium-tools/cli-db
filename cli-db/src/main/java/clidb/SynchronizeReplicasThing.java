package clidb;

import clidb.entities.Thing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;


public class SynchronizeReplicasThing extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(SynchronizeReplicasThing.class);


   public synchronized void checkThing(String env, String schema) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         String insertUpdateFileName = "insertUpdateFileNameThing.sql";

         System.out.println("Start record (default=1): ");
         Scanner scanner2 = new Scanner(new InputStreamReader(System.in));
         String startRecStr = scanner2.nextLine();
         System.out.println(" ");

         int from = 1;

         try {
            from = Integer.valueOf(startRecStr);
         } catch (Exception e) {
            from = 1;
         }

         System.out.println("Wait time in seconds: (default=60): ");
         String waitTimeStr = scanner2.nextLine();
         System.out.println(" ");

         int waitTimeInSeconds;

         try {
            waitTimeInSeconds = Integer.valueOf(waitTimeStr);
         } catch (Exception e) {
            waitTimeInSeconds = 60;
         }

         int chunk = 999;
         int to = from + chunk;

         System.out.println("counting Things ... ");
         int total = countThing(dataSource, schema);

         while (true) {

            System.out.println("\nLoading Owner Thing from: " + from + " to: " + to + " of " + total);

            ArrayList<Thing> arrThing = selectThing(dataSource, schema, from, to);

            System.out.println("\n Thing loaded: " + arrThing.size());


            for (Thing thing: arrThing) {

               Thing replica = getThingReplica(dataSource, schema, thing.getId());

               if (replica == null) {
                  printInLine("INSERT for seqId: " + thing.getId());
                  try {
                     String sqlInsert = thing.getInsertStatement(schema);
                     appendToFile(insertUpdateFileName, sqlInsert + ";\n");
                  } catch (Exception e){
                     LOGGER.error("error generating INSERT for seqId: {} - {} ", thing.getId(), e.getMessage());
                  }

               } else {
                  if (!thing.equals(schema, replica)) {
                     printInLine("UPDATE for seqId: " + thing.getId());

                     try {
                        String sqlUpdate = thing.getUpdateStatement(schema);
                        appendToFile(insertUpdateFileName, sqlUpdate + ";\n");

                     } catch (Exception e){
                        LOGGER.error("error generating UPDATE for seqId: {} - {} ", thing.getId(), e.getMessage());
                     }

                  } else {
                     printInLine("EQUALS for seqId: " + thing.getId());
                  }
               }
            }

            from = to + 1;
            to = from + chunk;

            wait(waitTimeInSeconds * 1000);
         }


      } catch (Exception e){
         printStackTrace(e);
      }
   }

   public static int countThing(final DataSource dataSource, String schema) {

      int count = 0;
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql = "SELECT COUNT(1) " +
                  "FROM  \"DEVICE\".SMPR_TTHING Thing " ;

            if ("FILTER-DISPATCHER".equals(schema)){
               sql += "WHERE SEQ_ENTITY_TYPE IN (2,16,24,25)";
            }

            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
               count = rs.getInt(1);
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return count;

   }

   public static ArrayList<Thing> selectThing(final DataSource dataSource, String schema, int from, int to) {

      ArrayList<Thing> arrThing = new ArrayList<>();

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql = "SELECT c.* " +
            "FROM (SELECT c.*, ROWNUM as rnum " +
            "      FROM ( " +
            "            SELECT " + Thing.getFieldNames(schema) +
            "            FROM  \"DEVICE\".SMPR_TTHING thing " +
            "            WHERE SEQ_ENTITY_TYPE IN (2,16,24,25) " +
            "            ORDER BY thing.SEQ_ID " +
            "      ) c) c " +
            "WHERE c.rnum BETWEEN " + from + " AND " + to;

            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
               Thing thing = new Thing(schema, rs);
               arrThing.add(thing);
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return arrThing;

   }


   public static Thing getThingReplica(final DataSource dataSource, String schema, Long seqId) {
      Thing replica = null;
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql =
                  "SELECT  " + Thing.getFieldNames(schema) + " " +
                  "FROM  \""+ schema +"\".SMPR_TTHING thing " +
                  "WHERE thing.SEQ_ID = " + seqId;

            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
               replica = new Thing(schema, rs);
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return replica;

   }

}
