package clidb;

import oracle.jdbc.pool.OracleDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.postgresql.ds.PGSimpleDataSource;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.sql.DataSource;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.util.*;


public class QryDB {


   private static final Logger LOGGER = LogManager.getLogger(QryDB.class);

   static boolean UPDATE_IN_TEST_MODE = false; // true=olny shows logs  false=execute updates
   static int terminalWidth = 150;
   static boolean isOracle = false;
   static boolean isPostgres = false;
   static int rowsToShow = 200;
   static final String SUCCESS = "success";

   static Properties prop = new Properties();

   static Properties scriptProperties = new Properties();



   public void start(String[] args) {

      String jdbcUrl = System.getenv("JDBC_URL");
      String username = System.getenv("JDBC_USER");
      String password = System.getenv("JDBC_PASSWORD");


      if (args.length > 0) {

         if (args[0].equals("help")) {
            System.out.println("Help: ");
            System.out.println("    multiSelect <env>");
            System.out.println("       - Will be ask for a .sql script to be executed");
            System.out.println("       - This .sql file could contains html tag inside sql comments --");
            System.out.println("       - Before a select statement could add a sql comments -- with color tag: i.e.: <orange/>");
            System.out.println("       - Will generate a .html file as result");
            System.out.println("    ");
            System.out.println("    multiUpdate <env> ");
            System.out.println("       - Will be ask for a .sql script to be executed");
            System.out.println("       - First line must be schema:SCHEMA1,SCHEMA3,SCHEMA3");
            System.out.println("       - Execution will be logged on cli-db.log");
            System.out.println("       - Comments in .sql file also will be logged on cli-db.log");
            System.out.println("    ");
            System.out.println("    largeScript <env> SCHEMA");
            System.out.println("    ");
            System.out.println("    largeScript & (without param load properties from scriptConfig.properties, run in background &)");
            System.out.println("    ");
            System.out.println("    userInfo <env>");
            System.out.println("    ");
            System.out.println("    <JDBC_URL> <JDBC_USER> <JDBC_PASSWORD>");
            System.out.println("    ");
            System.out.println("    (JDBC_URL JDBC_USER JDBC_PASSWORD) setted on environment");
            System.out.println("    ");

            System.out.println("  datafixes <env> {auto}");

            System.out.println("Show Custom taskc? (y/n):    ");
            String customTasks = enterValue("");

            if ("y".equals(customTasks.toLowerCase())) {

               System.out.println("    ");
               System.out.println("* synchronizeReplicasInstallationAlias <env> schema AM-42358");
               System.out.println("  synchronizeReplicasInstallation <env> schema AM-42358");
               System.out.println("  synchronizeReplicasUser <env> schema AM-42358");
               System.out.println("* synchronizeReplicasThing <env> schema AM-42358");
               System.out.println("* synchronizeReplicasThingAlias <env> schema AM-42358");

               System.out.println("    ");
               System.out.println("  generateDeleteInstalationTreeSmnt <env> installationId userId / ADSMART-39056");
               System.out.println("    ");
               System.out.println("  eventMultisiteGenerator (multisede)/ AM-40013");
               System.out.println("    ");
               System.out.println("  eventIndividualGenerator (individuo)/ AM-40967");
               System.out.println("    ");

               System.out.println("  ----------------------------------  ");
               System.out.println("  multiSiteGenerateTestSh <env> / AM-40107");
               System.out.println("    ");
               System.out.println("  compareMultiSiteJson <env> / AM-40107");
               System.out.println("    ");
               System.out.println("  testCrono ");
               System.out.println("  ----------------------------------  ");
               System.out.println("    ");
               System.out.println("    testEventDao (Tira la query segun los filtros seteados o custom)");
               System.out.println("    ");
               System.out.println("    countMultiSiteEvents (busca el user y sus serviceId, muetra la query que tira el ms)");
               System.out.println("    ");

               System.out.println("    testLess (tira la bateria de 24 requests a los jar actual y nuevo y baja el json)");
               System.out.println("    ");
               System.out.println("    testLessTimed (generó el string para crear el testTimed4report.sh)");
               System.out.println("    ");
               System.out.println("    formatTimeComparationFile (Formatea el .txt resultado de testTimed4report.sh )");
               System.out.println("    ");
               System.out.println("    formatJson inputfileName outputfileName");
               System.out.println("    ");
               System.out.println("    checkSchedulerExecution ");
               System.out.println("       - Compara logs vs DB en scheduler, para detectar cuantas escenas no se ejecutan,");
               System.out.println("       - Bajar log de scheduler de cloudwatch ej: para el jueves 26-dic a desde las 22:00:00hs hasta las 22:02:59hs ");
               System.out.println("       - Bajar 24 logs, uno por cada hora (evaluar si hay horas que no existan escenas)");
               System.out.println("       - Bajarlo como csv con formato:");
               System.out.println("          2024-11-19 22:02:02.081,scheduled scene: 344026 was fired,info,smart,scheduler");
               System.out.println("       - Guardarlo en la carpeta: /home/cristian/workspace/prosegur/prod/soporte/EscenasNoSeEjecuta/run-{Numero}/");
               System.out.println("          Al nombre agregarle la {hora} ej: logs-insights-results24-11-2024-22:00.csv");
               System.out.println("    ");
            }

            // DEPRECADOS
            //System.out.println("    ");
            //System.out.println("       checkRepetedInstalations <env> / AM-38078");
            //System.out.println("    ");
            //System.out.println("       checkRepetedInstalationsAlias <env> / AM-40349");

            //System.out.println("    ");
            //System.out.println("  purgeInstallationsBaja <env> AM-39543");
            //System.out.println("    ");
            //System.out.println("       checkInstallationsMultiOwner <env> installationIdFrom  / AM-38618");
           /* System.out.println("    ");
            System.out.println("  checkInstallationsMultiOwnerFilter <env> AM-38618");
            System.out.println("    ");*/
            //System.out.println("  checkInstallationsMultiOwnerPurge <env> / AM-38618 paso 2");
            // System.out.println("    ");
            // System.out.println("       checkInstallationsMultiOwnerAnalisis / AM-38618");
            // System.out.println("    ");
            /*System.out.println("  fixErrorMigracion <env> / AM-39726");
            System.out.println("    ");
            System.out.println("  fixErrorMigracion4ERROR <env> / AM-39726");
            System.out.println("    ");
            System.out.println("  parsefixErrorMigracion4ERROR <env> / AM-39726");
            System.out.println("    ");*/


            //System.out.println("       fixCameraManagerSessionClientId <env> / AM-39999");
            // System.out.println("       checkThirdPartySession <env> / AM-39938");
            // System.out.println("       checkCameraManagerSession <env> / AM-39938");
            // System.out.println("    ");
            // System.out.println("  checkFixCameraManagerSessionClientId <env> / AM-39987");

           /* System.out.println("  checkThingAlias <env>");
            System.out.println("    ");
            System.out.println("  monitorCheckThingAlias <env> &");
            System.out.println("    ");
            System.out.println("  checkThingAliasHuerfanas <env>");
            System.out.println("    ");*/
            return;
         }


         if (args[0].equals("test")) {
            Test test = new Test();
            test.main();
            return;
         }

         if (args[0].equals("datafixes")) {
            DatafixesMenu datafixesMenu = new DatafixesMenu();
            String env = args[1];
            String mode = null;
            if (args.length > 2) {
               mode = args[2];
            }

            datafixesMenu.main(env, mode);
            return;
         }

         if (args[0].equals("testEventDao")) {
            TestEventDao test = new TestEventDao();
            test.main();
            return;
         }

         if (args[0].equals("countMultiSiteEvents")) {
            TestEventDao test = new TestEventDao();
            test.mainMultiSite();
            return;
         }

         if (args[0].equals("testLess")) {
            TestEventDao test = new TestEventDao();
            test.testLess();
            return;
         }

         if (args[0].equals("formatTimeComparationFile")) {
            TestEventDao test = new TestEventDao();

            test.formatCsv();
            return;
         }



         if (args[0].equals("testLessTimed")) {
            TestEventDao test = new TestEventDao();
            test.testLessTimed();
            return;
         }

         if (args[0].equals("formatJson")) {
            String filename = args[1];
            String outputFilename = args[2];
            FormatJson formatJson = new FormatJson();
            formatJson.main(filename, outputFilename);
            return;
         }

         if (args[0].equals("multiSelect")) {
            String env = args[1];
            MultiSelect multiSelect = new MultiSelect();
            multiSelect.runMultiSelect(env);
            return;
         }

         if (args[0].equals("multiUpdate")) {
            String env = args[1];
            LargeScript largeScript = new LargeScript();
            largeScript.runMultiUpdate(env);
            return;
         }

         if (args[0].equals("largeScript")) {
            LargeScript largeScript = new LargeScript();
            if (args.length > 1) {
               String env = args[1];
               String schema = args[2];
               largeScript.runScript(env, schema);
            } else {
               largeScript.runScript();
            }

            return;
         }

         if (args[0].equals("userInfo")) {
            String env = args[1];
            UserInfo userInfo = new UserInfo();
            userInfo.runUserInfo(env);
            return;
         }

         if (args[0].equals("synchronizeReplicasInstallationAlias")) {
            String env = args[1];
            String schema = args[2];
            SynchronizeReplicasInstallationAlias synchronizeReplicasInstallationAlias = new SynchronizeReplicasInstallationAlias();
            synchronizeReplicasInstallationAlias.checkInstalationsAlias(env, schema);
            return;
         }

         if (args[0].equals("synchronizeReplicasThingAlias")) {
            String env = args[1];
            String schema = args[2];
            SynchronizeReplicasThingAlias synchronizeReplicasThingAlias = new SynchronizeReplicasThingAlias();
            synchronizeReplicasThingAlias.checkThingAlias(env, schema);
            return;
         }


         if (args[0].equals("synchronizeReplicasThing")) {
            String env = args[1];
            String schema = args[2];
            SynchronizeReplicasThing synchronizeReplicasThing = new SynchronizeReplicasThing();
            synchronizeReplicasThing.checkThing(env, schema);
            return;
         }

         if (args[0].equals("checkThingAlias")) {
            String env = args[1];
            ThingAliasIssue thingAliasIssue = new ThingAliasIssue();
            thingAliasIssue.runCheckThingAlias(env);
            return;
         }

         if (args[0].equals("monitorCheckThingAlias")) {
            String env = args[1];
            ThingAliasIssue thingAliasIssue = new ThingAliasIssue();
            thingAliasIssue.runMonitorCheckThingAlias(env);
            return;
         }

         if (args[0].equals("checkThingAliasHuerfanas")) {
            String env = args[1];
            ThingAliasIssue thingAliasIssue = new ThingAliasIssue();
            thingAliasIssue.runCheckThingAliasHuerfana(env);
            return;
         }

        /* if (args[0].equals("checkRepetedInstalations")) {
            String env = args[1];
            RepetedInstallations repetedInstallations = new RepetedInstallations();
            repetedInstallations.checkRepetedInstalations(env);
            return;
         }*/


         if (args[0].equals("checkRepetedInstalationsAlias")) {
            String env = args[1];
            RepetedInstallationsAlias repetedInstallationsAlias = new RepetedInstallationsAlias();
            //repetedInstallationsAlias.checkRepetedInstalationsAlias(env);
            return;
         }

         if (args[0].equals("generateDeleteInstalationTreeSmnt")) {
            String env = args[1];
            Long installationId = null;
            Long userId = null;
            installationId = Long.valueOf(args[2]);
            userId = Long.valueOf(args[3]);

            RepetedInstallations generateDeleteInstalationTree = new RepetedInstallations();
            generateDeleteInstalationTree.generateDeleteIntallationStmnt(env, installationId, userId);
            return;
         }


         if (args[0].equals("purgeInstallationsBaja")) {
            String env = args[1];
            PurgeInstallationsBaja purgeInstallationsBaja = new PurgeInstallationsBaja();
            purgeInstallationsBaja.main(env);
            return;
         }

         /*if (args[0].equals("checkInstallationsMultiOwner")) {
            String env = args[1];
            Long installationFrom = null;
            if (args.length == 3 ) installationFrom = Long.valueOf(args[2]);

            CheckInstallationsMultiOwner checkInstallationsMultiOwner = new CheckInstallationsMultiOwner();
            checkInstallationsMultiOwner.main(env, installationFrom);
            return;
         }*/


         if (args[0].equals("checkInstallationsMultiOwnerAnalisis")) {
            String env = args[1];

            CheckInstallationsMultiOwner checkInstallationsMultiOwner = new CheckInstallationsMultiOwner();
            checkInstallationsMultiOwner.analisis(env);
            return;
         }

         if (args[0].equals("fixErrorMigracion")) {
            String env = args[1];
            FixErrorMigracion fixErrorMigracion = new FixErrorMigracion();
            fixErrorMigracion.main(env);
            return;
         }

         if (args[0].equals("fixErrorMigracion4ERROR")) {
            String env = args[1];
            FixErrorMigracion fixErrorMigracion = new FixErrorMigracion();
            fixErrorMigracion.main4ERROR(env);
            return;
         }

         if (args[0].equals("parsefixErrorMigracion4ERROR")) {
            String env = args[1];
            FixErrorMigracion fixErrorMigracion = new FixErrorMigracion();
            fixErrorMigracion.parse4ERROR(env);
            return;
         }


         if (args[0].equals("eventMultisiteGenerator")) {
            EventMultisiteGenerator eventGenerator = new EventMultisiteGenerator();
            eventGenerator.main();
            return;
         }

         if (args[0].equals("eventIndividualGenerator")) {
            EventIndividualGenerator eventIndividualGenerator = new EventIndividualGenerator();
            eventIndividualGenerator.main();
            return;
         }

         if (args[0].equals("multiSiteGenerateTestSh")) {
            MultiSiteTest multiSiteTest = new MultiSiteTest();
            //String env = args[1];
            multiSiteTest.main();
            return;
         }

         if (args[0].equals("testCrono")) {
            MultiSiteTest multiSiteTest = new MultiSiteTest();
            //String env = args[1];
            multiSiteTest.testCrono();
            return;
         }


         if (args[0].equals("compareMultiSiteJson")) {
            MultiSiteTest multiSiteTest = new MultiSiteTest();
            //String env = args[1];
            multiSiteTest.compareMultiSiteJson();
            return;
         }

         if (args[0].equals("compareMultiSiteJson")) {
            MultiSiteTest multiSiteTest = new MultiSiteTest();
            //String env = args[1];
            multiSiteTest.compareMultiSiteJson();
            return;
         }

         if (args[0].equals("checkSchedulerExecution")) {
            CheckSchedulerExecution checkSchedulerExecution = new CheckSchedulerExecution();
            checkSchedulerExecution.main();
            return;
         }


         jdbcUrl = args[0];
         username = args[1];
         password = args[2];

      } else if (jdbcUrl == null && username == null && password == null) {
         System.out.println("JDBC_URL, JDBC_USER and JDBC_PASSWORD environment variables must be setted or pass by args");
         return;
      }

      runQry(jdbcUrl, username, password);

   }

   public String runCommandLine(String command) {
      String[] cmd = {"bash", "-c", command};
      String line = "";

      //System.out.println("command: " + command);

      try {
         Process p = Runtime.getRuntime().exec(cmd);
         BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));

         while ((line = input.readLine()) != null)
            line += input.readLine();

         input.close();
      } catch (Exception e) {
         printStackTrace(e);
      }

      return line;
   }


   public void loadScriptProperties(String propertiesFilePath) {
      System.out.println("Loading properties from file " + propertiesFilePath);
      if (!isEmpty(propertiesFilePath)) {
         File propertiesFile = new File(propertiesFilePath);
         if (propertiesFile.exists() && !propertiesFile.isDirectory()) {
            try {
               InputStream inputStream = new FileInputStream(propertiesFilePath);
               loadScriptPropFromInputStream(inputStream);
               System.out.println("Properties file has been loaded successfully");
            } catch (FileNotFoundException e) {
               System.out.println("Can not open the properties file: Check OS I/O or permissions, the property loading process has an interruption");
            }
         } else {
            System.out.println("The properties file does not exist or is a directory");
         }
      } else {
         System.out.println("The properties file path is null or empty");
      }
   }

   private void loadScriptPropFromInputStream(InputStream inputStream) {
      Reader reader = null;
      try {
         reader = new InputStreamReader(inputStream, "UTF-8");
         scriptProperties.load(reader);
      } catch (UnsupportedEncodingException e) {
         System.out.println("Unsupported Encoding: Check OS supported encoding, the property loading process has an interruption");
      } catch (IOException e) {
         System.out.println("Can not read the properties from file: Check OS I/O, the property loading process has an interruption");
      }
   }


   public void loadExternalFileProperties() {

      String fileName = "connection.properties";
      String folder = "./";
      String propertiesFilePath;

      while (true) {

         propertiesFilePath = folder + fileName;

         //System.out.println("Loading properties from file " + propertiesFilePath);
         if (!isEmpty(propertiesFilePath)) {
            File propertiesFile = new File(propertiesFilePath);
            if (propertiesFile.exists() && !propertiesFile.isDirectory()) {
               try {
                  InputStream inputStream = new FileInputStream(propertiesFilePath);
                  loadFromInputStream(inputStream);
                  break;
                  //System.out.println("Properties file has been loaded successfully");
               } catch (FileNotFoundException e) {
                  System.out.println("Can not open the properties file: Check OS I/O or permissions, the property loading process has an interruption");
               }
            } else {
               System.out.println("The properties file: " + folder + fileName + " does not exist or is a directory");

               Scanner scanner1 = new Scanner(new InputStreamReader(System.in));
               System.out.println(" ");
               System.out.println("connection.properties folder: ");
               folder = scanner1.nextLine();
            }
         } else {
            System.out.println("The properties file path is null or empty");
         }
      }
   }

   private void loadFromInputStream(InputStream inputStream) {
      Reader reader = null;
      try {
         reader = new InputStreamReader(inputStream, "UTF-8");
         prop.load(reader);
      } catch (UnsupportedEncodingException e) {
         System.out.println("Unsupported Encoding: Check OS supported encoding, the property loading process has an interruption");
      } catch (IOException e) {
         System.out.println("Can not read the properties from file: Check OS I/O, the property loading process has an interruption");
      }
   }

   private boolean isEmpty(Object str) {
      return (str == null || "".equals(str));
   }


   public String getProperty(String key) {
      String value = prop.getProperty(key);
      if (value == null) {
         System.out.println("Property " + key + " is not defined ");
      }
      return value;
   }

   public static void writeToFile(String fileToWrite, String stringToWrite) {

      File file = new File(fileToWrite);
      FileWriter fr = null;
      try {

         fr = new FileWriter(file);
         fr.write(stringToWrite);

         System.out.println("File writed: " + fileToWrite);

      } catch (Exception e) {
         printStackTrace(e);
      } finally {
         try {
            fr.close();
         } catch (IOException e) {
            printStackTrace(e);
         }
      }
   }

   public static void appendToFile(String fileToWrite, String stringToWrite) {

      File file = new File(fileToWrite);
      FileWriter fr = null;
      try {

         fr = new FileWriter(file, true);
         fr.write(stringToWrite);

         //System.out.println("File append: " + fileToWrite);

      } catch (Exception e) {
         printStackTrace(e);
      } finally {
         try {
            fr.close();
         } catch (IOException e) {
            printStackTrace(e);
         }
      }
   }

   public void runQry(String jdbcUrl, String username, String password) {

      DataSource dataSource = null;

      try {
         terminalWidth = Integer.valueOf(runCommandLine("echo $COLUMNS"));
      } catch (Exception e) {
         //System.out.println("Must execute command: export COLUMNS");
         //return;
         terminalWidth = 150;
      }

      try {
         System.out.println("Connecting to " + username + " ...");
         dataSource = getDataSource(jdbcUrl, username, password);
      } catch (Exception e) {
         System.out.println(" ");
         System.out.println(e.getMessage());
         //printStackTrace(e);
         return;
      }

      String sqlInput = "";
      boolean pageMode = false;


      if (jdbcUrl.indexOf("jdbc:oracle") > -1) {
         isOracle = true;
      } else if (jdbcUrl.indexOf("jdbc:postgresql") > -1) {
         isPostgres = true;
      }

      ArrayList<String> arrQrys = new ArrayList<>();

      while (true) {

         Scanner scanner = new Scanner(new InputStreamReader(System.in));
         System.out.println(" ");
         System.out.println("q=quit, h=history, tables=tables, columns <tablename>, p=page mode: " + (pageMode == false ? "OFF" : "ON") + " f=from query.sql");
         System.out.println("sql: ");
         sqlInput = scanner.nextLine();

         if (sqlInput != null && sqlInput.equalsIgnoreCase("h")) {
            for (String qry : arrQrys) {
               System.out.println("-> " + qry);
            }
            continue;
         }

         if (sqlInput != null && sqlInput.equalsIgnoreCase("p")) {
            if (pageMode == true) {
               pageMode = false;
            } else {
               pageMode = true;
            }
            continue;
         }

         if (sqlInput != null && sqlInput.equalsIgnoreCase("tables")) {
            if (isOracle) {
               sqlInput = "select table_name FROM all_tables where owner = '" + username + "'";
            } else if (isPostgres) {
               sqlInput = "select table_name from information_schema.tables where table_schema = 'public'";
            }
            select(dataSource, sqlInput);
            continue;
         }

         if (sqlInput != null && sqlInput.startsWith("columns") && sqlInput.contains(" ")) {
            String tableName = sqlInput.substring(sqlInput.indexOf(" ") + 1);
            if (isOracle) {
               sqlInput = "SELECT table_name, column_name, data_type, data_length FROM all_tab_columns where table_name = '" + tableName + "'";
            } else if (isPostgres) {
               sqlInput = "select column_name, data_type, character_maximum_length from information_schema.columns  where table_name = '" + tableName + "'";
            }
            select(dataSource, sqlInput);
            continue;
         }


         if (sqlInput != null && sqlInput.equalsIgnoreCase("f")) {
            // TODO Levantar lo que tenga query.sql

         }


         if (sqlInput.equalsIgnoreCase("q")) {
            break;
         }

         if (!sqlInput.endsWith(";")) sqlInput = sqlInput + ";";

         String[] arrScripts = sqlInput.split(";");

         for (String script : arrScripts) {

            if (script.trim().toLowerCase().startsWith("select")) {

               if (pageMode == true) {
                  selectPaged(dataSource, script.trim());
               } else {
                  select(dataSource, script.trim());
               }
            } else {
               update(dataSource, script.trim());
            }

            System.out.println("-> " + script);
            arrQrys.add(script);
         }
      }

      try {
         dataSource.getConnection().close();
         System.out.println("connection to " + username + " closed");
      } catch (Exception e) {
         printStackTrace(e);
      }
   }

   public static DataSource getDataSource(String jdbcUrl, String username, String password) throws Exception {

      //System.out.println("Connecting to: " + username);
      OracleDataSource orclDataSource = null;
      PGSimpleDataSource pgdataSource = null;

      try {
         if (jdbcUrl.indexOf("jdbc:oracle") > -1) {
            orclDataSource = new OracleDataSource();
            // Esta linea se usó cuando no funcionaba la conexion por vpn desde local
            //orclDataSource.setServerName("pro-smart-rds-main.c9gmtf1sahmr.eu-central-1.rds.prosegur.local");
            orclDataSource.setURL(jdbcUrl);
            orclDataSource.setUser(username);
            orclDataSource.setPassword(password);
         } else if (jdbcUrl.indexOf("jdbc:postgresql") > -1) {
            pgdataSource = new PGSimpleDataSource();
            pgdataSource.setURL(jdbcUrl);
            pgdataSource.setUser(username);
            pgdataSource.setPassword(password);
         }
      } catch (Exception e) {
         LOGGER.error("Error connection to " + username);
         printStackTrace(e);
      }


      if (orclDataSource != null) {
         //LOGGER.info("Connected OK to " + username + " on oracle.");
         //System.out.println("connected OK to " + username + " on oracle.");

         return orclDataSource;
      } else {
         //LOGGER.info("Connected OK to " + username + " on postgresql.");
         //System.out.println("connected OK to " + username + " on postgresql.");
         return pgdataSource;
      }
   }

   public static void select(final DataSource dataSource, String sql) {
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         Statement countStmt = null;
         ResultSet countRs = null;
         ResultSet rs = null;

         try {

            sql = sql.replace(";", "");

            String sqlCount = "Select count(1) as cnt from (" + sql + ")";

            //System.out.println("sqlCount: " + sqlCount);

            countStmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            countRs = countStmt.executeQuery(sqlCount);

            countRs.next();
            int numRows = countRs.getInt(1);

            //System.out.println("count: " + countRs.getInt(1));

            if (numRows > rowsToShow) {
               if (isOracle) {
                  if (sql.toLowerCase().contains("where")) {

                     sql = sql + " and rownum <= " + rowsToShow;
                  } else {
                     sql = sql + " where rownum <= " + rowsToShow;
                  }
               } else if (isPostgres) {
                  sql = sql + " limit " + rowsToShow;
               }
            }

            //System.out.println("sql: " + sql);

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);
            boolean showColName = true;
            String rowColNames = "";
            int widthToShow = terminalWidth;

            Map<String, Integer> columnsWidth = new HashMap<>();

            // Get max row width
            while (rs.next()) {
               ResultSetMetaData rsmd = rs.getMetaData();

               for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                  //System.out.println(rsmd.getColumnName(i) +" CW: " + columnsWith.get(rsmd.getColumnName(i)) );
                  if (columnsWidth.containsKey(rsmd.getColumnName(i))) {
                     //check col columns
                     Integer colWithSaved = columnsWidth.get(rsmd.getColumnName(i));
                     if (rs.getString(rsmd.getColumnName(i)) != null &&
                           rs.getString(rsmd.getColumnName(i)).length() > colWithSaved) {
                        columnsWidth.put(rsmd.getColumnName(i), rs.getString(rsmd.getColumnName(i)).length());
                     }
                  } else {
                     // put init value
                     columnsWidth.put(rsmd.getColumnName(i), rsmd.getColumnName(i).length());
                  }
               }
            }

            if (rs.last()) rs.beforeFirst();

            int rowCount = 0;
            System.out.println(" ");

            // Get row values to show
            while (rs.next()) {
               rowCount++;
               ResultSetMetaData rsmd = rs.getMetaData();
               String row = "";
               String col = "";

               if (showColName) {

                  for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                     col = completeWithSpace(rsmd.getColumnName(i),
                           columnsWidth.get(rsmd.getColumnName(i))) + " | ";

                     if (row.length() + col.length() > terminalWidth && widthToShow == terminalWidth) {
                        widthToShow = row.length();
                     }

                     row = row + col;

                  }

                  rowColNames = row;

                  // Show Row
                  System.out.println(chunkRow(row, widthToShow));

                  row = "";
                  showColName = false;
               }

               for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                  row = row + completeWithSpace(rs.getString(rsmd.getColumnName(i)),
                        columnsWidth.get(rsmd.getColumnName(i))) + " | ";
               }

               // Show Row
               System.out.println(chunkRow(row, widthToShow));
            }

            if (rowColNames.length() > terminalWidth) {
               System.out.println(" ");
               System.out.println("There are columns that haven't showed: ");
               System.out.println("-> " + rowColNames.substring(widthToShow));
            }

            System.out.println(" ");
            System.out.println(rowCount + " of " + numRows + " rows.");

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            //printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               //printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         //e1.printStackTrace();
      }
   }


   public static String selectToScript(final DataSource dataSource, String sql, String color) {
      String row = "";
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            row = "<table border='1'>";

            int columns = 0;
            int rowCount = 0;
            if (rs.next()) {
               ResultSetMetaData rsmd = rs.getMetaData();

               for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                  columns ++;

                  row = row + "<th bgcolor='" + color + "'>" + rsmd.getColumnName(i) + "</th>\n";

               }

               if (rs.last()) rs.beforeFirst();

               // Get row values to show
               while (rs.next()) {
                  row = row + "<tr>";
                  rowCount ++;
                  for (int i = 1; i <= rsmd.getColumnCount(); i++) {

                     row = row + "<td>" + rs.getString(rsmd.getColumnName(i)) + "</td>";
                  }

                  row = row + "</tr>\n";
               }

               row = row + "<tr><td colspan='" + columns +"'> count: " + rowCount + " </td></tr>\n";

            } else {
               row = row + "<th bgcolor='yellow'> Empty Result </th>\n";
            }

            row = row + "</table>";

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return row;
   }

   public static String selectToCSV(final DataSource dataSource, String sql, boolean showHeaders) {
      String row = "";
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            //int rowCount = 0;
            if (rs.next()) {
               ResultSetMetaData rsmd = rs.getMetaData();

               // Headers row
               if (showHeaders){
                  for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                     row = row + rsmd.getColumnName(i) + "|";
                  }
                  row = row + "\n";
               }

               if (rs.last()) rs.beforeFirst();

               // Get row values to show
               while (rs.next()) {

                  //rowCount ++;
                  for (int i = 1; i <= rsmd.getColumnCount(); i++) {

                     row = row + rs.getString(rsmd.getColumnName(i)) + "|";
                  }

                  row = row + "\n";
               }

               //row = row + "count: " + rowCount + "\n";

            } else {
               row = row + "--Empty Result--\n";
            }


         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return row;
   }

   public static String selectToReport(final DataSource dataSource, String sql, String color) {
      String row = "";
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            System.out.println("------------------------------------------------");
            System.out.println(sql);

            int columns = 0;
            int rowCount = 0;
            if (rs.next()) {
               ResultSetMetaData rsmd = rs.getMetaData();

               for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                  columns ++;
                  row = row + "<th bgcolor='" + color + "'>" + rsmd.getColumnName(i) + "</th>\n";
               }

               if (rs.last()) rs.beforeFirst();

               // Get row values to show
               while (rs.next()) {
                  row = row + "<tr>";

                  for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                     row = row + "<td>" + rs.getString(rsmd.getColumnName(i)) + "</td>";
                  }

                  row = row + "</tr>\n";
                  rowCount ++;
               }

               row = row + "<tr><td> <b>" + rowCount + " rows.</b> </td>" +
                     "<td colspan='" + (columns -1) +"'>" + sql + " </td></tr>\n";

            } else {
               row = row + "<th> Empty Result </th>\n";
               row = row + "<tr><td>" + sql + " </td></tr>\n";
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return row;
   }

   public static void selectPaged(final DataSource dataSource, String sql) {
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            Map<Integer, String> columnsNames = new HashMap<>();

            // Get max row width
            while (rs.next()) {
               ResultSetMetaData rsmd = rs.getMetaData();

               for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                  columnsNames.put(i, rsmd.getColumnName(i));
               }
            }

            int rowCount = 0;
            if (rs != null) {
               rs.last();    // moves cursor to the last row
               rowCount = rs.getRow(); // get row id
            }

            if (rs.last()) rs.beforeFirst();

            System.out.println(" ");

            boolean end = false;

            // Get row values to show
            while (rs.next()) {
               //rowCount++;
               ResultSetMetaData rsmd = rs.getMetaData();

               for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                  System.out.println("# " + columnsNames.get(i) + " -> " + rs.getString(rsmd.getColumnName(i)));
               }

               System.out.println(" ");
               System.out.println("row " + rs.getRow() + " of " + rowCount);

               while (rowCount > 1){

                  // Show Row
                  Scanner scanner = new Scanner(new InputStreamReader(System.in));
                  System.out.println("enter=next f=first p=previous l=last e=end");
                  System.out.println("option:");
                  String option = scanner.nextLine();

                  if (option != null && option.toLowerCase().equals("e")) {
                     end = true;
                     break;
                  }

                  if (option != null && option.toLowerCase().equals("") && !rs.isLast()) {
                     break;
                  }

                  if (option != null && option.toLowerCase().equals("f") && !rs.isFirst()){
                     rs.beforeFirst();
                     break;
                  }

                  if (option != null && option.toLowerCase().equals("p") && !rs.isFirst()){
                     rs.previous();
                     rs.previous();
                     break;
                  }

                  if (option != null && option.toLowerCase().equals("l") && !rs.isLast()){
                     rs.afterLast();
                     rs.previous();
                     rs.previous();
                     break;
                  }

                  if (option != null && option.toLowerCase().equals("l") && rs.isLast()){
                     System.out.println("Is the last record.");
                  }
                  if (option != null && option.toLowerCase().equals("") && rs.isLast()){
                     System.out.println("Is the last record.");
                  }
                  if (option != null && option.toLowerCase().equals("p") && rs.isFirst()){
                     System.out.println("Is the first record.");
                  }
                  if (option != null && option.toLowerCase().equals("f") && rs.isFirst()){
                     System.out.println("Is the first record.");
                  }
                  //enter=next f=first p=previous l=last e=end"
                  if(!option.toLowerCase().equals("") &&
                           !option.toLowerCase().equals("l") &&
                           !option.toLowerCase().equals("p") &&
                           !option.toLowerCase().equals("f")){
                        System.out.println("Wrong option.");
                  }
               }

               if(end==true){
                  break;
               }
            }

            System.out.println(" ");
            System.out.println(rowCount + " rows.");

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            //printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               //printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         //e1.printStackTrace();
      }
   }

   private static String chunkRow(String row, int widthToShow) {

      if(row.length() > widthToShow) {
         return row.substring(0, widthToShow);
      } else {
         return row;
      }

   }

   private static String completeWithSpace(String line, int width) {

      if (line == null) {
         line = "null";
      }

      if (line.length() <= width) {
         for (int i = line.length() - 1; i < width; i++) {
            line = line + " ";
         }
      }
      return line;
   }

   public static void update(final DataSource dataSource, String sql) {

      try {

         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            stmt = con.createStatement();
            int result;

            if (UPDATE_IN_TEST_MODE){
               result = 1;
            } else {
               result = stmt.executeUpdate(sql);
            }

            if (result == 1) {
               LOGGER.info("Executed ok.");
               //System.out.println("Executed ok.");
               //System.out.println(" ");
            } else {
               LOGGER.info("result: " + result);
               //System.out.println("result: " + result);
            }

         } catch (Exception e) {
            LOGGER.info("ERROR executing {} - {}: " , sql, e.getMessage());
            System.out.println(" ");
            System.out.println(e.getMessage());
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               //printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         LOGGER.info("ERROR {}: ", e1.getMessage());
         System.out.println(" ");
         System.out.println(e1.getMessage());
         //e1.printStackTrace();
      }
   }

   public static String updateBatch(final DataSource dataSource, String sql) {
      String success = "";

      try {

         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            stmt = con.createStatement();
            int result;

            if (UPDATE_IN_TEST_MODE){
               result = 1;
            } else {
               result = stmt.executeUpdate(sql);
            }

            if (result >= 0) {
               //LOGGER.info("Executed ok.");
               success = SUCCESS;
            } else {

               success = "Executing sql: " + sql + " result: " + result;
               LOGGER.error(success);
            }

         } catch (Exception e) {
            //System.out.println(" ");
            success = "Error executing sql: " + sql + " ERROR: " + e.getMessage();
            LOGGER.error(success);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               success = "Error Clossing connection ERROR: " + e.getMessage();
               LOGGER.error(success);
            }
         }

      } catch (Exception e1) {
         success = "Error : " + e1.getMessage();
         LOGGER.error(success);
      }

      return success;
   }

   public void sendEmail(String env, String subject, String body, String filePath, String fileName) {
      sendEmail(env, subject, body, filePath, fileName, null, false);
   }

   public void sendEmail(String env, String subject, String body, String filePath, String fileName, boolean cc) {
      sendEmail(env, subject, body, filePath, fileName, null, cc);
   }

   public void sendEmail(String env, String subject, String body, String filePath, String fileName, String to) {
      sendEmail(env, subject, body, filePath, fileName, to, false);
   }

   public void sendEmail(String env, String subject, String body, String filePath, String fileName, String to, boolean cc) {

      final String username = getProperty(env + ".mail.smtp.username");
      final String password = getProperty(env + ".mail.smtp.password");
      final String host = getProperty(env + ".mail.smtp.host");
      final String port = getProperty(env + ".mail.smtp.port");
      final String from = getProperty(env + ".mail.smtp.from");

      if (to == null) {
         to = getProperty(env + ".mail.smtp.to");
      }

      if (cc) {
         to = to + "," + getProperty(env + ".mail.smtp.cc");
      }

      Properties props = new Properties();
      props.put("mail.smtp.auth", true);
      props.put("mail.smtp.starttls.enable", true);
      props.put("mail.smtp.host", host);
      props.put("mail.smtp.port", port);

      Session session = Session.getInstance(props,
            new javax.mail.Authenticator() {
               protected PasswordAuthentication getPasswordAuthentication() {
                  return new PasswordAuthentication(username, password);
               }
            });

      try {

         Message message = new MimeMessage(session);
         message.setFrom(new InternetAddress(from));
         message.setRecipients(Message.RecipientType.TO,
               InternetAddress.parse(to));
         message.setSubject(subject);
         message.setText(body);

         if (filePath != null) {
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            Multipart multipart = new MimeMultipart();
            FileDataSource source = new FileDataSource(filePath);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(fileName);
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
         }

         System.out.println("Sending e-mail: " + subject + " to: " + to);

         Transport.send(message);

         System.out.println("Done");

      } catch (MessagingException e) {
         printStackTrace(e);
      }
   }



   public String getFormattedTime(long elapsedTime) {

      Duration duration = Duration.ofMillis(elapsedTime);
      long h = duration.toHours();
      long m = duration.toMinutes() % 60;
      long s = duration.getSeconds() % 60;
      long ml = s % 1000;

      return  String.format("%02d:%02d:%02d::%02d", h, m, s, ml);
   }

   public String getFormattedDate(Date date) {
      SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/YYYY hh:mm:ss");
      String dateString=sdf.format(date);
      return dateString;
   }


   public static void printInLine(String text) {
      StringBuilder msg = new StringBuilder(140);
      msg
            .append('\r')
            .append(text);
      System.out.print(msg);
   }

   public String formatJson4Report(String input){

      String[] arrlines = input.split("\n");
      String[] arrKeyValue;
      String key, value;
      String result = "";

      for (String line:arrlines){

         //System.out.println("@line :" + line);

         if (line.contains(":&nbsp;")){
            arrKeyValue = line.split(":&nbsp;");

            //System.out.println("@arrKeyValue[0] :" + arrKeyValue[0]);
            //System.out.println("@arrKeyValue[1] :" + arrKeyValue[1]);

            if (arrKeyValue[0].contains("\"")){
               key = "<font color=\"blue\">" + arrKeyValue[0] + "</font>";
            } else {
               key = arrKeyValue[0];
            }

            if (arrKeyValue[1].contains("\"")){
               value = "<font color=\"green\">" + arrKeyValue[1] + "</font>";
            } else {
               value =  arrKeyValue[1];
            }

            result += key + "&nbsp;:&nbsp;"+ value + "\n";

         } else {
            result += line + "\n";
         }
      }

      return result;
   }

   public String formatJson(String input){

      String[] arrlines = input.split("\n");
      String[] arrKeyValue;
      String key, value;
      String result = "";

      for (String line:arrlines){

         //System.out.println("@line :" + line);

         if (line.contains(":")){
            arrKeyValue = line.split(":");

            //System.out.println("@arrKeyValue[0] :" + arrKeyValue[0]);
            //System.out.println("@arrKeyValue[1] :" + arrKeyValue[1]);

           /* if (arrKeyValue[0].contains("\"")){
               key = "<font color=\"blue\">" + arrKeyValue[0] + "</font>";
            } else {
               key = arrKeyValue[0];
            }

            if (arrKeyValue[1].contains("\"")){
               value = "<font color=\"green\">" + arrKeyValue[1] + "</font>";
            } else {
               value =  arrKeyValue[1];
            }9*/

            key = arrKeyValue[0];
            value =  arrKeyValue[1];
            result += key + " : "+ value + "\n";

         } else {
            result += line + "\n";
         }
      }

      return result;
   }


   public String chooseSqlFile(){

      String  scriptFile = "";

      while (true) {
         Scanner scanner1 = new Scanner(new InputStreamReader(System.in));

         List<String> sqlFileList = getSqlFiles();

         if (!sqlFileList.isEmpty()) {
            for (int i=0; i < sqlFileList.size(); i++) {
               System.out.println((i +1)+ ") - " +  sqlFileList.get(i));
            }

            String sqlIndex = "";

            while(true) {
               Scanner scannerOP = new Scanner(new InputStreamReader(System.in));
               System.out.println(" ");
               System.out.println("sql to execute: ");
               sqlIndex = scannerOP.nextLine();

               if (Integer.valueOf(sqlIndex).intValue() -1 <= sqlFileList.size()) {
                  scriptFile = sqlFileList.get(Integer.valueOf(sqlIndex).intValue() - 1);
                  System.out.println("script selected: " + scriptFile);
                  break;

               } else {
                  System.out.println("wrong option.");
                  continue;
               }
            }

            break;

         } else {
            System.out.println("Sql files not found in folder ./");
            System.out.println("Script path: ");
            scriptFile = scanner1.nextLine();

            Path path = Paths.get(scriptFile);

            if (Files.exists(path)) {
               break;
            } else {
               System.out.println("File Not Found.");
            }
         }
      }

      return scriptFile;

   }
   private static List<String> getSqlFiles() {

      String[] cmd = {"bash","-c", "ls *.sql"};
      List<String> sqlFileList = new ArrayList<>();

      try{
         String line;
         Process p = Runtime.getRuntime().exec(cmd);
         BufferedReader input =
               new BufferedReader(new InputStreamReader(p.getInputStream()));

         while ((line = input.readLine()) != null) {
            sqlFileList.add(line);
         }
         input.close();
      } catch (Exception e){
         printStackTrace(e);
      }
      return sqlFileList;
   }

   public void checkSessions(DataSource dataSource, String dbUsername){
      Connection con = null;
      Statement stmt = null;
      ResultSet rs = null;
      try {
            con = dataSource.getConnection();
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            dbUsername = dbUsername.toUpperCase();

            String sqlCheckSession = "SELECT count(*) from v$session b WHERE username = '"+ dbUsername +"'";
            rs = stmt.executeQuery(sqlCheckSession);
            if (rs.next()) {
               System.out.println("################################################");
               System.out.println("       Sessions for "+ dbUsername +": " + rs.getInt(1));
               System.out.println("################################################");
            }
      } catch (Exception e) {
         printStackTrace(e);
      } finally {
         try {
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (con != null) con.close();
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         }
      }
   }

   public static void printStackTrace(Exception e){
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw);
      e.printStackTrace(pw);
      String sStackTrace = sw.toString();
      System.out.println(sStackTrace);
   }

   public static boolean isInList(String value, ArrayList<String> list){

      for (String listValue:list){
         if (value.equals(listValue)) return true;
      }
      return false;
   }

   public int getDayOfWeek(){
      LocalDate localDate = LocalDate.now();
      java.time.DayOfWeek dayOfWeek = localDate.getDayOfWeek();
      return dayOfWeek.getValue();
   }

   public String enterValue(String message) {
      Scanner scanner1 = new Scanner(new InputStreamReader(System.in));
      System.out.println(" ");
      System.out.println(message);
      String value = scanner1.nextLine();

      return value;
   }

   public void generateAndSendUserReport(DataSource dataSource, String env, String userName, String to){
      UserInfo userInfo = new UserInfo();
      String userInfoReport = null;

      userInfo.setWithReplicas(true);
      userInfoReport = userInfo.generateUserReport(dataSource, env, userName);

      if (userInfoReport != null) {
         sendEmail(env, "Report for : " + userName,
                 "Attached Report",
                 userInfoReport,
                 userInfoReport.substring(userInfoReport.lastIndexOf("/") + 1),
                 to);
      }
   }

   public  String selectToHTML(final DataSource dataSource, String sql, String color) {
      String row = "";
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            //System.out.println("------------------------------------------------");
            //System.out.println(sql);
            if (color == null){
               color = "orange";
            }

            if (rs.next()) {
               ResultSetMetaData rsmd = rs.getMetaData();

               for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                  row = row + "<th bgcolor='" + color + "'>" + rsmd.getColumnName(i) + "</th>\n";
               }

               if (rs.last()) rs.beforeFirst();

               // Get row values to show
               while (rs.next()) {
                  row = row + "<tr>";

                  for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                        row = row + "<td>" + rs.getString(rsmd.getColumnName(i)) + "</td>";
                  }

                  row = row + "</tr>\n";
               }

            } else {
               row = row + "<th> Empty Result </th>\n";
               row = row + "<tr><td>" + sql + " </td></tr>\n";
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return row;
   }
}
