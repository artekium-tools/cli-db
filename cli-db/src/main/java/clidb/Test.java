package clidb;

import clidb.entities.EventTypeUtils;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClient;
import com.amazonaws.services.cognitoidp.model.*;
import com.google.common.base.Strings;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import javax.ws.rs.core.MultivaluedMap;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class Test extends QryDB {


   private  final Logger LOGGER = LogManager.getLogger(Test.class);


   public void main() {

      //loadExternalFileProperties();

      //getCognitoData("pro","canxavisitges2009@gmail.com");

      //System.out.println(loginAs("leira.sotelo@hotmail.com"));

     // countMultiSiteEvents();

      //showFrontCodes();

      //fixSqlFile();

      //runDatafixes();

      //CheckSchedulerExecution checkSchedulerExecution = new CheckSchedulerExecution();

      //checkSchedulerExecution.main();

      //DatafixesMenuTestMode datafixesMenuTestMode = new DatafixesMenuTestMode();
      //datafixesMenuTestMode.main("pro", true);

      //System.out.println(getValidTimeZoneWhereSmtn());

      formatReport();

     /* long yourmilliseconds = System.currentTimeMillis();
      SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SS");
      Date resultdate = new Date(yourmilliseconds);
      System.out.println(sdf.format(resultdate));*/

      /*GrpcCallTest grpcCallTest = new GrpcCallTest();
      grpcCallTest.main();*/
      //getThingId();

      //System.out.println(getInsertWithoutNameStatement("123456", "9753640", "12390130", "1821493"));


   }

   private void getThingId(){


      String sql = "INSERT INTO SMPR_TTHING_ALIAS" +
            "(SEQ_ID       , SEQ_INSTALLATION_ALIAS_ID, SEQ_THING_ID, SEQ_PARENT_ID, DES_NAME, SEQ_USER_ID)                        VALUES " +
            "(:thingAliasId, NULL                      ,9753640      ,12390130     ,NULL     ,1821493);";

      String[] arrLine = sql.split(",");

      for(int i=0; i< arrLine.length; i++){
         System.out.println(i + " - " +  arrLine[i]);
      }


      System.out.println(" Thing - " +  arrLine[7]);
   }

   private String getInsertWithoutNameStatement(String thingAliasId, String thingId, String parentThingId, String userId){

      String sqlInsertWithoutName = "INSERT INTO SMPR_TTHING_ALIAS(SEQ_ID, SEQ_INSTALLATION_ALIAS_ID, SEQ_THING_ID, SEQ_PARENT_ID, SEQ_USER_ID) VALUES (";

      return sqlInsertWithoutName + thingAliasId + ", NULL, " + thingId + ", " + parentThingId + ", " +  userId  + ")";

   }

   private void formatReport(){

      CheckSchedulerExecution checkSchedulerExecution = new CheckSchedulerExecution();

      checkSchedulerExecution.formatReport("/home/cristian/workspace/prosegur/prod/soporte/AM-43380-EscenasNoSeEjecuta/run-11-3-2025/refinado/");
   }


   private String getValidTimeZoneWhereSmtn(){


      String dayDate = enterValue("dayDate: ej: 2 for 2/1/2025");
      String month = enterValue("month number: ");
      String time = enterValue("time: ej 23 for 23:00 UTC");

   String myString =  time + ":00 "+ dayDate +"/"+month+"/2025";
   SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
   Date myDateTime = null;

   //Parse your string to SimpleDateFormat
   try {
      myDateTime = simpleDateFormat.parse(myString);
   } catch (ParseException e){
      e.printStackTrace();
   }







      /* Para cada timezone restarle al "time" el offset y

      Consultar por cada timezone para distintas horas en el cron expression
         ej: 22:00
            Europe/Lisbon 22

         String sql = "SELECT TRIGGER_NAME, CRON_EXPRESSION FROM SCHEDULER.QRTZ_CRON_TRIGGERS " +
               "WHERE
               (cron_expression LIKE '0 % 22 ? *%' AND TIME_ZONE_ID = 'Europe/Lisbon')"
               OR
               (cron_expression LIKE '0 % 21 ? *%' AND TIME_ZONE_ID = 'Europe/Madrid')"
               OR
               (cron_expression LIKE '0 % 19 ? *%' AND TIME_ZONE_ID = 'Europe/Istanbul')"
               OR
               (cron_expression LIKE '0 % 01 ? *%' AND TIME_ZONE_ID IN ('America/Santiago', 'America/Buenos_Aires', 'America/Montevideo')
               OR
               (cron_expression LIKE '0 % 02 ? *%' AND TIME_ZONE_ID = 'America/Asuncion')"
               OR
               (cron_expression LIKE '0 % 03 ? *%' AND TIME_ZONE_ID IN ('America/Bogota', 'America/Lima')";


       */


      String sqlWhere = "WHERE "+
         "(cron_expression LIKE '0 % " + time + " ? *%' AND TIME_ZONE_ID = 'Europe/Lisbon') " +
         "OR " +
         "      (cron_expression LIKE '0 % " + getTimePlusOffset(myDateTime, "Europe/Madrid") + " ? *%' " +
            "AND TIME_ZONE_ID = 'Europe/Madrid') " +
         "OR " +
         "      (cron_expression LIKE '0 % " + getTimePlusOffset(myDateTime, "Europe/Istanbul") + " ? *%' " +
            "AND TIME_ZONE_ID = 'Europe/Istanbul') " +
         "OR " +
         "      (cron_expression LIKE '0 % " + getTimePlusOffset(myDateTime, "America/Santiago") +" ? *%' " +
            "AND TIME_ZONE_ID IN ('America/Santiago', 'America/Buenos_Aires', 'America/Montevideo')) " +
         "OR " +
         "      (cron_expression LIKE '0 % " + getTimePlusOffset(myDateTime, "America/Asuncion") + " ? *%' " +
            "AND TIME_ZONE_ID = 'America/Asuncion') " +
         "OR " +
         "      (cron_expression LIKE '0 % " + getTimePlusOffset(myDateTime, "America/Bogota") + " ? *%' " +
            "AND TIME_ZONE_ID IN ('America/Bogota', 'America/Lima'))";


      return sqlWhere;
   }

   private String getTimePlusOffset(Date myDateTime, String timeZone){

      System.out.println("This is the Actual Date:"+myDateTime);
      Calendar cal = new GregorianCalendar();
      cal.setTime(myDateTime);

      String result = "";

      HashMap<String, Integer> timeZoneOffsetMap = new HashMap<>();

      timeZoneOffsetMap.put("Europe/Lisbon",	0);

      timeZoneOffsetMap.put("Europe/Madrid", +1);

      timeZoneOffsetMap.put("Europe/Istanbul", +3);

      timeZoneOffsetMap.put("America/Santiago",	-3);
      timeZoneOffsetMap.put("America/Buenos_Aires", -3);
      timeZoneOffsetMap.put("America/Montevideo", -3);

      timeZoneOffsetMap.put("America/Asuncion", -3);

      timeZoneOffsetMap.put("America/Bogota", -5);
      timeZoneOffsetMap.put("America/Lima", -5);


      int offset = timeZoneOffsetMap.get(timeZone);
      System.out.println("offset: " + offset);
//Adding 21 Hours to your Date

      cal.add(Calendar.HOUR_OF_DAY, -offset);
      System.out.println("This is Hours Added Date:"+cal.getTime());

      result = "" + cal.getTime().getHours();

      if (result.length() == 1) result = "0"+ result;
      return result;

   }


   public int getDayOfWeek(){
      LocalDate localDate = LocalDate.now();
      java.time.DayOfWeek dayOfWeek = localDate.getDayOfWeek();
      return dayOfWeek.getValue();
   }

   public void runDatafixes() {

      String strDate = new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date());

      String fileName = "runDatafixes-" + strDate + ".txt";


      System.out.println("RUN DATAFIXES: " + fileName);
      appendToFile(fileName, "" + strDate );

   }


   private void checkSchedulerExecution(){

      loadExternalFileProperties();

      String jdbcUrl = "";
      String dbUsername = "";
      String password = "";

      String env = "pro";
      if (env == null) env = getProperty("env");
      String connectionType = getProperty("connectionType");

      // select jdbc url
      if ("vpn".equals(connectionType)) {
         jdbcUrl = getProperty("vpn." + env + ".url");
      } else {
         jdbcUrl = getProperty("telepresence." + env + ".url");
      }

      dbUsername = "SCHEDULER" ;
      password = getProperty(env + ".SCHEDULER" +".password");

      DataSource dataSource;
      Connection con = null;
      Statement stmt = null;
      ResultSet rs = null;

      try {

         dataSource = getDataSource(jdbcUrl, dbUsername, password);
         con = dataSource.getConnection();
         stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);


         checkSessions(dataSource, dbUsername);


         ArrayList<String> arrTriggerName = new ArrayList<>();

         String dayNumber = "4"; // 1- dom 2-Lun 3-mar 4-Mie 5-jue 6-vie 7-sab
         String dayDate = "20" ;
         String month = "11";
         String dayName = "Miercoles"; //"Martes"; // ""Domingo";


         String logFile = "/home/cristian/workspace/prosegur/prod/soporte/EscenasNoSeEjecuta/logs-insights-results"+
               dayDate+"-"+month+"-2024.csv";
         String reportFile = "/home/cristian/workspace/prosegur/prod/soporte/EscenasNoSeEjecuta/noEjecutadas" +
               dayName + "-" +dayDate +".txt";
// 2024-11-19 22:02:02.081,scheduled scene: 344026 was fired,info,smart,scheduler

         System.out.println("Reading log file: " + logFile);
         BufferedReader br = new BufferedReader(new FileReader(logFile));

         String sqlCheck = "SELECT CRON_EXPRESSION, TIME_ZONE_ID FROM SCHEDULER.QRTZ_CRON_TRIGGERS WHERE TRIGGER_NAME = ";

         String sCurrentLine;
         int inx = 1;
         int countInLog = 1;
         while ((sCurrentLine = br.readLine()) != null) {

            String triggerName = sCurrentLine.substring(41, sCurrentLine.indexOf(" was"));
            //System.out.println(triggerName);
            printInLine("log file line: " + inx );

            // Solo si corresponde al dia lo agrego al array
            rs = stmt.executeQuery(sqlCheck + "'" + triggerName +"'");

            if (rs.next()) {
               String cronExpr = rs.getString(1);
               String timeZoneId = rs.getString(2);
               // Si corresponde al timezone y al dia Miercoles
               if ("Europe/Madrid".equals(timeZoneId) &&
                     (cronExpr.contains("*") || cronExpr.contains(dayNumber))){
                  arrTriggerName.add(triggerName) ;
                  countInLog ++;
               }

            }

            inx ++;
            //if (inx == 200) break;
         }

         System.out.println("jods added from log: " + inx + " for day: " + dayNumber);

         String sql = "SELECT TRIGGER_NAME, CRON_EXPRESSION FROM SCHEDULER.QRTZ_CRON_TRIGGERS " +
               "WHERE cron_expression LIKE '0 00 23 ? *%' " +
               "AND TIME_ZONE_ID = 'Europe/Madrid' " ;

          rs = stmt.executeQuery(sql);

/*
 cargar en un array los ids levantados del log,

y ver si luego recorrer el resultado la consulta,
 Ver cuales corresponda que se hallan ejecutado el martes
 y ver si existe en el arrary*/

         inx = 0;
         int countDayOk = 0;
         int totalFromDB = 0;
         boolean caseFound = false;
         while (rs.next()) {

            String triggerName = rs.getString(1);
            String cronExpressionDays = rs.getString(2).substring(12);

            /*if (triggerName.equals("184734")){
               System.out.println("##### : 184734 - FOUND in DB " );

               //appendToFile(reportFile, "##### : 184734 - FOUND in DB for: " + dayName + "\n");
            }*/
            //System.out.println("cronExpression: " + cronExpressionDays);
            // Si incluye al martes: expresison contains 3
            // Si incluye al miercoles: expression contains 4

            if (cronExpressionDays.contains("*") || cronExpressionDays.contains(dayNumber)) {
               countDayOk++;
               // Buscamos la escena Filtrada de la DB en el log
               // Si no la encontramos, asumimos que no se ejecutó
              /* List<String> matches = arrTriggerName
                     .stream()
                     .filter(it -> it.contains(triggerName)).collect(Collectors.toList());*/


               // Si está en la DB y no se encontró en el log
               if (!isInList(triggerName, arrTriggerName)) {
                  System.out.println("@ Escena no ejecutada: " + triggerName +  " cronExp: " +
                        rs.getString(2) + " cronExpressionDays: " + cronExpressionDays );

                  if (triggerName.equals("184734")){
                     caseFound = true;
                  }
                  appendToFile(reportFile, triggerName + " cronExp: " + rs.getString(2) + "\n");
                  inx ++;
               }
            }

            totalFromDB ++;
            //if (inx == 200) break;
         }

         System.out.println("Report for : " + dayName + " - " + dayDate + "/" + month);
         System.out.println("    countInLog: linas en log corresponden a timeZone (Europe/Madrid): " + countInLog);
         System.out.println("    totalFromDB: (0 00 23 ? *% && Europe/Madrid): " + totalFromDB);
         System.out.println("    countScenes: escenas from DB que deben ejecutarse en dia (" + dayName+ "): " + countDayOk);
         System.out.println("    ##### : 184734 - FOUND in Not Executed for: " + dayName +" -> " + caseFound);
         System.out.println("    Escena no ejecutadas (en DB, no en log): " + inx );


      } catch (Exception e) {
         printStackTrace(e);
      } finally {
         try {
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (con != null) con.close();
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         }
      }

   }


   public void fixSqlFile() {



      try {


         String scriptFile = "/home/cristian/workspace/prosegur/prod/soporte/FilterDispatcher/error.log";
         String scriptFixedFile = "/home/cristian/workspace/prosegur/prod/soporte/FilterDispatcher/update-Things.sql";

         BufferedReader br = new BufferedReader(new FileReader(scriptFile));

         String sCurrentLine;
         String sql ="";
         boolean startRead = false;
         while ((sCurrentLine = br.readLine()) != null) {

            //System.out.println(sCurrentLine);

            if (sCurrentLine.contains("UPDATE ")) {

               /*System.out.println( sCurrentLine.substring(sCurrentLine.indexOf("INSERT INTO"),
                           sCurrentLine.indexOf(" ERROR: ORA-00904")));*/
               if (sCurrentLine.contains("ERROR: ORA-00933:")) {

                  appendToFile(scriptFixedFile, sCurrentLine.substring(sCurrentLine.indexOf("UPDATE"),
                        sCurrentLine.indexOf(" ERROR: ORA-00933:")) + "; \n");
               } else {
                  System.out.println( "DIFFERENT: " + sCurrentLine);
               }
            }

         }


      } catch (Exception e) {
         printStackTrace(e);
      }

      return;
   }

   private void showFrontCodes(){

      EventTypeUtils eventTypeUtils = new EventTypeUtils();

      loadExternalFileProperties();

      String jdbcUrl = "";
      String dbUsername = "";
      String password = "";

      String env = "pro";
      if (env == null) env = getProperty("env");
      String connectionType = getProperty("connectionType");


      // select jdbc url
      if ("vpn".equals(connectionType)) {
         jdbcUrl = getProperty("vpn." + env + ".url");
      } else {
         jdbcUrl = getProperty("telepresence." + env + ".url");
      }

      dbUsername = "EVENT-PROCESSOR" ;//getProperty(env + ".username");
      password = getProperty(env + ".EVENT-PROCESSOR" +".password");

      DataSource dataSource;
      try {
         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         System.out.println("loading from DB");

         eventTypeUtils.load(dataSource);


      }catch (Exception e) {
         printStackTrace(e);
      }


      System.out.println("call to isVideoDeviceEvent");

      eventTypeUtils.isVideoDeviceEvent(1);

      eventTypeUtils.isVideoDeviceAnalyticsEvent(1);

      eventTypeUtils.isVideoDeviceDoorbellEvent(1);

      eventTypeUtils.isPrivacyModeVideoEvent(1);

      eventTypeUtils.isInformPanelStatusEvent(1);

      eventTypeUtils.isPanicButtonEvent(1);

      eventTypeUtils.isKeywordEvent(1);


      LOGGER.info("----------------------------------------");
      LOGGER.info("getBatteryRelatedEventFrontCodes: " + eventTypeUtils.getBatteryRelatedEventFrontCodes());

      LOGGER.info("----------------------------------------");
      LOGGER.info("getVideoDeviceEventFrontCodes: " + eventTypeUtils.getVideoDeviceEventFrontCodes());

      LOGGER.info("----------------------------------------");
      LOGGER.info("getVideoDeviceAnalyticsEventFrontCodes: " + eventTypeUtils.getVideoDeviceAnalyticsEventFrontCodes());

      LOGGER.info("----------------------------------------");
      LOGGER.info("getVideoDeviceDoorbellEventFrontCodes: " + eventTypeUtils.getVideoDeviceDoorbellEventFrontCodes());

      LOGGER.info("----------------------------------------");
      LOGGER.info("getPrivacyModeVideoEventFrontCodes: " + eventTypeUtils.getPrivacyModeVideoEventFrontCodes());

      LOGGER.info("----------------------------------------");
      LOGGER.info("getInformPanelStatusEventFrontCodes: " + eventTypeUtils.getInformPanelStatusEventFrontCodes());

      LOGGER.info("----------------------------------------");
      LOGGER.info("getPanicButtonEventFrontCodes: " + eventTypeUtils.getPanicButtonEventFrontCodes());

      LOGGER.info("----------------------------------------");
      LOGGER.info("getKeywordEventFrontCodes: " + eventTypeUtils.getKeywordEventFrontCodes());

      System.out.println("end.");


   }


   private void printSql() {


      // country, userId, fromDate, toDate, limit);

      Scanner scanner1 = new Scanner(new InputStreamReader(System.in));

      System.out.println("userId:  ");
      String userId = scanner1.nextLine();

      System.out.println("country:  ");
      String country = scanner1.nextLine();

      System.out.println("fromDate DD/MM/YYYY:  ");
      String fromDate = scanner1.nextLine();

      System.out.println("toDate DD/MM/YYYY:  ");
      String toDate = scanner1.nextLine();

      System.out.println("limit:  ");
      String limit = scanner1.nextLine();

      ArrayList<Long> serviceId  = null; /// = getServicesId(Long.valueOf(userId));

      String serviceIdCommaSeparated = getServicePredicate(serviceId);

      String sql = "SELECT * FROM ( \n" +
            "SELECT DISTINCT event.*, \n" +
            "insta.SEQ_ID AS installationId, \n" +
            "insta.DES_CONTRACT_NUMBER, \n" +
            "insta.DES_LOCATION_STREET_TYPE, \n" +
            "insta.DES_LOCATION_STREET, \n" +
            "insta.DES_LOCATION_STREET_NUMBER, \n" +
            "insta.DES_LOCATION_CITY, \n" +
            "insta.DES_LOCATION_REGION, \n" +
            "insta.DES_CUSTOMER_REFERENCE, \n" +
            "thingPanel.COD_CONNECTION_ID, \n" +
            "thingDevice.DES_NAME AS cameraName, \n" +
            "eventText.COD_FRONT_CODE, \n" +
            "image.SEQ_ID AS imageId, \n" +
            "video.SEQ_ID AS videoId,  \n" +
            "video.SEQ_IMAGEPREVIEW_ID, \n" +
            "video.SEQ_DEVICE_ID as videoDeviceId, \n" +
            "thingVideoDevice.DES_NAME AS videoDeviceName, \n" +
            "usr.des_nick_name AS eventUserName, \n" +
            "alias.DES_ALIAS AS eventUserAlias, \n" +
            "panelUserAlias.DES_ALIAS AS panelUserAlias, "+
            "CASE event.BOL_IS_FULL_PARTITION "+
            " WHEN 1 THEN \n" +
            "  (SELECT count(1) FROM SMPR_TTHING thn WHERE thn.SEQ_PARENT_ID = event.SEQ_PARENT_THING_ID AND thn.SEQ_ENTITY_TYPE = 18 AND thn.BOL_IS_ACTIVE = 1) \n" +
            " ELSE 0 \n" +
            "END AS partitionCount, \n" +
            "partitionUserAlias.DES_NAME AS partitionUserAlias, \n" +
            "partitionEventUserAlias.DES_NAME AS partitionEventUserAlias, \n" +
            "partitionAlias.DES_NAME AS partitionAlias, \n" +
            "partitionAlias.SEQ_ENTITY_TYPE AS partitionEntiyType "+
            "FROM SMPR_TEVENT event \n" +
            "-- Join to take SEQ_NOTIFICATION_TYPE_ID for event  \n" +
            "INNER JOIN SMPR_TEVENT_NOTIFICATION_MAP notifMap ON (notifMap.SEQ_EVENT_TYPE_ID = event.SEQ_EVENT_TYPE_ID) \n" +
            "-- isNotificationTypeEnabledForCountry: Join to know if notification is enable by country \n" +
            "-- isEventFilteredByDelay: Multisite event should not delay. \n" +
            "-- multisiteFilter: user.isMultisite() = TRUE is enough \n" +
            "INNER JOIN SMPR_TNOTIFICATION_DELAY notifDelay ON (notifDelay.SEQ_NOTIFICATION_TYPE_ID = notifMap.SEQ_NOTIFICATION_TYPE_ID \n" +
            " AND notifDelay.BOL_ENABLED = 1 AND notifDelay.DES_COUNTRY_ID = '" + country + "') \n" +
            "LEFT OUTER JOIN SMPR_TINSTALLATION insta \n" +
            "     ON (event.SEQ_INSTALLATION_ID = insta.SEQ_ID) \n" +
            "LEFT OUTER JOIN SMPR_TTHING thingPanel \n" +
            "     ON (thingPanel.SEQ_ID = event.SEQ_PARENT_THING_ID AND thingPanel.SEQ_ENTITY_TYPE = 13 AND thingPanel.BOL_IS_ACTIVE = 1) \n" +
            "LEFT OUTER JOIN SMPR_TTHING thingDevice \n" +
            "     ON (thingDevice.SEQ_ID = event.SEQ_THING_ID AND thingDevice.SEQ_ENTITY_TYPE <> 18 AND thingDevice.BOL_IS_ACTIVE = 1) \n" +
            "LEFT OUTER JOIN SMPR_TEVENT_TEXT eventText \n" +
            "     ON  (event.SEQ_EVENT_TYPE_ID = eventText.SEQ_EVENT_TYPE_ID) \n" +
            "LEFT OUTER JOIN SMPR_TIMAGE image \n" +
            "     ON (image.SEQ_EVENT_ID = event.SEQ_ID AND image.bol_acuda_image = 0) \n" +
            "LEFT OUTER JOIN SMPR_TVIDEO video \n" +
            "     ON (video.SEQ_EVENT_ID = event.SEQ_ID) \n" +
            "LEFT OUTER JOIN SMPR_TTHING thingVideoDevice \n" +
            "     ON (thingVideoDevice.SEQ_ID = video.SEQ_DEVICE_ID AND thingVideoDevice.BOL_IS_ACTIVE = 1) \n" +
            "LEFT OUTER JOIN SMPR_TUSER usr \n" +
            "     ON (event.SEQ_USER_ID IS NOT NULL AND usr.SEQ_ID = event.SEQ_USER_ID) \n" +
            "LEFT OUTER JOIN SMPR_TALIAS alias \n" +
            "     ON (event.SEQ_USER_ID IS NOT NULL AND alias.SEQ_INSTALLATION_ID = event.SEQ_INSTALLATION_ID AND alias.DES_USER = \n" +
            "         (SELECT des_nick_name FROM SMPR_TUSER WHERE SEQ_ID = event.SEQ_USER_ID)) \n" +
            "LEFT OUTER JOIN SMPR_TALIAS panelUserAlias \n" +
            "     ON (event.DES_PANEL_USER IS NOT NULL AND panelUserAlias.SEQ_INSTALLATION_ID = event.SEQ_INSTALLATION_ID AND panelUserAlias.DES_USER = event.DES_PANEL_USER) \n" +
            "LEFT OUTER JOIN SMPR_TTHING_ALIAS partitionUserAlias \n" +
            "     ON (partitionUserAlias.SEQ_ID = event.SEQ_THING_ID AND partitionUserAlias.SEQ_USER_ID = "+ userId +" AND partitionUserAlias.BOL_IS_ACTIVE = 1 ) \n" +
            "LEFT OUTER JOIN SMPR_TTHING_ALIAS partitionEventUserAlias \n" +
            "     ON (event.SEQ_USER_ID IS NOT NULL AND partitionEventUserAlias.SEQ_ID = event.SEQ_THING_ID AND partitionEventUserAlias.SEQ_USER_ID = event.SEQ_USER_ID AND partitionEventUserAlias.BOL_IS_ACTIVE = 1) \n" +
            "LEFT OUTER JOIN SMPR_TTHING partitionAlias \n" +
            "     ON (partitionAlias.SEQ_ID = event.SEQ_THING_ID AND partitionAlias.SEQ_ENTITY_TYPE = 18 AND partitionAlias.BOL_IS_ACTIVE = 1) \n" +
            "WHERE event.SEQ_EVENT_TYPE_ID IS NOT NULL \n" +
            "AND event.SEQ_PARENT_THING_ID IN (" + serviceIdCommaSeparated + ") \n" +
            "AND event.FYH_EVENT_DATE >= to_date('"+ fromDate +"','DD/MM/YYYY') \n" +
            "AND event.FYH_EVENT_DATE <=  to_date('" + toDate +"','DD/MM/YYYY') \n" +
            "-- isTheftAlarmConfirmed: Clause to filter if is theaft alarm confirmed \n" +
            "AND ((event.SEQ_EVENT_TYPE_ID = 10 AND event.BOL_MASTERMIND_EVENT = 1 ) OR (event.SEQ_EVENT_TYPE_ID <> 10)) \n" +
            "ORDER BY event.FYH_EVENT_DATE DESC \n" +
            ") \n" +
            "WHERE rownum <= " + limit;


      System.out.println("----------------------------------------------- ");
      System.out.println(" ");
      System.out.println(sql);

      System.out.println(" ");
      System.out.println("----------------------------------------------- ");
      //return jdbcTemplate.query(sql, new EventAdapterRowMapper(), country, userId, fromDate, toDate, limit);

   }



   private void countMultiSiteEvents(){

      loadExternalFileProperties();

      String jdbcUrl = "";
      String dbUsername = "";
      String password = "";

      String env = "pro";
      if (env == null) env = getProperty("env");
      String connectionType = getProperty("connectionType");


      // select jdbc url
      if ("vpn".equals(connectionType)) {
         jdbcUrl = getProperty("vpn." + env + ".url");
      } else {
         jdbcUrl = getProperty("telepresence." + env + ".url");
      }

      dbUsername = "EVENT-PROCESSOR" ;//getProperty(env + ".username");
      password = getProperty(env + ".EVENT-PROCESSOR" +".password");

      DataSource dataSource;
      try {
         dataSource = getDataSource(jdbcUrl, dbUsername, password);

      /*
      1898973	ES	ana.caso@harri1.com	536
      1895865	CL	pflores@integra.cl	338
      1886351	ES	ssgg@dontegroup.com	329
      1898982	ES	soporte.red@carglass.es	216
      1649029	PY	camila.figueredo@biggie.com.py	204
      1649031	PY	cesar.vera@puntofarma.com.py	195
      1886159	ES	rgomezl@adeslasdental.com	190
      1893791	PE	javier.piscoya@prosegur.com	189
      1645139	CL	gperez@losheroes.cl	165
      1891709	CL	michael.fernandez@pjchile.com	157
       */

         ArrayList<Long> arrUserId = new ArrayList<>();

         arrUserId.add(1898973L);
         arrUserId.add(1895865L);
         arrUserId.add(1886351L);
         arrUserId.add(1898982L);
         arrUserId.add(1649029L);
         arrUserId.add(1649031L);
         arrUserId.add(1886159L);
         arrUserId.add(1893791L);
         arrUserId.add(1645139L);
         arrUserId.add(1891709L);

         for (Long userId: arrUserId){

            getSqlCount( dataSource, userId);


         }


      }catch (Exception e) {
         printStackTrace(e);
      }

   }

   private void getSqlCount(DataSource dataSource, Long userId) {


      ArrayList<Long> serviceId = getServicesId(dataSource, Long.valueOf(userId));

      String serviceIdCommaSeparated = getServicePredicate(serviceId);

      String sql = "SELECT count(1) FROM ( \n" +
            "SELECT DISTINCT event.*, \n" +
            "insta.SEQ_ID AS installationId, \n" +
            "insta.DES_CONTRACT_NUMBER, \n" +
            "insta.DES_LOCATION_STREET_TYPE, \n" +
            "insta.DES_LOCATION_STREET, \n" +
            "insta.DES_LOCATION_STREET_NUMBER, \n" +
            "insta.DES_LOCATION_CITY, \n" +
            "insta.DES_LOCATION_REGION, \n" +
            "insta.DES_CUSTOMER_REFERENCE, \n" +
            "thingPanel.COD_CONNECTION_ID, \n" +
            "thingDevice.DES_NAME AS cameraName, \n" +
            "eventText.COD_FRONT_CODE, \n" +
            "image.SEQ_ID AS imageId, \n" +
            "video.SEQ_ID AS videoId,  \n" +
            "video.SEQ_IMAGEPREVIEW_ID, \n" +
            "video.SEQ_DEVICE_ID as videoDeviceId, \n" +
            "thingVideoDevice.DES_NAME AS videoDeviceName, \n" +
            "usr.des_nick_name AS eventUserName, \n" +
            "alias.DES_ALIAS AS eventUserAlias, \n" +
            "panelUserAlias.DES_ALIAS AS panelUserAlias, "+
            "CASE event.BOL_IS_FULL_PARTITION "+
            " WHEN 1 THEN \n" +
            "  (SELECT count(1) FROM SMPR_TTHING thn WHERE thn.SEQ_PARENT_ID = event.SEQ_PARENT_THING_ID AND thn.SEQ_ENTITY_TYPE = 18 AND thn.BOL_IS_ACTIVE = 1) \n" +
            " ELSE 0 \n" +
            "END AS partitionCount, \n" +
            "partitionUserAlias.DES_NAME AS partitionUserAlias, \n" +
            "partitionEventUserAlias.DES_NAME AS partitionEventUserAlias, \n" +
            "partitionAlias.DES_NAME AS partitionAlias, \n" +
            "partitionAlias.SEQ_ENTITY_TYPE AS partitionEntiyType "+
            "FROM SMPR_TEVENT event \n" +
            "-- Join to take SEQ_NOTIFICATION_TYPE_ID for event  \n" +
            "INNER JOIN SMPR_TEVENT_NOTIFICATION_MAP notifMap ON (notifMap.SEQ_EVENT_TYPE_ID = event.SEQ_EVENT_TYPE_ID) \n" +
            "-- isNotificationTypeEnabledForCountry: Join to know if notification is enable by country \n" +
            "-- isEventFilteredByDelay: Multisite event should not delay. \n" +
            "-- multisiteFilter: user.isMultisite() = TRUE is enough \n" +
            "INNER JOIN SMPR_TNOTIFICATION_DELAY notifDelay ON (notifDelay.SEQ_NOTIFICATION_TYPE_ID = notifMap.SEQ_NOTIFICATION_TYPE_ID \n" +
            " AND notifDelay.BOL_ENABLED = 1 AND notifDelay.DES_COUNTRY_ID = " +
               " (SELECT DES_COUNTRY  FROM smpr_tuser WHERE seq_id = " + userId +" )) \n" +
            "LEFT OUTER JOIN SMPR_TINSTALLATION insta \n" +
            "     ON (event.SEQ_INSTALLATION_ID = insta.SEQ_ID) \n" +
            "LEFT OUTER JOIN SMPR_TTHING thingPanel \n" +
            "     ON (thingPanel.SEQ_ID = event.SEQ_PARENT_THING_ID AND thingPanel.SEQ_ENTITY_TYPE = 13 AND thingPanel.BOL_IS_ACTIVE = 1) \n" +
            "LEFT OUTER JOIN SMPR_TTHING thingDevice \n" +
            "     ON (thingDevice.SEQ_ID = event.SEQ_THING_ID AND thingDevice.SEQ_ENTITY_TYPE <> 18 AND thingDevice.BOL_IS_ACTIVE = 1) \n" +
            "LEFT OUTER JOIN SMPR_TEVENT_TEXT eventText \n" +
            "     ON  (event.SEQ_EVENT_TYPE_ID = eventText.SEQ_EVENT_TYPE_ID) \n" +
            "LEFT OUTER JOIN SMPR_TIMAGE image \n" +
            "     ON (image.SEQ_EVENT_ID = event.SEQ_ID AND image.bol_acuda_image = 0) \n" +
            "LEFT OUTER JOIN SMPR_TVIDEO video \n" +
            "     ON (video.SEQ_EVENT_ID = event.SEQ_ID) \n" +
            "LEFT OUTER JOIN SMPR_TTHING thingVideoDevice \n" +
            "     ON (thingVideoDevice.SEQ_ID = video.SEQ_DEVICE_ID AND thingVideoDevice.BOL_IS_ACTIVE = 1) \n" +
            "LEFT OUTER JOIN SMPR_TUSER usr \n" +
            "     ON (event.SEQ_USER_ID IS NOT NULL AND usr.SEQ_ID = event.SEQ_USER_ID) \n" +
            "LEFT OUTER JOIN SMPR_TALIAS alias \n" +
            "     ON (event.SEQ_USER_ID IS NOT NULL AND alias.SEQ_INSTALLATION_ID = event.SEQ_INSTALLATION_ID AND alias.DES_USER = \n" +
            "         (SELECT des_nick_name FROM SMPR_TUSER WHERE SEQ_ID = event.SEQ_USER_ID)) \n" +
            "LEFT OUTER JOIN SMPR_TALIAS panelUserAlias \n" +
            "     ON (event.DES_PANEL_USER IS NOT NULL AND panelUserAlias.SEQ_INSTALLATION_ID = event.SEQ_INSTALLATION_ID AND panelUserAlias.DES_USER = event.DES_PANEL_USER) \n" +
            "LEFT OUTER JOIN SMPR_TTHING_ALIAS partitionUserAlias \n" +
            "     ON (partitionUserAlias.SEQ_ID = event.SEQ_THING_ID AND partitionUserAlias.SEQ_USER_ID = "+ userId +" AND partitionUserAlias.BOL_IS_ACTIVE = 1 ) \n" +
            "LEFT OUTER JOIN SMPR_TTHING_ALIAS partitionEventUserAlias \n" +
            "     ON (event.SEQ_USER_ID IS NOT NULL AND partitionEventUserAlias.SEQ_ID = event.SEQ_THING_ID AND partitionEventUserAlias.SEQ_USER_ID = event.SEQ_USER_ID AND partitionEventUserAlias.BOL_IS_ACTIVE = 1) \n" +
            "LEFT OUTER JOIN SMPR_TTHING partitionAlias \n" +
            "     ON (partitionAlias.SEQ_ID = event.SEQ_THING_ID AND partitionAlias.SEQ_ENTITY_TYPE = 18 AND partitionAlias.BOL_IS_ACTIVE = 1) \n" +
            "WHERE event.SEQ_EVENT_TYPE_ID IS NOT NULL \n" +
            "AND event.SEQ_PARENT_THING_ID IN (" + serviceIdCommaSeparated + ") \n" +
            //"AND event.FYH_EVENT_DATE >= to_date('"+ fromDate +"','DD/MM/YYYY') \n" +
            //"AND event.FYH_EVENT_DATE <=  to_date('" + toDate +"','DD/MM/YYYY') \n" +
            "-- isTheftAlarmConfirmed: Clause to filter if is theaft alarm confirmed \n" +
            "AND ((event.SEQ_EVENT_TYPE_ID = 10 AND event.BOL_MASTERMIND_EVENT = 1 ) OR (event.SEQ_EVENT_TYPE_ID <> 10)) \n" +
            "ORDER BY event.FYH_EVENT_DATE DESC \n" +
            ") ";
            //"WHERE rownum <= " + limit;

      String sqlCount = "SELECT count(1) FROM  \n" +
            "SMPR_TEVENT event \n" +
            "WHERE event.SEQ_EVENT_TYPE_ID IS NOT NULL \n" +
            "AND event.SEQ_PARENT_THING_ID IN (" + serviceIdCommaSeparated + ") \n";
      Connection con = null;
      Statement stmt = null;
      ResultSet rs = null;

      try {
         con = dataSource.getConnection();
         stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
         rs = stmt.executeQuery(sqlCount);

        // System.out.println("sqlCount: " + sqlCount);

         while (rs.next()) {
            serviceId.add(rs.getLong(1));
            System.out.println("UserId: " + userId + " count: " + rs.getInt(1));
         }

      } catch (Exception e) {
         printStackTrace(e);
      } finally {
      try {
         if (rs != null) rs.close();
         if (stmt != null) stmt.close();
         if (con != null) con.close();
      } catch (Exception e) {
         System.out.println(" ");
         System.out.println(e.getMessage());
         printStackTrace(e);
      }
   }


   }

   private ArrayList<Long> getServicesId(DataSource dataSource, Long userId) {


      System.out.println("search services id for user: " + userId );

      ArrayList<Long> serviceId = new ArrayList<>();

      Connection con = null;
      Statement stmt = null;
      ResultSet rs = null;

      try {

         String sql = "SELECT thna.SEQ_THING_ID FROM SMPR_TINSTALLATION_ALIAS insta \n" +
               "INNER JOIN SMPR_TTHING_ALIAS thna ON " +
               "  (THNA.SEQ_INSTALLATION_ALIAS_ID = insta.SEQ_ID AND THNA.BOL_IS_ACTIVE = 1 AND thna.SEQ_USER_ID = "+ userId+") \n" +
               "INNER JOIN SMPR_TTHING thn ON (thn.SEQ_ID = thna.SEQ_THING_ID AND thn.SEQ_TYPE_ID <> 22) \n" +
               "WHERE insta.SEQ_USER_ID = " + userId;


         con = dataSource.getConnection();
         stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
         rs = stmt.executeQuery(sql);

         while (rs.next()) {
            serviceId.add(rs.getLong(1));
         }

      } catch (Exception e ) {
         printStackTrace(e);
      } finally {
      try {
         if (rs != null) rs.close();
         if (stmt != null) stmt.close();
         if (con != null) con.close();
      } catch (Exception e) {
         System.out.println(" ");
         System.out.println(e.getMessage());
         printStackTrace(e);
      }
   }

      return serviceId;
   }


   private static final int MAX_ALLOWED_VALUES = 1000;

   private String getServicePredicate(Collection<Long> serviceIds) {

      String serviceIdCommaSeparated = "";
      int count = 0;
      for (Long serviceId:serviceIds) {
         if (count < MAX_ALLOWED_VALUES){
            serviceIdCommaSeparated += "" + serviceId;
            count++;
            if (count < serviceIds.size() && count < MAX_ALLOWED_VALUES) {
               serviceIdCommaSeparated += ",";
            }
         } else {
            break;
         }
      }
      LOGGER.debug("serviceIdCommaSeparated: {}", serviceIdCommaSeparated);
      return serviceIdCommaSeparated;
   }

   private String loginAs(String userName) {


      String accessToken = "eyJraWQiOiJXMm5IMGdVUm83MTNwcXArWkxMcVZRSHZQZHhHRnV3N0t1ZFVHNHdoT084PSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIza2o4ZTNkbXNqb3Y2bWExanNoNzdvdGxhNiIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoibG9naW4tYXNcL3Bvc3QiLCJhdXRoX3RpbWUiOjE3MjIyOTYxODcsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS1jZW50cmFsLTEuYW1hem9uYXdzLmNvbVwvZXUtY2VudHJhbC0xXzh0QXJCY2tnTiIsImV4cCI6MTcyMjI5OTc4NywiaWF0IjoxNzIyMjk2MTg3LCJ2ZXJzaW9uIjoyLCJqdGkiOiI2YTdlZjdmMS02MzIwLTQ1NmYtOGFhOS01OWRiZGIxYWY5ZWQiLCJjbGllbnRfaWQiOiIza2o4ZTNkbXNqb3Y2bWExanNoNzdvdGxhNiJ9.Vcuntk0SN-lfSQoZS3HQg16-sHOw0YNag1wETExNEUpqIO9J-sPhASXNcxw4E92DwG1eZ-Tmkhi7m4Q75u55lfMprVx3NfxNeGeWEGAlSCjVPg4TVJ7FnHLkP1qXAarWXZafGVPDzhsp2YXf1s_Iy8SeW5aeIF9i5K8KpW1UsmE0DSwqOFaokW4z6S4f9V_Ks-x0J27x8L0S5iGHzuf0BBBGsaUEgi4CFIcS98ahg80YPUhnZA863Daij7w0g82az0vYQrqoYuoNYZ5oFoxPPUhFsJO0EbXwdazgOD6IPzwZ1_HLYlpdzt9-IHlcYOqb9VUNzXF_GA44UGtZiVdDaA";

      String loginAsurl= "https://smart.prosegur.com/smart-server/ws/access/loginAs?user=\n" + userName;

      Client client = Client.create();

      WebResource webResource = client.resource(loginAsurl);
      WebResource.Builder builder =  webResource.getRequestBuilder();

      builder
            .header("accept", "application/json, text/plain, */*")
            .header("accept-language", "es-419,es;q=0.5")
      .header("content-type", "application/json;charset=UTF-8")
      .header("origin", "https://admin-smart.prosegur.cloud")
      .header("priority", "u=1, i")
      .header("referer", "https://admin-smart.prosegur.cloud/")
      .header("sec-ch-ua", "'Chromium';v='124', 'Brave';v='124', 'Not-A.Brand';v='99'")
      .header("sec-ch-ua-mobile","?0")
      .header("sec-ch-ua-platform", "Linux")
      .header("sec-fetch-dest","empty")
      .header("sec-fetch-mode", "cors")
      .header("sec-fetch-site","same-site")
      .header("sec-gpc", "1")
      .header("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36")
      .header("x-smart-token", accessToken)
      .header("Cookie", "_abck=656A6DF14770427BE93A153B7C981729~-1~YAAQZF0qyKQrzduPAQAA1nkm7wyP67pVPow0ishbyD4g3+CWomH8VuAPitb6F72TbT5B0TpYb6F0/bBDeuzTIRJoHdHIkMYibv0I0UJIXCG56XNla9qkn7zryl2c1foXemZ9oooYJDf7u6nTW8GfXRz24mLZXhAmwdX7fS5HCkYUiKIDhRetSJNQ2HnqefhfhDki2snqaVCfkhEUZn1lgAcOw7hHk/+y7I9kywSMpC74S2cHG3UrnKdtFOr9aoOpmt9kPEdKy0ysq8QVeCiPggmTKx2XD3dUNcgj2J5nnztcVC+ZndY6bHUkd9eohlYvFNODWDXcq/0Pjq0dJRjU3Be4APY4n2ZYaMN2Po4u0O7ZZCq59p/7OkjZB73pHUE=~0~-1~-1");

      MultivaluedMap formData = new MultivaluedMapImpl();

      formData.put("adminCountry", "ES");
      formData.put("adminUser", "ARX0001189");
      formData.put("isLoginAsAdmin", "true");
      formData.put("language", "es_ES");
      formData.put("platform", "SMART2");
      formData.put("origin", "Web");

      String data = "{ 'adminCountry': 'ES','adminUser': 'ARX0001189','isLoginAsAdmin': true, 'language': 'es_ES', 'platform': 'SMART2', 'origin': 'Web' }";

      ClientResponse response1 = webResource
            .post(ClientResponse.class, data);


      System.out.println(response1);

      String tokenResponse = response1.getEntity(String.class);


      return tokenResponse;
   }




   /*public String getCRMInstallation(String env){

      String start = "<h2> Installations from CRM </h2>\n\n" +
            "<table border='1'>\n\n" +
            "<tr width='100%'><td>\n";
      String end = "</td></tr>\n"+ "</table>";


      try {
         String host= getProperty(env + ".crm_host");


//#urlBase="/alarms/smart/rtf"
//#urlOauth="$/oauth/alarms/smart/rtf"

         String url= host + "/alarms/smart";

         String tokenUrl = host + "/api/oauth/token";
         String getInstallationUrl = url + "/v1/country/"+desCountry+"/client/"+desClientId+"/installation";

         Client client = Client.create();
         WebResource webResource1 = client.resource(tokenUrl);
         webResource1.header("Content-Type", "application/x-www-form-urlencoded");

         MultivaluedMap formData = new MultivaluedMapImpl();
         formData.add("scope", "alarms.write, alarms.read");
         formData.add("client_secret", getProperty(env + ".client_secret"));
         formData.add("client_id", getProperty(env + ".client_id"));
         formData.add("grant_type", "client_credentials");

         ClientResponse response1 = webResource1.post(ClientResponse.class, formData);
         String tokenResponse = response1.getEntity(String.class);
         String tokenValue = tokenResponse.substring(17,71);

         Client client2 = Client.create();

         //System.out.println("get Installation from crm: \n" + getInstallationUrl);

         WebResource webResource2 = client2.resource(getInstallationUrl);
         WebResource.Builder builder =  webResource2.getRequestBuilder();

         builder
               .header("authorization", "Bearer \n" + tokenValue)
               .header("Content-Type", "application/json")
               .header("ExtSys", "smart");

         ClientResponse response2 = builder.get(ClientResponse.class);
         String result = response2.getEntity(String.class);

         result = result.replaceAll("\n", "<br>\n");
         result = result.replaceAll(" ", "&nbsp;");

         String content = formatJson(result);
         content = content.replaceAll("<br><br>", "<br>");


         return  start + "<br><font color=\"blue\">\n" + getInstallationUrl + "</font><br>\n" + content + end;
      } catch (Exception e) {
         return start + e.getMessage() + end;
      }
   }*/


   protected AWSCognitoIdentityProviderClient identityUserPoolProviderClient = null;

   protected String getCognitoData(String env, String username){

      String jsonRet = "";

      try {

         String AWS_ACCESS_KEY = getProperty(env + ".aws_access_key");
         String AWS_SECRET_KEY = getProperty(env + ".aws_secret_key");

         String USER_POOL_REGION = getProperty(env + ".aws_user_pool_region");
         String USER_POOL_ID = getProperty(env + ".aws_user_pool_id");

         identityUserPoolProviderClient = new AWSCognitoIdentityProviderClient(new BasicAWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY)); // creds are loaded via variables that are supplied to my program dynamically
         identityUserPoolProviderClient.setRegion(RegionUtils.getRegion(USER_POOL_REGION)); // var loaded

         ListUsersRequest listUsersRequest = new ListUsersRequest();
         listUsersRequest.withUserPoolId(USER_POOL_ID); // id of the userpool, look this up in Cognito console
         listUsersRequest.withFilter("username = \"\n" + username + "\"");

         // get the results
         ListUsersResult result = identityUserPoolProviderClient.listUsers(listUsersRequest);

         List<UserType> userTypeList = result.getUsers();

         /*

         ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
         String json = ow.writeValueAsString(userTypeList.get(0));

         //System.out.println("---------");
         System.out.println(json);

         System.out.println("---------");*/

         if (userTypeList.size() > 0 ) {

            UserType userType = userTypeList.get(0);

            jsonRet = "{";
            jsonRet = jsonRet + " \"username\" : \n" + userType.getUsername() + ",\n";
            jsonRet = jsonRet + " \"userCreateDate\" : \n" + userType.getUserCreateDate() + ",\n";
            jsonRet = jsonRet + " \"userLastModifiedDate\" : \n" + userType.getUserLastModifiedDate() + ",\n";
            jsonRet = jsonRet + " \"enabled\" : \n" + userType.getEnabled() + ",\n";
            jsonRet = jsonRet + " \"userStatus\" : \n" + userType.getUserStatus() + ",\n";

            List<AttributeType> attributeList = userType.getAttributes();

            jsonRet = jsonRet + "[\n";

            int countAtt = 0;

            for (AttributeType attribute : attributeList) {
               countAtt++;
               String attName = attribute.getName();
               String attValue = attribute.getValue();

               jsonRet = jsonRet + "  {\"\n" + attName + "\" : \"\n" + attValue + "\"}";

               if (countAtt < attributeList.size()) {
                  jsonRet = jsonRet + ",\n";
               } else {
                  jsonRet = jsonRet + "\n";
               }
            }

            jsonRet = jsonRet + "]}";

         }

      } catch (AWSCognitoIdentityProviderException e) {
         printStackTrace(e);
      } catch (Exception e) {
         printStackTrace(e);

      }

      System.out.println(jsonRet);

      return jsonRet;

   }

}
