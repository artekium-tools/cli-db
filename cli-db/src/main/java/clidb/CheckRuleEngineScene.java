package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class CheckRuleEngineScene extends QryDB {

    private static final Logger LOGGER = LogManager.getLogger(CheckRuleEngineScene.class);
    static ArrayList<String> arr_sceneInconsistent = new ArrayList<>();

    public int findInconsistentSceneRuleEngine(String env, String datafixReportFile, String reportDate) {

        loadExternalFileProperties();
        DataSource dataSource = null;

        try {

            if (env == null) env = getProperty("env");
            String connectionType = getProperty("connectionType");

            String jdbcUrl = getProperty(connectionType + "." + env + ".url");
            String dbUsername = getProperty(env + ".username");
            String password = getProperty(env + ".password");

            dataSource = getDataSource(jdbcUrl, dbUsername, password);

            System.out.println("Executing step 1: check inconsistent Scene in rule engine ..." );
            checkSceneInconsistent(dataSource);

            System.out.println("Total record found: " + arr_sceneInconsistent.size());

            appendToFile(datafixReportFile,
                    "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-40876'>" +
                          "13) YH " +
                            "https://jira.prosegur.com/browse/AM-40876</a> - " +
                            "RULE-ENGINE - Desactivar escenas activas inconsistentes con schema scene<br>\n");

            if (arr_sceneInconsistent.size() > 0) {
                appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                        ", se fixearon " + arr_sceneInconsistent.size() + " casos. <br>\n");

                for (String cc: arr_sceneInconsistent){
                    appendToFile(datafixReportFile, cc + "<br>\n");
                }

            } else {
                appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                        ", no se encontraron casos a fixear<br>\n");
            }

        } catch (Exception e){
            printStackTrace(e);
        }

        return arr_sceneInconsistent.size();
    }

    public static void checkSceneInconsistent(final DataSource dataSource) {

        try {
            Connection con = dataSource.getConnection();
            Statement stmt = null;
            ResultSet rs = null;

            try {

                String sql1 = "SELECT SEQ_ID,SEQ_USER_ID FROM \"RULE-ENGINE\".SMPR_TSCENE rs " +
                        " WHERE rs.BOL_ACTIVE =1 AND NOT EXISTS " +
                        " (SELECT 1 FROM \"SCENE\".SMPR_TSCENE ss WHERE ss.BOL_ACTIVE =1 AND ss.seq_id = rs.SEQ_ID)";

                stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                rs = stmt.executeQuery(sql1);


                while (rs.next()) {

                    String deleteStmt = "UPDATE SMPR_TSCENE SET BOL_ACTIVE=0 WHERE SEQ_ID= " + rs.getLong(1) + ";";
                    appendToFile("deactivateInconsistentScene.sql",  deleteStmt + "\n");

                    arr_sceneInconsistent.add("sceneid ".concat(rs.getString(1).concat(" userid ").concat(rs.getString(2))));
                }

            } catch (Exception e) {
                System.out.println(" ");
                System.out.println(e.getMessage());
                printStackTrace(e);
            } finally {
                try {
                    if (rs != null) rs.close();
                    if (stmt != null) stmt.close();
                    if (con != null) con.close();
                } catch (Exception e) {
                    System.out.println(" ");
                    System.out.println(e.getMessage());
                    printStackTrace(e);
                }
            }

        } catch (Exception e1) {
            System.out.println(" ");
            System.out.println(e1.getMessage());
            e1.printStackTrace();
        }
    }
}
