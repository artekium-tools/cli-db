package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;


public class CheckMultisiteFlagToKeptUsers extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(CheckMultisiteFlagToKeptUsers.class);

   static int countCases = 0;

   public int main(String env, String datafixReportFile, String reportDate, String updateStatementFileName) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         appendToFile(datafixReportFile,
               "<br><a target='_blank' href='https://jira.prosegur.com/browse/AM-44752'>" +
                     "18) MB " +
                     "https://jira.prosegur.com/browse/AM-44752</a> - " +
                     "Corregir los usuarios dependientes que han quedado sin actualizar el flag multisede<br>\n");

         System.out.println("Executing step 1: check and generate update statements ..." );
         checkMultisiteFlag(dataSource, updateStatementFileName);

         if (countCases > 0) {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", se fixearon " + countCases + " casos. <br>\n");
         } else {
            appendToFile(datafixReportFile,  "Ejecucion del dia " + reportDate+
                  ", no se encontraron casos a fixear<br>\n");
         }



      } catch (Exception e){
         printStackTrace(e);
      }



      return countCases;
   }



   public void checkMultisiteFlag(final DataSource dataSource, String updateStatementFileName) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT " +
               "    'UPDATE SMPR_TUSER ' || " +
               "    'SET BOL_MULTISITE = ' || adm.BOL_MULTISITE || " +
               "    ' WHERE SEQ_ID = ' || dep.SEQ_ID AS update_statement " +
               "FROM " +
               "    \"ACCOUNT-MANAGEMENT\".SMPR_TUSER dep " +
               "JOIN " +
               "    \"ACCOUNT-MANAGEMENT\".SMPR_TUSER adm " +
               "ON " +
               "    dep.DES_COUNTRY = adm.DES_COUNTRY " +
               "    AND dep.DES_CLIENT_ID = adm.DES_CLIENT_ID " +
               "WHERE " +
               "    dep.BOL_ADMINISTRATOR = 0 " +
               "    AND adm.BOL_ADMINISTRATOR = 1 " +
               "    AND dep.BOL_MULTISITE <> adm.BOL_MULTISITE";


         //System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            boolean addSchemasLine = true;
            while (rs.next()) {
               if (addSchemasLine){
                  String userSchemas = "schemas:" + getProperty("replicas.user");
                  appendToFile(updateStatementFileName,  userSchemas + "\n");

                  addSchemasLine = false;
               }
               appendToFile(updateStatementFileName,    rs.getString(1) + ";\n");
               countCases++;
            }

            System.out.println("\nEnd statement generation .." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }

}
