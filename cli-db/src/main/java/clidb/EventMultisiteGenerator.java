package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


public class EventMultisiteGenerator extends QryDB {

   private static final Logger LOGGER = LogManager.getLogger(EventMultisiteGenerator.class);

   public synchronized void main() {
      loadExternalFileProperties();
      try {
         //UPDATE_IN_TEST_MODE = true;

         System.out.println("- EventGenerator for multisede@yopmail.com -");
         System.out.println("UPDATE_IN_TEST_MODE = " + UPDATE_IN_TEST_MODE);

         Scanner scanner1 = new Scanner(new InputStreamReader(System.in));
         System.out.println("Cantidad de eventos a generar: ");
         String totalStr = scanner1.nextLine();

         Scanner scanner2 = new Scanner(new InputStreamReader(System.in));
         System.out.println("Fecha desde de eventos a generar yyyy-MM-dd: ");
         String eventDate = scanner2.nextLine();

         Scanner scanner3 = new Scanner(new InputStreamReader(System.in));
         System.out.println("Cantidad de dias: ");
         String totalDaysStr = scanner3.nextLine();

         Scanner scanner4 = new Scanner(new InputStreamReader(System.in));
         System.out.println("delay en segundos: ");
         String delayStr = scanner4.nextLine();

         DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
         Date date = df.parse(eventDate);

         int totalEvents = Integer.valueOf(totalStr);
         int totalDays = Integer.valueOf(totalDaysStr);
         int delay = Integer.valueOf(delayStr);

         Scanner scanner5 = new Scanner(new InputStreamReader(System.in));
         System.out.println("Se van a generar " + totalEvents + " eventos en " + totalDays + " dias, " + totalEvents/totalDays + " x dia desde el " + eventDate + " confirma (y/n):");
         String confirm = scanner5.nextLine();

         if (!"y".equals(confirm)) return;

         String jdbcUrl = getProperty("telepresence.pre.url");
         String schema = "EVENT-PROCESSOR";
         String password =  getProperty("pre.EVENT-PROCESSOR." + "password");

         DataSource dataSource;
         dataSource = getDataSource(jdbcUrl, schema, password);
         int day = 1;
         int i = 1;
         int t = delay * 1000;



/*
       Cambié el enfoque: Solo voy a generar 10 tipos de eventos, no voy a intentar generar uno de cada uno.
       Encontré el bug por el cual se colgababa la DB de PRe

       Faltan generar 6 tipos y hacer pruebas en PRe para dar por cerrado el ticket y
       Luego cuando los necesite los genero on demand.

       Esto es poruqe me di cuenta que en el refactor, todos los seteos de datos los reemplazé
       en vez de sacarlos el "event" los tomo del "eventAdapter"


                1|AR - HECHO |Panel armado parcialmente [SOURCE] por Usuario
                2|AR - HECHO |Panel armado totalmente [SOURCE] por Usuario
                3|AR - HECHO |Panel desarmado [SOURCE] por Usuario
                4|AR  N/A    |Petición de imagen   (El usuario de PRE no tiene pircam
                5|AR  N/A    |Pedido de imágen erróneo
                6|AR   HECHO |Error en armado total
                7|AR   HECHO |Error en desarmado
                8|AR   HECHO |Error en armado parcial
               10|AR   N/A    |Salto de alarma en zona (Los paneles son genéricos)
               15|AR  - HECHO|Fallo línea de comunicación
 */

         while(true) {

            // 15 - Fallo de comunicacion
            System.out.println("event " + i +" of " + totalEvents + " day " + day);
            if (i == (totalEvents/totalDays * day)) day ++;
            insertEvent(dataSource, date, i, day -1, 15, "NULL", "30023", "");
            i++;
            if (i == totalEvents) break;
            System.out.println("waiting for: " + delay + " seconds");
            //this.waitFor(t);

            // 1 - Armado Parcial -  AP
            System.out.println("event " + i +" of " + totalEvents + " day " + day);
            if (i == (totalEvents/totalDays * day)) day ++;
            insertEvent(dataSource, date, i, day -1, 1, "'multisede@yopmail.com'", "30023", "30027");
            i++;
            if (i == totalEvents) break;
            System.out.println("waiting for: " + delay + " seconds");
            //this.waitFor(t);


            // 2 - Armmado Total - AT
            System.out.println("event " + i +" of " + totalEvents + " day " + day);
            if (i == (totalEvents/totalDays * day)) day ++;
            insertEvent(dataSource, date, i, day -1, 2, "'multisede@yopmail.com'", "30023", "30027");
            i++;
            if (i == totalEvents) break;
            System.out.println("waiting for: " + delay + " seconds");
            //this.waitFor(t);


            // 3 - Desarmado - DA
            System.out.println("event " + i +" of " + totalEvents + " day " + day);
            if (i == (totalEvents/totalDays * day)) day ++;
            insertEvent(dataSource, date, i, day -1, 3, "'multisede@yopmail.com'", "30023", "30027");
            i++;
            if (i == totalEvents) break;
            System.out.println("waiting for: " + delay + " seconds");
            //this.waitFor(t);

            // 6 - Error en armado total
            System.out.println("event " + i +" of " + totalEvents + " day " + day);
            if (i == (totalEvents/totalDays * day)) day ++;
            insertEvent(dataSource, date, i, day -1, 6, "'multisede@yopmail.com'", "30023", "30027");
            i++;
            if (i == totalEvents) break;
            System.out.println("waiting for: " + delay + " seconds");
            //this.waitFor(t);

            //7 - Error en desarmado                                                                                     |
            System.out.println("event " + i +" of " + totalEvents + " day " + day);
            if (i == (totalEvents/totalDays * day)) day ++;
            insertEvent(dataSource, date, i, day -1, 7, "'multisede@yopmail.com'", "30023", "30027");
            i++;
            if (i == totalEvents) break;
            System.out.println("waiting for: " + delay + " seconds");
            //this.waitFor(t);

            // 8 - Error en armado parcial
            System.out.println("event " + i +" of " + totalEvents + " day " + day);
            if (i == (totalEvents/totalDays * day)) day ++;
            insertEvent(dataSource, date, i, day -1, 8, "'multisede@yopmail.com'", "30023", "30027");
            i++;
            if (i == totalEvents) break;
            System.out.println("waiting for: " + delay + " seconds");
            //this.waitFor(t);



         }


         System.out.println("End process.");
      } catch(Exception e) {
         printStackTrace(e);
      }


   }


   public void insertEvent(DataSource dataSource, Date date, int secondToAdd, int dayToAdd, int seqEventTypeId, String desPanelUser, String parentThingId, String thingId) {

      /*
      *
      * INSERT INTO SMPR_TEVENT
(SEQ_ID, SEQ_PARENT_THING_ID, SEQ_EVENT_TYPE_ID, DES_TYPE, DES_MESSAGE, FYH_CREATION_DATE, FYH_EVENT_DATE, SEQ_USER_ID, SEQ_LOG_ID, BOL_SMART_EVENT, BOL_MASTERMIND_EVENT, BOL_EXTERNAL_PROVIDER_EVENT, BOL_DOMOTIC_EVENT, DES_DEVICE_NAME, DES_PANEL_USER, DES_CONTRACT, DES_PRIORITY, DES_COLOR, DES_EVENT_ID, DES_LOGIN_AS_USER, DES_SCENE_NAME, DES_SITE_NAME, DES_ZONE_ID, DES_AREA, COD_ACUDA_CASE, DES_COMMENTS, DES_DM_VARIABLE_NAME, DES_DM_VARIABLE_VALUE, SEQ_DEVICE_ID, DES_PARTITION_ID, DES_JOB_ID, BOL_INCLUDE_VIDEO, SEQ_INSTALLATION_ID, DES_USER, DES_USER_ALIAS, DES_PARTITION_NAME, SEQ_SCENE_ID, DES_COUNTRY, BOL_IS_FULL_PARTITION, SEQ_THING_ID)
VALUES ('dbf3218b-7d72-4af2-9528-8f618ab178d8', 30023, 15, NULL, ' ', TIMESTAMP '2024-06-14 17:51:44.613000', TIMESTAMP '2024-06-14 17:51:44.607000', NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 7822, NULL, NULL, NULL, NULL, 'ES', 0, NULL);
      *
      * */

      try {


         Calendar calendar = Calendar.getInstance();
         calendar.setTime(date);
         calendar.add(Calendar.SECOND, secondToAdd * 10);
         calendar.add(Calendar.DAY_OF_MONTH, dayToAdd);
         Date modifiedDate = calendar.getTime();

         SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
         String dateFormatted = formatter.format(modifiedDate);

         String sql = "INSERT INTO SMPR_TEVENT " +
               "(SEQ_ID, " +
               "SEQ_PARENT_THING_ID, " +
               "SEQ_EVENT_TYPE_ID, " +
               "DES_TYPE, " +
               "DES_MESSAGE, " +
               "FYH_CREATION_DATE, " +
               "FYH_EVENT_DATE, " +
               "SEQ_USER_ID, " +
               "SEQ_LOG_ID, " +
               "BOL_SMART_EVENT, " +
               "BOL_MASTERMIND_EVENT, " +
               "BOL_EXTERNAL_PROVIDER_EVENT" +
               ", BOL_DOMOTIC_EVENT" +
               ", DES_DEVICE_NAME" +
               ", DES_PANEL_USER, DES_CONTRACT, DES_PRIORITY, DES_COLOR, DES_EVENT_ID, DES_LOGIN_AS_USER, DES_SCENE_NAME, DES_SITE_NAME, DES_ZONE_ID, DES_AREA, COD_ACUDA_CASE, DES_COMMENTS, DES_DM_VARIABLE_NAME, DES_DM_VARIABLE_VALUE, SEQ_DEVICE_ID, DES_PARTITION_ID, DES_JOB_ID, BOL_INCLUDE_VIDEO, SEQ_INSTALLATION_ID, DES_USER, DES_USER_ALIAS, DES_PARTITION_NAME, SEQ_SCENE_ID, DES_COUNTRY, BOL_IS_FULL_PARTITION, SEQ_THING_ID) " +
               "VALUES " +
               "('" + UUID.randomUUID().toString() + "'" +
               ", " + parentThingId +
               ", "+ seqEventTypeId +"" +
               ", NULL" +
               ", ' '" +
               ", TIMESTAMP '" + dateFormatted +".613000'" +
               ", TIMESTAMP '"+ dateFormatted +".607000'" +
               // TODO acá va el userId
               ", " + parentThingId +
               ", NULL" +
               ", 1" +
               ", 0" +
               ", 0" +
               ", 0" +
               ", NULL, " +
                desPanelUser +
               ", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0" +
               ", 7822, NULL, NULL, NULL, NULL, 'ES', 0" +
               ", " + thingId +")";

         //System.out.println(dateFormatted);
         //System.out.println(sql);
         //System.out.println("## ----------------------------------------------------------------");
         updateBatch(dataSource, sql);


      } catch (Exception e) {
         printStackTrace(e);
      }

   }


   public static void waitFor(int ms) {
      try
      {
         Thread.sleep(ms);
      }
      catch(InterruptedException ex)
      {
         Thread.currentThread().interrupt();
      }
   }

}
