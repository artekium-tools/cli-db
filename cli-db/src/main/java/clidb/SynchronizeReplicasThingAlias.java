package clidb;

import clidb.entities.ThingAlias;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;


public class SynchronizeReplicasThingAlias extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(SynchronizeReplicasThingAlias.class);

/*
1 1.000.000
1.000.001 2.000.000
2.000.001 3.000.000
3.000.001 4.000.000
4.000.001 5.000.000
5.000.001 6.000.000
6.000.001 7.271.994

 */
   public synchronized void checkThingAlias(String env, String schema) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         Scanner scanner2 = new Scanner(new InputStreamReader(System.in));

         System.out.println("counting thingAlias ...");
         int total = countThingAlias(dataSource);

        /* HashMap recRange = new HashMap<Integer, Integer[]>();

         recRange.put(1, new Integer[]{1, 1000000});
         recRange.put(2, new Integer[]{1000001, 2000000});
         recRange.put(3, new Integer[]{2000001, 3000000});
         recRange.put(4, new Integer[]{3000001, 4000000});
         recRange.put(5, new Integer[]{4000001, 5000000});
         recRange.put(6, new Integer[]{5000001, 6000000});
         recRange.put(7, new Integer[]{6000001, total});

         System.out.println("1) -> 1 to 1.000.000");
         System.out.println("2) -> 1.000.001 to 2.000.000");
         System.out.println("3) -> 2.000.001 to 3.000.000");
         System.out.println("4) -> 3.000.001 to 4.000.000");
         System.out.println("5) -> 4.000.001 to 5.000.000");
         System.out.println("6) -> 5.000.001 to 6.000.000");
         System.out.println("7) -> 6.000.001 to " + total);
         System.out.println("Select record range: ");

         String recRangeStr = scanner2.nextLine();
         int recRangeSelected = 1;

         recRangeSelected = Integer.valueOf(recRangeStr);*/

         String insertUpdateFileName = "insertUpdateThingAlias.sql";

         //Integer[] fromTo = (Integer[]) recRange.get(recRangeSelected);

         //System.out.println("Start record - CHECK selected range (default= " + fromTo[0]+ "): ");
         //String startRecStr = scanner2.nextLine();
         //System.out.println(" ");

         //System.out.println("Wait time in seconds: (default=60): ");
         //String waitTimeStr = scanner2.nextLine();
         //System.out.println(" ");

         //int waitTimeInSeconds;

         /*try {
            waitTimeInSeconds = Integer.valueOf(waitTimeStr);
         } catch (Exception e) {
            waitTimeInSeconds = 60;
         }*/

         System.out.println("Start record (default=1): ");
         //Scanner scanner2 = new Scanner(new InputStreamReader(System.in));
         String startRecStr = scanner2.nextLine();
         System.out.println(" ");

         System.out.println("Wait time in seconds: (default=60): ");
         String waitTimeStr = scanner2.nextLine();
         System.out.println(" ");

         int waitTimeInSeconds;

         try {
            waitTimeInSeconds = Integer.valueOf(waitTimeStr);
         } catch (Exception e) {
            waitTimeInSeconds = 60;
         }

         int from = 1;

         try {
            from = Integer.valueOf(startRecStr);
         } catch (Exception e) {
            from = 1;
         }

         int chunk = 999;
         int to = from + chunk;

         //int lastRecNum = fromTo[1];
         int recNum = from;

         System.out.println("\nStart processing " + insertUpdateFileName + " from: " + from + " to: " + to + " until: " + total);

         while (true) {

            System.out.println("\n loading Owner ThingAlias from: " + from + " to: " + to + " of " + total);

            ArrayList<ThingAlias> arrThingAlias = selectThingAlias(dataSource, schema, from, to);

            System.out.println("\n ThingAlias loaded: " + arrThingAlias.size());

            for (ThingAlias thingAlias: arrThingAlias) {

               ThingAlias replica = getThingAliasReplica(dataSource, schema, thingAlias.getId());

               if (replica == null) {
                  printInLine("record: " + recNum + " INSERT for seqId: " + thingAlias.getId());
                  try {
                     String sqlInsert = thingAlias.getInsertStatement(schema);
                     appendToFile(insertUpdateFileName, sqlInsert + ";\n");
                  } catch (Exception e){
                     LOGGER.error("error generating INSERT for seqId: {} - {} ", thingAlias.getId(), e.getMessage());
                  }

               } else {
                  if (!thingAlias.equals(schema, replica)) {
                     printInLine("record: " + recNum + " UPDATE for seqId: " + thingAlias.getId());
                     try {
                        String sqlUpdate = thingAlias.getUpdateStatement(schema);
                        appendToFile(insertUpdateFileName, sqlUpdate + ";\n");
                     } catch (Exception e){
                        LOGGER.error("error generating UPDATE for seqId: {} - {} ", thingAlias.getId(), e.getMessage());
                     }
                  } else {
                     printInLine("record: " + recNum + " EQUALS for seqId: " + thingAlias.getId());
                  }
               }

              /* if (recNum == lastRecNum){
                  System.out.println("\n End, record processed : " + lastRecNum);
                  break;
               }*/

               recNum ++;
            }

            if (recNum == total){
               break;
            }

            from = to + 1;
            to = from + chunk;

            wait(waitTimeInSeconds * 1000);
         }


      } catch (Exception e){
         printStackTrace(e);
      }


   }

   public static int countThingAlias(final DataSource dataSource) {

      int count = 0;
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql = "SELECT COUNT(1) " +
                  "FROM  \"DEVICE\".SMPR_TTHING_ALIAS thingAlias " +
                  // Filter applied olny for FILTER-DISPATCHER records
                  "WHERE thingAlias.SEQ_THING_ID IN " +
                  "(SELECT seq_id FROM \"DEVICE\".SMPR_TTHING WHERE SEQ_ENTITY_TYPE IN (2,16,24,25)) " ;

            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
               count = rs.getInt(1);
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return count;

   }

   public static ArrayList<ThingAlias> selectThingAlias(final DataSource dataSource, String schema, int from, int to) {

      ArrayList<ThingAlias> arrThingAlias = new ArrayList<>();

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql = "SELECT c.* " +
            "FROM (SELECT c.*, ROWNUM as rnum " +
            "      FROM ( " +
            "            SELECT SEQ_ID, SEQ_INSTALLATION_ALIAS_ID, SEQ_PARENT_ID, SEQ_USER_ID, SEQ_THING_ID, DES_NAME " +
            "            FROM  \"DEVICE\".SMPR_TTHING_ALIAS thingAlias " +
            // Filter applied olny for FILTER-DISPATCHER records
            "             WHERE thingAlias.SEQ_THING_ID IN " +
            "                    (SELECT seq_id FROM \"DEVICE\".SMPR_TTHING WHERE SEQ_ENTITY_TYPE IN (2,16,24,25)) " +
            "            ORDER BY thingAlias.SEQ_ID " +
            "      ) c) c " +
            "WHERE c.rnum BETWEEN " + from + " AND " + to;

            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
               ThingAlias thingAlias = new ThingAlias(schema, rs);
               arrThingAlias.add(thingAlias);
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return arrThingAlias;

   }


   public static ThingAlias getThingAliasReplica(final DataSource dataSource, String schema, Long seqId) {
      ThingAlias replica = null;
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql =
                  "SELECT SEQ_ID, SEQ_INSTALLATION_ALIAS_ID, SEQ_PARENT_ID, SEQ_USER_ID, SEQ_THING_ID, DES_NAME " +
                  "FROM  \""+ schema +"\".SMPR_TTHING_ALIAS thingAlias " +
                  "WHERE thingAlias.SEQ_ID = " + seqId;

            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
               replica = new ThingAlias(schema, rs);
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return replica;

   }

}
