package clidb;


import java.text.SimpleDateFormat;
import java.util.Date;

public class ChronoMeter extends QryDB {

   Long start, end;
   String measure;

   public void start() {
      start = Math.abs(System.currentTimeMillis());
   }

   public void stop(){
      end = Math.abs(System.currentTimeMillis());
   }

   public String getStartTime(){
      SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SS");
      Date resultdate = new Date(start);
      return sdf.format(resultdate);
   }

   public String getEndTime(){
      SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SS");
      Date resultdate = new Date(end);
      return sdf.format(resultdate);
   }

   public String getElapseTime(){
      String prefix = measure != null? measure + " - ": "";
      return prefix + (new SimpleDateFormat("mm:ss:SSS")).format(new Date(end - start));
   }

   public Long getElapseTimeLong(){
      return end - start;
   }

   public Long getElapseTimeFromStart(){
      return Math.abs(System.currentTimeMillis()) - start;
   }


   public Long getElapseTimeFromStartPlusPause(Long pause){
      return Math.abs(System.currentTimeMillis()) + pause - start;
   }

   public void setMeasure(String measure) {
      this.measure = measure;
   }
}
