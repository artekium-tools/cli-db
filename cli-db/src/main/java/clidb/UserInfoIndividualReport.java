package clidb;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClient;
import com.amazonaws.services.cognitoidp.model.*;
import com.amazonaws.util.StringUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import javax.ws.rs.core.MultivaluedMap;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class UserInfoIndividualReport extends QryDB {


   private  final Logger LOGGER = LogManager.getLogger(UserInfoIndividualReport.class);

   int maxEvents = 50;

   private Long seq_user_id;
   private int isAdministrator;
   private String desClientId;
   private String desCountry;
   private ArrayList<Long> arr_inst_alias_id = new ArrayList();
   private ArrayList<Long> arr_installation_id = new ArrayList();
   private ArrayList<Long> arr_ext_services_id = new ArrayList();
   private Long seq_panel_id;
   private Long seq_installation_id;
   private Long seq_smart_id;
   private Long seq_bath_id;
   private boolean objectTrackerInfo = false;
   private boolean withReplicas = false;
   protected AWSCognitoIdentityProviderClient identityUserPoolProviderClient = null;


   public String generateIndividualUserReport(DataSource dataSource, String env, String userName, String userEnrollInfo, String userAtEnrollInfo) {

      String content = "";
      try {
         arr_inst_alias_id = new ArrayList();
         arr_ext_services_id = new ArrayList();
         arr_installation_id  = new ArrayList();
         seq_bath_id = null;
         seq_smart_id = null;
         //desClientId = null;
         //desCountry = null;

         String isAdmin = "";
         String userInfo = "";
         String migrationData = "";
         String migrationObjectData = "";
         String auditoryData = "";
         String terminals = "";
         String dashboard = "";
         String dependents = "";
         String adminForDependent = "";
         String installation = "";
         String instAlias = "";
         String thingAlias = "";
         String thingTree = "";
         String panels = "";
         String events = "";
         String externaServices = "";
         String installationCRMData = "";
         String cognitoData = "";
         String installationData = "";


         cognitoData =  "<br><a target='_self' href='#' onclick=\"showHide('cognitoData');return false;\"> <b>&gt;&gt;&nbsp;&nbsp; Cognito Data</b>  </a>";
         cognitoData += "<div style=\"padding: 50px\" id='cognitoData'>";
         //cognitoData += getCognitoData(env, userName);
         cognitoData += "</div>";


         String schema = "\"ACCOUNT-MANAGEMENT\".";

         userInfo = getUserInfo(dataSource, userName, schema);

         if (isAdministrator == 1) {
            dependents = getDependents(dataSource, schema);
            isAdmin = " ** Administrator **";

            installationData = getInstallations(dataSource, schema);
         } else {
            isAdmin = " ** Dependent **";

            adminForDependent = getAdminForDependent(dataSource, schema);

            installationData ="<h2> Installations: Is a dependent user.</h2>\n";
         }

         installationCRMData =  "<br><a target='_self' href='#' onclick=\"showHide('installationCRMData');return false;\"> <b>&gt;&gt;&nbsp;&nbsp; Installations from CRM</b>  </a>";
         installationCRMData += "<div style=\"padding: 50px\" id='installationCRMData'>";
         installationCRMData += getCRMInstallation(env);
         installationCRMData += "</div>";

         getInstAliasId(dataSource, schema);

         schema = "\"MIGRATION-ORCHESTRATOR\".";
         if (isAdministrator == 1) {

            migrationData = getMigrationData4Admin(dataSource, schema);
         } else {
            //migrationData = "TODO";
         }

         migrationObjectData = getMigrationObjectData(dataSource, schema);

         schema = "\"AUDITORY\".";

         auditoryData = getAuditoryData(dataSource, schema);

         schema = "\"NOTIFICATION\".";

         terminals = getTerminals(dataSource, schema);

         // By installation alias
         int order = 1;
         for (Long seq_inst_alias_id : arr_inst_alias_id) {

            schema = "\"ACCOUNT-MANAGEMENT\".";

            instAlias = getInstallationAlias(dataSource, seq_inst_alias_id, schema, order);

            installation = getInstallation(dataSource, seq_installation_id, schema);

            dashboard = getDashboardWidget(dataSource, seq_installation_id, schema);

            schema = "\"DEVICE\".";

            panels = getPanels(dataSource, seq_installation_id, schema);

            thingAlias = getThingAlias(dataSource, seq_inst_alias_id, schema);

            thingTree = getThingTree(dataSource, seq_installation_id, schema);

            schema = "\"EVENT-PROCESSOR\".";

            events = getEvents(dataSource, schema, seq_inst_alias_id);

            installationData += instAlias;
            installationData += "<br><a target='_self' href='#' onclick=\"showHide('inastAlias" + seq_inst_alias_id + "');return false;\"> <b>&gt;&gt;&nbsp;&nbsp; Installation, Things & ThingAlias </b> </a>";
            installationData += "<div style=\"padding: 50px\" id='inastAlias" + seq_inst_alias_id + "'>";
            installationData += installation;
            installationData += dashboard;
            installationData += panels;
            installationData += thingAlias;
            installationData += thingTree;
            installationData += "<br><a target='_self' href='#' onclick=\"showHide('events" + seq_inst_alias_id + "');return false;\"> <b>&gt;&gt;&nbsp;&nbsp; Events</b>  </a>";
            installationData += "<div style=\"padding: 50px\" id='events" + seq_inst_alias_id + "'>";
            installationData += events;
            installationData += "</div>";

            installationData += "</div>";

            order ++;
         }

         String userReplicaData = "";
         String installationReplicaData = "";
         String installationAliasReplicaData = "";
         String thingAliasReplicaData = "";
         String thingReplicaData = "";

         if (withReplicas) {
            printInLine("getReplicas...                                              ");
            getInstallationId(dataSource);
            installationData += "<br><br><a target='_self' href='#' onclick=\"showHide('replicas01');return false;\"> <b>&gt;&gt;&nbsp;&nbsp; Replicas</b>  </a>";
            installationData += "<div style=\"padding: 50px\" id='replicas01'>";

            // replicas.user ----------------------------------------------------
            String userSchemas = getProperty("replicas.user");
            String[] arrUserSchemas = userSchemas.split(",");
            for (String schemaUser : arrUserSchemas) {
               userReplicaData += getUserReplicas(dataSource, "\"" + schemaUser + "\".");
            }

            installationData += "<br><br><a target='_self' href='#' onclick=\"showHide('userReplicas01');return false;\"> <b>&gt;&gt;&nbsp;&nbsp; tUser</b>  </a>";
            installationData += "<div style=\"padding: 50px\" id='userReplicas01'>";
            installationData += userReplicaData;
            installationData += "</div>";

            // replicas.installation ----------------------------------------------------
            String installationSchemas = getProperty("replicas.installation");
            String[] arrInstallationSchemas = installationSchemas.split(",");
            for (String schemaInstallation : arrInstallationSchemas) {
               installationReplicaData += getInstallationReplicas(dataSource, "\"" + schemaInstallation + "\".");
            }

            installationData += "<br><br><a target='_self' href='#' onclick=\"showHide('installationReplicas01');return false;\"> <b>&gt;&gt;&nbsp;&nbsp; tInstallation</b>  </a>";
            installationData += "<div style=\"padding: 50px\" id='installationReplicas01'>";
            installationData += installationReplicaData;
            installationData += "</div>";

            // replicas.installationAias ----------------------------------------------------
            String installationAliasSchemas = getProperty("replicas.installationAias");
            String[] arrInstallationAliasSchemas = installationAliasSchemas.split(",");
            for (String schemaInstallationAlias : arrInstallationAliasSchemas) {
               installationAliasReplicaData += getInstallationAliasReplicas(dataSource, "\"" + schemaInstallationAlias + "\".");
            }

            installationData += "<br><br><a target='_self' href='#' onclick=\"showHide('installationAliasReplicas01');return false;\"> <b>&gt;&gt;&nbsp;&nbsp; tInstallationAlias</b>  </a>";
            installationData += "<div style=\"padding: 50px\" id='installationAliasReplicas01'>";
            installationData += installationAliasReplicaData;
            installationData += "</div>";

            // replicas.thing ----------------------------------------------------
            String thingSchemas = getProperty("replicas.thing");
            String[] arrThingSchemas = thingSchemas.split(",");

            for (String schemaThing : arrThingSchemas) {
               thingReplicaData += "<h2> Thing replicas on: " + schemaThing +" </h2>\n";
               for (Long installationId:arr_installation_id){
                  thingReplicaData += getThingReplicas(dataSource, "\"" + schemaThing + "\".", installationId);
               }
            }

            installationData += "<br><br><a target='_self' href='#' onclick=\"showHide('thingReplicas01');return false;\"> <b>&gt;&gt;&nbsp;&nbsp; tThing</b>  </a>";
            installationData += "<div style=\"padding: 50px\" id='thingReplicas01'>";
            installationData += thingReplicaData;
            installationData += "</div>";

            // replicas.thingAlias ----------------------------------------------------
            String thingAliasSchemas = getProperty("replicas.thingAlias");
            String[] arrThingAliasSchemas = thingAliasSchemas.split(",");
            for (String schemaThingAlias : arrThingAliasSchemas) {
               thingAliasReplicaData += "<h2> ThingAlias replicas on: " + schemaThingAlias +" </h2>\n";
               for (Long instAliasId:arr_inst_alias_id){
                  thingAliasReplicaData += getThingAliasReplicas(dataSource, "\"" + schemaThingAlias + "\".", instAliasId);
               }
            }

            installationData += "<br><br><a target='_self' href='#' onclick=\"showHide('thingAliasReplicas01');return false;\"> <b>&gt;&gt;&nbsp;&nbsp; tThingAlias</b>  </a>";
            installationData += "<div style=\"padding: 50px\" id='thingAliasReplicas01'>";
            installationData += thingAliasReplicaData;
            installationData += "</div>";

            installationData += "</div>";

         }

         schema = "\"ACCOUNT-SYNC\".";

         externaServices = getExternalServices(dataSource, schema);

         String externaSevicesData = "";

         if (!externaServices.contains("Empty Result")) {
            externaSevicesData += "<br><a target='_self' href='#' onclick=\"showHide('externalServices01');return false;\"> <b>&gt;&gt;&nbsp;&nbsp; External Services</b>  </a>";
            externaSevicesData += "<div style=\"padding: 50px\" id='externalServices01'>";
            for (Long siebelCod : arr_ext_services_id) {

               if (siebelCod == 24) {
                  schema = "\"CAMERAMANAGER\".";
                  externaSevicesData += getCMSessionMInfo(dataSource, userName, schema);
                  externaSevicesData += getCMTThirdPartySession(dataSource, userName, schema);
                  externaSevicesData += getCMPrivacyMode(dataSource);

               }

               if (siebelCod == 27) {
                  schema = "\"PANIC-BUTTON\".";
                  externaSevicesData += getPBInfo(dataSource, schema);
               }

            }
            externaSevicesData += "</div>";
         }

         schema = "\"SCENE\".";

         String scenesData = "<br><a target='_self' href='#' onclick=\"showHide('scenes');return false;\"> <b>&gt;&gt;&nbsp;&nbsp; Scenes</b>  </a>";
         scenesData += "<div style=\"padding: 50px\" id='scenes'>";

         printInLine("getScenes...                                              ");
         scenesData += getScenes(dataSource, schema);

         scenesData += getScenesAction(dataSource, schema);

         scenesData += getScenesTrigger(dataSource, schema);

         scenesData += "</div>";

         String init = "<h1> Information for: " + userName + isAdmin + " </h1>\n " +
               "<script>\n" +
               "\tfunction showHide(divId) {\n" +
               "\t  var x = document.getElementById(divId);\n" +
               "\t  if (x.style.display === \"none\") {\n" +
               "\t    x.style.display = \"block\";\n" +
               "\t  } else {\n" +
               "\t    x.style.display = \"none\";\n" +
               "\t  }\n" +
               "\t}\n\n" +
               "\tfunction openWindow(text) { \n" +
               "\t   var newtab = window.open(\"\", \"Record\", \"width=500,height=700\"); \n" +
               "\t   newtab.document.write(text); \n" +
               "\t}\n" +
               "</script>";


         content = init +
               userEnrollInfo +
               //cognitoData +
               userAtEnrollInfo +
               userInfo +
               adminForDependent +
               migrationData +
               migrationObjectData +
               auditoryData +
               dependents +
               terminals +
               installationCRMData +
               installationData +
               externaServices +
               externaSevicesData +
               scenesData;

      } catch (Exception e) {
         printStackTrace(e);
      }

      return content;

   }


   public String getDesClientId() {
      return desClientId;
   }

   public void setDesClientId(String desClientId) {
      this.desClientId = desClientId;
   }

   public String getDesCountry() {
      return desCountry;
   }

   public void setDesCountry(String desCountry) {
      this.desCountry = desCountry;
   }

   public boolean isObjectTrackerInfo() {
      return objectTrackerInfo;
   }

   public void setObjectTrackerInfo(boolean objectTrackerInfo) {
      this.objectTrackerInfo = objectTrackerInfo;
   }

   public boolean isWithReplicas() {
      return withReplicas;
   }

   public void setWithReplicas(boolean withReplicas) {
      this.withReplicas = withReplicas;
   }

   public  int selectToInfoCount(final DataSource dataSource, String sqlCount) {

      int numRows = 0;
      try {
         Connection con = dataSource.getConnection();
         Statement countStmt = null;
         ResultSet countRs = null;

         try {

            //System.out.println("sqlCount: " + sqlCount);

            countStmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            countRs = countStmt.executeQuery(sqlCount);

            countRs.next();
            numRows = countRs.getInt(1);

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            //printStackTrace(e);
         } finally {
            try {
               if (countRs != null) countRs.close();
               if (countStmt != null) countStmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               //printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         //e1.printStackTrace();
      }

      return numRows;
   }

   public  String selectToInfo(final DataSource dataSource, String sql, String tableName) {
      return selectToInfo(dataSource, sql, tableName, "orange");
   }

   public  String selectToInfo(final DataSource dataSource, String sql, String tableName, String color) {
      String row = "";
      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql);

            //System.out.println("------------------------------------------------");
            //System.out.println(sql);

            int columns = 0;
            int rowCount = 0;
            if (rs.next()) {
               ResultSetMetaData rsmd = rs.getMetaData();

               row = row + "<th bgcolor='" + color + "'>&nbsp;</th>\n";

               for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                  columns++;
                  if ("tEvents".equals(tableName) &&
                        ("SEQ_ID".equals(rsmd.getColumnName(i)) || rsmd.getColumnName(i).equals("DES_TEXT"))) {
                     row = row + "<th bgcolor='" + color + "'>" + rsmd.getColumnName(i) +
                           "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>\n";
                  } else {
                     row = row + "<th bgcolor='" + color + "'>" + rsmd.getColumnName(i) + "</th>\n";
                  }
               }

               if (rs.last()) rs.beforeFirst();


               String recordStart = "<table border=1>" +
                     "<tr>" +
                     "<th bgcolor=" + color + ">Column</th><th bgcolor=" + color + ">Value</th>" +
                     "</tr>";
               String recordEnd = "</table><br><button onclick=self.close()>Close</button>";

               String recordContent;

               // Get row values to show
               while (rs.next()) {
                  row = row + "<tr>";

                  recordContent = "";

                  for (int i = 1; i <= rsmd.getColumnCount(); i++) {

                     recordContent = recordContent + "<tr><td>" + rsmd.getColumnName(i).toUpperCase() + "</td>" +
                           "<td>" + rs.getString(rsmd.getColumnName(i)) + "</td></tr>";
                  }

                  // Celda para ver registro en vertical
                  row = row + "<td> <button onclick=\"openWindow('" +  recordStart + recordContent + recordEnd + "')\"> # </button></td>";


                  for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                     if ("SEQ_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER".equals(tableName)) {
                        seq_user_id = rs.getLong(rsmd.getColumnName(i));
                     }

                     if ("BOL_ADMINISTRATOR".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER".equals(tableName)) {
                        isAdministrator = rs.getInt(rsmd.getColumnName(i));
                     }

                     if ("DES_CLIENT_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER_ENROLLMENT".equals(tableName)) {
                        desClientId = rs.getString(rsmd.getColumnName(i));
                     }
                     if ("DES_COUNTRY_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER_ENROLLMENT".equals(tableName)) {
                        desCountry = rs.getString(rsmd.getColumnName(i));
                     }

                     if ("SEQ_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "InstallationAlias".equals(tableName)) {
                        arr_inst_alias_id.add(rs.getLong(rsmd.getColumnName(i)));
                     }

                     if ("SEQ_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "Panels".equals(tableName)) {
                        seq_panel_id = (rs.getLong(rsmd.getColumnName(i)));
                     }

                     if ("SEQ_INSTALLATION_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SetInstallationId".equals(tableName)) {
                        seq_installation_id = (rs.getLong(rsmd.getColumnName(i)));
                     }

                     if ("SEQ_INSTALLATION_ID".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "getInstallationId".equals(tableName)) {
                        arr_installation_id.add(rs.getLong(rsmd.getColumnName(i)));
                     }

                     if ("DES_SIEBEL_CODE".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "SMPR_TUSER_EXTERNAL_SERVICE".equals(tableName)) {
                        arr_ext_services_id.add(rs.getLong(rsmd.getColumnName(i)));
                     }

                     if ("tAccount".equals(tableName) &&
                           "DES_MIGRATION_STATUS".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           "OK".equals(rs.getString(rsmd.getColumnName(i)))
                     ) {
                        seq_smart_id = rs.getLong("SEQ_SMART_USER_ID");
                        seq_bath_id = rs.getLong("SEQ_BATCH_PROCESS_STATE_ID");
                     }

                     // Pintamos el fondo si tiene fecha de borrado
                     if ("FYH_DELETED".equals(rsmd.getColumnName(i).toUpperCase()) &&
                           !StringUtils.isNullOrEmpty(rs.getString(rsmd.getColumnName(i)))){
                        row = row + "<td bgcolor='orange'>" + rs.getString(rsmd.getColumnName(i)) + "</td>";
                        // Pintamos el fondo de fecha de ultimo acceso
                     } else if ("FYH_LAST_ACCESS".equals(rsmd.getColumnName(i).toUpperCase())){
                        row = row + "<td bgcolor='green'>" + rs.getString(rsmd.getColumnName(i)) + "</td>";
                        // resto de las celdas
                     } else {
                        row = row + "<td>" + rs.getString(rsmd.getColumnName(i)) + "</td>";
                     }

                  }

                  row = row + "</tr>\n";
                  rowCount ++;
               }

               row = row + "<tr><td colspan='2'> <b>" + rowCount + " rows.</b> </td>" +
                     "<td colspan='" + (columns -1 ) +"'>" + sql + " </td></tr>\n";

            } else {
               row = row + "<th> Empty Result </th>\n";
               row = row + "<tr><td>" + sql + " </td></tr>\n";
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return row;
   }


   public String getCMSessionMInfo(DataSource dataSource, String userName, String schema) {

      String start = "<h2> Camera Manager Session | cameramanager- SMPR_TCAMERAMANAGER_SESSION </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT * FROM " + schema + "SMPR_TCAMERAMANAGER_SESSION WHERE upper (COD_USER) = upper( '" + userName + "')";

         body = selectToInfo(dataSource, sql, "SMPR_TUSER_EXTERNAL_SERVICE", "blue");

         try {
            dataSource.getConnection().close();
            ////System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   public String getCMTThirdPartySession(DataSource dataSource, String userName, String schema) {

      String start = "<h2> Camera Manager - ThirdPartySession Session | cameramanager- SMPR_TTHIRDPARTYSESSION </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT * FROM " + schema + "SMPR_TTHIRDPARTYSESSION WHERE upper (COD_USER) = upper( '" + userName + "')";

         body = selectToInfo(dataSource, sql, "SMPR_TUSER_EXTERNAL_SERVICE", "orange");

         try {
            dataSource.getConnection().close();
            ////System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   public String getCMPrivacyMode(DataSource dataSource) {

      String start = "<h2> Camera Manager - Privacy Mode | rule-engine - SMPR_TPRIVACY_MODE </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT * FROM \"RULE-ENGINE\".SMPR_TPRIVACY_MODE WHERE SEQ_PARENT_THING_ID IN ( " +
               "SELECT SEQ_ID FROM \"DEVICE\".SMPR_TTHING WHERE SEQ_INSTALLATION_ID in (" + getInstallationIdCommaSeparated()  + ") AND SEQ_TYPE_ID = 20)";

         body = selectToInfo(dataSource, sql, "SMPR_TUSER_EXTERNAL_SERVICE", "yellow");

         try {
            dataSource.getConnection().close();
            ////System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   public String getPBInfo(DataSource dataSource, String schema) {

      String start = "<h2> Panic Button User | panic-button - SMPR_TPANIC_BUTTON_USER </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT * FROM " + schema + "SMPR_TPANIC_BUTTON_USER WHERE seq_user_id  = " + seq_user_id;

         body = selectToInfo(dataSource, sql, "SMPR_TPANIC_BUTTON_USER", "orange");

         try {
            dataSource.getConnection().close();
            ////System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getUserInfo(DataSource dataSource, String userName, String schema) {

      printInLine("getUserInfo...                                        ");
      String start = "<h2> User | account-management - smpr_tuser </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TUSER where upper (DES_NICK_NAME) = upper ('" + userName + "')";

         body = selectToInfo(dataSource, sql, "SMPR_TUSER");

         try {
            dataSource.getConnection().close();
            ////System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getDependents(DataSource dataSource, String schema) {
      printInLine("getDependents...                                        ");
      String start = "<h2> User Dependents | account-management - smpr_tuser </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TUSER WHERE BOL_ADMINISTRATOR = 0 AND " +
               "DES_CLIENT_ID = '" + desClientId + "' AND DES_COUNTRY = '" + desCountry + "' ";


         body = selectToInfo(dataSource, sql, "Dependents", "LightSkyBlue");

         try {
            dataSource.getConnection().close();
            ////System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getAdminForDependent(DataSource dataSource, String schema) {

      printInLine("getAdminForDependent...                                        ");
      String start = "<h2> User Admin for it dependent | account-management - smpr_tuser </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TUSER WHERE BOL_ADMINISTRATOR = 1 AND " +
               "DES_CLIENT_ID = '" + desClientId + "' AND DES_COUNTRY = '" + desCountry + "' ";

         body = selectToInfo(dataSource, sql, "Dependents", "Green");

         try {
            dataSource.getConnection().close();
            ////System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getMigrationData4Admin(DataSource dataSource, String schema) {
      printInLine("getMigrationData4Admin...                                        ");
      String start = "<h2> Migration data | MIGRATION-ORCHESTRATOR  - SMPR_TACCOUNT </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TACCOUNT WHERE DES_CLIENT_ID = '" + desClientId +
               "' AND DES_COUNTRY = '" + desCountry + "' ORDER BY SEQ_ID DESC";

         body = selectToInfo(dataSource, sql, "tAccount", "red");

         try {
            dataSource.getConnection().close();
            ////System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }


   private String getMigrationObjectData(DataSource dataSource, String schema) {
      printInLine("getMigrationObjectData...                                        ");
      String start = "<h2> Migration Object Tracker data | MIGRATION-ORCHESTRATOR  - SMPR_TOBJECT_TRACKER (without EVENTS) </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      String sql = "SELECT * FROM " + schema + "SMPR_TOBJECT_TRACKER " +
            "WHERE DES_TABLE_NAME != 'SMPR_TEVENT' " +
            " AND SEQ_BATCH_PROCESS_STATE_ID = " + seq_bath_id +
            " AND SEQ_SMART_USER_ID = " + seq_smart_id + " ORDER BY SEQ_ID";

      StringBuilder contentBuilder = new StringBuilder();

      if (objectTrackerInfo) {

         try {

            String body = "";
            contentBuilder.append(body);

            body = selectToInfo(dataSource, sql, "tObjectTracker", "red");

            try {
               dataSource.getConnection().close();
               ////System.out.println("connection closed");
            } catch (Exception e) {
               printStackTrace(e);
            }


            if (body != "") {
               contentBuilder.append(start);
               contentBuilder.append(body);
               contentBuilder.append(end);
            }

         } catch (Exception e) {
            printStackTrace(e);
         }

         return contentBuilder.toString();

      } else {
         String content = "<tr>" +
               "<td>" +
               sql +
               "</td>" +
               "</tr>";

         if (seq_bath_id != null && seq_smart_id != null) {
            return start + content + end;
         } else {
            return "";
         }
      }

   }

   private  String getAuditoryData(DataSource dataSource, String schema) {

      printInLine("getAuditory...                                              ");
      String start = "<h2> Auditory | auditory- SMPR_TAUDIT_PROCESS, SMPR_TMICROSERVICE_AUDIT & SMPR_TMICROSERVICE_AUDIT_DETAIL </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT stp.SEQ_ID AS ID_PROCESS, stp.DES_STATUS AS STATUS_PROCESS, stp.FYH_CREATION_DATE, " +
               "stp.des_admin_operation, " +
               "sta.DES_MICROSERVICE_NAME, sta.DES_STATUS AS STATUS_MICROSERVICE, stad.* " +
               "FROM " + schema + "SMPR_TAUDIT_PROCESS stp " +
               "INNER JOIN " + schema + "SMPR_TMICROSERVICE_AUDIT sta ON stp.SEQ_ID = sta.SEQ_AUDIT_PROCESS_ID " +
               "INNER JOIN " + schema + "SMPR_TMICROSERVICE_AUDIT_DETAIL stad ON stad.SEQ_MICROSERVICE_AUDIT_ID = sta.SEQ_ID " +
               "WHERE stp.des_client_id = '" + desClientId + "'"  +
               "AND stp.des_client_country = '" + desCountry +  "'";

         body = selectToInfo(dataSource, sql, "SMPR_TAUDIT_PROCESS", "blue");

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getDashboardWidget(DataSource dataSource, Long seq_installation_id, String schema) {

      printInLine("getDashboardWidget...                                              ");
      String start = "<h2> DashboardWidget | account-management - smpr_tdashboard_widget </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT * FROM " + schema + "SMPR_TDASHBOARD_WIDGET " +
               "WHERE SEQ_USER_ID = " + seq_user_id +
               " AND SEQ_INSTALLATION_ID = " + seq_installation_id +
               " ORDER BY SEQ_ORDER";

         body = selectToInfo(dataSource, sql, "SMPR_TDASHBOARD_WIDGET", "green");

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getInstallations(DataSource dataSource, String schema) {
      printInLine("getInstallations...                                        ");
      String start = "<h2> Installations | account-management - smpr_tinstallation </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT* FROM "+ schema +"SMPR_TINSTALLATION WHERE SEQ_ADMIN = " + seq_user_id +
               " ORDER BY FYH_DELETED desc ";

         body = selectToInfo(dataSource, sql, "SMPR_TINSTALLTION_List", "yellow");

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getInstallation(DataSource dataSource, Long seq_installation_id, String schema) {

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TINSTALLATION where SEQ_ID = " + seq_installation_id;

         body += "<h2> Installation: | account-management - smpr_tinstallation </h2>\n" +
               "<table border='1'>\n";

         body += selectToInfo(dataSource, sql, "SMPR_TINSTALLATION", "Cyan");

         body += "</table>";


         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            //contentBuilder.append(start);
            contentBuilder.append(body);
            //contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  void getInstAliasId(DataSource dataSource, String schema) {
      try {
         printInLine("getInstallationAliasId...                                        ");
         String sql = "SELECT SEQ_ID " +
               "FROM " + schema + "SMPR_TINSTALLATION_ALIAS WHERE SEQ_USER_ID = " + seq_user_id;

         selectToInfo(dataSource, sql, "InstallationAlias");

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }
   }

   private void getInstallationId(DataSource dataSource) {
      try {

         String sql = "SELECT SEQ_INSTALLATION_ID FROM \"ACCOUNT-MANAGEMENT\".SMPR_TINSTALLATION_ALIAS WHERE SEQ_ID " +
               "IN ("+ getInstAliasIdCommaSeparated() +")";

         selectToInfo(dataSource, sql, "getInstallationId");

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }
   }

   private  String getInstallationAlias(DataSource dataSource, Long seq_alias_id, String schema, int order) {

      printInLine("getInstallationAlias...                                              ");
      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT * " +
               "FROM " + schema + "SMPR_TINSTALLATION_ALIAS WHERE SEQ_ID = " + seq_alias_id;

         body += "<h2> Installation Alias " + order + "): | account-management - smpr_tinstallation_alias </h2>\n" +
               "<table border='1'>\n";

         body += selectToInfo(dataSource, sql, "SetInstallationId");

         body += "</table>";


         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(body);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getTerminals(DataSource dataSource, String schema) {

      printInLine("getTerminals...                                              ");
      String start = "<h2> Terminals | notification - smpr_tterminal </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TTERMINAL where SEQ_USER_ID = " + seq_user_id;

         body = selectToInfo(dataSource, sql, "SMPR_TERMINAL", "grey");

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getExternalServices(DataSource dataSource, String schema) {

      printInLine("getExternaServices...                                              ");
      String start = "<h2> Services | account-sync - SMPR_TUSER_EXTERNAL_SERVICE </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT * FROM " + schema + "SMPR_TUSER_EXTERNAL_SERVICE WHERE SEQ_SERVICE_ACCESS_ID = (" +
               "SELECT SEQ_ID FROM " + schema + "SMPR_TUSER_SERVICE_ACCESS WHERE DES_CLIENT_ID = '" + desClientId + "' AND DES_COUNTRY = '" + desCountry + "')";

         body = selectToInfo(dataSource, sql, "SMPR_TUSER_EXTERNAL_SERVICE", "green");

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getThingTree(DataSource dataSource, Long seq_installation_id, String schema) {

      printInLine("getThingTree...                                              ");
      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT SEQ_ID, " +
               "CASE SEQ_ENTITY_TYPE " +
               "WHEN 1 THEN 'VIDEO_DETECTOR' " +
               "WHEN 2 THEN 'VIDEO_CAMERA' " +
               "WHEN 12 THEN 'DVR_CAMERA' " +
               "WHEN 13 THEN 'PANEL' " +
               "WHEN 14 THEN 'MOTION_VIEWER' " +
               "WHEN 15 THEN 'HIKVISON_DVR' " +
               "WHEN 16 THEN 'CAMERA_MANAGER' " +
               "WHEN 17 THEN 'BASE_PARTITION' " +
               "WHEN 18 THEN 'FULL_PARTITION' " +
               "WHEN 19 THEN 'TVT_DVR' " +
               "WHEN 20 THEN 'CONTIGO' " +
               "WHEN 21 THEN 'DAHUA_DVR' " +
               "WHEN 22 THEN 'MULTISITE_EVENT_CONSOLE' " +
               "WHEN 23 THEN 'OperateInstallation' " +
               "WHEN 24 THEN 'VMS_VIDEO' " +
               "WHEN 25 THEN 'VMS' " +
               "ELSE 'UNKNOWN' " +
               "END AS DEVICE_TYPE, " +
               "SEQ_PARENT_ID,SEQ_ENTITY_TYPE, SEQ_TYPE_ID, SEQ_INSTALLATION_ID, COD_CONNECTION_ID, COD_COUNTRY, DES_CONTRACT_NUMBER, " +
               "DES_CODE, DES_NAME, NUM_CHANNEL, SEQ_DOM_CATALOG_DEV_ID, DES_INDEX,SEQ_ROOM_ID, " +
               "STATE_1, STATE_2, STATE_3, STATE_4, STATE_5, STATE_6, STATE_7, STATE_8, STATE_9, STATE_10, STATE_11, " +
               "STATE_12, STATE_13, STATE_14, STATE_15, STATE_16, STATE_17, STATE_18, STATE_19, STATE_20, STATE_21, STATE_22 " +
               "FROM (  " +
               // "      -- all parents " +
               "      SELECT thn.*  " +
               "            FROM " + schema + "SMPR_TTHING thn " +
               "       WHERE thn.SEQ_INSTALLATION_ID =" + seq_installation_id +
               " " +
               "      UNION ALL " +
               " " +
               //"            -- all childs " +
               "      SELECT thn.* " +
               "            FROM " + schema + "SMPR_TTHING thn " +
               "      WHERE thn.SEQ_PARENT_ID IN ( " +
               "            SELECT st.SEQ_ID " +
               "            FROM " + schema + "SMPR_TTHING st " +
               "       WHERE st.SEQ_INSTALLATION_ID =" + seq_installation_id +
               "      ) " +
               ") ORDER BY DEVICE_TYPE";

         body += "<html><body>\n" +
               "<h2> Thing Tree for Installation: " + seq_installation_id + " | device-identification - smpr_tthing </h2>\n" +
               "<table border='1'>\n";

         body += selectToInfo(dataSource, sql, "Tree", "DarkMagenta");

         body += "</table></body></html>";


         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(body);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getPanels(DataSource dataSource, Long seq_installation_id, String schema) {

      printInLine("getPanels...                                              ");
      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT stt.DES_SERVICE_NAME,  " +
               "thn.SEQ_ID, thn.SEQ_PARENT_ID, thn.COD_COUNTRY, thn.SEQ_INSTALLATION_ID, thn.DES_CONTRACT_NUMBER, " +
               "thn.COD_CONNECTION_ID, thn.SEQ_ENTITY_TYPE, thn.SEQ_TYPE_ID, thn.SEQ_DEVICE_TYPE, " +
               "thn.STATE_1, thn.STATE_2, thn.STATE_3, thn.STATE_4, thn.STATE_5, thn.STATE_6, thn.STATE_7, thn.STATE_8, thn.STATE_9, thn.STATE_10, " +
               "thn.STATE_11, thn.STATE_12, thn.STATE_13, thn.STATE_14, thn.STATE_15, thn.STATE_16, thn.STATE_17, thn.STATE_18, thn.STATE_19, thn.STATE_20, thn.STATE_21, " +
               "thn.SEQ_DOM_CATALOG_DEV_ID, thn.SEQ_ROOM_ID, thn.NUM_CHANNEL, thn.DES_CODE, thn.DES_INDEX, thn.DES_NAME " +
               "FROM " + schema + "SMPR_TTHING thn, " + schema + "SMPR_TSERVICE_TYPE stt " +
               "WHERE thn.SEQ_ENTITY_TYPE = 13 AND thn.SEQ_TYPE_ID = stt.SEQ_ID AND thn.SEQ_INSTALLATION_ID =" + seq_installation_id;

         body += "<h2> Panel for Installation: " + seq_installation_id + " | device-identification - smpr_tthing SEQ_ENTITY_TYPE = 13 </h2>\n" +
               "<table border='1'>\n";

         body += selectToInfo(dataSource, sql, "Panels");

         body += "</table>";


         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(body);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getThingAlias(DataSource dataSource, Long seq_inst_alias_id, String schema) {

      printInLine("getThingAlias...                                              ");
      String start = "<h2> Thing Alias | device-identification - smpr_tthing_alias </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT thna.SEQ_ID, " +
               "thna.SEQ_INSTALLATION_ALIAS_ID, " +
               "thna.SEQ_THING_ID, " +
               "CASE thn.SEQ_ENTITY_TYPE " +
               "WHEN 1 THEN 'VIDEO_DETECTOR' " +
               "WHEN 2 THEN 'VIDEO_CAMERA' " +
               "WHEN 12 THEN 'DVR_CAMERA' " +
               "WHEN 13 THEN 'PANEL' " +
               "WHEN 14 THEN 'MOTION_VIEWER' " +
               "WHEN 15 THEN 'HIKVISON_DVR' " +
               "WHEN 16 THEN 'CAMERA_MANAGER' " +
               "WHEN 17 THEN 'BASE_PARTITION' " +
               "WHEN 18 THEN 'FULL_PARTITION' " +
               "WHEN 19 THEN 'TVT_DVR' " +
               "WHEN 20 THEN 'CONTIGO' " +
               "WHEN 21 THEN 'DAHUA_DVR' " +
               "WHEN 22 THEN 'MULTISITE_EVENT_CONSOLE' " +
               "WHEN 23 THEN 'OperateInstallation' " +
               "WHEN 24 THEN 'VMS_VIDEO' " +
               "WHEN 25 THEN 'VMS' " +
               "ELSE 'UNKNOWN' " +
               "END AS DEVICE_TYPE, " +
               "thna.SEQ_PARENT_ID, " +
               "thna.DES_NAME, " +
               "thna.SEQ_USER_ID " +
               "FROM " + schema + "SMPR_TTHING_ALIAS thna " +
               "LEFT JOIN " + schema + "SMPR_TTHING thn ON (thna.SEQ_THING_ID = thn.SEQ_ID) " +
               "WHERE  thna.SEQ_USER_ID = " + seq_user_id +
               " AND ( thna.SEQ_INSTALLATION_ALIAS_ID = " + seq_inst_alias_id +
               " OR thna.SEQ_PARENT_ID IN (" +
               "SELECT thna.SEQ_ID FROM " + schema + "SMPR_TTHING_ALIAS thna " +
               "WHERE thna.SEQ_INSTALLATION_ALIAS_ID = " + seq_inst_alias_id +
               " AND thna.SEQ_USER_ID = " + seq_user_id + ")) "+
               "ORDER BY DEVICE_TYPE";

         body = selectToInfo(dataSource, sql, "ThingAlias", "DarkKhaki");

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }


         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();

   }

   private  String getUserReplicas(DataSource dataSource, String schema) {

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT * " +
               "FROM " + schema + "SMPR_TUSER " +
               "WHERE SEQ_ID = " + seq_user_id +
               " ORDER BY FYH_DELETED DESC";
         body += "<h2> User replicas on: " + schema + " </h2>\n" +
               "<table border='1'>\n";
         body += selectToInfo(dataSource, sql, "replicas", "orange");
         body += "</table>";

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(body);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private String getInstallationReplicas(DataSource dataSource, String schema) {

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT * " +
               "FROM " + schema + "SMPR_TINSTALLATION " +
               "WHERE SEQ_ID IN( " + getInstallationIdCommaSeparated() + ") " +
               "ORDER BY FYH_DELETED DESC";
         body += "<h2> Installation replicas on: " + schema + " </h2>\n" +
               "<table border='1'>\n";
         body += selectToInfo(dataSource, sql, "replicas", "orange");
         body += "</table>";

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(body);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private String getInstallationAliasReplicas(DataSource dataSource, String schema) {

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "SELECT * " +
               "FROM " + schema + "SMPR_TINSTALLATION_ALIAS " +
               "WHERE SEQ_USER_ID = " + seq_user_id;
         body += "<h2> InstallationAlias replicas on: " + schema + " </h2>\n" +
               "<table border='1'>\n";
         body += selectToInfo(dataSource, sql, "replicas", "yellow");
         body += "</table>";

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(body);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private String getInstAliasIdCommaSeparated(){
      String result = "";
      if (arr_inst_alias_id.size() == 1){
         return "" + arr_inst_alias_id.get(0);
      } else if (arr_inst_alias_id.size() > 1) {

         for(int inx=0;  inx < arr_inst_alias_id.size(); inx ++){
            result += "" + arr_inst_alias_id.get(inx) + ",";
         }
         result = result.substring(0,result.length() -1);
      }

      return result;
   }

   private String getInstallationIdCommaSeparated(){
      String result = "";
      if (arr_installation_id.size() == 1){
         return "" + arr_installation_id.get(0);
      } else if (arr_installation_id.size() > 1) {

         for(int inx=0;  inx < arr_installation_id.size(); inx ++){
            result += "" + arr_installation_id.get(inx) + ",";
         }
         result = result.substring(0,result.length() -1);
      }

      return result;
   }

   private String getThingAliasReplicas(DataSource dataSource, String schema, Long instAliasId){

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body="";
         contentBuilder.append(body);

         String sql = "SELECT CASE thn.SEQ_ENTITY_TYPE " +
               "WHEN 1 THEN 'VIDEO_DETECTOR' " +
               "WHEN 2 THEN 'VIDEO_CAMERA' " +
               "WHEN 12 THEN 'DVR_CAMERA' " +
               "WHEN 13 THEN 'PANEL' " +
               "WHEN 14 THEN 'MOTION_VIEWER' " +
               "WHEN 15 THEN 'HIKVISON_DVR' " +
               "WHEN 16 THEN 'CAMERA_MANAGER' " +
               "WHEN 17 THEN 'BASE_PARTITION' " +
               "WHEN 18 THEN 'FULL_PARTITION' " +
               "WHEN 19 THEN 'TVT_DVR' " +
               "WHEN 20 THEN 'CONTIGO' " +
               "WHEN 21 THEN 'DAHUA_DVR' " +
               "WHEN 22 THEN 'MULTISITE_EVENT_CONSOLE' " +
               "WHEN 23 THEN 'OperateInstallation' " +
               "WHEN 24 THEN 'VMS_VIDEO' " +
               "WHEN 25 THEN 'VMS' " +
               "ELSE 'UNKNOWN' " +
               "END AS DEVICE_TYPE, " +
               "thna.* " +
               "FROM " + schema + "SMPR_TTHING_ALIAS thna " +
               "LEFT JOIN " + schema + "SMPR_TTHING thn ON (thna.SEQ_THING_ID = thn.SEQ_ID) " +
               "WHERE  thna.SEQ_USER_ID = " + seq_user_id +
               " AND ( thna.SEQ_INSTALLATION_ALIAS_ID = " + instAliasId +
               " OR thna.SEQ_PARENT_ID IN (" +
               "SELECT thna.SEQ_ID FROM " + schema + "SMPR_TTHING_ALIAS thna " +
               "WHERE thna.SEQ_INSTALLATION_ALIAS_ID = " + instAliasId +
               " AND thna.SEQ_USER_ID = " + seq_user_id + ")) "+
               "ORDER BY DEVICE_TYPE";

         body += "<table border='1'>\n";
         body += selectToInfo(dataSource, sql, "replicas", "blue");
         body += "</table>";

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body!= "" ) {
            contentBuilder.append(body);
         }

      } catch (Exception e)  {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private String getThingReplicas(DataSource dataSource, String schema, Long installationId){

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body="";
         contentBuilder.append(body);

         String sql = "SELECT ";
         if (!schema.equals("\"FILTER-DISPATCHER\".") &&
               !schema.equals("\"PANIC-BUTTON\".") &&
               !schema.equals("\"RULE-ENGINE\".") )
            sql +="BOL_IS_ACTIVE, ";

         sql +=   "SEQ_ID, " +
               "CASE SEQ_ENTITY_TYPE " +
               "WHEN 1 THEN 'VIDEO_DETECTOR' " +
               "WHEN 2 THEN 'VIDEO_CAMERA' " +
               "WHEN 12 THEN 'DVR_CAMERA' " +
               "WHEN 13 THEN 'PANEL' " +
               "WHEN 14 THEN 'MOTION_VIEWER' " +
               "WHEN 15 THEN 'HIKVISON_DVR' " +
               "WHEN 16 THEN 'CAMERA_MANAGER' " +
               "WHEN 17 THEN 'BASE_PARTITION' " +
               "WHEN 18 THEN 'FULL_PARTITION' " +
               "WHEN 19 THEN 'TVT_DVR' " +
               "WHEN 20 THEN 'CONTIGO' " +
               "WHEN 21 THEN 'DAHUA_DVR' " +
               "WHEN 22 THEN 'MULTISITE_EVENT_CONSOLE' " +
               "WHEN 23 THEN 'OperateInstallation' " +
               "WHEN 24 THEN 'VMS_VIDEO' " +
               "WHEN 25 THEN 'VMS' " +
               "ELSE 'UNKNOWN' " +
               "END AS DEVICE_TYPE, " +
               "SEQ_PARENT_ID,SEQ_ENTITY_TYPE, SEQ_TYPE_ID, SEQ_INSTALLATION_ID, COD_CONNECTION_ID, COD_COUNTRY, DES_CONTRACT_NUMBER, " +
               "DES_CODE, DES_NAME, NUM_CHANNEL, SEQ_DOM_CATALOG_DEV_ID, DES_INDEX,SEQ_ROOM_ID, " +
               "STATE_1, STATE_2, STATE_3, STATE_4, STATE_5, STATE_6, STATE_7, STATE_8, STATE_9, STATE_10, STATE_11, " +
               "STATE_12, STATE_13, STATE_14, STATE_15, STATE_16, STATE_17, STATE_18, STATE_19, STATE_20, STATE_21, STATE_22, FYH_UPDATE_DATE " +
               "FROM (  " +
               // "      -- all parents " +
               "      SELECT thn.*  " +
               "            FROM " + schema + "SMPR_TTHING thn " +
               "       WHERE thn.SEQ_INSTALLATION_ID =" + installationId +
               " " +
               "      UNION ALL " +
               " " +
               //"            -- all childs " +
               "      SELECT thn.* " +
               "            FROM " + schema + "SMPR_TTHING thn " +
               "      WHERE thn.SEQ_PARENT_ID IN ( " +
               "            SELECT st.SEQ_ID " +
               "            FROM " + schema + "SMPR_TTHING st " +
               "       WHERE st.SEQ_INSTALLATION_ID =" + installationId +
               "      ) " +
               ") ";

         if (!schema.equals("\"FILTER-DISPATCHER\".") &&
               !schema.equals("\"PANIC-BUTTON\".")  &&
               !schema.equals("\"RULE-ENGINE\"."))
            sql +="ORDER BY BOL_IS_ACTIVE DESC";

         body +=      "<table border='1'>\n";
         body += selectToInfo(dataSource, sql, "replicas", "green");
         body += "</table>";

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body!= "" ) {
            contentBuilder.append(body);
         }

      } catch (Exception e)  {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getEvents(DataSource dataSource, String schema, Long seq_inst_alias_id){

      printInLine("getEvents...                                              ");
      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body="";
         String sql;
         String sqlCount;
         int total;
         contentBuilder.append(body);

         sql = "SELECT * FROM ( " +
               "SELECT event.SEQ_ID, event.SEQ_EVENT_TYPE_ID, tpe.DES_TEXT, event.FYH_CREATION_DATE, event.FYH_EVENT_DATE, " +
               "event.BOL_SMART_EVENT, event.BOL_MASTERMIND_EVENT, event.BOL_EXTERNAL_PROVIDER_EVENT, " +
               "event.BOL_DOMOTIC_EVENT, event.DES_TYPE, event.DES_MESSAGE, event.SEQ_LOG_ID, event.DES_DEVICE_NAME, " +
               "event.DES_PANEL_USER, event.DES_CONTRACT, event.DES_PRIORITY, event.DES_COLOR, event.DES_EVENT_ID, " +
               "event.DES_LOGIN_AS_USER, event.DES_SCENE_NAME, event.DES_SITE_NAME, event.DES_ZONE_ID, event.DES_AREA, " +
               "event.COD_ACUDA_CASE, event.DES_COMMENTS, event.DES_DM_VARIABLE_NAME, event.DES_DM_VARIABLE_VALUE, " +
               "event.SEQ_DEVICE_ID, event.DES_PARTITION_ID, event.DES_JOB_ID, event.BOL_INCLUDE_VIDEO, event.SEQ_INSTALLATION_ID, " +
               "event.DES_USER, event.DES_USER_ALIAS, event.DES_PARTITION_NAME, event.SEQ_SCENE_ID, event.DES_COUNTRY, " +
               "event.BOL_IS_FULL_PARTITION, event.SEQ_THING_ID, event.SEQ_PARENT_THING_ID, event.SEQ_USER_ID " +
               "FROM " + schema +"SMPR_TEVENT event " +
               "LEFT JOIN "+ schema +"SMPR_TEVENT_TEXT tpe ON (event.SEQ_EVENT_TYPE_ID = TPE.SEQ_EVENT_TYPE_ID AND tpe.DES_LANGUAGE_ID = 'es_ES' ) " +
               "WHERE event.SEQ_EVENT_TYPE_ID IS NOT NULL " +
               "AND event.SEQ_PARENT_THING_ID " +
               "IN ( " +
               "SELECT SEQ_THING_ID FROM "+ schema +"SMPR_TTHING_ALIAS " +
               "WHERE SEQ_INSTALLATION_ALIAS_ID = " + seq_inst_alias_id +
               " AND SEQ_USER_ID = " + seq_user_id +
               " )" +
               " ORDER BY event.FYH_CREATION_DATE desc" +
               " ) WHERE ROWNUM <= " + maxEvents;

         sqlCount =  "SELECT count(1) as cnt " +
               "FROM "+ schema +"SMPR_TEVENT event " +
               "WHERE event.SEQ_EVENT_TYPE_ID IS NOT NULL " +
               "AND event.SEQ_PARENT_THING_ID " +
               "IN ( " +
               "SELECT SEQ_THING_ID FROM "+ schema +"SMPR_TTHING_ALIAS " +
               "WHERE SEQ_INSTALLATION_ALIAS_ID = " + seq_inst_alias_id +
               " AND SEQ_USER_ID = " + seq_user_id +
               " )";

         total = selectToInfoCount(dataSource, sqlCount);

         body += "<h2> Events for Panel: " + seq_panel_id + " or User: " + seq_user_id +" | event-processor - smpr_tevent - Total events: " + total + " (only " + maxEvents + " are shown) </h2>\n" +
               "<table border='1'>\n";

         body += selectToInfo(dataSource, sql, "tEvents", "blue");

         body += "</table>";


         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body!= "" ) {
            contentBuilder.append(body);
         }

      } catch (Exception e)  {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getScenes(DataSource dataSource, String schema) {

      String start = "<h2> Scenes | scenes - smpr_tscene </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TSCENE WHERE SEQ_USER_ID = " + seq_user_id;

         body = selectToInfo(dataSource, sql, "SMPR_TSCENE", "blue");

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }


   private  String getScenesAction(DataSource dataSource, String schema) {

      String start = "<h2> Scenes Action | scenes - smpr_tscene_action </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TSCENE_ACTION WHERE SEQ_SCENE_ID IN " +
               "(SELECT SEQ_ID FROM SCENE.SMPR_TSCENE WHERE SEQ_USER_ID =" + seq_user_id + ")";

         body = selectToInfo(dataSource, sql, "SMPR_TSCENE", "green");

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }

   private  String getScenesTrigger(DataSource dataSource, String schema) {

      String start = "<h2> Scenes Action | scenes - smpr_tscene_trigger </h2>\n" +
            "<table border='1'>\n";

      String end = "</table>";

      StringBuilder contentBuilder = new StringBuilder();

      try {

         String body = "";
         contentBuilder.append(body);

         String sql = "select * from " + schema + "SMPR_TSCENE_TRIGGER WHERE SEQ_SCENE_ID IN " +
               "(SELECT SEQ_ID FROM SCENE.SMPR_TSCENE WHERE SEQ_USER_ID =" + seq_user_id + ")";

         body = selectToInfo(dataSource, sql, "SMPR_TSCENE", "green");

         try {
            dataSource.getConnection().close();
            //System.out.println("connection closed");
         } catch (Exception e) {
            printStackTrace(e);
         }

         if (body != "") {
            contentBuilder.append(start);
            contentBuilder.append(body);
            contentBuilder.append(end);
         }

      } catch (Exception e) {
         printStackTrace(e);
      }

      return contentBuilder.toString();
   }


   public String getCRMInstallation(String env){

      String start = "<h2> Installations from CRM </h2>\n" +
            "<table border='1'>\n" +
            "<tr width='100%'><td>\n";
      String end = "</td></tr>\n"+ "</table>";


      try {
         String host= getProperty(env + ".crm_host");


//#urlBase="/alarms/smart/rtf"
//#urlOauth="$/oauth/alarms/smart/rtf"

         String url= host + "/alarms/smart";

         String tokenUrl = host + "/api/oauth/token";
         String getInstallationUrl = url + "/v1/country/"+desCountry+"/client/"+desClientId+"/installation";

         Client client = Client.create();
         WebResource webResource1 = client.resource(tokenUrl);
         webResource1.header("Content-Type", "application/x-www-form-urlencoded");

         MultivaluedMap formData = new MultivaluedMapImpl();
         formData.add("scope", "alarms.write, alarms.read");
         formData.add("client_secret", getProperty(env + ".client_secret"));
         formData.add("client_id", getProperty(env + ".client_id"));
         formData.add("grant_type", "client_credentials");

         ClientResponse response1 = webResource1.post(ClientResponse.class, formData);
         String tokenResponse = response1.getEntity(String.class);
         String tokenValue = tokenResponse.substring(17,71);

         Client client2 = Client.create();

         //System.out.println("get Installation from crm: " + getInstallationUrl);

         WebResource webResource2 = client2.resource(getInstallationUrl);
         WebResource.Builder builder =  webResource2.getRequestBuilder();

         builder
               .header("authorization", "Bearer " + tokenValue)
               .header("Content-Type", "application/json")
               .header("ExtSys", "smart");

         ClientResponse response2 = builder.get(ClientResponse.class);
         String result = response2.getEntity(String.class);

         result = result.replaceAll("\n", "<br>\n");
         result = result.replaceAll(" ", "&nbsp;");

         String content = formatJson4Report(result);
         content = content.replaceAll("<br><br>", "<br>");


         return  start + "<br><font color=\"blue\">" + getInstallationUrl + "</font><br>" + content + end;
      } catch (Exception e) {
         return start + e.getMessage() + end;
      }
   }

   public String getCRMContract(String env){

      String start = "<h2> Installations from CRM </h2>\n" +
            "<table border='1'>\n" +
            "<tr width='100%'><td>\n";
      String end = "</td></tr>\n"+ "</table>";


      try {
         String host="https://apiemea.prosegur.com";
         String url= host + "/alarms/smart";

         String tokenUrl = host + "/api/oauth/token";
         String getContractUrl = url + "/v1/country/"+desCountry+"/client/"+desClientId+"/contractsData";

         Client client = Client.create();
         WebResource webResource1 = client.resource(tokenUrl);
         webResource1.header("Content-Type", "application/x-www-form-urlencoded");

         MultivaluedMap formData = new MultivaluedMapImpl();
         formData.add("scope", "alarms.write, alarms.read");
         formData.add("client_secret", getProperty(env + ".client_secret"));
         formData.add("client_id", getProperty(env + ".client_id"));
         formData.add("grant_type", "client_credentials");

         ClientResponse response1 = webResource1.post(ClientResponse.class, formData);
         String tokenResponse = response1.getEntity(String.class);
         String tokenValue = tokenResponse.substring(17,71);

         Client client2 = Client.create();

         //System.out.println("get Installation from crm: " + getInstallationUrl);

         WebResource webResource2 = client2.resource(getContractUrl);
         WebResource.Builder builder =  webResource2.getRequestBuilder();

         builder
               .header("authorization", "Bearer " + tokenValue)
               .header("Content-Type", "application/json")
               .header("ExtSys", "smart");

         ClientResponse response2 = builder.get(ClientResponse.class);
         String result = response2.getEntity(String.class);


         result = result.replaceAll("\n", "<br>\n");
         result = result.replaceAll(" ", "&nbsp;");
         String content = formatJson4Report(result);
         content = content.replaceAll("<br><br>", "<br>");


         //return  start + "<br><br><font color=\"blue\">" + getContractUrl + "</font><br><br>" + content + end;
         return   getContractUrl + "\n" + content ;
      } catch (Exception e) {
         return start + e.getMessage() + end;
      }
   }


   protected String getCognitoData(String env, String username){

      String resultStr = "";

      String start = "<h2> Cognito data </h2>\n" +
            "<table border='1'>\n" +
            "<tr width='100%'><td>\n";
      String end = "</td></tr>\n"+ "</table>";

      try {

         String AWS_ACCESS_KEY = getProperty(env + ".aws_access_key");
         String AWS_SECRET_KEY = getProperty(env + ".aws_secret_key");

         String USER_POOL_REGION = getProperty(env + ".aws_user_pool_region");
         String USER_POOL_ID = getProperty(env + ".aws_user_pool_id");

         identityUserPoolProviderClient = new AWSCognitoIdentityProviderClient(new BasicAWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY)); // creds are loaded via variables that are supplied to my program dynamically
         identityUserPoolProviderClient.setRegion(RegionUtils.getRegion(USER_POOL_REGION)); // var loaded

         ListUsersRequest listUsersRequest = new ListUsersRequest();
         listUsersRequest.withUserPoolId(USER_POOL_ID); // id of the userpool, look this up in Cognito console
         listUsersRequest.withFilter("username = \"" + username + "\"");

         // get the results
         ListUsersResult result = identityUserPoolProviderClient.listUsers(listUsersRequest);

         List<UserType> userTypeList = result.getUsers();

         /*

         ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
         String json = ow.writeValueAsString(userTypeList.get(0));

         //System.out.println("---------");
         System.out.println(json);

         System.out.println("---------");*/

         if (userTypeList.size() > 0 ) {

            UserType userType = userTypeList.get(0);
            String jsonRet = "";

            jsonRet = "{";
            jsonRet = jsonRet + " \"username\" : \"" + userType.getUsername() + "\",\n";
            jsonRet = jsonRet + " \"userCreateDate\" : \"" + getFormattedDate(userType.getUserCreateDate()) + "\",\n";
            jsonRet = jsonRet + " \"userLastModifiedDate\" : \"" + getFormattedDate(userType.getUserLastModifiedDate()) + "\",\n";
            jsonRet = jsonRet + " \"enabled\" : \"" + userType.getEnabled() + "\",\n";
            jsonRet = jsonRet + " \"userStatus\" : \"" + userType.getUserStatus() + "\",\n";

            List<AttributeType> attributeList = userType.getAttributes();

            jsonRet = jsonRet + "[\n";

            int countAtt = 0;

            for (AttributeType attribute : attributeList) {
               countAtt++;
               String attName = attribute.getName();
               String attValue = attribute.getValue();

               jsonRet = jsonRet + "  {\"" + attName + "\" : \"" + attValue + "\"}";

               if (countAtt < attributeList.size()) {
                  jsonRet = jsonRet + ",\n";
               } else {
                  jsonRet = jsonRet + "\n";
               }
            }

            jsonRet = jsonRet + "]}";

            // System.out.println("----------");
            //System.out.println(jsonRet);

            jsonRet = jsonRet.replaceAll("\n", "<br>\n");
            jsonRet = jsonRet.replaceAll(" ", "&nbsp;");

            String content = formatJson4Report(jsonRet);
            content = content.replaceAll("<br><br>", "<br>");

            resultStr = start + content + end;
         } else {

            resultStr = start + "<font color=\"blue\">User " + username + " not found on cognito. </font>" + end;

         }

      } catch (AWSCognitoIdentityProviderException e) {
         printStackTrace(e);
      } catch (Exception e) {
         printStackTrace(e);
      }

      return resultStr;

   }



}
