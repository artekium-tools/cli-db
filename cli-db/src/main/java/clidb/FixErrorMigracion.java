package clidb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Properties;


public class FixErrorMigracion extends QryDB {


   private static final Logger LOGGER = LogManager.getLogger(FixErrorMigracion.class);

   static boolean UPDATE_IN_TEST_MODE = false; // true=olny shows logs  false=execute updates

   static Properties prop = new Properties();
   static Properties scriptProperties = new Properties();

   static ArrayList<String> arr_userName = new ArrayList<>();
   static ArrayList<String> arr_userNameToFix = new ArrayList<>();
   static ArrayList<Long> arr_seq_user_id = new ArrayList<>();

   public void main(String env) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);
         System.out.println("connectionType: " + connectionType);

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         System.out.println("Init Process: " );

         System.out.println("Checking on MIGRATION-ORCHESTRATOR " );
         selectUsersOnTAccount(dataSource);

         int count = 0;

         for (String userName : arr_userName) {
            count ++;

            System.out.println("checking nro: " + count + " userName : " + userName);
             checkUserName(dataSource, userName);

         }



         System.out.println("End Process");
      } catch (Exception e){
         printStackTrace(e);
      }

      LOGGER.info("End process.");

      return;
   }


   public void main4ERROR(String env) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);
         System.out.println("connectionType: " + connectionType);

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");

         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         System.out.println("Init Process: " );

         System.out.println("Checking on MIGRATION-ORCHESTRATOR " );

         selectUsersOnTAccount(dataSource);

         int count = 0;
         int total = arr_userName.size();

         for (String userName : arr_userName) {
            count ++;

            System.out.println("checking nro: " + count + " of " + total + " - userName : " + userName);
            checkUserName4ERROR(dataSource, userName);

         }



         System.out.println("End Process");
      } catch (Exception e){
         printStackTrace(e);
      }

      LOGGER.info("End process.");

      return;
   }


   public void parse4ERROR(String env) {

      loadExternalFileProperties();

      DataSource dataSource = null;

      try {

         if (env == null) env = getProperty("env");
         String connectionType = getProperty("connectionType");

         System.out.println("env: " + env);
         System.out.println("connectionType: " + connectionType);

         String jdbcUrl = getProperty(connectionType + "." + env + ".url");
         String dbUsername = getProperty(env + ".username");
         String password = getProperty(env + ".password");


         String installationsFile = "./userNamesToFix4ERROR.txt";

         BufferedReader br = new BufferedReader(new FileReader(installationsFile));

         int totalLines = 0;
         System.out.println("Counting lines ... " );
         while ((br.readLine()) != null) {
            totalLines++;
         }

         br.close();

         br = new BufferedReader(new FileReader(installationsFile));


         dataSource = getDataSource(jdbcUrl, dbUsername, password);

         System.out.println("Init Process: " );


         int count = 0;
         String sCurrentLine;
         while ((sCurrentLine = br.readLine()) != null) {
            count ++;

            System.out.println("checking nro: " + count + " of " + totalLines +" userName : " + sCurrentLine);

            checkUserName4ERRORcheck(dataSource, sCurrentLine);

         }

         System.out.println("End Process");
      } catch (Exception e){
         printStackTrace(e);
      }

      LOGGER.info("End process.");

      return;
   }


   public static String selectUserName(final DataSource dataSource, Long userId) {
      String result = "";

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT DES_NICK_NAME FROM \"ACCOUNT-MANAGEMENT\".SMPR_TUSER " +
               "WHERE SEQ_ID = " + userId;

         System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            System.out.println("Adding uesrNames .."  );
            //int rec = 0;

            if  (rs.next()) {

               result = rs.getString(1);
               //rec ++;
               //printInLine("record: " + rec);


            }

            System.out.println("\nEnd Adding uesrNames .." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }



      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }

      return result;
   }

   public static void selectUsersOnTAccount(final DataSource dataSource) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         String sql = "SELECT DES_USERNAME FROM \"MIGRATION-ORCHESTRATOR\".SMPR_TACCOUNT " +
               "WHERE DES_MIGRATION_MESSAGE='Error al invocar al servicio user-enrollment'  " +
               "AND SEQ_USER_ID IS NULL";

         System.out.println(sql);

         try {

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery(sql);

            System.out.println("Adding uesrNames .."  );
            int rec = 0;
            while (rs.next()) {
               arr_userName.add(rs.getString(1));
               rec ++;
               printInLine("record: " + rec);

              // if (rec == 100) break;
            }

            System.out.println("\nEnd Adding uesrNames .." );
         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }


   public static void checkUserName(final DataSource dataSource, String userName) {

         try {
            Connection con = dataSource.getConnection();
            Statement stmt = null;
            ResultSet rs = null;

            try {

               String sql1 = "SELECT migra.SEQ_ID AS accountId, usr.seq_id AS userId, migra.DES_MIGRATION_MESSAGE, " +
                     "migra.DES_MIGRATION_STATUS, lower(migra.DES_USERNAME) " +
                     "FROM \"MIGRATION-ORCHESTRATOR\".SMPR_TACCOUNT migra " +
                     "LEFT OUTER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr ON (lower(migra.DES_USERNAME) = usr.des_nick_name ) " +
                     "WHERE lower(migra.DES_USERNAME) = lower('" + userName + "') " +
                     "ORDER BY migra.FYH_START_DATE desc";

               // Query that finaly will be executed
               stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
               rs = stmt.executeQuery(sql1);

               /*migra.SEQ_ID AS accountId,
               usr.seq_id AS userId,
               migra.DES_MIGRATION_MESSAGE,
               migra.DES_MIGRATION_STATUS*/


               int size =0;
               if (rs != null){
                  rs.beforeFirst();
                  rs.last();
                  size = rs.getRow();
               }
               rs.beforeFirst();

               //1) Si el primer resultado es un DES_MIGRATION_STATUS = OK - No hacemos nada
               System.out.println("size: "+size);
               rs.next();

               if (size > 1) {
                  System.out.println("checked userName: " + userName + " - " + rs.getString(4));

               } else if (size == 1) {

                  //2)  Si tiene un unico resultado y es DES_MIGRATION_MESSAGE = 'Error al invocar al servicio user-enrollment',
                  // appendeamos un update en tAccount para el accountId y el userId
                  if ("ERROR".equals(rs.getString(4)) &&
                        "Error al invocar al servicio user-enrollment".equals(rs.getString(3)) ){

                     if (Long.valueOf(rs.getLong(2)) != null){

                        System.out.println("checked userName: " + userName + " To Fix");

                        String updatetAccountStmnt = "UPDATE SMPR_TACCOUNT SET SEQ_USER_ID = " +rs.getLong(2) +
                              " WHERE SEQ_ID = " + rs.getLong(1);

                        appendToFile("updatetAccount.sql",  updatetAccountStmnt + "\n");
                        appendToFile("userNamesToFix.txt",  userName + "\n");
                      } else {
                        //3) Si el userId es Null, Guardamos agregamos un delete para ese userName en tUserEnrollment
                        System.out.println("checked userName: " + userName + " To Delete");

                        String deletetUserEnrollmentStmnt = "DELETE FROM SMPR_TUSER_ENROLLMENT WHERE DES_USER_NAME = " + rs.getString(5);

                        appendToFile("updatetAccount.sql",  deletetUserEnrollmentStmnt + "\n");
                     }

                  }
               }


            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            } finally {
               try {
                  if (rs != null) rs.close();
                  if (stmt != null) stmt.close();
                  if (con != null) con.close();
               } catch (Exception e) {
                  System.out.println(" ");
                  System.out.println(e.getMessage());
                  printStackTrace(e);
               }
            }

         } catch (Exception e1) {
            System.out.println(" ");
            System.out.println(e1.getMessage());
            e1.printStackTrace();
         }
      }

   public static void checkUserName4ERROR(final DataSource dataSource, String userName) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql1 = "SELECT migra.SEQ_ID AS accountId, usr.seq_id AS userId, migra.DES_MIGRATION_MESSAGE, " +
                  "migra.DES_MIGRATION_STATUS, lower(migra.DES_USERNAME) " +
                  "FROM \"MIGRATION-ORCHESTRATOR\".SMPR_TACCOUNT migra " +
                  "LEFT OUTER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr ON (lower(migra.DES_USERNAME) = usr.des_nick_name ) " +
                  "WHERE lower(migra.DES_USERNAME) = lower('" + userName + "') " +
                  "ORDER BY migra.FYH_START_DATE desc";

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql1);

               /*migra.SEQ_ID AS accountId,
               usr.seq_id AS userId,
               migra.DES_MIGRATION_MESSAGE,
               migra.DES_MIGRATION_STATUS*/


            int size =0;
            if (rs != null){
               rs.beforeFirst();
               rs.last();
               size = rs.getRow();
            }
            rs.beforeFirst();

            //1) Si el primer resultado es un DES_MIGRATION_STATUS = OK - No hacemos nada
            System.out.println("size: "+size);

            boolean hasToBeFixed = false;
            String updatetAccountStmnt = "";
            int count = 0;
            if (size > 1) {
               while (rs.next()) {
                  count ++;
                  System.out.println("checked userName: " + userName + " - " + count + ") - "+ rs.getString(4));
                  // Si el primer registro de tAccount ordenado por fecha descendente es ERROR
                  // y el mensaje No es: No se pudo marcar como migrado el usuario en Smart
                  // y si el id de usuario fue encontrado en la tUser de account-management
                  if (count == 1 &&
                        "ERROR".equals(rs.getString(4)) &&
                        !"No se pudo marcar como migrado el usuario en Smart".equals(rs.getString(3)) &&
                        rs.getLong(2) != 0L) {
                     updatetAccountStmnt = "UPDATE SMPR_TACCOUNT SET SEQ_USER_ID = " + rs.getLong(2) +
                           " WHERE SEQ_ID = " + rs.getLong(1) + ";";
                     hasToBeFixed = true;
                  }

                  // Si posee OK en alguno de los registros, no hay que desmigrar
                  if ("OK".equals(rs.getString(4))){
                     hasToBeFixed = false;
                  }
               }

               if (hasToBeFixed) {
                  appendToFile("updatetAccount4ERROR.sql", updatetAccountStmnt + "\n");
                  appendToFile("userNamesToFix4ERROR.txt", userName + "\n");
               }
            }

         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }


   public static void checkUserName4ERRORcheck(final DataSource dataSource, String userName) {

      try {
         Connection con = dataSource.getConnection();
         Statement stmt = null;
         ResultSet rs = null;

         try {

            String sql1 = "SELECT migra.SEQ_ID AS accountId, usr.seq_id AS userId, migra.DES_MIGRATION_MESSAGE, " +
                  "migra.DES_MIGRATION_STATUS, lower(migra.DES_USERNAME) " +
                  "FROM \"MIGRATION-ORCHESTRATOR\".SMPR_TACCOUNT migra " +
                  "LEFT OUTER JOIN \"ACCOUNT-MANAGEMENT\".SMPR_TUSER usr ON (lower(migra.DES_USERNAME) = usr.des_nick_name ) " +
                  "WHERE lower(migra.DES_USERNAME) = lower('" + userName + "') " +
                  "ORDER BY migra.FYH_START_DATE desc";

            // Query that finaly will be executed
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql1);

               /*migra.SEQ_ID AS accountId,
               usr.seq_id AS userId,
               migra.DES_MIGRATION_MESSAGE,
               migra.DES_MIGRATION_STATUS*/


            int size =0;
            if (rs != null){
               rs.beforeFirst();
               rs.last();
               size = rs.getRow();
            }
            rs.beforeFirst();

            //1) Si el primer resultado es un DES_MIGRATION_STATUS = OK - No hacemos nada
            System.out.println("size: "+size);

            int count = 0;
            String line = "";

            appendToFile("checkUserNamesToFix4ERROR.txt", "checked userName: " + userName + " - size: " + size + "\n");

            while (rs.next()) {
               count ++;
               line = count + ") - "+ rs.getString(4);
               System.out.println(line);
               appendToFile("checkUserNamesToFix4ERROR.txt", line + "\n");
            }



         } catch (Exception e) {
            System.out.println(" ");
            System.out.println(e.getMessage());
            printStackTrace(e);
         } finally {
            try {
               if (rs != null) rs.close();
               if (stmt != null) stmt.close();
               if (con != null) con.close();
            } catch (Exception e) {
               System.out.println(" ");
               System.out.println(e.getMessage());
               printStackTrace(e);
            }
         }

      } catch (Exception e1) {
         System.out.println(" ");
         System.out.println(e1.getMessage());
         e1.printStackTrace();
      }
   }

   public static void showMemUsage(){
      Runtime runtime = Runtime.getRuntime();

      NumberFormat format = NumberFormat.getInstance();

      StringBuilder sb = new StringBuilder();
      long maxMemory = runtime.maxMemory();
      long allocatedMemory = runtime.totalMemory();
      long freeMemory = runtime.freeMemory();

      sb.append("free memory: " + format.format(freeMemory / 1024) + "<br/>");
      sb.append("allocated memory: " + format.format(allocatedMemory / 1024) + "<br/>");
      sb.append("max memory: " + format.format(maxMemory / 1024) + "<br/>");
      sb.append("total free memory: " + format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024) + "<br/>");

      System.out.println(sb.toString());

   }
}
