package clidb;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import login.helper.service.LoginHelperServiceGrpc;
import login.helper.service.SetupSyncRequest;
import login.helper.service.SetupSyncResponse;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GrpcCallTest extends QryDB {


   //private static final Logger LOGGER = LogManager.getLogger(GrpcCallTest.class);



   public void main() {

      try {

         String host = "localhost";
         int port = 7001;

         System.setProperty("java.util.logging.ConsoleHandler.level", "FINE");


         Logger logger = Logger.getLogger("io.grpc");
         logger.setLevel(Level.FINE);

         // Agregar un mensaje de prueba para verificar que los logs funcionan


         ManagedChannel channel = ManagedChannelBuilder.forAddress(
           host, port)
         .usePlaintext()
         .build();

         channel.getState(false);

         logger.info("Habilitando logs de gRPC..." + channel.toString());

         System.out.println("entered GrpcCallTest");
         LoginHelperServiceGrpc.LoginHelperServiceStub stub =
                 LoginHelperServiceGrpc.newStub(channel);



         SetupSyncRequest request = SetupSyncRequest.newBuilder()
                 .setIdUser("173605")
                 .setUsername("expanelw@yopmail.com")
                 .setCountryId("ES")
                 .setClientId("6338659")
                 .setIsFirstLogin(false)
                 .build();

         System.out.println(" GrpcCallTest 2)");

         logger.info(stub.toString());
         logger.info("call option: " + stub.getCallOptions());

         stub.setupSync(request, handleResponse());

         System.out.println(" GrpcCallTest 3)");

         channel.shutdown();

      } catch (Exception e) {
         System.out.println("Got an exception for sqrt : ");
         e.printStackTrace();
      }



   }

   private StreamObserver<SetupSyncResponse> handleResponse() {
      System.out.println("handleResponse init. " );

      return new StreamObserver<SetupSyncResponse>() {

         private SetupSyncResponse statusReply;

         @Override
         public void onNext(SetupSyncResponse statusReply) {
            System.out.println("replay: " + statusReply.toString());
            this.statusReply = statusReply;
         }

         @Override
         public void onError(Throwable thr) {
            System.out.println("Error: " + thr.getMessage());
         }

         @Override
         public void onCompleted() {
            System.out.println("Completed !");
            System.out.println(statusReply.toString());

         }
      };
   }




}
